package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.User.Model.Gallery_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryFolder_Screen;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryPicList_Screen;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class GalleryList_adapter extends RecyclerView.Adapter<GalleryList_adapter.ViewHolder> {

    private Context context;
    private Gallery_model List;
    String LoginType;


    public GalleryList_adapter(Context context, Gallery_model List, String LoginType) {

        this.context = context;
        this.List = List;
        this.LoginType = LoginType;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_lists_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Picasso.get().load(Constants.GALLARYFOLDER + List.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((GalleryPicList_Screen) context).DeleteImageAPI(List.getData().get(position).getId(), "2", position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageViewZoom imageView;
        LinearLayout LayoutForEdits;
        ImageView edit, delete;

        public ViewHolder(final View itemView) {
            super(itemView);

            imageView = (ImageViewZoom) itemView.findViewById(R.id.image);
            LayoutForEdits = (LinearLayout) itemView.findViewById(R.id.LayoutForEdits);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            if (LoginType.equalsIgnoreCase("1")) {

            } else {
                LayoutForEdits.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("amenities"))
                    {
                        ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container,new Amenities_fragment()).commit();
                    }*/

                }
            });

        }
    }

    public void removeAt(int adapterPosition) {
        List.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, List.getData().size());
    }

}