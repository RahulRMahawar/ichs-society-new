package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.SecurityGuardsList_Screen;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FamilyMember_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_Profile;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

public class FamilyMember_adapter extends RecyclerView.Adapter<FamilyMember_adapter.ViewHolder> {

    private Context context;
    private FamilyMember_Model familyMemberModel;
    AlertDialog.Builder builder;


    public FamilyMember_adapter(Context context, FamilyMember_Model familyMemberModel) {
        this.context = context;
        this.familyMemberModel = familyMemberModel;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.family_member_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemView.setTag(familyMemberModel.getData().get(position));

        holder.name.setText(familyMemberModel.getData().get(position).getFullName());
        holder.flateNo.setText(familyMemberModel.getData().get(position).getFlat_no());
        holder.mobileNo.setText(familyMemberModel.getData().get(position).getPhone());
        holder.relation.setText("Uncle");

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((User_Profile)context).showAddOrEditFamilyPopUp(familyMemberModel.getData().get(position).getFullName(),familyMemberModel.getData().get(position).getFlat_no(),
                        familyMemberModel.getData().get(position).getPhone(),"Uncle",familyMemberModel.getData().get(position).getId(),"editType" );
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((User_Profile) context).DeleteFamilyMemberAPI(familyMemberModel.getData().get(position).getId(), "2", position);
            }
        });

    }

    public void removeAt(int adapterPosition) {
        familyMemberModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition,familyMemberModel.getData().size());
    }

    @Override
    public int getItemCount() {
        return familyMemberModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView flateNo;
        public TextView mobileNo;
        public TextView relation;
        public ImageView edit,delete;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            flateNo = (TextView) itemView.findViewById(R.id.flatNo);
            mobileNo = (TextView) itemView.findViewById(R.id.mobileNo);
            relation = (TextView) itemView.findViewById(R.id.relation);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}