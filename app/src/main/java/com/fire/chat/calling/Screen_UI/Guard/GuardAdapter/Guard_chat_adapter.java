package com.fire.chat.calling.Screen_UI.Guard.GuardAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.Guard_Chat_model;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

public class Guard_chat_adapter extends RecyclerView.Adapter<Guard_chat_adapter.ViewHolder> {

    private Context context;
    private List<Guard_Chat_model> List;
    AlertDialog.Builder builder;

    public Guard_chat_adapter(Context context, List List) {
        this.context = context;
        this.List = List;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.guard_chat_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(List.get(position));

        holder.name.setText(List.get(position).getTitle());
        holder.date.setText(List.get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView date;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}