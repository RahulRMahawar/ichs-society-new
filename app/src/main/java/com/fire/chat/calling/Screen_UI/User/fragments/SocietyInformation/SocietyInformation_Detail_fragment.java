package com.fire.chat.calling.Screen_UI.User.fragments.SocietyInformation;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fire.chat.calling.Utils.PerformCall;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.SocietyContact_adapter;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_GetSet;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_Model;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocietyInformation_Detail_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private SocietyContact_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    List<SocietyContact_GetSet> contactModelList;
    ConnectionDetector cd;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String ReceivedID, ReceivedTITLE;
    LinearLayout emptyDataLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_page_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();

        getIntentBundles();
        ((DashboardActivity) getActivity()).setActionBarTitle(ReceivedTITLE);
        initializeViews();
        initializeRecycler();
        handleListeners();
        getCommitteeList();
        return view;
    }


    private void getIntentBundles() {
        try {
            ReceivedID = getArguments().getString("ID");
        } catch (NullPointerException e) {
            ReceivedID = "";
        }
        try {
            ReceivedTITLE = getArguments().getString("TITLE");
        } catch (NullPointerException e) {
            ReceivedTITLE = getString(R.string.committeeMembersTitle);
        }
    }

    private void initializeRecycler() {

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void initializeViews() {
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);

    }


    private void handleListeners() {


    }

    private void getCommitteeList() {
        if (cd.isConnectingToInternet()) {
            getCommitteeListAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getCommitteeListAPI() {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("database_name", "ichs");
            requestBody.put("society_committee_id", ReceivedID);
            /* requestBody.put("sid", userDetail.get(SessionManager.SID));*/
            Call<SocietyContact_Model> call = apiInterface.getCommitteeList(requestBody);
            call.enqueue(new Callback<SocietyContact_Model>() {
                @Override
                public void onResponse(Call<SocietyContact_Model> call, Response<SocietyContact_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                SocietyContact_Model societyContactModel = response.body();
                                if (!response.body().getDatas().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new SocietyContact_adapter(context, societyContactModel, "societyInfo", new SocietyContact_adapter.SocietyContactCallChat() {
                                        @Override
                                        public void OnSocietyContactCallChat(int actionType) {
                                            switch (actionType) {
                                                case 0:
                                                    break;
                                                case 1:
                                                    break;
                                                case 2:
                                                    new PerformCall(getActivity()).performCall(true, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                                                    break;
                                                case 3:
                                                    new PerformCall(getActivity()).performCall(false, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                                                    break;
                                            }
                                        }
                                    });
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SocietyContact_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
}

