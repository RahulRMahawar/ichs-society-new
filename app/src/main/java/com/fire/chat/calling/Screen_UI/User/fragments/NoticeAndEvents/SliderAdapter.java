package com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Add_NoticeEvents_Fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.Notice_Detail_fragment;
import com.fire.chat.calling.Screen_UI.User.Model.NoticeAndEvent_Model;
import com.smarteist.autoimageslider.SliderViewAdapter;

public class SliderAdapter extends
        SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private Context context;
    private NoticeAndEvent_Model noticeAndEventModel ;
    String LoginType;

    public SliderAdapter(Context context, NoticeAndEvent_Model noticeAndEventModel, String LoginType) {
        this.context = context;
        this.noticeAndEventModel = noticeAndEventModel;
        this.LoginType = LoginType;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_and_events_adapter, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        viewHolder.name.setText(noticeAndEventModel.getData().get(position).getTitle());
        viewHolder.date.setText(noticeAndEventModel.getData().get(position).getEvent_date()
                +"  "+noticeAndEventModel.getData().get(position).getTime());
        viewHolder.des.setText(noticeAndEventModel.getData().get(position).getDescription());
        if (noticeAndEventModel.getData().get(position).getDescription().equals(""))
        {
            viewHolder.des.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.des.setVisibility(View.VISIBLE);
        }


        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "delete", Toast.LENGTH_SHORT).show();
            }
        });
        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {

                    Add_NoticeEvents_Fragment fragment=new Add_NoticeEvents_Fragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("ID",noticeAndEventModel.getData().get(position).getId());
                    bundle.putString("TITLE",noticeAndEventModel.getData().get(position).getTitle());
                    bundle.putString("DES",noticeAndEventModel.getData().get(position).getDescription());
                    bundle.putString("DATE",noticeAndEventModel.getData().get(position).getEvent_date());
                    bundle.putString("TIME",noticeAndEventModel.getData().get(position).getTime());
                    bundle.putString("TYPE","edit");
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }

            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {
                    Notice_Detail_fragment fragment=new Notice_Detail_fragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("ID",noticeAndEventModel.getData().get(position).getId());
                    bundle.putString("TITLE","Event Detail");
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }


                // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return noticeAndEventModel.getData().size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        public TextView name;
        public TextView date,des;
        public LinearLayout LayoutForEdits;
        ImageView delete,edit;

        public SliderAdapterVH(final View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            des = (TextView) itemView.findViewById(R.id.des);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            LayoutForEdits= (LinearLayout) itemView.findViewById(R.id.LayoutForEdits);

            if (LoginType.equalsIgnoreCase("2"))
            {
                LayoutForEdits.setVisibility(View.GONE);
            }
            this.itemView = itemView;

        }
    }

}