package com.fire.chat.calling.Screen_UI.Admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Members.Add_Members_fragment;
import com.fire.chat.calling.Screen_UI.Admin.Screens.Add_society;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.CommonTabView_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.VehicleManagement_fragment;
import com.fire.chat.calling.Screen_UI.User.Model.DashboardContent_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryFolder_Screen;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.User_Detail_Register;
import com.fire.chat.calling.Screen_UI.User.fragments.Amenities.Amenities_fragment;

import java.util.List;

public class AdminDashboardContent_adapter extends RecyclerView.Adapter<AdminDashboardContent_adapter.ViewHolder> {

    private Context context;
    private List<DashboardContent_model> List;
    String LoginType;

    public AdminDashboardContent_adapter(Context context, List List, String LoginType) {
        this.context = context;
        this.List = List;
        this.LoginType = LoginType;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_dashboard_content_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemName.setText(List.get(position).getItemName());
        holder.imageView.setImageResource(List.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName;
        ImageView imageView;

        public ViewHolder(final View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.itemName);
            imageView = (ImageView) itemView.findViewById(R.id.image);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("amenities")) {

                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            Amenities_fragment fragment = new Amenities_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("gallery")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("LoginType", LoginType);
                        Intent intent = new Intent(context, GalleryFolder_Screen.class);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("VEHICLE MANAGEMENT")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            VehicleManagement_fragment fragment = new VehicleManagement_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("BROADCAST MESSAGE")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            CommonTabView_fragment fragment = new CommonTabView_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("CalledFor", "BROADCAST MESSAGE");
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("COMPLAINTS/SUGGESTIONS")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            CommonTabView_fragment fragment = new CommonTabView_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("CalledFor", "COMPLAINTS/SUGGESTIONS");
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("SOCIETY REGISTER")) {
                        Intent intent = new Intent(context, Add_society.class);
                        context.startActivity(intent);
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("ADD MEMBERS")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, new Add_Members_fragment());
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    }

                }
            });

        }
    }

}