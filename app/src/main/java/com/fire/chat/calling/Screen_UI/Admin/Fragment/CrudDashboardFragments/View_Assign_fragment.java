package com.fire.chat.calling.Screen_UI.Admin.Fragment.CrudDashboardFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.AdminDashboardContent_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.DashboardContent_model;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Rahul Mahawar.
 */

public class View_Assign_fragment extends Fragment implements View.OnClickListener /*SwipeRefreshLayout.OnRefreshListener*/ {

    Context context;
    private Toolbar toolbar;
    RecyclerView contentRecycleView;
    private View view;
    ConnectionDetector cd;
    private RecyclerView.LayoutManager layoutManager;
    List<DashboardContent_model> List;
    private AdminDashboardContent_adapter Adapter;
    String LoginType;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.create_approval_fragment, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        initializeViews();
        click();
        return view;
    }


    private void initializeViews() {
        findView();
        setdata();


    }

    private void setdata() {
        contentRecycleView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);

        contentRecycleView.setLayoutManager(layoutManager);

        List = new ArrayList<>();

        //Adding Data into ArrayList
        List.add(new DashboardContent_model("COMPLAINTS/SUGGESTIONS", R.drawable.ic_complaint));
        List.add(new DashboardContent_model("GALLERY", R.drawable.ic_gallery));
        List.add(new DashboardContent_model("PRINT", R.drawable.ic_gallery));
        List.add(new DashboardContent_model("ALARM REPORT", R.drawable.ic_gallery));


        Adapter = new AdminDashboardContent_adapter(context, List, LoginType);

        contentRecycleView.setAdapter(Adapter);
    }


    private void findView() {
        contentRecycleView = view.findViewById(R.id.contentRecycleview);
    }


    private void click() {


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }

    }

}
