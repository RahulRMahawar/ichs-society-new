package com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.VehicleManagement_Model;
import com.fire.chat.calling.Screen_UI.User.Adapter.MarketPlace_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.Notification_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryFolder_Screen;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class MarketPlace_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private MarketPlace_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    List<Notification_model> notificationList;
    ConnectionDetector cd;
    LinearLayout LayoutForSearch, myLayout;
    FloatingActionButton floating_AddMarketPlace;
    EditText searchMarketPlace;
    ProgressDialog dialog;
    Boolean isSearching = false;
    LinearLayout emptyDataLayout;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    private String UserType;
    AlertDialog alertDialog;
    ImageView imageView;
    Intent myFiles;
    String ReceivedFileName;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.marketplace_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        UserType = userDetail.get(SessionManager.LOGIN_TYPE);
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.marketPlaceTitle));
        initializeViews();
        initializeRecycler();
        SetViewsForUsers();
        handleListeners();
        getMarketPlace();
        myLayout = (LinearLayout) view.findViewById(R.id.my_layout);
        myLayout.requestFocus();
        return view;
    }


    private void SetViewsForUsers() {
        if (UserType.equalsIgnoreCase("1")) {
            LayoutForSearch.setVisibility(View.GONE);
        } else {
            floating_AddMarketPlace.hide();
        }
    }


    private void initializeViews() {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        LayoutForSearch = (LinearLayout) view.findViewById(R.id.LayoutForSearch);
        searchMarketPlace = (EditText) view.findViewById(R.id.search);
        floating_AddMarketPlace = view.findViewById(R.id.floating_AddMarketPlace);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void handleListeners() {
        floating_AddMarketPlace.setOnClickListener(this);

    }

    private void getMarketPlace() {
        if (cd.isConnectingToInternet()) {
            getMarketPlaceAPI("");
            Searching();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void Searching() {
        searchMarketPlace.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isSearching = true;
                getMarketPlaceAPI(editable.toString());
            }
        });

        searchMarketPlace.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
    }

    public void getMarketPlaceAPI(String searchKey) {
        if (!isSearching) {
            dialog = ProgressDialog.show(context, "", "Please wait...", false);
        }
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            if (UserType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            requestBody.put("keyword", searchKey);

            Call<MarketPlaceList_Model> call = apiInterface.getMarketPlace(requestBody);
            call.enqueue(new Callback<MarketPlaceList_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceList_Model> call, Response<MarketPlaceList_Model> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceList_Model marketPlaceListModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new MarketPlace_adapter(context, marketPlaceListModel, UserType, MarketPlace_fragment.this);
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.floating_AddMarketPlace:
                showPopUp("", "", "Add", "");

                break;

        }


    }

    public void showPopUp(final String ID, final String image, final String type, final String receivedName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.add_marketplace_popup, null);
        final EditText name = (EditText) customView.findViewById(R.id.name);
        Button done = (Button) customView.findViewById(R.id.done);
        imageView = (ImageView) customView.findViewById(R.id.image);
        Picasso.get().load(Constants.GALLARYFOLDER + image).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(imageView);
        requestAppPermissions();
        if (type.equalsIgnoreCase("edit")) {
            name.setText(receivedName);
            ReceivedFileName = image;
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFile();
            }
        });
        builder.setView(customView);
        alertDialog = builder.create();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                    name.setError(getString(R.string.validate_MarketName));
                    name.requestFocus();
                } else {
                    AddOrEditMarketPlaceAPI(ID, type, name.getText().toString());
                }
            }
        });

        alertDialog.show();
    }

    public void AddOrEditMarketPlaceAPI(String ID, String type, String marketName) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();

            if (type.equalsIgnoreCase("edit")) {
                requestBody.put("id", ID);
            }
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("title", marketName);
            requestBody.put("image", ReceivedFileName);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<MarketPlaceList_Model> call = apiInterface.addOrEditMarketPlace(requestBody);
            call.enqueue(new Callback<MarketPlaceList_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceList_Model> call, Response<MarketPlaceList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceList_Model marketPlaceListModel = response.body();
                                Toast.makeText(context, marketPlaceListModel.getReplyMsg(), Toast.LENGTH_SHORT).show();

                                alertDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DeleteMarketPlaceAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<MarketPlaceList_Model> call = apiInterface.deleteMarketPlace(requestBody);
            call.enqueue(new Callback<MarketPlaceList_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceList_Model> call, Response<MarketPlaceList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceList_Model marketPlaceListModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, marketPlaceListModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                        Picasso.get().load(path).fit()
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(imageView);
                    //Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getContext().getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

