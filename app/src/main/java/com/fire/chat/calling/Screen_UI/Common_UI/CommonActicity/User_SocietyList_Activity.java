package com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter.Society_List_adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.SocietyList_Model;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User_SocietyList_Activity extends AppCompatActivity {

    private Context context;
    private SessionManager sessionManager;
    ConnectionDetector cd;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView SocietyListRecycler;
    Society_List_adapter Adapter;
    private HashMap<String, String> userDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_society_list_activity);
        context = User_SocietyList_Activity.this;
        cd = new ConnectionDetector(getApplicationContext());
        sessionManager = new SessionManager(this);
        userDetail = sessionManager.getLoginSavedDetails();

        Toast.makeText(context, userDetail.get(SessionManager.SID) + " " + userDetail.get(SessionManager.LOGIN_TYPE), Toast.LENGTH_SHORT).show();
        initializeView();
        CallSocietyListAPI();
    }

    private void initializeView() {
        SocietyListRecycler = findViewById(R.id.SocietyListRecycler);
        layoutManager = new LinearLayoutManager(context);
    }

    private void CallSocietyListAPI() {


        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("role_id", userDetail.get(SessionManager.LOGIN_TYPE));
            Call<SocietyList_Model> call = apiInterface.getUserSocietyList(requestBody);
            call.enqueue(new Callback<SocietyList_Model>() {
                @Override
                public void onResponse(Call<SocietyList_Model> call, Response<SocietyList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                SocietyList_Model model = response.body();
                                Adapter = new Society_List_adapter(context, model);
                                //Setting the adapter
                                SocietyListRecycler.setAdapter(Adapter);
                                SocietyListRecycler.setLayoutManager(layoutManager);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SocietyList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
