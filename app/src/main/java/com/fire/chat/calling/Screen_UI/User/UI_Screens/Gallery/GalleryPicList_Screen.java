package com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.User.Adapter.GalleryList_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.Gallery_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.User_Chat_Screen;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryPicList_Screen extends AppCompatActivity implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    TextView title;
    RecyclerView RecycleView;
    private View view;
    ConnectionDetector cd;
    private RecyclerView.LayoutManager layoutManager;
    java.util.List<Gallery_model> List;
    private GalleryList_adapter Adapter;
    String LoginType, ReceivedID, ReceivedTITLE;
    LinearLayout LayoutForUploadButton;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    LinearLayout emptyDataLayout;
    Button done;
    Intent myFiles;
    String ReceivedFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_gallery_pic_screen_activity);
        context = GalleryPicList_Screen.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        getIntentBundles();
        setToolbar();
        initializeViews();
        setViewForUsers();
        initializeRecycler();
        handleListeners();
        getGalleryList();
    }

    private void setViewForUsers() {
        if (LoginType.equalsIgnoreCase("1")) {
        } else {
            LayoutForUploadButton.setVisibility(View.GONE);
        }
    }

    private void getIntentBundles() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            LoginType = bundle.getString("LoginType");
            ReceivedID = bundle.getString("ID");
            ReceivedTITLE = bundle.getString("TITLE");
        }
    }


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(ReceivedTITLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeViews() {
        RecycleView = findViewById(R.id.contentRecycleview);
        LayoutForUploadButton = findViewById(R.id.LayoutForUploadButton);
        emptyDataLayout = (LinearLayout) findViewById(R.id.emptyDataLayout);
        done = findViewById(R.id.done);
    }

    private void initializeRecycler() {
        RecycleView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(context, 4);
        RecycleView.setLayoutManager(layoutManager);
    }

    private void getGalleryList() {
        if (cd.isConnectingToInternet()) {
            getGalleryListAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getGalleryListAPI() {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            if (LoginType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }
            requestBody.put("folder_id", ReceivedID);
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());
            Call<Gallery_model> call = apiInterface.getGalleryFoldersDetails(requestBody);
            call.enqueue(new Callback<Gallery_model>() {
                @Override
                public void onResponse(Call<Gallery_model> call, Response<Gallery_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Gallery_model galleryModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    RecycleView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new GalleryList_adapter(context, galleryModel, LoginType);
                                    RecycleView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();

                                } else {
                                    RecycleView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Gallery_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void handleListeners() {

        done.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.done:

                selectFile();

                break;
        }

    }

    private void selectFile() {
        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    //Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }
    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Log.e("ResponseMSG", response.body().getMsg());

                                AddImageToFolderAPI(ReceivedFileName);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddImageToFolderAPI(String receivedFileName) {

        Call<Gallery_model> call = null;
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();

           /* if (LoginType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }*/
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("image_folder_id", ReceivedID);
            requestBody.put("image", receivedFileName);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("device_token", "");
            call = apiInterface.addImageToFolder(requestBody);

            call.enqueue(new Callback<Gallery_model>() {
                @Override
                public void onResponse(Call<Gallery_model> call, Response<Gallery_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            getGalleryListAPI();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    progressDialog.cancel();


                }

                @Override
                public void onFailure(Call<Gallery_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    progressDialog.cancel();
                    Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void DeleteImageAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<Gallery_model> call = apiInterface.deleteImageInFolder(requestBody);
            call.enqueue(new Callback<Gallery_model>() {
                @Override
                public void onResponse(Call<Gallery_model> call, Response<Gallery_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Gallery_model galleryModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, galleryModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Gallery_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
