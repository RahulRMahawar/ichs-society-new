package com.fire.chat.calling.Screen_UI.Guard.Guard_Screens;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.Notification_model;
import com.fire.chat.calling.Screen_UI.Utlity.CustomDateTimePicker;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fire.chat.calling.sessionData.CommonMethod.isValidTelephone;

public class Add_Visitor extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title;
    LinearLayout layoutForAdhar, layoutForVehicleNo, layoutForPurpose, layoutForRemark, layoutForFrom, layoutForTo;
    EditText name, mobileNo, adharCardNo, vehicleNo, purpose, remark, startDate, endDate;
    Button done;
    ConnectionDetector cd;
    private Context context;

    String receivedLoginType = "";
    final Calendar myCalendar = Calendar.getInstance();
    String dateType = "";
    CustomDateTimePicker custom;
    String startDateText, startTimeText, endDateText, endTimeText;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    ImageViewZoom userImg;
    Intent myFiles;
    String ReceivedFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_visitor_activity);
        context = Add_Visitor.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        findViews();
        setToolbar();
        setViews();
        handleListeners();
        InitializeCustomDateAndTimePicker();
    }

    private void InitializeCustomDateAndTimePicker() {
        custom = new CustomDateTimePicker(this,
                new CustomDateTimePicker.ICustomDateTimeListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int day,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {
                        //                        ((TextInputEditText) findViewById(R.id.edtEventDateTime))
                        if (dateType.endsWith("startDate")) {
                            startDate.setText("");
                            startDate.setText("Date : " + calendarSelected.get(Calendar.DAY_OF_MONTH) + "-" + (monthNumber + 1) + "-" + year + "   "
                                            + "Time : " + hour24 + ":" + min + " " + AM_PM
                                    /*+ ":" + sec*/);
                            startDateText = year + "-" + (monthNumber + 1) + "-" + calendarSelected.get(Calendar.DAY_OF_MONTH);
                            startTimeText = +hour24 + ":" + min + ":" + sec;
                        } else if (dateType.endsWith("endDate")) {
                            endDate.setText("");
                            endDate.setText("Date : " + calendarSelected.get(Calendar.DAY_OF_MONTH) + "-" + (monthNumber + 1) + "-" + year + "   "
                                            + "Time : " + hour24 + ":" + min + " " + AM_PM
                                    /*+ ":" + sec*/);
                            endDateText = year + "-" + (monthNumber + 1) + "-" + calendarSelected.get(Calendar.DAY_OF_MONTH);
                            endTimeText = +hour24 + ":" + min + ":" + sec;
                        }

                    }

                    @Override
                    public void onCancel() {

                    }
                });
        custom.set24HourFormat(true);
        custom.setDate(Calendar.getInstance());
    }

    private void findViews() {
        layoutForAdhar = findViewById(R.id.layoutForAdhar);
        layoutForVehicleNo = findViewById(R.id.layoutForVehicelNo);
        layoutForPurpose = findViewById(R.id.layoutForPurpose);
        layoutForRemark = findViewById(R.id.layoutForRemark);
        layoutForFrom = findViewById(R.id.layoutForFrom);
        layoutForTo = findViewById(R.id.layoutForTo);
        done = findViewById(R.id.done);
        name = findViewById(R.id.name);
        mobileNo = findViewById(R.id.mobileNo);
        adharCardNo = findViewById(R.id.adharCardNo);
        vehicleNo = findViewById(R.id.vehicleNo);
        purpose = findViewById(R.id.purpose);
        remark = findViewById(R.id.remark);
        endDate = findViewById(R.id.endDate);
        startDate = findViewById(R.id.startDate);
        userImg = findViewById(R.id.userImg);
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText("Add Visitor");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setViews() {

        if (userDetail.get(SessionManager.LOGIN_TYPE).equalsIgnoreCase("1")) {
            ForAdmin();
        } else if (userDetail.get(SessionManager.LOGIN_TYPE).equalsIgnoreCase("2")) {
            ForUser();
        } else {
            ForGuard();
        }
    }

    private void ForUser() {
        layoutForVehicleNo.setVisibility(View.GONE);
    }

    private void ForGuard() {

        layoutForAdhar.setVisibility(View.GONE);
        layoutForRemark.setVisibility(View.GONE);
        layoutForFrom.setVisibility(View.GONE);
        layoutForTo.setVisibility(View.GONE);
    }

    private void ForAdmin() {

        layoutForAdhar.setVisibility(View.GONE);
        layoutForVehicleNo.setVisibility(View.GONE);
        layoutForFrom.setVisibility(View.GONE);
        layoutForTo.setVisibility(View.GONE);
    }

    private void handleListeners() {

        done.setOnClickListener(this);
        startDate.setOnClickListener(this);
        endDate.setOnClickListener(this);
        userImg.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.done:
                validate();

                break;
            case R.id.startDate:
                dateType = "startDate";
                custom.showDialog();

                break;
            case R.id.endDate:
                dateType = "endDate";
                custom.showDialog();

                break;
            case R.id.userImg:
                requestAppPermissions();
                selectFile();

                break;

        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(dateType);
        }

    };

    private void updateLabel(String dateType) {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if (dateType.endsWith("startDate")) {
            startDate.setText(sdf.format(myCalendar.getTime()));
        } else if (dateType.endsWith("endDate")) {
            endDate.setText(sdf.format(myCalendar.getTime()));
        }

    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (userDetail.get(SessionManager.LOGIN_TYPE).equalsIgnoreCase("2")) {
                if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                    name.setError(getString(R.string.validate_userName));
                    name.requestFocus();
                } else if (!isValidTelephone(mobileNo.getText().toString())) {
                    mobileNo.setError(getResources().getString(R.string.validate_userMobile));
                    mobileNo.requestFocus();
                } else if (TextUtils.isEmpty(adharCardNo.getText().toString().trim()) || adharCardNo.length() <= 0) {
                    adharCardNo.setError(getString(R.string.validate_adharCard));
                    adharCardNo.requestFocus();
                } else if (TextUtils.isEmpty(purpose.getText().toString().trim()) || purpose.length() <= 0) {
                    purpose.setError(getString(R.string.validate_Purpose));
                    purpose.requestFocus();
                } else if (TextUtils.isEmpty(remark.getText().toString().trim()) || remark.length() <= 0) {
                    remark.setError(getString(R.string.validate_remark));
                    remark.requestFocus();
                } else if (startDate.getText().toString().equalsIgnoreCase("start date")) {
                    startDate.setError(getString(R.string.validate_Date));
                    startDate.requestFocus();
                } else if (endDate.getText().toString().equalsIgnoreCase("end date")) {
                    endDate.setError(getString(R.string.validate_Date));
                    endDate.requestFocus();
                } else {
                    SubmitAddVisitorApi("user");
                }
            } else if (userDetail.get(SessionManager.LOGIN_TYPE).equalsIgnoreCase("1")) {
                if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                    name.setError(getString(R.string.validate_userName));
                    name.requestFocus();
                } else if (!isValidTelephone(mobileNo.getText().toString())) {
                    mobileNo.setError(getResources().getString(R.string.validate_userMobile));
                    mobileNo.requestFocus();
                } else if (TextUtils.isEmpty(purpose.getText().toString().trim()) || purpose.length() <= 0) {
                    purpose.setError(getString(R.string.validate_Purpose));
                    purpose.requestFocus();
                } else if (TextUtils.isEmpty(remark.getText().toString().trim()) || remark.length() <= 0) {
                    remark.setError(getString(R.string.validate_remark));
                    remark.requestFocus();
                } else {
                    SubmitAddVisitorApi("admin");
                }
            } else {
                if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                    name.setError(getString(R.string.validate_userName));
                    name.requestFocus();
                } else if (!isValidTelephone(mobileNo.getText().toString())) {
                    mobileNo.setError(getResources().getString(R.string.validate_userMobile));
                    mobileNo.requestFocus();
                } else if (TextUtils.isEmpty(adharCardNo.getText().toString().trim()) || adharCardNo.length() <= 0) {
                    adharCardNo.setError(getString(R.string.validate_adharCard));
                    adharCardNo.requestFocus();
                } else if (TextUtils.isEmpty(purpose.getText().toString().trim()) || purpose.length() <= 0) {
                    purpose.setError(getString(R.string.validate_Purpose));
                    purpose.requestFocus();
                } else if (TextUtils.isEmpty(vehicleNo.getText().toString().trim()) || vehicleNo.length() <= 0) {
                    vehicleNo.setError(getString(R.string.validate_VehicleNo));
                    vehicleNo.requestFocus();
                } else {
                    SubmitAddVisitorApi("guard");
                }

            }


        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitAddVisitorApi(String calledFrom) {

        Call<Notification_model> call = null;
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            if (calledFrom.equalsIgnoreCase("user")) {
                requestBody.put("remark", remark.getText().toString());
                requestBody.put("no_of_persons", "1");
                requestBody.put("time_from", startTimeText);
                requestBody.put("time_to", endTimeText);
                requestBody.put("date_from", startDateText);
                requestBody.put("date_to", endDateText);
            } else if (calledFrom.equalsIgnoreCase("admin")) {
                requestBody.put("remarks", remark.getText().toString());
            } else {
                requestBody.put("no_of_persons", "1");
                requestBody.put("vehicle_no", "1");
                requestBody.put("adhar_no", "1");
            }
            requestBody.put("image", mobileNo.getText().toString());
            requestBody.put("phone", mobileNo.getText().toString());
            requestBody.put("fullName", name.getText().toString());
            requestBody.put("purpose", purpose.getText().toString());
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("device_token", "");
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());

            call = apiInterface.addVisitor(requestBody);
            call.enqueue(new Callback<Notification_model>() {
                @Override
                public void onResponse(Call<Notification_model> call, Response<Notification_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            resetViews();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Notification_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetViews() {

        mobileNo.setText("");
        name.setText("");
        adharCardNo.setText("");
        purpose.setText("");
        remark.setText("");
        startDate.setText(getString(R.string.startDate));
        endDate.setText(getString(R.string.endDate));
        vehicleNo.setText("");


    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(Add_Visitor.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(userImg);
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
