package com.fire.chat.calling.Screen_UI.Common_UI.CommonModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Users_Request_Model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;
    @SerializedName("data")
    public List<DataValue> data;
    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public List<DataValue> getData() {
        return data;
    }

    public class DataValue {

        String id,role_id,parent_id,email,phone,password,gender,fullName,flat_no,parking_no,parking_type,is_parking,qr_code,image,verification_doc,
                date_of_joining,service_id,status,lat,lng,gate_no,location,description,verified,notification,otp,deviceToken,profile_completed,show_contact,
                service_broadcast,created,modified;
        public String getId() {
            return id;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getFlat_no() {
            return flat_no;
        }

        public void setFlat_no(String flat_no) {
            this.flat_no = flat_no;
        }

        public String getParking_no() {
            return parking_no;
        }

        public void setParking_no(String parking_no) {
            this.parking_no = parking_no;
        }

        public String getParking_type() {
            return parking_type;
        }

        public void setParking_type(String parking_type) {
            this.parking_type = parking_type;
        }

        public String getIs_parking() {
            return is_parking;
        }

        public void setIs_parking(String is_parking) {
            this.is_parking = is_parking;
        }

        public String getQr_code() {
            return qr_code;
        }

        public void setQr_code(String qr_code) {
            this.qr_code = qr_code;
        }

        public String getVerification_doc() {
            return verification_doc;
        }

        public void setVerification_doc(String verification_doc) {
            this.verification_doc = verification_doc;
        }

        public String getDate_of_joining() {
            return date_of_joining;
        }

        public void setDate_of_joining(String date_of_joining) {
            this.date_of_joining = date_of_joining;
        }

        public String getService_id() {
            return service_id;
        }

        public void setService_id(String service_id) {
            this.service_id = service_id;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getGate_no() {
            return gate_no;
        }

        public void setGate_no(String gate_no) {
            this.gate_no = gate_no;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVerified() {
            return verified;
        }

        public void setVerified(String verified) {
            this.verified = verified;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getProfile_completed() {
            return profile_completed;
        }

        public void setProfile_completed(String profile_completed) {
            this.profile_completed = profile_completed;
        }

        public String getShow_contact() {
            return show_contact;
        }

        public void setShow_contact(String show_contact) {
            this.show_contact = show_contact;
        }

        public String getService_broadcast() {
            return service_broadcast;
        }

        public void setService_broadcast(String service_broadcast) {
            this.service_broadcast = service_broadcast;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }




        public String getStatus() {
            return status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public void setStatus(String status) {
            this.status = status;
        }


        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getNotification() {
            return notification;
        }

        public void setNotification(String notification) {
            this.notification = notification;
        }
    }
}