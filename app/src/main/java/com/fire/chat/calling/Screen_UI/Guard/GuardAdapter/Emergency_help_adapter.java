package com.fire.chat.calling.Screen_UI.Guard.GuardAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.SecurityGuardsList_adapter;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.HelpLine_Model;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.HelpLine_Screen;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class Emergency_help_adapter extends RecyclerView.Adapter<Emergency_help_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String LoginType;
    HelpLine_Model helpLineModel;

    public interface EmergencyHelpCall {
        void OnEmergencyHelpCall();
    }

    EmergencyHelpCall emergencyHelpCall;


    public Emergency_help_adapter(Context context, HelpLine_Model helpLineModel, String LoginType, EmergencyHelpCall emergencyHelpCall) {
        this.context = context;
        this.LoginType = LoginType;
        this.helpLineModel = helpLineModel;
        this.emergencyHelpCall = emergencyHelpCall;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.emergency_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(helpLineModel.getData().get(position));

        holder.name.setText(helpLineModel.getData().get(position).getFull_name());

        Picasso.get().load(Constants.GALLARYFOLDER + helpLineModel.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HelpLine_Screen) context).deleteEmergencyContact(helpLineModel.getData().get(position).getId(), "2", position);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HelpLine_Screen) context).showPopUp(helpLineModel.getData().get(position).getFull_name(),
                        helpLineModel.getData().get(position).getPhone(), "edit", helpLineModel.getData().get(position).getId());
            }
        });

        holder.callEmergencyHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emergencyHelpCall.OnEmergencyHelpCall();
            }
        });

    }

    @Override
    public int getItemCount() {
        return helpLineModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        LinearLayout LayoutForEdits;
        ImageView delete, edit, callEmergencyHelp;
        ImageViewZoom imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            delete = itemView.findViewById(R.id.delete);
            edit = itemView.findViewById(R.id.edit);
            callEmergencyHelp = itemView.findViewById(R.id.call_emergency_help);
            LayoutForEdits = itemView.findViewById(R.id.LayoutForEdits);
            imageView = itemView.findViewById(R.id.userImg);
            if (LoginType.equalsIgnoreCase("2")) {
                LayoutForEdits.setVisibility(View.GONE);
            } else {
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void removeAt(int adapterPosition) {
        helpLineModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, helpLineModel.getData().size());
    }
}