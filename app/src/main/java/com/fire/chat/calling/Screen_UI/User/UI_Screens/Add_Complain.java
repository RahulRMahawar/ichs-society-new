package com.fire.chat.calling.Screen_UI.User.UI_Screens;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.User.Model.Complaint_model;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_Complain extends AppCompatActivity implements View.OnClickListener {
    ConnectionDetector cd;
    private Context context;
    private Toolbar toolbar;
    TextView title;
    Button done;
    Spinner typeSpinner, prioritySpinner, topicSpinner, subTopicSpinner;
    EditText description;
    String topicName, topicID;
    ArrayList<String> TopicNames = new ArrayList<String>();
    ArrayList<String> TopicID = new ArrayList<String>();
    String subTopicName, subTopicID;
    ArrayList<String> SubTopicNames = new ArrayList<String>();
    ArrayList<String> SubTopicID = new ArrayList<String>();
    Complaint_model responseBody;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    ImageView mediaFile;
    Intent myFiles;
    String ReceivedFileName;
    String fileExt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_complain_activity);
        context = Add_Complain.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        setToolbar();
        initializeViews();
        handleListeners();
        loadTopicSpinner();

    }


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.addComplaintitle));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeViews() {
        typeSpinner = findViewById(R.id.typeSpinner);
        prioritySpinner = findViewById(R.id.prioritySpinner);
        topicSpinner = findViewById(R.id.topicSpinner);
        subTopicSpinner = findViewById(R.id.subTopicSpinner);
        description = findViewById(R.id.description);
        mediaFile = findViewById(R.id.mediaFile);
        done = findViewById(R.id.done);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.selectComplaintType, R.layout.spinner_style);
        adapter.setDropDownViewResource(R.layout.spinner_style);
        typeSpinner.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
                this, R.array.selectPriorityType, R.layout.spinner_style);
        adapter.setDropDownViewResource(R.layout.spinner_style);
        prioritySpinner.setAdapter(adapter1);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(
                this, R.array.selectSubTopic, R.layout.spinner_style);
        adapter.setDropDownViewResource(R.layout.spinner_style);
        subTopicSpinner.setAdapter(adapter3);


    }

    private void handleListeners() {
        done.setOnClickListener(this);
        mediaFile.setOnClickListener(this);
        topicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (responseBody == null) {

                } else {
                    topicName = adapterView.getSelectedItem().toString();
                    topicID = TopicID.get(adapterView.getSelectedItemPosition());
                    if (!topicID.equalsIgnoreCase("0")) {
                        loadSubTopicSpinner(topicID);
                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        subTopicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                subTopicName = adapterView.getSelectedItem().toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        description.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.description) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void loadTopicSpinner() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());
            requestBody.put("parent_id", "0");
            Call<Complaint_model> call = apiInterface.getTopicList(requestBody);
            call.enqueue(new Callback<Complaint_model>() {
                @Override
                public void onResponse(Call<Complaint_model> call, Response<Complaint_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                TopicNames.add("Select your topic");
                                TopicID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    TopicNames.add(response.body().getData().get(i).getTitle());
                                    TopicID.add(response.body().getData().get(i).getId());
                                }

                                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>
                                        (context, R.layout.spinner_style, TopicNames);
                                topicSpinner.setAdapter(categoryAdapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Complaint_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadSubTopicSpinner(String topicID) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());
            requestBody.put("parent_id", topicID);
            Call<Complaint_model> call = apiInterface.getTopicList(requestBody);
            call.enqueue(new Callback<Complaint_model>() {
                @Override
                public void onResponse(Call<Complaint_model> call, Response<Complaint_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                SubTopicNames.add("Select your sub topic");
                                SubTopicID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    SubTopicNames.add(response.body().getData().get(i).getTitle());
                                    SubTopicID.add(response.body().getData().get(i).getId());
                                }

                                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>
                                        (context, R.layout.spinner_style, SubTopicNames);
                                subTopicSpinner.setAdapter(categoryAdapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Complaint_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                validate();
                break;
            case R.id.mediaFile:
                requestAppPermissions();
                selectFile();
                break;
        }
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (topicID.equalsIgnoreCase("0")) {
                Toast.makeText(context, "Please select topic", Toast.LENGTH_SHORT).show();
            } else if (subTopicName.equalsIgnoreCase("\"Select your sub topic")) {
                Toast.makeText(context, "Please select subtopic", Toast.LENGTH_SHORT).show();
            } else if (typeSpinner.getSelectedItem().toString().equalsIgnoreCase("Type")) {
                Toast.makeText(context, "Please select type", Toast.LENGTH_SHORT).show();
            } else if (prioritySpinner.getSelectedItem().toString().equalsIgnoreCase("priority")) {
                Toast.makeText(context, "Please select priority", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(description.getText().toString().trim()) || description.length() <= 0) {
                description.setError(getString(R.string.validate_Discription));
                description.requestFocus();
            } else {
                SubmitComplaintApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitComplaintApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("topic", topicName);
            requestBody.put("sub_topic", subTopicName);
            requestBody.put("description", description.getText().toString());
            requestBody.put("type", typeSpinner.getSelectedItem().toString());
            requestBody.put("priority", prioritySpinner.getSelectedItem().toString());
            requestBody.put("image", ReceivedFileName);
            requestBody.put("media_type", fileExt);
            requestBody.put("device_token", "");

            Call<Complaint_model> call = apiInterface.addComplaint(requestBody);
            call.enqueue(new Callback<Complaint_model>() {
                @Override
                public void onResponse(Call<Complaint_model> call, Response<Complaint_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Complaint_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(Add_Complain.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("*/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    if (fileExt.equalsIgnoreCase("mp4")) {
                        Picasso.get().load(R.drawable.videoformat)
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(mediaFile);
                        /*play.setVisibility(View.VISIBLE);
                        play.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(context, link, Toast.LENGTH_SHORT).show();
                            }
                        });*/
                    } else if (fileExt.equalsIgnoreCase("jpg")) {
                        Picasso.get().load(path).fit()
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(mediaFile);

                    } else if (fileExt.equalsIgnoreCase("aac")) {
                        Picasso.get().load(R.drawable.audioformat)
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(mediaFile);
                    }
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) context.getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
