package com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.SecurityGuardsList_Screen;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Guard_Home_Fragment;
import com.fire.chat.calling.Screen_UI.User.Adapter.DashboardContent_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.DashboardContent_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.User_Chat_Screen;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by Rahul Mahawar.
 */

public class DashBoardFragment extends Fragment implements View.OnClickListener /*SwipeRefreshLayout.OnRefreshListener*/ {

    Context context;
    private Toolbar toolbar;
    RecyclerView contentRecycleView;
    private View view;
    FloatingActionButton floating_call,floating_msg,floating_GuardsList;
    ConnectionDetector cd;
    private RecyclerView.LayoutManager layoutManager;
    List<DashboardContent_model> List;
    private DashboardContent_adapter Adapter;
    String USERTYPE,HIDEDASHBOARD;
    private SessionManager sessionManager;
    private HashMap<String, String> userDetail;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.dashboard_fragment, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        USERTYPE=userDetail.get(SessionManager.LOGIN_TYPE);
        initializeViews();
        click();
        return view;
    }


    private void initializeViews() {
        findView();
        SetViewsForUsers();
        setData();


    }

    private void SetViewsForUsers() {
        if (USERTYPE.equalsIgnoreCase("2"))
        {
            floating_GuardsList.hide();
            HIDEDASHBOARD="true";

        }
        else
        {
            floating_GuardsList.hide();
            HIDEDASHBOARD="false";
        }
    }

    private void setData() {
        contentRecycleView.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(context,3);

        contentRecycleView.setLayoutManager(layoutManager);

        List = new ArrayList<>();

        if (HIDEDASHBOARD.equalsIgnoreCase("False"))
        {
            //Adding Data into ArrayList
            List.add(new DashboardContent_model("MARKETPLACE",R.drawable.ic_marketplace));
            List.add(new DashboardContent_model("VISITORS",R.drawable.ic_visit));
            List.add(new DashboardContent_model("AMENITIES",R.drawable.ic_amenities));
            List.add(new DashboardContent_model("COMPLAINTS/SUGGESTIONS",R.drawable.ic_complaint));
            List.add(new DashboardContent_model("BROADCAST MESSAGE",R.drawable.ic_broadcast));
            List.add(new DashboardContent_model("DASHBOARD",R.drawable.ic_dashboard));
            List.add(new DashboardContent_model("ALARM",R.drawable.ic_alarm));
            List.add(new DashboardContent_model("SECURITY GUARDS",R.drawable.ic_society));
            List.add(new DashboardContent_model("EVENTS",R.drawable.ic_event));
            List.add(new DashboardContent_model("NOTICES",R.drawable.ic_notice));
            List.add(new DashboardContent_model("HELP & SERVICES",R.drawable.ic_24_hours_support));
            List.add(new DashboardContent_model("VEHICLE MANAGEMENT",R.drawable.ic_vehicle));
            List.add(new DashboardContent_model("EMERGENCY HELPLINE",R.drawable.ic_sos));
            List.add(new DashboardContent_model("PAYMENTS\n" + "(Coming Soon)",R.drawable.ic_payment));
            List.add(new DashboardContent_model("GPS & CAMERA\n" + "(Coming Soon)",R.drawable.ic_gps));
            List.add(new DashboardContent_model("GALLERY",R.drawable.ic_gallery));
            List.add(new DashboardContent_model("SOCIETY INFORMATION",R.drawable.ic_society));
            List.add(new DashboardContent_model("Contact us",R.drawable.ic_contact_us));

        }
        else
        {
            //Adding Data into ArrayList
            List.add(new DashboardContent_model("MARKETPLACE",R.drawable.ic_marketplace));
            List.add(new DashboardContent_model("VISITORS",R.drawable.ic_visit));
            List.add(new DashboardContent_model("AMENITIES",R.drawable.ic_amenities));
            List.add(new DashboardContent_model("COMPLAINTS/SUGGESTIONS",R.drawable.ic_complaint));
            List.add(new DashboardContent_model("BROADCAST MESSAGE",R.drawable.ic_broadcast));
            List.add(new DashboardContent_model("ALARM",R.drawable.ic_alarm));
            List.add(new DashboardContent_model("SECURITY GUARDS",R.drawable.ic_society));
            List.add(new DashboardContent_model("EVENTS",R.drawable.ic_event));
            List.add(new DashboardContent_model("NOTICES",R.drawable.ic_notice));
            List.add(new DashboardContent_model("HELP & SERVICES",R.drawable.ic_24_hours_support));
            List.add(new DashboardContent_model("VEHICLE MANAGEMENT",R.drawable.ic_vehicle));
            List.add(new DashboardContent_model("EMERGENCY HELPLINE",R.drawable.ic_sos));
            List.add(new DashboardContent_model("PAYMENTS\n" + "(Coming Soon)",R.drawable.ic_payment));
            List.add(new DashboardContent_model("GPS & CAMERA\n" + "(Coming Soon)",R.drawable.ic_gps));
            List.add(new DashboardContent_model("GALLERY",R.drawable.ic_gallery));
            List.add(new DashboardContent_model("SOCIETY INFORMATION",R.drawable.ic_society));
//            List.add(new DashboardContent_model("SOCIETY INFORMATION",R.drawable.ic_society));
            List.add(new DashboardContent_model("Contact us".toUpperCase(),R.drawable.ic_contact_us));

        }

        Adapter = new DashboardContent_adapter(context, List,USERTYPE);

        contentRecycleView.setAdapter(Adapter);
    }


    private void findView() {
        floating_call = view.findViewById(R.id.floating_call);
        floating_msg = view.findViewById(R.id.floating_msg);
        floating_GuardsList= view.findViewById(R.id.floating_GuardsList);
        contentRecycleView=view.findViewById(R.id.contentRecycleview);
    }



    private void click() {

        floating_call.setOnClickListener(this);
        floating_msg.setOnClickListener(this);
        floating_GuardsList.setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        if (USERTYPE.equalsIgnoreCase("1"))
        {
            ((DashboardActivity) getActivity()).setActionBarTitle("ADMIN");

        }
        else if (USERTYPE.equalsIgnoreCase("2"))
        {
            ((DashboardActivity) getActivity()).setActionBarTitle("USER");
        }

    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.floating_call:


                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {

                    Guard_Home_Fragment fragment=new Guard_Home_Fragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("Type","UserPhonebook");
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }

                break;

            case R.id.floating_msg:

                startActivity(new Intent(getContext(), User_Chat_Screen.class));
                break;
            case R.id.floating_GuardsList:
                startActivity(new Intent(getContext(), SecurityGuardsList_Screen.class));
                break;

        }

    }


}
