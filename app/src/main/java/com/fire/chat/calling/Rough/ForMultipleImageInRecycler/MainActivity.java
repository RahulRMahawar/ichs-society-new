package com.fire.chat.calling.Rough.ForMultipleImageInRecycler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.fire.chat.calling.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ListInterface {
    RecyclerView mRecyclerview;
    List<ListModel> listModels = new ArrayList<>();
    int mCurrentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_documents_popup);
        mRecyclerview = findViewById(R.id.addDocumentsRecycler);
        mRecyclerview.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new GridLayoutManager(MainActivity.this,2);
        mRecyclerview.setLayoutManager(layoutManager);
        listModels.add(new ListModel("", null));
        mRecyclerview.setAdapter(new ListAdapter(this, listModels));
    }

    @Override
    public void onClickDelete(String id, int position) {
        if (listModels.size() > 1) {
            listModels.remove(position);
            if (mRecyclerview.getAdapter() != null)
                mRecyclerview.getAdapter().notifyDataSetChanged();
        } else {
            Toast.makeText(this, "Can Not Delete This Item ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClickAdd(String id, int position) {
        listModels.add(new ListModel("", null));
        mRecyclerview.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onClickImage(String id, int position) {
        mCurrentPosition = position;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 101);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,}, 120);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            if (data != null) {
                listModels.get(mCurrentPosition).setId(String.valueOf(System.currentTimeMillis()));
                listModels.get(mCurrentPosition).setImage(data.getData());
                if (mRecyclerview.getAdapter() != null)
                    mRecyclerview.getAdapter().notifyDataSetChanged();
            } else {
                Toast.makeText(this, "Null", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 101);
        }
    }
}