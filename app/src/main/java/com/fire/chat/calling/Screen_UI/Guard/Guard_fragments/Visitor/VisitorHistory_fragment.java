package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Visitor;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Visitors_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.Visitors_adapter;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.Visitors_getset;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorHistory_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private Visitors_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    List<Visitors_getset> visitorsModelList;
    ConnectionDetector cd;
    ProgressDialog dialog;
    LinearLayout LayoutForSearch;
    EditText searchVisitor;
    Boolean isSearching = false;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_page_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        // ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.amenities));
        initializeViews();
        initializeRecycler();
        LayoutForSearch.setVisibility(View.VISIBLE);
        handleListeners();
        if (cd.isConnectingToInternet()) {
            getVisitorListAPI(" ");
            Searching();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        LinearLayout myLayout = (LinearLayout) view.findViewById(R.id.my_layout);
        myLayout.requestFocus();
        return view;
    }


    private void initializeViews() {
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
        LayoutForSearch = (LinearLayout) view.findViewById(R.id.LayoutForSearch);
        searchVisitor = (EditText) view.findViewById(R.id.searchContact);

    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void handleListeners() {


    }

    private void Searching() {
        searchVisitor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isSearching = true;
                getVisitorListAPI(editable.toString());
            }
        });

        searchVisitor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
    }

    public void getVisitorListAPI(String searchKey) {
        if (!isSearching) {
            dialog = ProgressDialog.show(context, "", "Please wait...", false);
        }


        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("type", "2");
            requestBody.put("sid", "pv");
            requestBody.put("stage", "0");
            requestBody.put("keyword",searchKey);

            Call<Visitors_Model> call = apiInterface.getVisitors(requestBody);
            call.enqueue(new Callback<Visitors_Model>() {
                @Override
                public void onResponse(Call<Visitors_Model> call, Response<Visitors_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Visitors_Model visitorsModel = response.body();
                                Adapter = new Visitors_adapter(context, visitorsModel, "Myvisitor");
                                recyclerView.setAdapter(Adapter);
                                Adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    if (!isSearching) {
                        dialog.cancel();
                    }


                }

                @Override
                public void onFailure(Call<Visitors_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    if (!isSearching) {
                        dialog.cancel();
                    }
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
}

