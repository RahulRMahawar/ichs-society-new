package com.fire.chat.calling.Screen_UI.Admin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Visitors_Model;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class Admin_Visitors_adapter extends RecyclerView.Adapter<Admin_Visitors_adapter.ViewHolder> {

    private Context context;
    private Visitors_Model adminVisitorsModel;
    AlertDialog.Builder builder;

    public interface AdminVisitorCall {
        void OnAdminVisitorCall();
    }

    AdminVisitorCall adminVisitorCall;

    public Admin_Visitors_adapter(Context context, Visitors_Model adminVisitorsModel, AdminVisitorCall adminVisitorCall) {
        this.context = context;
        this.adminVisitorsModel = adminVisitorsModel;
        this.adminVisitorCall = adminVisitorCall;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_visitors_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(adminVisitorsModel.getData().get(position));

        holder.name.setText(adminVisitorsModel.getData().get(position).getFull_name());
        holder.flateNo.setText(adminVisitorsModel.getData().get(position).getFlat_no());
        holder.purpose.setText(adminVisitorsModel.getData().get(position).getPurpose());
        holder.MobileNo.setText(adminVisitorsModel.getData().get(position).getPhone());
        holder.Date.setText(adminVisitorsModel.getData().get(position).getCreated());
        holder.RegisteredBy.setText("register by");
        holder.carNo.setText(adminVisitorsModel.getData().get(position).getVehicle_no());
        Picasso.get().load(Constants.GALLARYFOLDER + adminVisitorsModel.getData().get(position).getImage()).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);

        holder.callAdminVisitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminVisitorCall.OnAdminVisitorCall();
            }
        });

    }

    @Override
    public int getItemCount() {
        return adminVisitorsModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, flateNo, purpose, MobileNo, Date, carNo, RegisteredBy;
        ImageViewZoom imageView;
        LinearLayout callAdminVisitor;

        public ViewHolder(View itemView) {
            super(itemView);

            callAdminVisitor = itemView.findViewById(R.id.call_admin_visitor);

            name = itemView.findViewById(R.id.name);
            flateNo = itemView.findViewById(R.id.flatNo);
            purpose = itemView.findViewById(R.id.Purpose);
            MobileNo = itemView.findViewById(R.id.MobileNo);
            Date = itemView.findViewById(R.id.Date);
            carNo = itemView.findViewById(R.id.carNo);
            RegisteredBy = itemView.findViewById(R.id.RegisteredBy);
            imageView = itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}