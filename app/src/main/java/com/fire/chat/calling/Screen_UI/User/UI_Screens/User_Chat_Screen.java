package com.fire.chat.calling.Screen_UI.User.UI_Screens;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.ChatsAdapter;
import com.fire.chat.calling.Utils.GroupTyping;
import com.fire.chat.calling.Utils.constants.FireConstants;
import com.fire.chat.calling.Utils.constants.FireManager;
import com.fire.chat.calling.Utils.constants.MessageStat;
import com.fire.chat.calling.Utils.constants.MessageType;
import com.fire.chat.calling.Utils.constants.TypingStat;
import com.fire.chat.calling.Utils.realms.Chat;
import com.fire.chat.calling.Utils.realms.FireListener;
import com.fire.chat.calling.Utils.realms.Message;
import com.fire.chat.calling.Utils.realms.RealmHelper;
import com.fire.chat.calling.Utils.realms.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

public class User_Chat_Screen extends AppCompatActivity implements GroupTyping.GroupTypingListener {
    private Toolbar toolbar;
    TextView title;
    Context context;
    private RecyclerView rvChats;
    ChatsAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    RealmResults<Chat> chatList;
    OrderedRealmCollectionChangeListener<RealmResults<Chat>> changeListener;
    ValueEventListener typingEventListener, voiceMessageListener, lastMessageStatListener;

    List<GroupTyping> groupTypingList;
    FireListener fireListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guard_chat_screen_activity);
        setToolbar();
        setdata();
    }


    @Override
    public void onResume() {
        super.onResume();
        addTypingStatListener();
        addVoiceMessageStatListener();
        addMessageStatListener();
        try {
            chatList.addChangeListener(changeListener);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            fireListener.cleanup();
            if (groupTypingList != null) {
                for (GroupTyping groupTyping : groupTypingList) {
                    groupTyping.cleanUp();
                }
            }
            chatList.removeChangeListener(changeListener);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addTypingStatListener() {
        if (FireManager.getUid() == null)
            return;
        try {
            if (chatList.size() > 0) {
                for (Chat chat : chatList) {
                    User user = chat.getUser();
                    if (user == null) continue;
                    if (user.isGroupBool() && user.getGroup().isActive()) {

                        if (groupTypingList == null)
                            groupTypingList = new ArrayList<>();

                        GroupTyping groupTyping = new GroupTyping(user.getGroup().getUsers(), user.getUid(), this);
                        groupTypingList.add(groupTyping);

                    } else {
                        String receiverUid = user.getUid();
                        DatabaseReference typingStat = FireConstants.mainRef.child("typingStat").child(receiverUid)
                                .child(FireManager.getUid());
                        fireListener.addListener(typingStat, typingEventListener);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addVoiceMessageStatListener() {
        try {
            for (Chat chat : chatList) {
                Message lastMessage = chat.getLastMessage();

                User user = chat.getUser();
                if (user == null) continue;
                if (!user.isBroadcastBool() && lastMessage != null && lastMessage.getType() != MessageType.GROUP_EVENT && lastMessage.isVoiceMessage()
                        && lastMessage.getFromId().equals(FireManager.getUid())
                        && !lastMessage.isVoiceMessageSeen()) {

                    DatabaseReference reference = FireConstants.voiceMessageStat.child(lastMessage.getChatId()).child(lastMessage.getMessageId());
                    fireListener.addListener(reference, voiceMessageListener);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addMessageStatListener() {
        try {
            for (Chat chat : chatList) {
                Message lastMessage = chat.getLastMessage();
                if (!chat.getUser().isBroadcastBool() && lastMessage != null && lastMessage.getType() != MessageType.GROUP_EVENT && lastMessage.getMessageStat() != MessageStat.READ) {
                    DatabaseReference reference = FireConstants.messageStat.child(chat.getChatId()).child(lastMessage.getMessageId());
                    fireListener.addListener(reference, lastMessageStatListener);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //start user profile (Dialog-Like Activity)
  /*public  void userProfileClicked(User user) {
      Intent intent = Intent(this, ProfilePhotoDialog.class);
        intent.putExtra(IntentUtils.UID, user.uid);
        startActivity(intent);
    }

    public  void  addItemToActionMode(int itemsCount) {

        tvSelectedChatCount!!.text = itemsCount.toString() + ""


        if (itemsCount > 1) {
            if (isHasMutedItem)
                setMenuItemVisibility(false)
            else
                updateMutedIcon(toolbar!!.menu.findItem(R.id.menu_item_mute), false)//if there is no muted item then the user may select multiple chats and mute them all in once


        } else if (itemsCount == 1) {
            val adapter = getAdapter()
            if (adapter != null) {
                val isMuted = adapter.selectedChatForActionMode[0].isMuted
                //in case if it's hidden before
                setMenuItemVisibility(true)
                updateMutedIcon(toolbar!!.menu.findItem(R.id.menu_item_mute), isMuted)
            }
        }

        updateGroupItems()
    }*/


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.chat));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setdata() {
        try {
            chatList = RealmHelper.getInstance().getAllChats();
            adapter = new ChatsAdapter(chatList, true, this);
            linearLayoutManager = new LinearLayoutManager(this);
            rvChats.setLayoutManager(linearLayoutManager);
            rvChats.setAdapter(adapter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        /*recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);

        recyclerView.setLayoutManager(layoutManager);

        notificationList = new ArrayList<>();

        //Adding Data into ArrayList
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        Adapter = new Guard_chat_adapter(context, notificationList);

        recyclerView.setAdapter(Adapter);*/

    }

    @Override
    public void onTyping(int state, String groupId, User user) {
        try {
            Chat tempChat = new Chat();
            tempChat.setChatId(groupId);
            int i = chatList.indexOf(tempChat);

            if (i == -1) return;
            if (user == null) return;
            Chat chat = chatList.get(i);


            ChatsAdapter.ChatsHolder vh = (ChatsAdapter.ChatsHolder) rvChats.findViewHolderForAdapterPosition(i);

            if (vh == null) {
                return;
            }


            adapter.getTypingStatHashmap().put(chat.getChatId(), state);
            TextView typingTv = vh.tvTypingStat;
            TextView lastMessageTv = vh.tvLastMessage;
            ImageView lastMessageReadIcon = vh.imgReadTagChats;


            //if other user is typing or recording to this user
            //then hide last message textView with all its contents
            if (state == TypingStat.TYPING || state == TypingStat.RECORDING) {
                lastMessageTv.setVisibility(View.GONE);
                lastMessageReadIcon.setVisibility(View.GONE);
                typingTv.setVisibility(View.VISIBLE);
                typingTv.setText(user.getUserName() + " is " + TypingStat.getStatString(User_Chat_Screen.this, state));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onAllNotTyping(String groupId) {
        try {
            Chat tempChat = new Chat();
            tempChat.setChatId(groupId);
            int i = chatList.indexOf(tempChat);

            if (i == -1) return;
            Chat chat = chatList.get(i);

            ChatsAdapter.ChatsHolder vh = (ChatsAdapter.ChatsHolder) rvChats.findViewHolderForAdapterPosition(i);

            if (vh == null) {
                return;
            }


            TextView typingTv = vh.tvTypingStat;
            TextView lastMessageTv = vh.tvLastMessage;
            ImageView lastMessageReadIcon = vh.imgReadTagChats;

            adapter.getTypingStatHashmap().remove(chat.getChatId());
            typingTv.setVisibility(View.GONE);
            lastMessageTv.setVisibility(View.VISIBLE);
            Message lastMessage = chatList.get(i).getLastMessage();
            if (lastMessage != null &&
                    lastMessage.getType() != MessageType.GROUP_EVENT
                    && !MessageType.isDeletedMessage(lastMessage.getType())
                    && lastMessage.getFromId().equals(FireManager.getUid())) {
                lastMessageReadIcon.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
