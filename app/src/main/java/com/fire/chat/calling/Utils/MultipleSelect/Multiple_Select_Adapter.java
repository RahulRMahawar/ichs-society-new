package com.fire.chat.calling.Utils.MultipleSelect;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class Multiple_Select_Adapter extends RecyclerView.Adapter<Multiple_Select_Adapter.MultiViewHolder> {

    private Context context;
    private ArrayList<Days_model> employees;

    public Multiple_Select_Adapter(Context context, ArrayList<Days_model> employees) {
        this.context = context;
        this.employees = employees;
    }

    public void setEmployees(ArrayList<Days_model> employees) {
        this.employees = new ArrayList<>();
        this.employees = employees;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MultiViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.multiple_select_adapter, viewGroup, false);
        return new MultiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiViewHolder multiViewHolder, int position) {
        multiViewHolder.bind(employees.get(position));
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    class MultiViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        LinearLayout LayoutForColor;

        MultiViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            LayoutForColor = itemView.findViewById(R.id.LayoutForColor);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        void bind(final Days_model employee) {
            //LayoutForColor.setBackgroundColor(ContextCompat.getColor(context, R.color.orange));
            LayoutForColor.setBackground(employee.isChecked() ? context.getResources().getDrawable(R.drawable.selected_day) : context.getResources().getDrawable(R.drawable.non_selected_day));
            textView.setTextColor(employee.isChecked() ? ContextCompat.getColor(context, R.color.white) : ContextCompat.getColor(context, R.color.darkgrey));
            textView.setText(employee.getName());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    employee.setChecked(!employee.isChecked());
                    LayoutForColor.setBackground(employee.isChecked() ? context.getResources().getDrawable(R.drawable.selected_day) : context.getResources().getDrawable(R.drawable.non_selected_day));
                    textView.setTextColor(employee.isChecked() ? ContextCompat.getColor(context, R.color.white) : ContextCompat.getColor(context, R.color.darkgrey));
                }
            });
        }
    }

    public ArrayList<Days_model> getAll() {
        return employees;
    }

    public ArrayList<Days_model> getSelected() {
        ArrayList<Days_model> selected = new ArrayList<>();
        for (int i = 0; i < employees.size(); i++) {
            if (employees.get(i).isChecked()) {
                selected.add(employees.get(i));
            }
        }
        return selected;
    }
}