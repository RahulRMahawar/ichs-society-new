package com.fire.chat.calling.Screen_UI.Admin.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.fire.chat.calling.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class Add_OpeningTime_Adapter extends RecyclerView.Adapter<Add_OpeningTime_Adapter.ViewHolder>{

    Context context;
    ArrayList<String> steps;

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageButton plus, minus;

        public ViewHolder(View itemView) {
            super(itemView);
            plus = (ImageButton) itemView.findViewById(R.id.plus);
            minus = (ImageButton) itemView.findViewById(R.id.minus);

            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    try {
                        steps.remove(position);
                        notifyItemRemoved(position);
                    }catch (ArrayIndexOutOfBoundsException e){e.printStackTrace();}
                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    try {
                        steps.add(position + 1, "");
                        notifyItemInserted(position + 1);
                    }catch (ArrayIndexOutOfBoundsException e){e.printStackTrace();}
                }
            });

        }
    }


    public Add_OpeningTime_Adapter(ArrayList<String> steps, Context context){
        this.steps = steps;
        this.context = context;
    }


    @Override
    public int getItemCount() {
        return steps.size();
    }


    @Override
    public Add_OpeningTime_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_openingtime_adapter, viewGroup, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(Add_OpeningTime_Adapter.ViewHolder holder, final int i) {

        int x = holder.getLayoutPosition();

    }


    public ArrayList<String> getStepList(){
        return steps;
    }
}