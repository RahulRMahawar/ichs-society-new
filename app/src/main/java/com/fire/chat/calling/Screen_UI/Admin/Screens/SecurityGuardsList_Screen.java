package com.fire.chat.calling.Screen_UI.Admin.Screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.SecurityGuardsList_adapter;
import com.fire.chat.calling.Screen_UI.Admin.Model.SecurityGuardList_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.PerformCall;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecurityGuardsList_Screen extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title;
    Context context;
    private RecyclerView recyclerView;
    private SecurityGuardsList_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    SecurityGuardList_model securityGuardListModels;
    ConnectionDetector cd;
    String LoginType;
    FloatingActionButton floating_Add;
    LinearLayout emptyDataLayout;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_line_activity);
        context = SecurityGuardsList_Screen.this;
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        setToolbar();
        initializeViews();
        if (LoginType.equalsIgnoreCase("2")) {
            floating_Add.hide();
        }
        click();
    }

    private void initializeViews() {
        findView();
        initializeRecycler();
        cd = new ConnectionDetector(context);
        if (cd.isConnectingToInternet()) {
            getSecurityGuardListAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void findView() {
        floating_Add = findViewById(R.id.floating_AddHelpLine);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        emptyDataLayout = (LinearLayout) findViewById(R.id.emptyDataLayout);
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.securityGuardTitle));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void getSecurityGuardListAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("role_id", "5");
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<SecurityGuardList_model> call = apiInterface.getSecurityGuardList(requestBody);
            call.enqueue(new Callback<SecurityGuardList_model>() {
                @Override
                public void onResponse(Call<SecurityGuardList_model> call, Response<SecurityGuardList_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                securityGuardListModels = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new SecurityGuardsList_adapter(context, securityGuardListModels, LoginType, new SecurityGuardsList_adapter.SecurityGuardCall() {
                                        @Override
                                        public void OnSecurityGuardCall() {
                                            new PerformCall(SecurityGuardsList_Screen.this).performCall(false, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                                        }
                                    });
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SecurityGuardList_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void click() {
        floating_Add.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.floating_AddHelpLine:


                startActivity(new Intent(this, AddSecurityGuard.class));

                break;


        }
    }

    public void DeleteGuardAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<MarketPlaceList_Model> call = apiInterface.deleteUser(requestBody);
            call.enqueue(new Callback<MarketPlaceList_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceList_Model> call, Response<MarketPlaceList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceList_Model marketPlaceListModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, marketPlaceListModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
