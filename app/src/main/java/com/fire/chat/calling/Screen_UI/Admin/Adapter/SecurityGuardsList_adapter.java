package com.fire.chat.calling.Screen_UI.Admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Model.SecurityGuardList_model;
import com.fire.chat.calling.Screen_UI.Admin.Screens.AddSecurityGuard;
import com.fire.chat.calling.Screen_UI.Admin.Screens.SecurityGuardsDetails_Screen;
import com.fire.chat.calling.Screen_UI.Admin.Screens.SecurityGuardsList_Screen;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class SecurityGuardsList_adapter extends RecyclerView.Adapter<SecurityGuardsList_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String LoginType;
    SecurityGuardList_model securityGuardListModels;

    public interface SecurityGuardCall {
        public void OnSecurityGuardCall();
    }

    SecurityGuardCall securityGuardCall;


    public SecurityGuardsList_adapter(Context context, SecurityGuardList_model securityGuardListModels, String LoginType, SecurityGuardCall securityGuardCall) {
        this.context = context;
        this.securityGuardListModels = securityGuardListModels;
        this.LoginType = LoginType;
        this.securityGuardCall = securityGuardCall;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.security_guard_list_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(securityGuardListModels.getData().get(position));

        holder.name.setText(securityGuardListModels.getData().get(position).getFullName());
        holder.mobileNo.setText(securityGuardListModels.getData().get(position).getPhone());
        holder.gate.setText(securityGuardListModels.getData().get(position).getGate_no());
        holder.remarks.setText(securityGuardListModels.getData().get(position).getDescription());
        Picasso.get().load(Constants.GALLARYFOLDER + securityGuardListModels.getData().get(position).getImage()).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString("id", securityGuardListModels.getData().get(position).getId());
                bundle.putString("name", securityGuardListModels.getData().get(position).getFullName());
                bundle.putString("mobileNo", securityGuardListModels.getData().get(position).getPhone());
                bundle.putString("gateNo", securityGuardListModels.getData().get(position).getGate_no());
                bundle.putString("remarks", securityGuardListModels.getData().get(position).getDescription());
                bundle.putString("image", securityGuardListModels.getData().get(position).getImage());
                Intent intent = new Intent(context, AddSecurityGuard.class);
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SecurityGuardsList_Screen) context).DeleteGuardAPI(securityGuardListModels.getData().get(position).getId(), "2", position);
            }
        });

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                securityGuardCall.OnSecurityGuardCall();
            }
        });

    }

    @Override
    public int getItemCount() {
        return securityGuardListModels.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, mobileNo, gate, remarks;
        LinearLayout LayoutForEdits;
        ImageView delete, edit, call;
        ImageViewZoom imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            mobileNo = (TextView) itemView.findViewById(R.id.mobileNo);
            gate = (TextView) itemView.findViewById(R.id.gate);
            remarks = (TextView) itemView.findViewById(R.id.remarks);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            call = (ImageView) itemView.findViewById(R.id.call);
            LayoutForEdits = (LinearLayout) itemView.findViewById(R.id.LayoutForEdits);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            if (LoginType.equalsIgnoreCase("2")) {
                LayoutForEdits.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("UserId", securityGuardListModels.data.get(getAdapterPosition()).getId());
                    bundle.putString("title", securityGuardListModels.data.get(getAdapterPosition()).getFullName());
                    Intent intent = new Intent(context, SecurityGuardsDetails_Screen.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void removeAt(int adapterPosition) {
        securityGuardListModels.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, securityGuardListModels.getData().size());
    }

}