package com.fire.chat.calling.Screen_UI.Common_UI.CommonModel;

public class Login_Model {

    private String replyCode;
    private String replyMsg;
    private String otp;

    public Login_Model(String replyCode, String replyMsg, String otp) {
        this.replyCode = replyCode;
        this.replyMsg = replyMsg;
        this.otp = otp;
    }

    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}