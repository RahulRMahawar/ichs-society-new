package com.fire.chat.calling.Screen_UI.User.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AmenitiesDetails_Model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;

    @SerializedName("data")
    public DataValue data;

    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }


    public DataValue getData() {
        return data;
    }

    public void setData(DataValue data) {
        this.data = data;
    }

    public class DataValue {
        String id, title, image, description, time_from, time_to, breake_time_from, breake_time_to, type, qty, subscription_type, time_slots, time_slot_duration;

        @SerializedName("subscription_plans")
        public List<Subscription> subscription_plans;

        public String getId() {
            return id;
        }

        public List<Subscription> getSubscription_plans() {
            return subscription_plans;
        }

        public void setSubscription_plans(List<Subscription> subscription_plans) {
            this.subscription_plans = subscription_plans;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBreake_time_from() {
            return breake_time_from;
        }

        public void setBreake_time_from(String breake_time_from) {
            this.breake_time_from = breake_time_from;
        }

        public String getBreake_time_to() {
            return breake_time_to;
        }

        public void setBreake_time_to(String breake_time_to) {
            this.breake_time_to = breake_time_to;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getSubscription_type() {
            return subscription_type;
        }

        public void setSubscription_type(String subscription_type) {
            this.subscription_type = subscription_type;
        }

        public String getTime_slots() {
            return time_slots;
        }

        public void setTime_slots(String time_slots) {
            this.time_slots = time_slots;
        }

        public String getTime_slot_duration() {
            return time_slot_duration;
        }

        public void setTime_slot_duration(String time_slot_duration) {
            this.time_slot_duration = time_slot_duration;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTime_from() {
            return time_from;
        }

        public void setTime_from(String time_from) {
            this.time_from = time_from;
        }

        public String getTime_to() {
            return time_to;
        }

        public void setTime_to(String time_to) {
            this.time_to = time_to;
        }
    }

    public class Subscription {
        String id, amenity_id, monthly, quarterly, half_yearly, yearly, any, slots, subscription_type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public String getSubscription_type() {
            return subscription_type;
        }

        public void setSubscription_type(String subscription_type) {
            this.subscription_type = subscription_type;
        }

        public String getAmenity_id() {
            return amenity_id;
        }

        public void setAmenity_id(String amenity_id) {
            this.amenity_id = amenity_id;
        }

        public String getMonthly() {
            return monthly;
        }

        public void setMonthly(String monthly) {
            this.monthly = monthly;
        }

        public String getQuarterly() {
            return quarterly;
        }

        public void setQuarterly(String quarterly) {
            this.quarterly = quarterly;
        }

        public String getHalf_yearly() {
            return half_yearly;
        }

        public void setHalf_yearly(String half_yearly) {
            this.half_yearly = half_yearly;
        }

        public String getYearly() {
            return yearly;
        }

        public void setYearly(String yearly) {
            this.yearly = yearly;
        }

        public String getAny() {
            return any;
        }

        public void setAny(String any) {
            this.any = any;
        }

        public String getSlots() {
            return slots;
        }

        public void setSlots(String slots) {
            this.slots = slots;
        }
    }
}