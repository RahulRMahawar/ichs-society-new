package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.VehicleManagement_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.VehicleManagement_adapter;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Home;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuardVehicleManagement_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private VehicleManagement_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    FloatingActionButton fabAddVehicle;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.vehicle_management_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        ((Guard_Home) getActivity()).setActionBarTitle(getString(R.string.vehicleManagement));
        initializeViews();
        initializeRecycler();
        if (cd.isConnectingToInternet()) {
            getVehicleListAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        handleListeners();
        return view;
    }


    private void initializeViews() {


        fabAddVehicle=view.findViewById(R.id.fabAddVehicle);
        fabAddVehicle.hide();
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);


    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

    }
    public void getVehicleListAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("keyword", "");
            requestBody.put("sid", "pv");
            Call<VehicleManagement_Model> call = apiInterface.getUserVehicleList(requestBody);
            call.enqueue(new Callback<VehicleManagement_Model>() {
                @Override
                public void onResponse(Call<VehicleManagement_Model> call, Response<VehicleManagement_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                VehicleManagement_Model vehicleManagementModel = response.body();

                                Adapter = new VehicleManagement_adapter(context, vehicleManagementModel, "Guard","Guard");
                                recyclerView.setAdapter(Adapter);
                                Adapter.notifyDataSetChanged();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<VehicleManagement_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void handleListeners() {


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
}

