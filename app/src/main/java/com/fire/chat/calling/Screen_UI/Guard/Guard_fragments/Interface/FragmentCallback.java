package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Interface;

import android.view.ActionMode;

public interface FragmentCallback {

    void addMarginToFab(boolean isAdShowing);
    void startTheActionMode(ActionMode.Callback callback);

}
