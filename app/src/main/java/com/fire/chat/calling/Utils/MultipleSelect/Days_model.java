package com.fire.chat.calling.Utils.MultipleSelect;

import java.io.Serializable;

public class Days_model implements Serializable {

    private boolean isChecked = false;

    public Days_model(String name) {
        this.name=name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
