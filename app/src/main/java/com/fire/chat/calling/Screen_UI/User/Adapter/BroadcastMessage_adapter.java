package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.BroadcastMessageDetail;
import com.fire.chat.calling.Screen_UI.Admin.Screens.ViewComplaintsDetail;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.BroadCastMessage_Model;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class BroadcastMessage_adapter extends RecyclerView.Adapter<BroadcastMessage_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String LoginType;
    BroadCastMessage_Model broadCastMessageModel;
    String calledFrom;

    public BroadcastMessage_adapter(Context context, BroadCastMessage_Model broadCastMessageModel, String LoginType,String calledFrom) {
        this.context = context;
        this.broadCastMessageModel = broadCastMessageModel;
        this.LoginType = LoginType;
        this.calledFrom=calledFrom;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.broadcast_message_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(broadCastMessageModel.getData().get(position));

        holder.title.setText(broadCastMessageModel.getData().get(position).getNotification());
        holder.date.setText(broadCastMessageModel.getData().get(position).getCreated());
        Picasso.get().load(Constants.GALLARYFOLDER + broadCastMessageModel.getData().get(position).getImage()).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return broadCastMessageModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView date, des;
        LinearLayout LayoutForAdminEdit;
        ImageViewZoom imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            LayoutForAdminEdit = (LinearLayout) itemView.findViewById(R.id.LayoutForAdminEdit);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (LoginType.equalsIgnoreCase("1")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", broadCastMessageModel.data.get(getAdapterPosition()).getId());
                        bundle.putString("description", broadCastMessageModel.data.get(getAdapterPosition()).getNotification());
                        bundle.putString("file", broadCastMessageModel.data.get(getAdapterPosition()).getImage());
                        bundle.putString("calledFrom",calledFrom);
                        Intent intent = new Intent(context, BroadcastMessageDetail.class);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}