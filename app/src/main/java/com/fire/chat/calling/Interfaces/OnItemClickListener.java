package com.fire.chat.calling.Interfaces;


public interface OnItemClickListener {
    void onItemClick(Object object, int position, String tag);
}
