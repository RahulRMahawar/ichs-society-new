package com.fire.chat.calling.Screen_UI.Utlity;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.Help_And_Certificate.HelpAndServices_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.HelpAndServiceList_Model;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private HashMap<String, List<String>> id;
    private HashMap<String, List<String>> mobileNo;
    private HashMap<String, List<String>> rating;
    private HashMap<String, List<String>> image;
    HelpAndServiceList_Model List;
    HelpAndServices_fragment helpAndServices_fragment;
    String calledFrom;

    public ExpandableListAdapter(Context context, HelpAndServiceList_Model List) {
        this._context = context;
        this.List = List;
    }

    public ExpandableListAdapter(Context context, HelpAndServices_fragment helpAndServices_fragment, HelpAndServiceList_Model List, String calledFrom) {
        this._context = context;
        this.List = List;
        this.calledFrom = calledFrom;
        this.helpAndServices_fragment = helpAndServices_fragment;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.List.getData().get(groupPosition).getUsers();
    }

    /*public Object getMobile(int groupPosition, int childPosition) {
        return this.mobileNo.get(this.mobileNo.get(groupPosition)).get(childPosition);
    }*/
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String name = List.getData().get(groupPosition).getUsers().get(childPosition).getFullName();
        final String phone = List.getData().get(groupPosition).getUsers().get(childPosition).getPhone();
        final String id = List.getData().get(groupPosition).getUsers().get(childPosition).getId();
        final String rating = List.getData().get(groupPosition).getUsers().get(childPosition).getRating();
        /*final String mobileText = (String) getMobile(groupPosition, childPosition);*/
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }

        TextView childName = (TextView) convertView.findViewById(R.id.childName);
        TextView childMobileNo = (TextView) convertView.findViewById(R.id.childMobileNo);
        TextView childID = (TextView) convertView.findViewById(R.id.childID);
        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
        ImageViewZoom imageView = (ImageViewZoom) convertView.findViewById(R.id.userImg);
        Picasso.get().load(Constants.GALLARYFOLDER + List.getData().get(groupPosition).getUsers().get(childPosition).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(imageView);
        childName.setText(name);
        childMobileNo.setText(phone);
        childID.setText(id);
        ratingBar.setRating(Float.parseFloat(rating));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calledFrom.equalsIgnoreCase("HelpAndServices")) {
                    ((HelpAndServices_fragment) helpAndServices_fragment)
                            .callDetailPage(List.getData().get(groupPosition).getUsers().get(childPosition).getId(),
                                    List.getData().get(groupPosition).getUsers().get(childPosition).getFullName());
                }


            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.List.getData().get(groupPosition).getUsers().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.List.getData();
    }

    @Override
    public int getGroupCount() {
        return this.List.getData().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = List.getData().get(groupPosition).getTitle();
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}