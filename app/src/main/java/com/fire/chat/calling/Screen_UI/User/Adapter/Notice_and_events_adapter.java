package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Add_NoticeEvents_Fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.Notice_Detail_fragment;
import com.fire.chat.calling.Screen_UI.User.Model.NoticeAndEvent_Model;
import com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace.MarketPlace_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents.ImportantNotice_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents.PastEvents_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents.UpComingEvents_fragment;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class Notice_and_events_adapter extends RecyclerView.Adapter<Notice_and_events_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String LoginType;
    NoticeAndEvent_Model noticeAndEventModel;
    UpComingEvents_fragment upComingEvents_fragment;
    String CalledFrom;
    PastEvents_fragment pastEvents_fragment;
    ImportantNotice_fragment importantNotice_fragment;
    public Notice_and_events_adapter(Context context, NoticeAndEvent_Model noticeAndEventModel, String LoginType, UpComingEvents_fragment upComingEvents_fragment, String CalledFrom) {
        this.context = context;
        this.noticeAndEventModel = noticeAndEventModel;
        this.LoginType = LoginType;
        this.upComingEvents_fragment = upComingEvents_fragment;
        this.CalledFrom = CalledFrom;
    }

    public Notice_and_events_adapter(Context context, NoticeAndEvent_Model noticeAndEventModel, String LoginType, PastEvents_fragment pastEvents_fragment, String CalledFrom) {
        this.context = context;
        this.noticeAndEventModel = noticeAndEventModel;
        this.LoginType = LoginType;
        this.pastEvents_fragment = pastEvents_fragment;
        this.CalledFrom = CalledFrom;
    }

    public Notice_and_events_adapter(Context context, NoticeAndEvent_Model noticeAndEventModel, String LoginType, ImportantNotice_fragment importantNotice_fragment, String CalledFrom) {
        this.context = context;
        this.noticeAndEventModel = noticeAndEventModel;
        this.LoginType = LoginType;
        this.importantNotice_fragment = importantNotice_fragment;
        this.CalledFrom = CalledFrom;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_and_events_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(noticeAndEventModel.getData().get(position));

        holder.name.setText(noticeAndEventModel.getData().get(position).getTitle());
        holder.date.setText(noticeAndEventModel.getData().get(position).getEvent_date()
                + "  " + noticeAndEventModel.getData().get(position).getTime());
        holder.des.setText(noticeAndEventModel.getData().get(position).getDescription());
        if (noticeAndEventModel.getData().get(position).getDescription().equals("")) {
            holder.des.setVisibility(View.GONE);
        } else {
            holder.des.setVisibility(View.VISIBLE);
        }


        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CalledFrom.equalsIgnoreCase("UpcomingEvents")) {
                    ((UpComingEvents_fragment) upComingEvents_fragment).DeleteUpcomingAPI(noticeAndEventModel.getData().get(position).getId(), "2", position);

                } else if (CalledFrom.equalsIgnoreCase("pastEvent")) {
                    ((PastEvents_fragment) pastEvents_fragment).DeletePastEventAPI(noticeAndEventModel.getData().get(position).getId(), "2", position);

                } else if (CalledFrom.equalsIgnoreCase("ImportantNotice")){
                    ((ImportantNotice_fragment) importantNotice_fragment).DeleteImportantNoticeAPI(noticeAndEventModel.getData().get(position).getId(), "2", position);

                }
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {

                    Add_NoticeEvents_Fragment fragment = new Add_NoticeEvents_Fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("ID", noticeAndEventModel.getData().get(position).getId());
                    bundle.putString("TITLE", noticeAndEventModel.getData().get(position).getTitle());
                    bundle.putString("DES", noticeAndEventModel.getData().get(position).getDescription());
                    bundle.putString("DATE", noticeAndEventModel.getData().get(position).getEvent_date());
                    bundle.putString("TIME", noticeAndEventModel.getData().get(position).getTime());
                    bundle.putString("IMAGE", noticeAndEventModel.getData().get(position).getImage());
                    bundle.putString("TYPE", "edit");
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return noticeAndEventModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView date, des;
        public LinearLayout LayoutForEdits;
        ImageView delete, edit;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            des = (TextView) itemView.findViewById(R.id.des);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            LayoutForEdits = (LinearLayout) itemView.findViewById(R.id.LayoutForEdits);

            if (LoginType.equalsIgnoreCase("2")) {
                LayoutForEdits.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        Notice_Detail_fragment fragment = new Notice_Detail_fragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("ID", noticeAndEventModel.getData().get(getAdapterPosition()).getId());
                        bundle.putString("TITLE", "Event Detail");
                        fragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragment);
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void removeAt(int adapterPosition) {
        noticeAndEventModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, noticeAndEventModel.getData().size());
    }
}