package com.fire.chat.calling.Screen_UI.User.fragments.SocietyInformation;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.User.Adapter.SocietyInformation_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.SocietyInformation_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.MyCustomPagerAdapter;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocietyInformation_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private View view;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SocietyInformation_adapter Adapter;
    ConnectionDetector cd;
    ViewPager viewPager;
    MyCustomPagerAdapter myCustomPagerAdapter;
    int images[] = {R.drawable.yoga, R.drawable.yoga, R.drawable.yoga};
    LinearLayout LayoutForEdits;
    EditText description;
    CircleIndicator circleIndicator;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType;
    LinearLayout emptyDataLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.society_information_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.societyInformationtitle));
        initializeViews();
        initializeRecycler();
        initializeViewPager();
        setViewForUsers();
        handleListeners();
        getSocietyInfo();
        return view;
    }

    private void initializeViews() {
        recyclerView = view.findViewById(R.id.notificationrecycler);
        viewPager = view.findViewById(R.id.viewPager);
        LayoutForEdits = view.findViewById(R.id.LayoutForEdits);
        description = view.findViewById(R.id.description);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initializeViewPager() {
        myCustomPagerAdapter = new MyCustomPagerAdapter(context, images);
        viewPager.setAdapter(myCustomPagerAdapter);
        circleIndicator = view.findViewById(R.id.circle);
        circleIndicator.setViewPager(viewPager);
    }

    private void setViewForUsers() {
        if (LoginType.equalsIgnoreCase("2")) {
            LayoutForEdits.setVisibility(View.GONE);
            description.setEnabled(false);
        }
    }

    private void handleListeners() {


    }

    private void getSocietyInfo() {
        if (cd.isConnectingToInternet()) {
            getSocietyInfoAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getSocietyInfoAPI() {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("database_name",((GlobalVariables)getActivity().getApplication()).getDatabase_name());
            /* requestBody.put("sid", userDetail.get(SessionManager.SID));*/
            Call<SocietyInformation_Model> call = apiInterface.getSocietyInfo(requestBody);
            call.enqueue(new Callback<SocietyInformation_Model>() {
                @Override
                public void onResponse(Call<SocietyInformation_Model> call, Response<SocietyInformation_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                SocietyInformation_Model societyInformationModel = response.body();
                                description.setText(societyInformationModel.getData().getDescription());
                             /*   Picasso.get().load(Constants.GALLARYFOLDER+Constants.GALLARYFOLDER+societyInformationModel.getData().getImage())
                                        .placeholder(R.drawable.ic_splash_icon)
                                        .error(R.drawable.ic_splash_icon)
                                        .into(imageView);*/
                                if (!response.body().getData().getSociety_committee().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new SocietyInformation_adapter(context, societyInformationModel);
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SocietyInformation_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
}

