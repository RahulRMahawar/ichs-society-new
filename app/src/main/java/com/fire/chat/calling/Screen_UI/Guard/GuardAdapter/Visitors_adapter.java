package com.fire.chat.calling.Screen_UI.Guard.GuardAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Visitors_Model;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class Visitors_adapter extends RecyclerView.Adapter<Visitors_adapter.ViewHolder> {

    private Context context;
    private Visitors_Model List;
    AlertDialog.Builder builder;


    public Visitors_adapter(Context context, Visitors_Model List, String calledFrom) {
        this.context = context;
        this.List = List;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.visitors_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(List.getData().get(position));

        holder.name.setText(List.getData().get(position).getFull_name());
        holder.flatNo.setText(List.getData().get(position).getFlat_no());
        holder.purpose.setText(List.getData().get(position).getPurpose());
        holder.whitelist.setText("WhiteList");
        Picasso.get().load(Constants.GALLARYFOLDER+List.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return List.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView flatNo;
        public TextView purpose;
        public TextView whitelist;
        ImageViewZoom imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            flatNo = (TextView) itemView.findViewById(R.id.flatNo);
            purpose = (TextView) itemView.findViewById(R.id.purpose);
            whitelist = (TextView) itemView.findViewById(R.id.whitelist);
            imageView=(ImageViewZoom)itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}