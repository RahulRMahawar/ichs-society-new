package com.fire.chat.calling.Screen_UI.Guard.GuardAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.Notification_model;

import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class Guard_Notification_adapter extends RecyclerView.Adapter<Guard_Notification_adapter.ViewHolder> {

    private Context context;
    Notification_model notificationModel;


    public Guard_Notification_adapter(Context context, Notification_model notificationModel) {
        this.context = context;
        this.notificationModel = notificationModel;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.guard_notification_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(notificationModel.getData().get(position));

        holder.title.setText(notificationModel.getData().get(position).getTitle());
        holder.date.setText(notificationModel.getData().get(position).getCreated());
        holder.msg.setText(notificationModel.getData().get(position).getNotification());
       /* Picasso.get().load(Constants.GALLARYFOLDER+notificationModel.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);*/
    }

    @Override
    public int getItemCount() {
        return notificationModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView msg;
        public TextView date;
        ImageViewZoom imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            msg = (TextView) itemView.findViewById(R.id.msg);
            imageView=(ImageViewZoom)itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}