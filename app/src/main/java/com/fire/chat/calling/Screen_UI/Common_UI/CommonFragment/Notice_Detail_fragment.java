package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.User.Model.NoticeAndEvent_Model;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notice_Detail_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView description,date,title;
    ConnectionDetector cd;
    String receivedID,receivedTITLE;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notice_detail_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        try {
            receivedID = getArguments().getString("ID");
            receivedTITLE = getArguments().getString("TITLE");
        } catch (NullPointerException e) {
            receivedID ="";
            receivedTITLE = "Notices";
        }
        ((DashboardActivity) getActivity()).setActionBarTitle(receivedTITLE);
        initializeViews();
        if (cd.isConnectingToInternet()) {
            getDetailsAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        return view;
    }


    public void getDetailsAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", receivedID);
            Call<NoticeAndEvent_Model> call = apiInterface.getEventDetail(requestBody);
            call.enqueue(new Callback<NoticeAndEvent_Model>() {
                @Override
                public void onResponse(Call<NoticeAndEvent_Model> call, Response<NoticeAndEvent_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                NoticeAndEvent_Model noticeAndEventModel = response.body();

                                title.setText(noticeAndEventModel.getData().get(0).getTitle());
                                date.setText(noticeAndEventModel.getData().get(0).getEvent_date()
                                        +"  "+noticeAndEventModel.getData().get(0).getTime());
                                description.setText(noticeAndEventModel.getData().get(0).getDescription());


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<NoticeAndEvent_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeViews() {
        title=view.findViewById(R.id.title);
        date=view.findViewById(R.id.date);
        description=view.findViewById(R.id.description);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

           /* case R.id.done:


                return;*/

        }


    }
}

