package com.fire.chat.calling.Screen_UI.User.Model;

public class DashboardContent_model {

    private String itemName;
    Integer image;

    public DashboardContent_model(String itemName,Integer image) {
        this.itemName = itemName;
        this.image = image;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}