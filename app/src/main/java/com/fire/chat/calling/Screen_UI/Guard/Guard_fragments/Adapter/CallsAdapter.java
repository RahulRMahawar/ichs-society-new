package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devlomi.hidely.hidelyviews.HidelyImageView;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Utils.Database.CallLogData;
import com.fire.chat.calling.Utils.Database.FireAppLocalDatabase;
import com.fire.chat.calling.Utils.constants.FireCallType;
import com.fire.chat.calling.Utils.realms.BitmapUtils;
import com.fire.chat.calling.Utils.realms.TimeHelper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CallsAdapter extends RecyclerView.Adapter<CallsAdapter.PhoneCallHolder> {

    private List<CallLogData> callLogDataList;
    private List<CallLogData> originalList;
    private List<CallLogData> selectedItemForActionMode;
    private Context context;
    private OnClickListener onPhoneCallClick;
    private FireAppLocalDatabase appLocalDatabase;

    public CallsAdapter(@Nullable List<CallLogData> data,
                        List<CallLogData> selectedItemForActionMode, Context context, OnClickListener onPhoneCallClick) {
        this.callLogDataList = data;
        originalList = callLogDataList;
        this.selectedItemForActionMode = selectedItemForActionMode;
        this.context = context;
        this.onPhoneCallClick = onPhoneCallClick;
    }


    @NonNull
    @Override
    public PhoneCallHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_call, parent, false);
        return new PhoneCallHolder(row);
    }


    @Override
    public void onBindViewHolder(@NonNull PhoneCallHolder holder, int position) {
        holder.bind(callLogDataList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return callLogDataList.size();
    }

    class PhoneCallHolder extends RecyclerView.ViewHolder {
        private CircleImageView profileImage;
        private TextView tvUsername;
        private TextView tvCallTime;
        private ImageView callType;
        private ImageButton btnCall;
        private HidelyImageView imgSelected;


        public PhoneCallHolder(View itemView) {
            super(itemView);
            profileImage = itemView.findViewById(R.id.profile_image);
            tvUsername = itemView.findViewById(R.id.tv_username);
            tvCallTime = itemView.findViewById(R.id.tv_call_time);
            callType = itemView.findViewById(R.id.call_type);
            btnCall = itemView.findViewById(R.id.btn_call);
            imgSelected = itemView.findViewById(R.id.img_selected);
        }

        public void bind(final CallLogData callLogDataList, final int position) {
//            User user = callLogDataList.getUser();

            if (callLogDataList.getUserName() != null) {
                tvUsername.setText(callLogDataList.getUserName());
                if (callLogDataList.getUserImage() != null) {
                    try {
                        Log.e("image0", position + "---" + callLogDataList.getUserImage());
                        Glide.with(context).asBitmap().load(BitmapUtils.encodeImageAsBytes(callLogDataList.getUserImage())).into(profileImage);
                    } catch (Exception ex) {
                        Log.e("image0", position + "---not");
                        try {
                            Log.e("image0", position + "---inner" + callLogDataList.getUserImage());
                            Glide.with(context).load(callLogDataList.getUserImage()).into(profileImage);
                        } catch (Exception ex1) {
                            profileImage.setImageDrawable(AppCompatResources.getDrawable(context, R.drawable.user_img));
                        }
                        ex.printStackTrace();
                    }
                } else {
                    profileImage.setImageDrawable(AppCompatResources.getDrawable(context, R.drawable.user_img));
                }
            } else
                tvUsername.setText(callLogDataList.getUserNumber());

            int callDataType = 0;

            if (callLogDataList.getCallIncomingOutgoing().endsWith("INCOMING")) {
                if (callLogDataList.getCallMissedFull().endsWith("MISSED")) {
                    callDataType = 1;
                } else {
                    callDataType = 2;
                }
            } else {
                if (callLogDataList.getCallMissedFull().endsWith("MISSED")) {
                    callDataType = 3;
                } else {
                    callDataType = 4;
                }
            }

            callType.setImageDrawable(getPhoneCallType(callDataType));
            boolean isVideo;
            isVideo = callLogDataList.getCallType().contains("video");
            btnCall.setImageResource(isVideo ? R.drawable.ic_videocam_blue : R.drawable.ic_phone_blue);

            tvCallTime.setText(TimeHelper.getCallTime(Long.parseLong(callLogDataList.getCallTime())));

            if (selectedItemForActionMode.contains(callLogDataList)) {
                itemView.setBackgroundColor(context.getResources().getColor(R.color.light_blue));
                imgSelected.setVisibility(View.VISIBLE);

            } else {
                itemView.setBackgroundColor(-1);
                imgSelected.setVisibility(View.INVISIBLE);
            }


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onPhoneCallClick != null)
                        onPhoneCallClick.onItemClick(imgSelected, view, callLogDataList);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (onPhoneCallClick != null)
                        onPhoneCallClick.onLongClick(imgSelected, view, callLogDataList);
                    return true;
                }
            });

            btnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onPhoneCallClick != null)
                        onPhoneCallClick.onIconButtonClick(view, callLogDataList);
                }
            });


        }


        @SuppressLint("UseCompatLoadingForDrawables")
        private Drawable getPhoneCallType(int type) {

            switch (type) {

                case FireCallType.INCOMING_MISSED:
                    return context.getResources().getDrawable(R.drawable.ic_missed_incoming);

                case FireCallType.INCOMING_ANSWERED:
                    return context.getResources().getDrawable(R.drawable.ic_receive_incoming);

                case FireCallType.OUTGOING_ANSWERED:
                    return context.getResources().getDrawable(R.drawable.ic_receive_outgoing);

                case FireCallType.OUTGOING_MISSED:
                default:

                    return context.getResources().getDrawable(R.drawable.ic_missed_outgoing);


            }
        }
    }


    public interface OnClickListener {
        void onItemClick(HidelyImageView hidelyImageView, View itemView, CallLogData CallLogData);

        void onIconButtonClick(View itemView, CallLogData CallLogData);

        void onLongClick(HidelyImageView hidelyImageView, View itemView, CallLogData CallLogData);
    }

    public void filter(String query) {
        if (query.trim().isEmpty()) {
            callLogDataList = originalList;
        } else {
            this.callLogDataList = appLocalDatabase.getCallLogListByName(query);
        }

        notifyDataSetChanged();
    }
}
