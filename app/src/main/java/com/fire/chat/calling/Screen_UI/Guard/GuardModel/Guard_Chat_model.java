package com.fire.chat.calling.Screen_UI.Guard.GuardModel;

public class Guard_Chat_model {

    private String title;
    private String date;

    public Guard_Chat_model(String title, String date) {
        this.title = title;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}