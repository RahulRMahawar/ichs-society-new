package com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Add_NoticeEvents_Fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.TabAdapter;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.sessionData.SessionManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import java.util.HashMap;

public class NoticeAndEvent_fragment extends Fragment implements View.OnClickListener {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ConnectionDetector cd;
    Context context;
    FloatingActionButton fabAddNoticeEvents;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType;

    public NoticeAndEvent_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.notice_and_events_fragment, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.events));
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        fabAddNoticeEvents = (FloatingActionButton) view.findViewById(R.id.fabAddNoticeEvents);
        if (LoginType.equalsIgnoreCase("2")) {
            fabAddNoticeEvents.hide();
        }
        adapter = new TabAdapter(getChildFragmentManager());

        UpComingEvents_fragment upComingEventsFragment = new UpComingEvents_fragment();
        adapter.addFragment(upComingEventsFragment, "UPCOMING");

        PastEvents_fragment pastEventsFragment = new PastEvents_fragment();
        adapter.addFragment(pastEventsFragment, "PAST");


        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        initializeViews();
        click();
        return view;
    }


    private void initializeViews() {


    }


    private void click() {
        fabAddNoticeEvents.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fabAddNoticeEvents:

                /*Bundle bundle = new Bundle();
                bundle.putString("Type", "UserPhonebook");
                Guard_Home_Fragment fragment = new Guard_Home_Fragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();*/
                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {

                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, new Add_NoticeEvents_Fragment());
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }

                break;


        }


    }
}

