package com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter.Society_Flat_List_adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FlatList_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User_FlatList_Activity extends AppCompatActivity {
    private Context context;
    private SessionManager sessionManager;
    ConnectionDetector cd;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView SocietyListRecycler;
    Society_Flat_List_adapter Adapter;
    private HashMap<String, String> userDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_flat_list_activity);
        context = User_FlatList_Activity.this;
        cd = new ConnectionDetector(getApplicationContext());
        sessionManager = new SessionManager(this);
        userDetail = sessionManager.getLoginSavedDetails();
        Toast.makeText(context, ((GlobalVariables) getApplicationContext()).getDatabase_name(), Toast.LENGTH_SHORT).show();
        initializeView();
        CallFlatListAPI();
    }

    private void initializeView() {
        SocietyListRecycler = findViewById(R.id.SocietyListRecycler);
        layoutManager = new LinearLayoutManager(context);
    }

    private void CallFlatListAPI() {


        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("role_id", userDetail.get(SessionManager.LOGIN_TYPE));
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());
            Call<FlatList_Model> call = apiInterface.getUserFlatList(requestBody);
            call.enqueue(new Callback<FlatList_Model>() {
                @Override
                public void onResponse(Call<FlatList_Model> call, Response<FlatList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FlatList_Model model = response.body();
                                if (model.getData().isEmpty()) {
                                    if (userDetail.get(SessionManager.LOGIN_TYPE).equalsIgnoreCase("1")) {
                                        startActivity(new Intent(context, DashboardActivity.class));
                                        finishAffinity();
                                    }

                                } else {
                                    Adapter = new Society_Flat_List_adapter(context, model);
                                    //Setting the adapter
                                    SocietyListRecycler.setAdapter(Adapter);
                                    SocietyListRecycler.setLayoutManager(layoutManager);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FlatList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
