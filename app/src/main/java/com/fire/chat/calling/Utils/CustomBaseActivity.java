package com.fire.chat.calling.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public abstract class CustomBaseActivity extends Activity implements View.OnClickListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
