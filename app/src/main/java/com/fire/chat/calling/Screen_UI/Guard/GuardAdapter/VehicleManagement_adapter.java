package com.fire.chat.calling.Screen_UI.Guard.GuardAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.VehicleManagement_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.VehicleManagement_Model;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleManagement_adapter extends RecyclerView.Adapter<VehicleManagement_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String LoginType;
    VehicleManagement_Model vehicleManagementModel;
    VehicleManagement_fragment vehicleManagement_fragment;
    String CalledFrom, Status;
    RecyclerView ParkingStatusRecycler;
    LinearLayout emptyDataLayout;
    VehicleManagement_adapter Adapter;
    ArrayList<String> list = null;
    private HashMap<String, String> userDetail;
    RecyclerView.LayoutManager layoutManager;
    private SessionManager sessionManager;

    public VehicleManagement_adapter(Context context, VehicleManagement_Model vehicleManagementModel, String LoginType, String CalledFrom) {
        this.context = context;
        this.vehicleManagementModel = vehicleManagementModel;
        this.LoginType = LoginType;
        this.CalledFrom = CalledFrom;
    }

    public VehicleManagement_adapter(Context context, VehicleManagement_Model vehicleManagementModel
            , String LoginType, VehicleManagement_fragment vehicleManagement_fragment, String CalledFrom) {
        this.context = context;
        this.vehicleManagementModel = vehicleManagementModel;
        this.LoginType = LoginType;
        this.CalledFrom = CalledFrom;
        this.vehicleManagement_fragment = vehicleManagement_fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_management_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(vehicleManagementModel.getData().get(position));
        if (LoginType.equalsIgnoreCase("2")) {
            if (vehicleManagementModel.getData().get(position).getVerified() == 0) {
                holder.carNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
                holder.flatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
                Status = "Pending";

            } else if (vehicleManagementModel.getData().get(position).getVerified() == 1) {
                /*holder.carNo.setVisibility(View.GONE);
                holder.flatNo.setVisibility(View.GONE);*/
                holder.carNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
                holder.flatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
                Status = "Verified";
            } else {
               /* holder.carNo.setVisibility(View.GONE);
                holder.flatNo.setVisibility(View.GONE);*/
                holder.carNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
                holder.flatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
                Status = "Rejected";
            }
            holder.name.setText(vehicleManagementModel.getData().get(position).getFullName());
            holder.status.setText(Status);
          /*  holder.carNo.setVisibility(View.GONE);
            holder.flatNo.setVisibility(View.GONE);*/
            holder.layoutForUserEdit.setVisibility(View.GONE);
            holder.requestCard.setVisibility(View.GONE);
        } else if (LoginType.equalsIgnoreCase("1")) {
            if (vehicleManagementModel.getData().get(position).getVerified() == 0) {
                Status = "Pending";
                holder.mainCard.setVisibility(View.GONE);
                holder.RequestName.setText(vehicleManagementModel.getData().get(position).getFullName());
                holder.RequestFlatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
                holder.RequestCarNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
                Picasso.get().load(Constants.GALLARYFOLDER + vehicleManagementModel.getData().get(position).getImage()).fit()
                        .placeholder(R.drawable.ic_splash_icon)
                        .error(R.drawable.ic_splash_icon)
                        .into(holder.RequestUserImg);
            } else if (vehicleManagementModel.getData().get(position).getVerified() == 1) {
              /*  holder.carNo.setVisibility(View.GONE);
                holder.flatNo.setVisibility(View.GONE);*/
                holder.carNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
                holder.flatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
                Status = "Verified";
                holder.requestCard.setVisibility(View.GONE);
                holder.mainCard.setVisibility(View.VISIBLE);
            } else {
               /* holder.carNo.setVisibility(View.GONE);
                holder.flatNo.setVisibility(View.GONE);*/
                holder.carNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
                holder.flatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
                Status = "Rejected";
                holder.requestCard.setVisibility(View.GONE);
                holder.mainCard.setVisibility(View.GONE);
            }
        } else if (CalledFrom.equalsIgnoreCase("Requests")) {
            holder.layoutForUserEdit.setVisibility(View.GONE);
            holder.requestCard.setVisibility(View.GONE);
            if (vehicleManagementModel.getData().get(position).getVerified() == 0) {
                Status = "Pending";
            } else if (vehicleManagementModel.getData().get(position).getVerified() == 1) {
                Status = "Verified";
            } else {
                Status = "Rejected";
            }

        } else {
            holder.layoutForUserEdit.setVisibility(View.GONE);
            holder.status.setVisibility(View.GONE);
            holder.requestCard.setVisibility(View.GONE);
        }

        holder.status.setText(Status);
        holder.name.setText(vehicleManagementModel.getData().get(position).getFullName());
        holder.flatNo.setText(vehicleManagementModel.getData().get(position).getFlat_no());
        holder.carNo.setText(vehicleManagementModel.getData().get(position).getVehicle_no());
        Picasso.get().load(Constants.GALLARYFOLDER + vehicleManagementModel.getData().get(position).getImage()).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((VehicleManagement_fragment) vehicleManagement_fragment).DeleteVehicleAPI(String.valueOf(vehicleManagementModel.getData().get(position).getId()), "2", position);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CalledFrom.equalsIgnoreCase("VehicleManagement")) {
                    ((VehicleManagement_fragment) vehicleManagement_fragment)
                            .showPopUp(String.valueOf(vehicleManagementModel.getData().get(position).getId()), "Edit",
                                    vehicleManagementModel.getData().get(position).getParking_no(),
                                    vehicleManagementModel.getData().get(position).getVehicle_no(),
                                    vehicleManagementModel.getData().get(position).getBrand_name(),
                                    String.valueOf(vehicleManagementModel.getData().get(position).getFlat_id()));
                } else {

                }

            }
        });
        holder.Info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CallParkingStatusPopup(vehicleManagementModel.getData().get(position).getUser_id());
            }
        });
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((VehicleManagement_fragment) vehicleManagement_fragment).AcceptOrRejectVehicleAPI(String.valueOf(vehicleManagementModel.getData().get(position).getId()), "1", position);
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((VehicleManagement_fragment) vehicleManagement_fragment).AcceptOrRejectVehicleAPI(String.valueOf(vehicleManagementModel.getData().get(position).getId()), "2", position);
            }
        });
    }

    private void CallParkingStatusPopup(int user_id) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View customView = layoutInflater.inflate(R.layout.parking_status_popup, null);
        ParkingStatusRecycler = customView.findViewById(R.id.ParkingStatusRecycler);
        emptyDataLayout = customView.findViewById(R.id.emptyDataLayout);
        getVehicleListAPI(user_id);
        builder.setView(customView);
        builder.create();
        builder.show();
    }

    public void getVehicleListAPI(int user_id) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("keyword", "");
            requestBody.put("user_id", String.valueOf(user_id));
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<VehicleManagement_Model> call = apiInterface.getVehicleList(requestBody);
            call.enqueue(new Callback<VehicleManagement_Model>() {
                @Override
                public void onResponse(Call<VehicleManagement_Model> call, Response<VehicleManagement_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                VehicleManagement_Model vehicleManagementModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    ParkingStatusRecycler.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new VehicleManagement_adapter(context, vehicleManagementModel, "User", "Requests");
                                    layoutManager = new LinearLayoutManager(context);

                                    //Setting the adapter
                                    ParkingStatusRecycler.setAdapter(Adapter);
                                    ParkingStatusRecycler.setLayoutManager(layoutManager);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    ParkingStatusRecycler.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<VehicleManagement_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return vehicleManagementModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, RequestName, RequestFlatNo, RequestCarNo, flatNo, carNo, status, accept, decline;
        LinearLayout layoutForUserEdit;
        ImageView delete, edit;
        ImageViewZoom imageView, RequestUserImg;
        CardView mainCard, requestCard;
        ImageView Info;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            status = (TextView) itemView.findViewById(R.id.status);
            carNo = (TextView) itemView.findViewById(R.id.carNo);
            flatNo = (TextView) itemView.findViewById(R.id.flatNo);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            layoutForUserEdit = (LinearLayout) itemView.findViewById(R.id.layoutForUserEdit);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            RequestUserImg = (ImageViewZoom) itemView.findViewById(R.id.RequestUserImg);
            mainCard = (CardView) itemView.findViewById(R.id.mainCard);
            requestCard = (CardView) itemView.findViewById(R.id.requestCard);

            RequestName = (TextView) itemView.findViewById(R.id.RequestName);
            RequestFlatNo = (TextView) itemView.findViewById(R.id.RequestFlatNo);
            RequestCarNo = (TextView) itemView.findViewById(R.id.RequestCarNo);
            Info = (ImageView) itemView.findViewById(R.id.Info);
            accept = (TextView) itemView.findViewById(R.id.accept);
            decline = (TextView) itemView.findViewById(R.id.decline);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void removeAt(int adapterPosition) {
        vehicleManagementModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, vehicleManagementModel.getData().size());
    }

}