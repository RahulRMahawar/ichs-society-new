package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.Help_And_Certificate;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.HelpAndServiceList_Model;
import com.fire.chat.calling.Screen_UI.Utlity.ExpandableListAdapter;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpAndServices_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    ConnectionDetector cd;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType;
    LinearLayout emptyDataLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.help_support_fragment, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.helpAndService));
        initializeViews();
        click();
        callApi();
        return view;
    }

    public void callApi() {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            /* requestBody.put("sid", userDetail.get(SessionManager.SID));*/
            Call<HelpAndServiceList_Model> call = apiInterface.getServiceList(requestBody);
            call.enqueue(new Callback<HelpAndServiceList_Model>() {
                @Override
                public void onResponse(Call<HelpAndServiceList_Model> call, Response<HelpAndServiceList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject objectParent = new JSONObject(new Gson().toJson(response.body()));
                                HelpAndServiceList_Model helpAndServiceListModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    expListView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    listAdapter = new ExpandableListAdapter(context, HelpAndServices_fragment.this,
                                            helpAndServiceListModel, "HelpAndServices");
                                    // setting list adapter
                                    expListView.setAdapter(listAdapter);
                                    listAdapter.notifyDataSetChanged();
                                } else {
                                    expListView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<HelpAndServiceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initializeViews() {

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }

    private void click() {

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }

    public void callDetailPage(String userId, String title) {
        String backStateName = context.getClass().getName();
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) {

            HelpAndServices_Details_fragment fragment = new HelpAndServices_Details_fragment();
            Bundle bundle = new Bundle();
            bundle.putString("UserId", userId);
            bundle.putString("title", title);
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();

        }
    }
}

