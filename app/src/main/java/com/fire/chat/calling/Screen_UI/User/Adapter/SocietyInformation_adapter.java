package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.User.Model.SocietyInformation_Model;
import com.fire.chat.calling.Screen_UI.User.fragments.SocietyInformation.SocietyInformation_Detail_fragment;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class SocietyInformation_adapter extends RecyclerView.Adapter<SocietyInformation_adapter.ViewHolder> {

    private Context context;
    private SocietyInformation_Model List;
    AlertDialog.Builder builder;

    public SocietyInformation_adapter(Context context, SocietyInformation_Model List) {
        this.context = context;
        this.List = List;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.society_information_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(List.getData().getSociety_committee().get(position));

        holder.name.setText(List.getData().getSociety_committee().get(position).getTitle());
        Picasso.get().load(Constants.GALLARYFOLDER+List.getData().getSociety_committee().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return List.getData().getSociety_committee().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        ImageViewZoom imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            imageView=(ImageViewZoom)itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        SocietyInformation_Detail_fragment fragment=new SocietyInformation_Detail_fragment();
                        Bundle bundle=new Bundle();
                        bundle.putString("ID",List.getData().getSociety_committee().get(getAdapterPosition()).getId());
                        bundle.putString("TITLE",List.getData().getSociety_committee().get(getAdapterPosition()).getTitle());
                        fragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container,fragment);
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }


           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}