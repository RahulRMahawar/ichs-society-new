package com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.Screen_UI.Admin.Screens.SecurityGuardsList_Screen;
import com.fire.chat.calling.Utils.PerformCall;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceDetails_Model;
import com.fire.chat.calling.Screen_UI.User.Adapter.Offering_adapter;
import com.fire.chat.calling.Screen_UI.User.Adapter.ReviewLists_adapter;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketPlace_Detail_fragment extends Fragment implements View.OnClickListener, RatingDialogListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView writeReview, name, openingDays, openingTime, description;
    private RecyclerView reviewRecycler, offeringRecycler;
    private ReviewLists_adapter Adapter;
    private Offering_adapter offeringAdapter;
    private ImageView image;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    RatingBar ratingBar;
    LinearLayout LayoutForSharing, share, callMarketDetailsLayout, layoutForFeatures;
    private ImageView videoCallMarketDetailsImage;
    private ImageView audioCallMarketDetailsImage;
    private ImageView chatMarketDetailsImage;
    String ReceivedID, ReceivedTITLE;
    ProgressDialog dialog;
    MarketPlaceDetails_Model marketPlaceListModel;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    SliderView sliderView;
    private MarketPlaceSliderAdapter adapter;
    String LoginType;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.marketplace_detail_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        try {
            ReceivedID = getArguments().getString("ID");
        } catch (NullPointerException e) {
            ReceivedID = "";
        }
        try {
            ReceivedTITLE = getArguments().getString("TITLE");
        } catch (NullPointerException e) {
            ReceivedTITLE = getString(R.string.marketPlaceTitle);
        }
        ((DashboardActivity) getActivity()).setActionBarTitle(ReceivedTITLE);
        initializeViews();
        createSlider();
        initializeRecycler();
        handleListeners();
        getMarketPlaceDetails();
        return view;
    }


    private void initializeViews() {

        LayoutForSharing = view.findViewById(R.id.LayoutForSharing);
        layoutForFeatures = view.findViewById(R.id.layout_for_features);
        share = view.findViewById(R.id.share);
        callMarketDetailsLayout = view.findViewById(R.id.call_market_details);
        videoCallMarketDetailsImage = view.findViewById(R.id.video_call_market_details);
        chatMarketDetailsImage = view.findViewById(R.id.chat_market_details);
        audioCallMarketDetailsImage = view.findViewById(R.id.audio_call_market_details);
        writeReview = view.findViewById(R.id.writeReview);
        ratingBar = view.findViewById(R.id.ratingBar);
        reviewRecycler = (RecyclerView) view.findViewById(R.id.reviewRecycler);
        offeringRecycler = (RecyclerView) view.findViewById(R.id.offeringRecycler);
        name = view.findViewById(R.id.name);
        openingDays = view.findViewById(R.id.openingDays);
        openingTime = view.findViewById(R.id.openingTime);
        description = view.findViewById(R.id.description);
    }

    private void createSlider() {

        sliderView = view.findViewById(R.id.imageSlider);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();


       /* sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                Log.i("GGG", "onIndicatorClicked: " + sliderView.getCurrentPagePosition());
            }
        });*/

    }

    private void initializeRecycler() {
        reviewRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        reviewRecycler.setLayoutManager(layoutManager);

        ////////FOR OFFERING///////////////////////////////
        offeringRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        offeringRecycler.setLayoutManager(layoutManager);
    }

    private void handleListeners() {

        writeReview.setOnClickListener(this);
        share.setOnClickListener(this);
        callMarketDetailsLayout.setOnClickListener(this);
        videoCallMarketDetailsImage.setOnClickListener(this);
        audioCallMarketDetailsImage.setOnClickListener(this);
        chatMarketDetailsImage.setOnClickListener(this);
    }

    private void getMarketPlaceDetails() {
        if (cd.isConnectingToInternet()) {
            getMarketPlaceDetailsAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getMarketPlaceDetailsAPI() {
        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("market_place_shop_id", ReceivedID);
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<MarketPlaceDetails_Model> call = apiInterface.getMarketPlaceDetails(requestBody);
            call.enqueue(new Callback<MarketPlaceDetails_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceDetails_Model> call, Response<MarketPlaceDetails_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                marketPlaceListModel = response.body();
                                SetDataToViews(marketPlaceListModel);
                                SetSliderData(marketPlaceListModel.data.images);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceDetails_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetSliderData(List<MarketPlaceDetails_Model.Images> marketPlaceListModel) {
        adapter = new MarketPlaceSliderAdapter(context, marketPlaceListModel, LoginType);
        sliderView.setSliderAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void SetDataToViews(MarketPlaceDetails_Model List) {
        if (List.data != null) {
            name.setText(List.data.getTitle());
            openingDays.setText("MON" + List.data.getOpening_time());
            openingTime.setText(List.data.getOpening_time() + " To " + List.data.getClosing_time());
            description.setText(List.data.getDescription());
            ratingBar.setRating(Float.parseFloat(List.data.getRating()));
            if (marketPlaceListModel.getData().getMy_reviews().isEmpty()) {
                writeReview.setText("Write Reviews");
            } else {
                writeReview.setText("Edit Reviews");
            }
            SetRatingRecycler(List);
            SetOfferingRecycler(List);
        }


    }


    private void SetRatingRecycler(MarketPlaceDetails_Model List) {
        Adapter = new ReviewLists_adapter(context, List);
        reviewRecycler.setAdapter(Adapter);
        Adapter.notifyDataSetChanged();
    }

    private void SetOfferingRecycler(MarketPlaceDetails_Model List) {
        offeringAdapter = new Offering_adapter(context, List);
        offeringRecycler.setAdapter(offeringAdapter);
        offeringAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.writeReview:

                if (writeReview.getText().toString().equalsIgnoreCase("Write Reviews")) {
                    initializeReviewPop("add", "1", "Please select some stars and give your feedback");
                } else if (writeReview.getText().toString().equalsIgnoreCase("Edit Reviews")) {
                    initializeReviewPop("edit", marketPlaceListModel.getData().getMy_reviews().get(0).getRating()
                            , marketPlaceListModel.getData().getMy_reviews().get(0).getReview());
                }
                break;
            case R.id.share:
                if (LayoutForSharing.getVisibility() == View.GONE) {
                    LayoutForSharing.setVisibility(View.VISIBLE);
                    layoutForFeatures.setVisibility(View.GONE);
                } else {
                    LayoutForSharing.setVisibility(View.GONE);
                }
                break;
            case R.id.call_market_details:
                if (layoutForFeatures.getVisibility() == View.GONE) {
                    layoutForFeatures.setVisibility(View.VISIBLE);
                    LayoutForSharing.setVisibility(View.GONE);
                } else {
                    layoutForFeatures.setVisibility(View.GONE);
                }
                break;
            case R.id.video_call_market_details:
                new PerformCall(getActivity()).performCall(true, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                break;
            case R.id.chat_market_details:
                break;
            case R.id.audio_call_market_details:
                new PerformCall(getActivity()).performCall(false, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                break;


        }


    }

    private void initializeReviewPop(String type, String rating, String review) {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(Integer.parseInt(rating))
                .setTitle("Rate This Shop")
                .setDescription("Please select some stars and give your feedback")
                .setCommentInputEnabled(true)
                .setDefaultComment(review)
                .setStarColor(R.color.starColor)
                .setNoteDescriptionTextColor(R.color.black)
                .setTitleTextColor(R.color.titleTextColor)
                .setDescriptionTextColor(R.color.descriptionTextColor)
                .setWindowAnimation(R.style.MyDialogSlideHorizontalAnimation)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.darkgrey)
                .setCommentTextColor(R.color.commentTextColor)
                .setCommentBackgroundColor(R.color.commentBackgroundColor)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(getActivity())
                .setTargetFragment(this, 0)
                .show();
    }

    @Override
    public void onNegativeButtonClicked() {
    }

    @Override
    public void onNeutralButtonClicked() {

        Toast.makeText(context, "Please do provide feedback later.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositiveButtonClicked(int rating, String review) {
        SendReviewAPI(rating, review);
    }

    public void SendReviewAPI(int rating, String review) {

        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("market_place_shop_id", marketPlaceListModel.getData().getMarket_place_category_id());
            requestBody.put("rating", String.valueOf(rating));
            requestBody.put("review", review);
            requestBody.put("database_name", "ichs");
            Call<MarketPlaceDetails_Model> call = apiInterface.addOrEditReview(requestBody);
            call.enqueue(new Callback<MarketPlaceDetails_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceDetails_Model> call, Response<MarketPlaceDetails_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceDetails_Model marketPlaceDetailsModel = response.body();

                                Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceDetails_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

