package com.fire.chat.calling.Screen_UI.Admin.Fragment.Members;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.AddSecurityGuard;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter.Add_Documents_Adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_GetSet;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.fire.chat.calling.sessionData.CommonMethod.isEmailValid;
import static com.fire.chat.calling.sessionData.CommonMethod.isValidTelephone;

public class Add_Members_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private RecyclerView addDocumentsRecycler;
    private Add_Documents_Adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    ImageView AddDocument;
    ArrayList<String> list;
    EditText Username, flatNo, mobileNo, alternateNo, userEmail;
    Intent myFiles;
    ImageViewZoom userImg;
    Button done;
    String ReceivedFileName;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.add_members_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.addMembersTitle));
        initializeViews();
        handleListeners();
        return view;
    }


    private void initializeViews() {

        addDocumentsRecycler = view.findViewById(R.id.addDocumentsRecycler);
        AddDocument = view.findViewById(R.id.AddDocument);
        Username = view.findViewById(R.id.name);
        flatNo = view.findViewById(R.id.flatNo);
        mobileNo = view.findViewById(R.id.mobileNo);
        alternateNo = view.findViewById(R.id.alternateNo);
        userEmail = view.findViewById(R.id.userEmail);
        userImg = view.findViewById(R.id.userImg);
        done = view.findViewById(R.id.done);
    }


    private void handleListeners() {
        AddDocument.setOnClickListener(this);
        done.setOnClickListener(this);
        userImg.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.AddDocument:

                if (list == null || list.size() == 0) {
                    list = new ArrayList<>();
                    list.add("");
                }
                if (addDocumentsRecycler.getVisibility() == View.GONE) {
                    addDocumentsRecycler.setVisibility(View.VISIBLE);
                } else {
                    addDocumentsRecycler.setVisibility(View.GONE);
                }
                Adapter = new Add_Documents_Adapter(list, context, Add_Members_fragment.this, "AddMember");
                layoutManager = new GridLayoutManager(context, 2);

                //Setting the adapter
                addDocumentsRecycler.setAdapter(Adapter);
                addDocumentsRecycler.setLayoutManager(layoutManager);

                break;

            case R.id.done:
                validate();

                break;
            case R.id.userImg:
                requestAppPermissions();
                selectFile();

                break;
        }


    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(Username.getText().toString().trim()) || Username.length() <= 0) {
                Username.setError(getString(R.string.validate_userName));
                Username.requestFocus();
            } else if (!isValidTelephone(mobileNo.getText().toString())) {
                mobileNo.setError(getResources().getString(R.string.validate_userMobile));
                mobileNo.requestFocus();
            } else if (TextUtils.isEmpty(flatNo.getText().toString().trim()) || flatNo.length() <= 0) {
                flatNo.setError(getString(R.string.validate_userFlatNo));
                flatNo.requestFocus();
            } else if (!isValidTelephone(alternateNo.getText().toString())) {
                alternateNo.setError(getResources().getString(R.string.validate_userMobile));
                alternateNo.requestFocus();
            } else if (!isEmailValid(userEmail.getText().toString())) {
                userEmail.setError(getResources().getString(R.string.validate_userEmail));
                userEmail.requestFocus();
            } else {
                SubmitAddUserApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitAddUserApi() {

        Call<Verification_Model> call = null;
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("phone", mobileNo.getText().toString());
            requestBody.put("fullName", Username.getText().toString());
            requestBody.put("email", userEmail.getText().toString());
            requestBody.put("image",ReceivedFileName);
            requestBody.put("flat_no", flatNo.getText().toString());
            requestBody.put("alternate_no", alternateNo.getText().toString());
            requestBody.put("device_token", "");
            call = apiInterface.AddMembers(requestBody);

            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(context, Request_Approval_Info.class));
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//////For DOCUMENTS//////////////////////////////////////////////////////////////
   /* public void selectImage(int adapterPosition) {
        position = adapterPosition;
        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    if (fileExt.equalsIgnoreCase("mp4")) {

                    } else if (fileExt.equalsIgnoreCase("jpg")) {
                        Adapter.SetImageAt(position, path);
                        ArrayList<String> list = null;
                        list= new ArrayList<String>();

                        if (list.isEmpty() || position >= list.size()) {
                            // Adding new item to list.
                            list.add(position, path.toString());
                        } else {
                            list.set(position, path.toString());
                        }

                        Log.e("PATHH",String.valueOf(list));
                    } else if (fileExt.equalsIgnoreCase("aac")) {

                    } else if (fileExt.equalsIgnoreCase("pdf")) {

                    }
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                }
        }

    }*/
//////end DOCUMENTS//////////////////////////////////////////////////////////////
    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(userImg);
                    //Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getActivity().getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

