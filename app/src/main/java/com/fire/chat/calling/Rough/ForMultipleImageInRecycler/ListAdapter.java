package com.fire.chat.calling.Rough.ForMultipleImageInRecycler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private Context mContext;
    List<ListModel> listModels;
    ListInterface mCallbacks;

    public ListAdapter(Context mContext, List<ListModel> listModels) {
        this.mContext = mContext;
        this.listModels = listModels;
        mCallbacks = (ListInterface) mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_documents_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int i) {
        final ListModel lm = listModels.get(i);
        if (lm.getImage() != null) {
            Picasso.get().load(lm.getImage()).fit()
                    .placeholder(R.drawable.ic_splash_icon)
                    .error(R.drawable.ic_splash_icon)
                    .into(holder.imageView);
        } else {
            Picasso.get().load(R.drawable.ic_splash_icon).fit()
                    .placeholder(R.drawable.ic_splash_icon)
                    .error(R.drawable.ic_splash_icon)
                    .into(holder.imageView);
        }
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onClickImage(lm.getId(), i);
            }
        });
        holder.addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onClickAdd(lm.getId(), i);
            }
        });
        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onClickDelete(lm.getId(), i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, addItem, deleteItem;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            addItem = view.findViewById(R.id.plus);
            deleteItem = view.findViewById(R.id.minus);
        }
    }
}
