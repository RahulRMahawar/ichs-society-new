package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace.MarketPlace_Detail_fragment;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class MarketPlaceLists_adapter extends RecyclerView.Adapter<MarketPlaceLists_adapter.ViewHolder> {

    private Context context;
    private MarketPlaceList_Model List;
    AlertDialog.Builder builder;

    public MarketPlaceLists_adapter(Context context, MarketPlaceList_Model List) {
        this.context = context;
        this.List = List;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.marketplacelists_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(List.getData().get(position));

        holder.name.setText(List.getData().get(position).getTitle());
        holder.shopNo.setText(List.getData().get(position).getShop_no());
        if (List.data.get(position).getRating() != null) {
            holder.ratingBar.setRating(Float.parseFloat(List.data.get(position).getRating()));
        }
        holder.timmings.setText(List.getData().get(position).getOpening_time() + " To " + List.getData().get(position).getClosing_time());
        Picasso.get().load(Constants.GALLARYFOLDER + List.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return List.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, timmings, shopNo;
        ImageViewZoom imageView;

        RatingBar ratingBar;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            timmings = (TextView) itemView.findViewById(R.id.shopOpenTime);
            shopNo = (TextView) itemView.findViewById(R.id.shopNo);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        MarketPlace_Detail_fragment fragment = new MarketPlace_Detail_fragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("ID", List.getData().get(getAdapterPosition()).getMarket_place_category_id());
                        bundle.putString("TITLE", List.getData().get(getAdapterPosition()).getTitle());
                        fragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragment);
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }

                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}