package com.fire.chat.calling.Utils.constants;

public class FireCallType {
    public static final int OUTGOING = 1;
    public static final int ANSWERED = 2;
    public static final int MISSED = 3;
    public static final int INCOMING = 4;

    public static final int INCOMING_MISSED = 1;
    public static final int INCOMING_ANSWERED = 2;
    public static final int OUTGOING_MISSED = 3;
    public static final int OUTGOING_ANSWERED = 4;
}
