package com.fire.chat.calling.Screen_UI.Admin.Fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.AddSecurityGuard;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class Add_NoticeEvents_Fragment extends Fragment implements View.OnClickListener {

    EditText eventTitle, description, selectDate, selectTime;
    String receivedID, receivedTITLE, receivedDES, receivedDATE, receivedTIME, receivedTYPE, receivedIMAGE;
    Button submit;
    ConnectionDetector cd;
    private Context context;
    final Calendar myCalendar = Calendar.getInstance();
    ImageViewZoom userImg;
    Intent myFiles;
    String ReceivedFileName;
    RadioGroup radioGroup;
    RadioButton noticeRadio, eventRadio, importantRadio;
    int SelectedRadioType = 0;

    public Add_NoticeEvents_Fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_notice_events_fragment, container, false);
        // Setting ViewPager for each Tabs
        context = getActivity();
        cd = new ConnectionDetector(context);
        try {
            receivedID = getArguments().getString("ID");
            receivedTITLE = getArguments().getString("TITLE");
            receivedDES = getArguments().getString("DES");
            receivedDATE = getArguments().getString("DATE");
            receivedTIME = getArguments().getString("TIME");
            receivedTYPE = getArguments().getString("TYPE");
            receivedIMAGE = getArguments().getString("TYPE");
        } catch (NullPointerException e) {
            receivedID = "";
            receivedTITLE = "";
            receivedDES = "";
            receivedDATE = "";
            receivedTIME = "";
            receivedTYPE = "add";
            receivedIMAGE = "";
        }
        initializeViews(view);
        if (receivedTYPE.equalsIgnoreCase("edit")) {
            setViewForEdit();
        }
        handleListeners();

        return view;

    }

    private void setViewForEdit() {
        eventTitle.setText(receivedTITLE);
        description.setText(receivedDES);
        selectDate.setText(receivedDATE);
        selectTime.setText(receivedTIME);
        ReceivedFileName = receivedIMAGE;
        Picasso.get().load(Constants.GALLARYFOLDER + receivedIMAGE).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(userImg);
    }


    private void initializeViews(View view) {

        submit = view.findViewById(R.id.done);
        selectDate = view.findViewById(R.id.date);
        selectTime = view.findViewById(R.id.time);
        description = view.findViewById(R.id.description);
        eventTitle = view.findViewById(R.id.title);
        userImg = view.findViewById(R.id.userImg);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        noticeRadio = (RadioButton) view.findViewById(R.id.noticeRadio);
        eventRadio = (RadioButton) view.findViewById(R.id.eventRadio);
        importantRadio = (RadioButton) view.findViewById(R.id.importantRadio);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.noticeRadio) {
                    SelectedRadioType = 1;
                } else if (checkedId == R.id.eventRadio) {
                    SelectedRadioType = 2;
                } else if (checkedId == R.id.importantRadio) {
                    SelectedRadioType = 3;
                }

            }
        });
    }

    private void handleListeners() {

        submit.setOnClickListener(this);
        selectDate.setOnClickListener(this);
        selectTime.setOnClickListener(this);
        userImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.done:
                validate();
                break;
            case R.id.date:
                selectDate();
                break;
            case R.id.time:
                selectTime();
                break;
            case R.id.userImg:
                requestAppPermissions();
                selectFile();
                break;
        }
    }

    private void selectDate() {
        new DatePickerDialog(context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void selectTime() {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);
        final int sec = currentTime.get(Calendar.SECOND);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                selectTime.setText(selectedHour + ":" + selectedMinute + ":" + sec);
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDate();
        }

    };

    private void updateDate() {
        String myFormat = "yy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        selectDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(eventTitle.getText().toString().trim()) || eventTitle.length() <= 0) {
                eventTitle.setError(getString(R.string.validate_EventTitle));
                eventTitle.requestFocus();
            } else if (TextUtils.isEmpty(description.getText().toString().trim()) || description.length() <= 0) {
                description.setError(getString(R.string.validate_Discription));
                description.requestFocus();
            } else if (TextUtils.isEmpty(selectDate.getText().toString().trim()) || selectDate.length() <= 0) {
                selectDate.setError(getString(R.string.validate_Date));
                selectDate.requestFocus();
            } else if (TextUtils.isEmpty(selectTime.getText().toString().trim()) || selectTime.length() <= 0) {
                selectTime.setError(getString(R.string.validate_Time));
                selectTime.requestFocus();
            } else {
                SubmitAddOrEditEventsApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitAddOrEditEventsApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            if (receivedTYPE.equalsIgnoreCase("edit")) {
                requestBody.put("id", receivedID);
            } else {
                requestBody.put("type", "2");
            }

            requestBody.put("title", eventTitle.getText().toString());
            requestBody.put("description", description.getText().toString());
            requestBody.put("event_date", selectDate.getText().toString());
            requestBody.put("time", selectTime.getText().toString());
            requestBody.put("image", ReceivedFileName);
            requestBody.put("device_token", "");
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());


            Call<Verification_Model> call = apiInterface.AddEvents(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            resetFields();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetFields() {
        selectTime.setText("");
        selectDate.setText("");
        description.setText("");
        eventTitle.setText("");
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(userImg);
                    //Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getActivity().getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}