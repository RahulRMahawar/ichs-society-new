package com.fire.chat.calling.placespicker.model

import com.google.gson.annotations.SerializedName

data class VenuePage(
        @SerializedName("id")
        val id: String
)