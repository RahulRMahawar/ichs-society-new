package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.ViewComplaintsDetail;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.ComplaintAndSuggest_Model;
import com.fire.chat.calling.Screen_UI.OTP_Verification_Screen;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Complaint_Suggestion_adapter extends RecyclerView.Adapter<Complaint_Suggestion_adapter.ViewHolder> {

    private Context context;
    private ComplaintAndSuggest_Model complaintAndSuggestModel;
    AlertDialog.Builder builder;
    String LoginType;
    String CALLEDFROM;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    public Complaint_Suggestion_adapter(Context context, ComplaintAndSuggest_Model complaintAndSuggestModel, String LoginType, String CALLEDFROM) {
        this.context = context;
        this.complaintAndSuggestModel = complaintAndSuggestModel;
        this.LoginType = LoginType;
        this.CALLEDFROM = CALLEDFROM;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.complain_suggestion_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemView.setTag(complaintAndSuggestModel.getData().get(position));

        holder.title.setText(complaintAndSuggestModel.getData().get(position).getTitle());
        holder.date.setText(complaintAndSuggestModel.getData().get(position).getCreated());
        Picasso.get().load(Constants.GALLARYFOLDER + complaintAndSuggestModel.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.reopen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reopenComplaintAPI(holder, "0", complaintAndSuggestModel.getData().get(position).getId());
            }
        });
        holder.complaintId.setText("# " + complaintAndSuggestModel.getData().get(position).getId());
    }

    @Override
    public int getItemCount() {
        return complaintAndSuggestModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title, complaintId;
        public TextView date, des, reopen;
        LinearLayout LayoutForAdminEdit, LayoutForReopen;
        ImageViewZoom imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            reopen = (TextView) itemView.findViewById(R.id.reopen);
            LayoutForAdminEdit = (LinearLayout) itemView.findViewById(R.id.LayoutForAdminEdit);
            LayoutForReopen = (LinearLayout) itemView.findViewById(R.id.LayoutForReopen);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            complaintId = (TextView) itemView.findViewById(R.id.complaintId);
            if (LoginType.equalsIgnoreCase("1")) {
                LayoutForAdminEdit.setVisibility(View.VISIBLE);
            }
            if (CALLEDFROM.equalsIgnoreCase("Approved")) {
                LayoutForReopen.setVisibility(View.VISIBLE);
            }


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (LoginType.equalsIgnoreCase("1")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", complaintAndSuggestModel.data.get(getAdapterPosition()).getId());
                        bundle.putString("title", complaintAndSuggestModel.data.get(getAdapterPosition()).getTitle());
                        bundle.putString("date", complaintAndSuggestModel.data.get(getAdapterPosition()).getCreated());
                        bundle.putString("calledFrom", CALLEDFROM);
                        Intent intent = new Intent(context, ViewComplaintsDetail.class);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                       /* String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, new BroadCastMessageDetails_Fragment());
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }*/
                    }


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void reopenComplaintAPI(final ViewHolder holder, String type, String id) {

        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", id);
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("stage", type);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());

            Call<ComplaintAndSuggest_Model> call = apiInterface.updateComplaint(requestBody);
            call.enqueue(new Callback<ComplaintAndSuggest_Model>() {
                @Override
                public void onResponse(Call<ComplaintAndSuggest_Model> call, Response<ComplaintAndSuggest_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                ComplaintAndSuggest_Model complaintAndSuggestModel = response.body();
                                removeAt(holder.getAdapterPosition());
                                Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<ComplaintAndSuggest_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeAt(int adapterPosition) {
        complaintAndSuggestModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, complaintAndSuggestModel.getData().size());
    }

}