package com.fire.chat.calling.Screen_UI.Admin.Fragment.Amenities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.User.Model.Amenities_model;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddAmenitiesNew_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private EditText name;
    ConnectionDetector cd;
    int mHour, mMinute, sec;
    LinearLayout subscriptionLayout;
    EditText startTime, endTime, monthly, quarterly, halfYearly, yearly, any, quantity, des;
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    Spinner typeSpinner;
    String type;
    Button done;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    ImageViewZoom userImg;
    ImageView addImage;
    Intent myFiles;
    String ReceivedFileName, ReceivedId;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.add_amenities_new_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();

        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.amenities));
        initializeViews();
        getIntents();
        click();
        setView();
        return view;
    }

    private void getIntents() {
        Bundle args = getArguments();
        if (args != null) {

            Amenities_model.DataValue list = (Amenities_model.DataValue) args.getParcelable("data");
            ReceivedId = list.getId();
            name.setText(list.getTitle());
            des.setText(list.getDescription());
            startTime.setText(list.getTime_from());
            endTime.setText(list.getTime_to());
            ReceivedFileName = list.getImage();
            Picasso.get().load(Constants.GALLARYFOLDER + list.getImage()).fit()
                    .placeholder(R.drawable.ic_splash_icon)
                    .error(R.drawable.ic_splash_icon)
                    .into(userImg);
        } else {
            ReceivedId = "";
            ReceivedFileName = "";
        }
    }

    private void setView() {
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (typeSpinner.getSelectedItem().toString().equalsIgnoreCase("free")) {
                    subscriptionLayout.setVisibility(View.GONE);
                    type = "0";
                } else {
                    subscriptionLayout.setVisibility(View.VISIBLE);
                    type = "1";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void initializeViews() {
        endTime = view.findViewById(R.id.endTime);
        startTime = view.findViewById(R.id.startTime);
        name = view.findViewById(R.id.name);
        typeSpinner = view.findViewById(R.id.typeSpinner);
        subscriptionLayout = view.findViewById(R.id.subscriptionLayout);
        monthly = view.findViewById(R.id.monthly);
        quarterly = view.findViewById(R.id.quarterly);
        halfYearly = view.findViewById(R.id.halfYearly);
        yearly = view.findViewById(R.id.yearly);
        any = view.findViewById(R.id.any);
        quantity = view.findViewById(R.id.quantity);
        des = view.findViewById(R.id.des);
        done = view.findViewById(R.id.done);
        userImg = view.findViewById(R.id.userImg);
        addImage = view.findViewById(R.id.addImage);
    }


    private void click() {

        startTime.setOnClickListener(this);
        endTime.setOnClickListener(this);
        done.setOnClickListener(this);
        addImage.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.startTime:

                calendar = Calendar.getInstance();
                mHour = calendar.get(Calendar.HOUR_OF_DAY);
                mMinute = calendar.get(Calendar.MINUTE);
                sec = calendar.get(Calendar.SECOND);
                // Launch Time Picker Dialog
                timePickerDialog = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {


                                startTime.setText(hourOfDay + ":" + minute + ":" + sec);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

                break;
            case R.id.endTime:

                calendar = Calendar.getInstance();
                mHour = calendar.get(Calendar.HOUR_OF_DAY);
                mMinute = calendar.get(Calendar.MINUTE);
                sec = calendar.get(Calendar.SECOND);
                // Launch Time Picker Dialog
                timePickerDialog = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                endTime.setText(hourOfDay + ":" + minute + ":" + sec);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
            case R.id.done:
                cd = new ConnectionDetector(context);
                if (cd.isConnectingToInternet()) {
                    validate();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.addImage:
                cd = new ConnectionDetector(context);
                if (cd.isConnectingToInternet()) {
                    requestAppPermissions();
                    selectFile();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;
        }


    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                name.setError(getString(R.string.validate_amenitiesName));
                name.requestFocus();
            } else if (TextUtils.isEmpty(startTime.getText().toString().trim()) || startTime.length() <= 0) {
                Toast.makeText(context, "Select start time.", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(endTime.getText().toString().trim()) || endTime.length() <= 0) {
                Toast.makeText(context, "Select end time.", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(des.getText().toString().trim()) || des.length() <= 0) {
                des.setError(getString(R.string.description));
                des.requestFocus();
            } else if (type.equalsIgnoreCase("1") && monthly.getText().toString().isEmpty() && quarterly.getText().toString().isEmpty()
                    && halfYearly.getText().toString().isEmpty() && yearly.getText().toString().isEmpty() && any.getText().toString().isEmpty()) {
                Toast.makeText(context, "Select at least one subscription type.", Toast.LENGTH_SHORT).show();
            } else {
                SubmitAddAmenityApi();
            }
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitAddAmenityApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);
        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("title", name.getText().toString());
            requestBody.put("description", des.getText().toString());
            requestBody.put("time_from", startTime.getText().toString());
            requestBody.put("time_to", endTime.getText().toString());
            requestBody.put("qty", quantity.getText().toString());
            requestBody.put("amenity_id", ReceivedId);
            requestBody.put("monthly", monthly.getText().toString());
            requestBody.put("quarterly", quarterly.getText().toString());
            requestBody.put("half_yearly", halfYearly.getText().toString());
            requestBody.put("yearly", yearly.getText().toString());
            requestBody.put("any", any.getText().toString());
            requestBody.put("slots", "");
            requestBody.put("subscription_type", "");
            requestBody.put("type", type);
            requestBody.put("image", ReceivedFileName);


            Call<Verification_Model> call = apiInterface.addAmenity(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            clearViews();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearViews() {
        name.getText().clear();
        monthly.getText().clear();
        yearly.getText().clear();
        quarterly.getText().clear();
        halfYearly.getText().clear();
        any.getText().clear();
        startTime.getText().clear();
        endTime.getText().clear();
        quantity.getText().clear();
        des.getText().clear();
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(userImg);
                    //Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
                break;

        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) context.getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

