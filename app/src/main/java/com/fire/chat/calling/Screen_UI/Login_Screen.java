package com.fire.chat.calling.Screen_UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.User_Detail_Register;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.PermissionsUtil;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.ActivityCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login_Screen extends AppCompatActivity implements View.OnClickListener {
    Button sendotp;
    EditText MobileNo;
    ConnectionDetector cd;
    private Context context;
    String idToken = "";
    String USERTYPE;
    private static final int PERMISSION_REQUEST_CODE = 159;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen_activity);
        context = Login_Screen.this;
        cd = new ConnectionDetector(context);
        USERTYPE = ((GlobalVariables) context.getApplicationContext()).getRole_id();
        //gettoken();
        _initializeView();
        handleListeners();
    }

    private void handleListeners() {
        sendotp.setOnClickListener(this);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PermissionsUtil.permissions, PERMISSION_REQUEST_CODE);
    }

    private void _initializeView() {
        sendotp = findViewById(R.id.sendOTP);
        MobileNo = findViewById(R.id.MobileNo);
        requestPermissions();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.sendOTP:
                //Toast.makeText(context, idToken, Toast.LENGTH_SHORT).show();
                if (TextUtils.isEmpty(MobileNo.getText().toString().trim()) || MobileNo.length() < 10) {
                    MobileNo.setError(getString(R.string.mobileNoRequired));
                    MobileNo.requestFocus();
                } else {
                    if (cd.isConnectingToInternet()) {
                        callLoginAPI(MobileNo.getText().toString());
                    } else {
                        Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }


                break;
        }
    }

    public void callLoginAPI(final String mobile) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("phone", mobile);
            requestBody.put("role_id", USERTYPE);
            Call<Verification_Model> call = apiInterface.getUser(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Verification_Model rs = response.body();
                                String otp = rs.getOtp();
                                Bundle bundle = new Bundle();
                                bundle.putString("OTP", otp);
                                bundle.putString("UserNo", mobile);
                                Intent intent = new Intent(context, OTP_Verification_Screen.class);
                                intent.putExtras(bundle);
                                startActivity(intent);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                            PopUpForSignUP(response.body().getReplyMsg());

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PopUpForSignUP(String replyMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Information");
        builder.setMessage(replyMsg);
        builder.setPositiveButton("SIGN UP",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(context, User_Detail_Register.class));
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setCancelable(false);
        builder.show();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //check if user granted permissions
        //then check if he is already logged in start nextActivity
        // if he is not logged in then launch login activity
        //if he does not grant the permissions then show alert
        // dialog to make him grant the permissions9
        if (!PermissionsUtil.permissionsGranted(grantResults)) {
            requestPermissions();
        }
    }
}
