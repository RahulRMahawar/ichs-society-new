package com.fire.chat.calling.Screen_UI.Admin.Screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Model.SecurityGuardList_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.ComplaintAndSuggest_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.SocietyList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Request_Approval_Info;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.User_Detail_Register;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fire.chat.calling.sessionData.CommonMethod.isEmailValid;

public class ViewComplaintsDetail extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title, complaintId, complaintTitle, complaintDate;
    EditText message;
    Spinner spinnerAssign;
    Button done;
    ConnectionDetector cd;
    private Context context;
    String assignToName, assignToID;
    ArrayList<String> AssignToNames = new ArrayList<String>();
    ArrayList<String> AssignToID = new ArrayList<String>();
    SecurityGuardList_model responseBody;
    String receivedID, receivedDate, receivedTitle, receivedCalledFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_complaint_detail_activity);
        context = ViewComplaintsDetail.this;
        cd = new ConnectionDetector(context);
        setToolbar();
        _initializeView();
        getIntents();
        setData();
        handleListeners();
        loadAssignSpinner();
    }


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.viewComplaintitle));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void _initializeView() {
        complaintId = findViewById(R.id.complaintId);
        complaintTitle = findViewById(R.id.complaintTitle);
        complaintDate = findViewById(R.id.complaintDate);
        message = findViewById(R.id.message);
        spinnerAssign = findViewById(R.id.spinnerAssign);
        done = findViewById(R.id.done);
    }

    private void getIntents() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            receivedID = bundle.getString("id");
            receivedDate = bundle.getString("date");
            receivedTitle = bundle.getString("title");
            receivedCalledFrom = bundle.getString("calledFrom");
        }
    }

    private void setData() {

        complaintId.setText(receivedID);
        complaintDate.setText(receivedDate);
        complaintTitle.setText(receivedTitle);
        if (receivedCalledFrom.equalsIgnoreCase("approved")) {
            done.setVisibility(View.GONE);
        }
    }

    private void handleListeners() {
        done.setOnClickListener(this);
        spinnerAssign.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (responseBody == null) {

                } else {
                    assignToName = adapterView.getSelectedItem().toString();
                    assignToID = AssignToID.get(adapterView.getSelectedItemPosition());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadAssignSpinner() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("role_id", "6");
            Call<SecurityGuardList_model> call = apiInterface.getAllEmployeesList(requestBody);
            call.enqueue(new Callback<SecurityGuardList_model>() {
                @Override
                public void onResponse(Call<SecurityGuardList_model> call, Response<SecurityGuardList_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                AssignToNames.add("Assign to");
                                AssignToID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    AssignToNames.add(response.body().getData().get(i).getFullName());
                                    AssignToID.add(response.body().getData().get(i).getId());
                                }
                                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>
                                        (context, R.layout.center_spinner_style, AssignToNames);
                                spinnerAssign.setAdapter(categoryAdapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SecurityGuardList_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.done:
                validate();
                break;
        }
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (assignToName.equalsIgnoreCase("Assign to")) {
                Toast.makeText(context, "Please assign someone", Toast.LENGTH_SHORT).show();
            } else {
                SubmitAssignComplaintApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void SubmitAssignComplaintApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("complaint_id", receivedID);
            requestBody.put("service_provider_id", assignToID);
            requestBody.put("message", message.getText().toString());
            requestBody.put("device_token", "");
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());

            Call<ComplaintAndSuggest_Model> call = apiInterface.AssignComplaint(requestBody);
            call.enqueue(new Callback<ComplaintAndSuggest_Model>() {
                @Override
                public void onResponse(Call<ComplaintAndSuggest_Model> call, Response<ComplaintAndSuggest_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<ComplaintAndSuggest_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
