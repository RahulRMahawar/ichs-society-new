package com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Screens.AddSecurityGuard;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter.Add_Documents_Adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.EditFamilyMember_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FamilyMember_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Profile_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_GetSet;
import com.fire.chat.calling.Screen_UI.User.Adapter.Documents_adapter;
import com.fire.chat.calling.Screen_UI.User.Adapter.FamilyMember_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.Gallery_model;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fire.chat.calling.sessionData.CommonMethod.isValidTelephone;

public class User_Profile extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title;
    Context context;
    private RecyclerView recyclerView, documentrecycler;
    private FamilyMember_adapter familyAdapter;
    private Documents_adapter documentsAdapter;
    FloatingActionButton floating_AddDocuments, floating_AddFamily, floating_Add;

    TextView mobileNo, array;
    private RecyclerView.LayoutManager layoutManager;
    List<SocietyContact_GetSet> contactModelList;
    EditText name, flatNo, type, vehicleNo, slotNo;
    Spinner propertySpinner;
    boolean isFABExpend = false;
    boolean isFABOpen;
    ConnectionDetector cd;
    AlertDialog dialog;
    ImageButton editProfile;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    ImageViewZoom image;
    Intent myFiles;
    ImageButton play;
    String ReceivedFileName;
    ImageView documentImage1, documentImage2, documentImage3, documentImage4, documentImage5, documentImage6;
    Handler handler;
    int place = 0;
    Bitmap bitmap;
    ArrayList<String> nameList;
    AlertDialog alertDialog;
    Button done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_activity);
        context = User_Profile.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(this);
        userDetail = sessionManager.getLoginSavedDetails();
        nameList = new ArrayList<String>();
        setToolbar();
        initializeViews();
        initializeRecycler();

        if (cd.isConnectingToInternet()) {
            setData();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        click();
    }


    private void initializeViews() {
        floating_AddDocuments = findViewById(R.id.floating_AddDocuments);
        floating_Add = findViewById(R.id.floating_Add);
        floating_AddFamily = findViewById(R.id.floating_AddFamily);
        mobileNo = findViewById(R.id.mobileNo);
        play = findViewById(R.id.play);
        name = findViewById(R.id.name);
        flatNo = findViewById(R.id.flatNo);
        type = findViewById(R.id.type);
        vehicleNo = findViewById(R.id.vehicleNo);
        slotNo = findViewById(R.id.slotNo);
        editProfile = findViewById(R.id.editProfile);
        propertySpinner = findViewById(R.id.propertySpinner);
        image = findViewById(R.id.image);
        array = findViewById(R.id.array);
        image.setEnabled(false);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.propertyFor, R.layout.spinner_style);
        adapter.setDropDownViewResource(R.layout.spinner_style);
        propertySpinner.setAdapter(adapter);
    }

    private void initializeRecycler() {
        //////////FOR DOCUMENTS/////////////////////////////
        documentrecycler = (RecyclerView) findViewById(R.id.documentrecycler);
        documentrecycler.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 3);
        documentrecycler.setLayoutManager(layoutManager);

        //////////FOR FAMILY MEMBERS/////////////////////////////
        recyclerView = (RecyclerView) findViewById(R.id.familyrecycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setData() {

        getProfileAPI();
        getFamilyMemberAPI();
        setDataForDocuments();
        //setDataForFamilyMember();

    }


    public void getProfileAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<Profile_Model> call = apiInterface.getProfile(requestBody);
            call.enqueue(new Callback<Profile_Model>() {
                @Override
                public void onResponse(Call<Profile_Model> call, Response<Profile_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Profile_Model profileModel = response.body();
                                setProfileViews(profileModel);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Profile_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProfileViews(Profile_Model profileModel) {

        name.setText(profileModel.getData().getFullName());
        mobileNo.setText(profileModel.getData().getPhone());
        flatNo.setText(profileModel.getData().getFlat_no());
        vehicleNo.setText(profileModel.getData().getParking_no());
        Picasso.get().load(Constants.GALLARYFOLDER + profileModel.getData().getImage()).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(image);
        ReceivedFileName = profileModel.getData().getImage();
    }

    private void getFamilyMemberAPI() {
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<FamilyMember_Model> call = apiInterface.getFamilyMembers(requestBody);
            call.enqueue(new Callback<FamilyMember_Model>() {
                @Override
                public void onResponse(Call<FamilyMember_Model> call, Response<FamilyMember_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FamilyMember_Model familyMemberModel = response.body();

                                familyAdapter = new FamilyMember_adapter(context, familyMemberModel);
                                recyclerView.setAdapter(familyAdapter);
                                familyAdapter.notifyDataSetChanged();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FamilyMember_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataForDocuments() {

        contactModelList = new ArrayList<>();

        //Adding Data into ArrayList
        contactModelList.add(new SocietyContact_GetSet("", "", ""));
        contactModelList.add(new SocietyContact_GetSet("", "", ""));
        contactModelList.add(new SocietyContact_GetSet("", "", ""));

        documentsAdapter = new Documents_adapter(this, contactModelList);

        documentrecycler.setAdapter(documentsAdapter);
    }

    private void click() {
        floating_AddDocuments.setOnClickListener(this);
        floating_AddFamily.setOnClickListener(this);
        floating_Add.setOnClickListener(this);
        mobileNo.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        image.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.floating_Add:

                if (isFABExpend) {
                    isFABExpend = false;
                    collapseFABMenu();
                } else {
                    isFABExpend = true;
                    expendFABMenu();
                }

                break;
            case R.id.floating_AddDocuments:

                showAddDocumentPopUp(view);

                break;

            case R.id.floating_AddFamily:

                showAddOrEditFamilyPopUp("", "", "", "", "", "addType");

                break;
            case R.id.MobileNo:

                showMobileNoPopUp(view);

                break;
            case R.id.image:

                requestAppPermissions();
                selectFile();

                break;
            case R.id.editProfile:

                if (editProfile.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_edit).getConstantState()) {
                    editProfile.setImageDrawable(getResources().getDrawable(R.drawable.ic_done));
                    setViewsForEdit(true);

                } else {
                    validate();
                }

                break;

        }
    }

    private void setViewsForEdit(boolean trueOrFalse) {
        image.setEnabled(trueOrFalse);
        name.setEnabled(trueOrFalse);
        flatNo.setEnabled(trueOrFalse);
        type.setEnabled(trueOrFalse);
        vehicleNo.setEnabled(trueOrFalse);
        slotNo.setEnabled(trueOrFalse);
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                name.setError(getString(R.string.validate_userName));
                name.requestFocus();
            } else if (TextUtils.isEmpty(flatNo.getText().toString().trim()) || flatNo.length() <= 0) {
                flatNo.setError(getString(R.string.validate_userFlatNo));
                flatNo.requestFocus();
            } else if (TextUtils.isEmpty(type.getText().toString().trim()) || type.length() <= 0) {
                type.setError(getString(R.string.validate_UserType));
                type.requestFocus();
            }
           /* else if (TextUtils.isEmpty(vehicleNo.getText().toString().trim()) || vehicleNo.length() <= 0) {
                vehicleNo.setError(getString(R.string.validate_VehicleNo));
                vehicleNo.requestFocus();
            }
            else if (TextUtils.isEmpty(slotNo.getText().toString().trim()) || slotNo.length() <= 0) {
                slotNo.setError(getString(R.string.validate_Slot));
                slotNo.requestFocus();
            }*/
            else {
                SubmitEditProfileApi(name.getText().toString(), flatNo.getText().toString(),
                        type.getText().toString(), vehicleNo.getText().toString(), slotNo.getText().toString());
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitEditProfileApi(String name, String flatNo, String type, String vehicleNo, String slotNo) {

        Call<Verification_Model> call = null;
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("fullName", name);
            requestBody.put("flat_no", flatNo);
            requestBody.put("phone", mobileNo.getText().toString());
            requestBody.put("image", ReceivedFileName);
            requestBody.put("device_token", "");
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            call = apiInterface.editProfile(requestBody);

            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            editProfile.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit));
                            setViewsForEdit(false);
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMobileNoPopUp(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.edit_mobile_no_popup, null);
       /* TextView complaint = (TextView) customView.findViewById(R.id.complaint);
        TextView reply = (TextView) customView.findViewById(R.id.reply);*/
        builder.setView(customView);
        builder.create();
        builder.show();
    }

    public void showAddOrEditFamilyPopUp(String name, final String flatNo, String mobileNo, String relation, final String id, final String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.add_familymember_popup, null);
        final EditText Name = (EditText) customView.findViewById(R.id.name);
        final EditText FlatNo = (EditText) customView.findViewById(R.id.flatNo);
        final EditText MobileNo = (EditText) customView.findViewById(R.id.mobileNo);
        final EditText Relation = (EditText) customView.findViewById(R.id.relation);
        Button Submit = (Button) customView.findViewById(R.id.done);

        Name.setText(name);
        FlatNo.setText(flatNo);
        MobileNo.setText(mobileNo);
        Relation.setText(relation);
        builder.setView(customView);
        dialog = builder.create();

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {

                    if (TextUtils.isEmpty(Name.getText().toString().trim()) || Name.length() <= 0) {
                        Name.setError(getString(R.string.validate_userName));
                        Name.requestFocus();
                    } else if (!isValidTelephone(MobileNo.getText().toString())) {
                        MobileNo.setError(getResources().getString(R.string.validate_userMobile));
                        MobileNo.requestFocus();
                    } else if (TextUtils.isEmpty(FlatNo.getText().toString().trim()) || FlatNo.length() <= 0) {
                        FlatNo.setError(getString(R.string.validate_userFlatNo));
                        FlatNo.requestFocus();
                    } else if (TextUtils.isEmpty(Relation.getText().toString().trim()) || Relation.length() <= 0) {
                        Relation.setError(getString(R.string.relation));
                        Relation.requestFocus();
                    } else {
                        AddOrEditFamilyMembersAPI(Name.getText().toString(), FlatNo.getText().toString(),
                                MobileNo.getText().toString(), Relation.getText().toString(), id, type);

                    }

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();

    }

    private void AddOrEditFamilyMembersAPI(String name, String flatNo, String mobileNo, String relation, String id, String type) {

        Call<EditFamilyMember_Model> call = null;
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("phone", mobileNo);
            requestBody.put("fullName", name);
            requestBody.put("flat_no", flatNo);
            requestBody.put("relation", relation);
            requestBody.put("device_token", "");

            if (type.equalsIgnoreCase("editType")) {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
                requestBody.put("user_id", id);
                requestBody.put("role_id", ((GlobalVariables) context.getApplicationContext()).getRole_id());
                call = apiInterface.editFamilyMembers(requestBody);
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
                requestBody.put("role_id", ((GlobalVariables) context.getApplicationContext()).getRole_id());
                call = apiInterface.addFamilyMembers(requestBody);
            }
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            call.enqueue(new Callback<EditFamilyMember_Model>() {
                @Override
                public void onResponse(Call<EditFamilyMember_Model> call, Response<EditFamilyMember_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    progressDialog.cancel();


                }

                @Override
                public void onFailure(Call<EditFamilyMember_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    progressDialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAddDocumentPopUp(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View customView = layoutInflater.inflate(R.layout.add_documents_popupnew, null);
        documentImage1 = customView.findViewById(R.id.documentImage1);
        documentImage2 = customView.findViewById(R.id.documentImage2);
        documentImage3 = customView.findViewById(R.id.documentImage3);
        documentImage4 = customView.findViewById(R.id.documentImage4);
        documentImage5 = customView.findViewById(R.id.documentImage5);
        documentImage6 = customView.findViewById(R.id.documentImage6);
        done = customView.findViewById(R.id.done);
        init();
        builder.setView(customView);
        alertDialog = builder.create();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < nameList.size(); i++) {
                    stringBuilder.append(nameList.get(i));
                    stringBuilder.append(",");
                }
                String categorylist = stringBuilder.substring(0, stringBuilder.length() - 1);
                //showToast("[" + category + "]");
                Log.e("documents", "[ " + categorylist + "]");

                uploadDocumentAPI(categorylist);
               /* for (int i = 0; i < nameList.size(); i++) {
                    array.setText(array.getText() + nameList.get(i) + " , ");
                }
                String text = array.getText().toString();
                String sub = text.substring(0, text.length() - 1);
                Log.e("documents", sub);*/
            }
        });


        alertDialog.show();

    }

    private void uploadDocumentAPI(String categorylist) {

        Call<Verification_Model> call = null;
        final ProgressDialog progressDialog = ProgressDialog.show(context, "Uploading Files", "Uploading Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();

           /* if (LoginType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }*/
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("documents", "[ " + categorylist + "]");
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("device_token", "");

            call = apiInterface.UploadDocument(requestBody);

            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            progressDialog.cancel();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    progressDialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    progressDialog.cancel();
                    Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {

        handler = new Handler();

        documentImage1.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            2000);
                } else {
                    place = 0;
                    selectImage(context, place);
                }


            }
        });

        documentImage2.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (documentImage1.getDrawable().getConstantState().equals(documentImage1.getContext()
                        .getDrawable(R.drawable.add_file).getConstantState())) {

                    //your code for stopping uploading here!
                    Toast.makeText(context, "Select first image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                2000);
                    } else {
                        place = 1;
                        selectImage(context, place);
                    }

                }


            }
        });

        documentImage3.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {


                if (documentImage2.getDrawable().getConstantState().equals(documentImage2.getContext()
                        .getDrawable(R.drawable.add_file).getConstantState())) {

                    //your code for stopping uploading here!
                    //Toast.makeText(context, "Select first image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                2000);
                    } else {
                        place = 2;
                        selectImage(context, place);
                    }

                }


            }
        });

        documentImage4.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (documentImage3.getDrawable().getConstantState().equals(documentImage3.getContext()
                        .getDrawable(R.drawable.add_file).getConstantState())) {

                    //your code for stopping uploading here!
                    //Toast.makeText(context, "Select first image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                2000);
                    } else {
                        place = 3;
                        selectImage(context, place);
                    }

                }


            }
        });

        documentImage5.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {


                if (documentImage4.getDrawable().getConstantState().equals(documentImage4.getContext()
                        .getDrawable(R.drawable.add_file).getConstantState())) {

                    //your code for stopping uploading here!
                    //Toast.makeText(context, "Select first image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                2000);
                    } else {
                        place = 4;
                        selectImage(context, place);
                    }

                }


            }
        });
        documentImage5.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {


                if (documentImage5.getDrawable().getConstantState().equals(documentImage5.getContext()
                        .getDrawable(R.drawable.add_file).getConstantState())) {

                    //your code for stopping uploading here!
                    //Toast.makeText(context, "Select first image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                2000);
                    } else {
                        place = 5;
                        selectImage(context, place);
                    }

                }


            }
        });

    }

    private void selectImage(Context context, final int place) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {

                    Intent intent = new Intent();
                    intent.setType("*/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, 1);
                  /*  Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);*/

                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void expendFABMenu() {
        floating_AddFamily.animate().translationY(-
                getResources().getDimension(R.dimen.standard_55));
        floating_AddDocuments.animate().translationY(-
                getResources().getDimension(R.dimen.standard_105));
    }

    private void collapseFABMenu() {
        isFABOpen = false;
        floating_AddFamily.animate().translationY(0);
        floating_AddDocuments.animate().translationY(0);
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(User_Profile.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(image);
                    //Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
            case 0:
                if (resultCode == RESULT_OK && data != null) {
                    Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                    if (place == 0) {
                        documentImage1.setImageBitmap(selectedImage);
                    } else if (place == 1) {
                        documentImage2.setImageBitmap(selectedImage);
                    } else if (place == 2) {
                        documentImage3.setImageBitmap(selectedImage);
                    } else if (place == 3) {
                        documentImage4.setImageBitmap(selectedImage);
                    } else if (place == 4) {
                        documentImage5.setImageBitmap(selectedImage);
                    } else if (place == 5) {
                        documentImage6.setImageBitmap(selectedImage);
                    }

                   /* encodedImage = convertToString(selectedImage);
                    savetoServer(encodedImage);*/
                }

                break;
            case 1:
                if (resultCode == RESULT_OK && data != null) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                        if (place == 0) {
                            Picasso.get().load(path).fit()
                                    .placeholder(R.drawable.ic_add)
                                    .error(R.drawable.ic_splash_icon)
                                    .into(documentImage1);
                            Toast.makeText(context, link, Toast.LENGTH_SHORT).show();
                        } else if (place == 1) {
                            Picasso.get().load(path).fit()
                                    .placeholder(R.drawable.ic_add)
                                    .error(R.drawable.ic_splash_icon)
                                    .into(documentImage2);
                            Toast.makeText(context, link, Toast.LENGTH_SHORT).show();
                        } else if (place == 2) {
                            Picasso.get().load(path).fit()
                                    .placeholder(R.drawable.ic_add)
                                    .error(R.drawable.ic_splash_icon)
                                    .into(documentImage3);
                            Toast.makeText(context, link, Toast.LENGTH_SHORT).show();
                        } else if (place == 3) {
                            Picasso.get().load(path).fit()
                                    .placeholder(R.drawable.ic_add)
                                    .error(R.drawable.ic_splash_icon)
                                    .into(documentImage4);
                        } else if (place == 4) {
                            Picasso.get().load(path).fit()
                                    .placeholder(R.drawable.ic_add)
                                    .error(R.drawable.ic_splash_icon)
                                    .into(documentImage5);
                        } else if (place == 5) {
                            Picasso.get().load(path).fit()
                                    .placeholder(R.drawable.ic_add)
                                    .error(R.drawable.ic_splash_icon)
                                    .into(documentImage6);
                        }
                        uploadFiles(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                break;
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                try {
                                    nameList.get(place);
                                    int itemIndex = nameList.indexOf(place);
                                    nameList.set(place, ReceivedFileName);
                                } catch (IndexOutOfBoundsException e) {

                                    if (nameList.isEmpty() || place >= nameList.size()) {
                                        // Adding new item to list.
                                        nameList.add(place, ReceivedFileName);
                                    } else {
                                        nameList.set(place, ReceivedFileName);
                                    }
                                }
                                Log.e("ARAAYYY", String.valueOf(nameList));
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void DeleteFamilyMemberAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<MarketPlaceList_Model> call = apiInterface.deleteUser(requestBody);
            call.enqueue(new Callback<MarketPlaceList_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceList_Model> call, Response<MarketPlaceList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceList_Model marketPlaceListModel = response.body();
                                familyAdapter.removeAt(position);
                                Toast.makeText(context, marketPlaceListModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
