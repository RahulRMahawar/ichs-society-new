package com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FlatList_Model;
import com.fire.chat.calling.sessionData.SessionManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

public class Society_Flat_List_adapter extends RecyclerView.Adapter<Society_Flat_List_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String type;
    FlatList_Model model;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    public Society_Flat_List_adapter(Context context, FlatList_Model model) {
        this.context = context;
        this.model = model;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_society_list_adapter, parent, false);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(model.getData().get(position));
        holder.name.setText("Flat NO. " + model.getData().get(position).getFlat_no());

    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {


                        context.startActivity(new Intent(context, DashboardActivity.class));
                        ((Activity) context).finishAffinity();


                }
            });

        }
    }

}