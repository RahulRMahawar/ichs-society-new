package com.fire.chat.calling.Screen_UI.User.fragments.BroadcastMessage;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Model.SecurityGuardList_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.BroadCastMessage_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Add_Complain;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_BroadcastMessage extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    ConnectionDetector cd;
    Context context;
    TextView title;
    String LoginType;
    LinearLayout LayoutForContacts;
    Spinner contactSpinner;
    TextView selectedContact;
    Button done;
    EditText description;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    ImageView mediaFile;
    Intent myFiles;
    String ReceivedFileName;
    String fileExt;
    SecurityGuardList_model responseBody;
    String userName, userID;
    ArrayList<String> UserNames = new ArrayList<String>();
    ArrayList<String> UserID = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_broadcast_message_activity);
        context = Add_BroadcastMessage.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            LoginType = bundle.getString("LoginType");

        }
        setToolbar();
        initializeViews();
        loadContactsSpinner();
        handleListeners();
        LayoutForContacts = findViewById(R.id.LayoutForContacts);
        if (LoginType.equalsIgnoreCase("1")) {
            LayoutForContacts.setVisibility(View.GONE);
        }
    }


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.addBroadcastMessage));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeViews() {
        contactSpinner = (Spinner) findViewById(R.id.contactSpinner);
        selectedContact = (TextView) findViewById(R.id.selectedContact);
        description = (EditText) findViewById(R.id.description);
        done = (Button) findViewById(R.id.done);
        mediaFile=findViewById(R.id.mediaFile);


    }

    private void loadContactsSpinner() {
        contactListAPI();
    }

    private void contactListAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("role_id", "2");
            Call<SecurityGuardList_model> call = apiInterface.getAllUserList(requestBody);
            call.enqueue(new Callback<SecurityGuardList_model>() {
                @Override
                public void onResponse(Call<SecurityGuardList_model> call, Response<SecurityGuardList_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                UserNames.add("Select Contact");
                                UserID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    UserNames.add(response.body().getData().get(i).getFullName());
                                    UserID.add(response.body().getData().get(i).getId());
                                }
                                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>
                                        (context, R.layout.center_spinner_style, UserNames);
                                contactSpinner.setAdapter(categoryAdapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SecurityGuardList_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void handleListeners() {

        done.setOnClickListener(this);
        mediaFile.setOnClickListener(this);
        contactSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (responseBody == null) {

                } else {
                    userName = parent.getSelectedItem().toString();
                    userID = UserID.get(parent.getSelectedItemPosition());
                }
                selectedContact.setText(contactSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.done:
                validate();
                break;
            case R.id.mediaFile:
                requestAppPermissions();
                selectFile();
                break;
        }


    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(description.getText().toString().trim()) || description.length() <= 0) {
                description.setError(getString(R.string.validate_Discription));
                description.requestFocus();
            } else if (contactSpinner.getSelectedItem().toString().equalsIgnoreCase("Select Contact")) {
                Toast.makeText(context, "Please select contact", Toast.LENGTH_SHORT).show();
            }
            else if (userName.equalsIgnoreCase("Select contact")) {
                Toast.makeText(context, "Please select contact.", Toast.LENGTH_SHORT).show();
            }else {
                SubmitBroadCastApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitBroadCastApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("role_id", userDetail.get(SessionManager.LOGIN_TYPE));
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("message", description.getText().toString());
            requestBody.put("database_name",((GlobalVariables) getApplicationContext()).getDatabase_name());
            requestBody.put("image", ReceivedFileName);
            requestBody.put("users", userID);
            requestBody.put("device_token", "");


            Call<BroadCastMessage_Model> call = apiInterface.sendBroadCastMessage(requestBody);
            call.enqueue(new Callback<BroadCastMessage_Model>() {
                @Override
                public void onResponse(Call<BroadCastMessage_Model> call, Response<BroadCastMessage_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            description.setText("");
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<BroadCastMessage_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(Add_BroadcastMessage.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("*/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    if (fileExt.equalsIgnoreCase("mp4")) {
                        Picasso.get().load(R.drawable.videoformat)
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(mediaFile);
                        /*play.setVisibility(View.VISIBLE);
                        play.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(context, link, Toast.LENGTH_SHORT).show();
                            }
                        });*/
                    } else if (fileExt.equalsIgnoreCase("jpg")) {
                        Picasso.get().load(path).fit()
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(mediaFile);

                    } else if (fileExt.equalsIgnoreCase("aac")) {
                        Picasso.get().load(R.drawable.audioformat)
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(mediaFile);
                    }
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) context.getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
