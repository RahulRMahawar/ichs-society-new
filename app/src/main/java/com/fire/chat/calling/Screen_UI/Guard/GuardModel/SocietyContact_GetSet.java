package com.fire.chat.calling.Screen_UI.Guard.GuardModel;

public class SocietyContact_GetSet {

    private String Name;
    private String FlatNo;
    private String MobileNo;

    public SocietyContact_GetSet(String Name, String FlatNo, String MobileNo) {
        this.Name = Name;
        this.FlatNo = FlatNo;
        this.MobileNo = MobileNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFlatNo() {
        return FlatNo;
    }

    public void setFlatNo(String flatNo) {
        FlatNo = flatNo;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}