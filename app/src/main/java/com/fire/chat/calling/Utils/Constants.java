package com.fire.chat.calling.Utils;



public class Constants {

    public static final String GALLARY = "Gallery";
    public static final String GALLARYFOLDER = "http://3.7.63.119/ichs/uploads/";
    public static class JsonParams {

        public static final String RESULT = "result";
    }

    public class PrefKeys {
        public static final String TOKEN = "token";
    }

    public class Arg {

        public static final String TYPE = "arg_type";
        public static final String JSON_RES = "arg_json_res";
        public static final String ID = "arg_id";
    }

    public class RequestType {
        public static final int MY_ADS = 1;
        public static final int SENT_REQUESTS = 2;
        public static final int RECEIVED_REQUESTS = 3;
    }



}
