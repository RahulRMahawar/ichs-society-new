package com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Alarm_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.User.Adapter.Gallery_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.Gallery_model;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryFolder_Screen extends AppCompatActivity implements View.OnClickListener {
    Context context;
    private Toolbar toolbar;
    TextView title;
    RecyclerView RecycleView;
    private View view;
    ConnectionDetector cd;
    private RecyclerView.LayoutManager layoutManager;
    private Gallery_adapter Adapter;
    String LoginType;
    FloatingActionButton floating_AddGalleryFolder;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    LinearLayout emptyDataLayout;
    AlertDialog alertDialog;
    Intent myFiles;
    ImageView imageView;
    String ReceivedFileName;
    String ischecked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_screen_activity);
        context = GalleryFolder_Screen.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            LoginType = bundle.getString("LoginType");
          /*  if (USERTYPE.equalsIgnoreCase("ADMIN")) {
                SetViewsForADMIN();
            } else {
                SetViewsForUSER();
            }*/
        }

        setToolbar();
        initializeViews();
        initializeRecycler();
        handleListeners();
        getGallery();
        if (LoginType.equalsIgnoreCase("2")) {
            floating_AddGalleryFolder.hide();
        }
    }


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.gallery));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeViews() {
        RecycleView = findViewById(R.id.contentRecycleview);
        floating_AddGalleryFolder = findViewById(R.id.floating_AddGalleryFolder);
        emptyDataLayout = (LinearLayout) findViewById(R.id.emptyDataLayout);
    }

    private void initializeRecycler() {
        RecycleView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(context, 3);
        RecycleView.setLayoutManager(layoutManager);
    }

    private void getGallery() {

        if (cd.isConnectingToInternet()) {
            getGalleryAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getGalleryAPI() {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("keyword", "");
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());
            Call<Gallery_model> call = apiInterface.getGalleryFolders(requestBody);
            call.enqueue(new Callback<Gallery_model>() {
                @Override
                public void onResponse(Call<Gallery_model> call, Response<Gallery_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Gallery_model galleryModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    RecycleView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new Gallery_adapter(context, galleryModel, LoginType);
                                    RecycleView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();

                                } else {
                                    RecycleView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Gallery_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void handleListeners() {
        floating_AddGalleryFolder.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.floating_AddGalleryFolder:

                showPopUp("", "", "", "0", "add");

                break;


        }
    }

    public void showPopUp(final String id, final String image, final String folderName, String selected, final String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.add_galleryfolder_popup, null);
        Button done = (Button) customView.findViewById(R.id.done);
        final CheckBox showAdmin = (CheckBox) customView.findViewById(R.id.showAdmin);
        final EditText albumName = (EditText) customView.findViewById(R.id.albumName);
        imageView = (ImageView) customView.findViewById(R.id.imageView);
        albumName.setText(folderName);
        Picasso.get().load(Constants.GALLARYFOLDER + image)
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(imageView);
        requestAppPermissions();

        if (type.equalsIgnoreCase("edit")) {
            ReceivedFileName = image;
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFile();
            }
        });
        if (selected.equalsIgnoreCase("1")) {
            showAdmin.setChecked(true);
        } else {
            showAdmin.setChecked(false);
        }
        showAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    // Do your coding
                    ischecked = "1";
                }
                else{
                    // Do your coding
                    ischecked= "0";
                }
            }
        });
        builder.setView(customView);
        alertDialog = builder.create();
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet()) {

                    if (TextUtils.isEmpty(albumName.getText().toString().trim()) || albumName.length() <= 0) {
                        albumName.setError(getString(R.string.hint_entername));
                        albumName.requestFocus();
                    } else {
                        AddOREditFolder(id, image, albumName.getText().toString(), ischecked, type);
                    }

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        alertDialog.show();
    }

    private void selectFile() {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("*/*");
        startActivityForResult(myFiles, 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    if (fileExt.equalsIgnoreCase("mp4")) {
                        Picasso.get().load(R.drawable.videoformat)
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(imageView);
                        /*play.setVisibility(View.VISIBLE);
                        play.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(context, link, Toast.LENGTH_SHORT).show();
                            }
                        });*/
                    } else if (fileExt.equalsIgnoreCase("jpg")) {
                        Picasso.get().load(path).fit()
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(imageView);

                    } else if (fileExt.equalsIgnoreCase("aac")) {
                        Picasso.get().load(R.drawable.audioformat)
                                .placeholder(R.drawable.ic_splash_icon)
                                .error(R.drawable.ic_splash_icon)
                                .into(imageView);
                    }
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(GalleryFolder_Screen.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void AddOREditFolder(String id, String image, String folderName, String selected, String type) {

        Call<Gallery_model> call = null;
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
/*
            if (LoginType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }*/
            if (type.equalsIgnoreCase("edit")) {
                requestBody.put("id", id);
            }
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("title", folderName);
            requestBody.put("image", ReceivedFileName);
            requestBody.put("only_me_visible", selected);
            requestBody.put("device_token", "");
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            call = apiInterface.addOrEditGalleryFolder(requestBody);

            call.enqueue(new Callback<Gallery_model>() {
                @Override
                public void onResponse(Call<Gallery_model> call, Response<Gallery_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    progressDialog.cancel();


                }

                @Override
                public void onFailure(Call<Gallery_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    progressDialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DeleteFolderAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<Gallery_model> call = apiInterface.deleteFolder(requestBody);
            call.enqueue(new Callback<Gallery_model>() {
                @Override
                public void onResponse(Call<Gallery_model> call, Response<Gallery_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Gallery_model galleryModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, galleryModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Gallery_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }
}
