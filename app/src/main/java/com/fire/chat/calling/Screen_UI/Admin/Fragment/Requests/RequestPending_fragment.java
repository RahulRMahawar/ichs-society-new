package com.fire.chat.calling.Screen_UI.Admin.Fragment.Requests;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.VehicleManagementRequests_adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Users_Request_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.User.Adapter.Complaint_Suggestion_adapter;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestPending_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView RequestsRecycler;
    private VehicleManagementRequests_adapter requestsAdapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    LinearLayout searchLayout;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    EditText searchVisitor;
    Boolean isSearching = false;
    ProgressDialog dialog;
    LinearLayout emptyDataLayout;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.request_pending_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.requests));
        initializeViews();
        initializeRecycler();
        handleListeners();
        if (cd.isConnectingToInternet()) {
            getRequestPendingListAPI("");
            Searching();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        LinearLayout myLayout = (LinearLayout) view.findViewById(R.id.my_layout);
        myLayout.requestFocus();
        return view;
    }


    private void initializeViews() {
        searchLayout = view.findViewById(R.id.searchLayout);
        RequestsRecycler = (RecyclerView) view.findViewById(R.id.Requestsrecycler);
        searchVisitor = (EditText) view.findViewById(R.id.searchContact);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }
    private void initializeRecycler() {
        RequestsRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        RequestsRecycler.setLayoutManager(layoutManager);
    }
    private void Searching() {
        searchVisitor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isSearching = true;
                getRequestPendingListAPI(editable.toString());
            }
        });

        searchVisitor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
    }

    public void getRequestPendingListAPI(String searchKey) {
        if (!isSearching) {
            dialog = ProgressDialog.show(context, "", "Please wait...", false);
        }


        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("type", "2");
            //requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("verified", "0");
            requestBody.put("keyword",searchKey);
            requestBody.put("database_name",((GlobalVariables)getActivity().getApplication()).getDatabase_name());
            Call<Users_Request_Model> call = apiInterface.getPendingUsersRequests(requestBody);
            call.enqueue(new Callback<Users_Request_Model>() {
                @Override
                public void onResponse(Call<Users_Request_Model> call, Response<Users_Request_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Users_Request_Model pendingRequestModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    RequestsRecycler.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    requestsAdapter = new VehicleManagementRequests_adapter(context, pendingRequestModel,RequestPending_fragment.this, "Pending");
                                    RequestsRecycler.setAdapter(requestsAdapter);
                                    requestsAdapter.notifyDataSetChanged();
                                } else {
                                    RequestsRecycler.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    if (!isSearching) {
                        dialog.cancel();
                    }


                }

                @Override
                public void onFailure(Call<Users_Request_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    if (!isSearching) {
                        dialog.cancel();
                    }
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void handleListeners() {

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
           /* case R.id.fabAddVehicle:

                break;*/

        }


    }
    public void AcceptOrRejectUserAPI(String ID, String verified, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("verified", verified);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<Users_Request_Model> call = apiInterface.acceptOrDeclineUserRequest(requestBody);
            call.enqueue(new Callback<Users_Request_Model>() {
                @Override
                public void onResponse(Call<Users_Request_Model> call, Response<Users_Request_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Users_Request_Model pendingRequestModel = response.body();
                                requestsAdapter.removeAt(position);
                                Toast.makeText(context, pendingRequestModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Users_Request_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

