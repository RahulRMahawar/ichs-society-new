package com.fire.chat.calling.Screen_UI.User.fragments.Amenities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.User.Model.AmenitiesDetails_Model;
import com.fire.chat.calling.Screen_UI.User.Model.Amenities_model;
import com.fire.chat.calling.Screen_UI.Utlity.CustomDateTimePicker;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Amenities_BookingFor_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name, rate, availableQuantity;
    ConnectionDetector cd;
    ImageView imageView;
    Button bookNow;
    String ID;
    EditText startDate, endDate, adminNote;
    Spinner selectSubscriptionSpinner;
    final Calendar myCalendar = Calendar.getInstance();
    String dateType = "";
    CustomDateTimePicker custom;
    String startDateText, startTimeText, endDateText, endTimeText;
    AmenitiesDetails_Model amenitiesModel;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    List<String> subscriptionName = new ArrayList<String>();
    List<String> subscriptionValue = new ArrayList<String>();
    String selectedValue;
    String myFormat;
    SimpleDateFormat sdf;
    int NoOfDays = 0;
    String dueDate;
    LinearLayout endDateLayout;
    Boolean isAnySelected = false;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.amenities_booking_for_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.amenities));
        initializeViews();
        try {
            ID = getArguments().getString("ID");
        } catch (NullPointerException e) {
            ID = "";
        }
        if (cd.isConnectingToInternet()) {
            getAmenitiesDetailAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        handleListeners();
        //InitializeCustomDateAndTimePicker();
        return view;
    }


    private void initializeViews() {

        startDate = view.findViewById(R.id.startDate);
        adminNote = view.findViewById(R.id.adminNote);
        bookNow = view.findViewById(R.id.done);
        selectSubscriptionSpinner = view.findViewById(R.id.selectSubscriptionSpinner);
        rate = view.findViewById(R.id.rate);
        availableQuantity = view.findViewById(R.id.availableQuantity);
        endDateLayout = view.findViewById(R.id.endDateLayout);
        endDate = view.findViewById(R.id.endDate);

    }

    public void getAmenitiesDetailAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("amenity_id", ID);
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            Call<AmenitiesDetails_Model> call = apiInterface.getAmenityDetails(requestBody);
            call.enqueue(new Callback<AmenitiesDetails_Model>() {
                @Override
                public void onResponse(Call<AmenitiesDetails_Model> call, Response<AmenitiesDetails_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                amenitiesModel = response.body();
                                setDataToViews();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<AmenitiesDetails_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataToViews() {
        availableQuantity.setText(amenitiesModel.data.getQty());

        if (!amenitiesModel.data.subscription_plans.isEmpty()) {
            if (!amenitiesModel.data.subscription_plans.get(0).getMonthly().equals("0")) {
                subscriptionName.add("Monthly");
                subscriptionValue.add(amenitiesModel.data.subscription_plans.get(0).getMonthly());
            }
            if (!amenitiesModel.data.subscription_plans.get(0).getYearly().equals("0")) {
                subscriptionName.add("Yearly");
                subscriptionValue.add(amenitiesModel.data.subscription_plans.get(0).getYearly());
            }
            if (!amenitiesModel.data.subscription_plans.get(0).getHalf_yearly().equals("0")) {
                subscriptionName.add("Half yearly");
                subscriptionValue.add(amenitiesModel.data.subscription_plans.get(0).getHalf_yearly());
            }
            if (!amenitiesModel.data.subscription_plans.get(0).getQuarterly().equals("0")) {
                subscriptionName.add("Quarterly");
                subscriptionValue.add(amenitiesModel.data.subscription_plans.get(0).getQuarterly());
            }
            if (!amenitiesModel.data.subscription_plans.get(0).getAny().equals("0")) {
                subscriptionName.add("Any");
                subscriptionValue.add(amenitiesModel.data.subscription_plans.get(0).getAny());
            }
        } else {
            subscriptionName.add("No subscription available");
            subscriptionValue.add("0");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, subscriptionName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectSubscriptionSpinner.setAdapter(adapter);
    }

    private void handleListeners() {
        bookNow.setOnClickListener(this);
        startDate.setOnClickListener(this);
        selectSubscriptionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedName = parent.getSelectedItem().toString();

                if (selectedName.equalsIgnoreCase("Monthly")) {
                    NoOfDays = 30;
                    endDateLayout.setVisibility(View.GONE);
                    isAnySelected = false;
                    selectedValue = subscriptionValue.get(position);
                    rate.setText(selectedValue);
                } else if (selectedName.equalsIgnoreCase("Yearly")) {
                    NoOfDays = 365;
                    endDateLayout.setVisibility(View.GONE);
                    isAnySelected = false;
                    selectedValue = subscriptionValue.get(position);
                    rate.setText(selectedValue);
                } else if (selectedName.equalsIgnoreCase("Half yearly")) {
                    NoOfDays = 180;
                    endDateLayout.setVisibility(View.GONE);
                    isAnySelected = false;
                    selectedValue = subscriptionValue.get(position);
                    rate.setText(selectedValue);
                } else if (selectedName.equalsIgnoreCase("Quarterly")) {
                    NoOfDays = 90;
                    endDateLayout.setVisibility(View.GONE);
                    isAnySelected = false;
                    selectedValue = subscriptionValue.get(position);
                    rate.setText(selectedValue);
                } else if (selectedName.equalsIgnoreCase("Any")) {
                    endDateLayout.setVisibility(View.VISIBLE);
                    isAnySelected = true;
                    selectedValue = subscriptionValue.get(position);
                    rate.setText(selectedValue);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void InitializeCustomDateAndTimePicker() {
        custom = new CustomDateTimePicker(getActivity(),
                new CustomDateTimePicker.ICustomDateTimeListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int day,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {
                        //                        ((TextInputEditText) findViewById(R.id.edtEventDateTime))
                        if (dateType.endsWith("startDate")) {
                            startDate.setText("");
                            startDate.setText("Date : " + calendarSelected.get(Calendar.DAY_OF_MONTH) + "-" + (monthNumber + 1) + "-" + year + "   "
                                            + "Time : " + hour24 + ":" + min + " " + AM_PM
                                    /*+ ":" + sec*/);
                            startDateText = year + "-" + (monthNumber + 1) + "-" + calendarSelected.get(Calendar.DAY_OF_MONTH);
                            startTimeText = +hour24 + ":" + min + ":" + sec;

                        }

                    }

                    @Override
                    public void onCancel() {

                    }
                });
        custom.set24HourFormat(true);
        custom.setDate(Calendar.getInstance());
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(dateType);
        }

    };

    private void updateLabel(String dateType) {
        myFormat = "yy-MM-dd"; //In which you need put here
        sdf = new SimpleDateFormat(myFormat, Locale.US);

        if (dateType.endsWith("startDate")) {
            startDate.setText(sdf.format(myCalendar.getTime()));

            myCalendar.add(Calendar.DAY_OF_YEAR, NoOfDays);
            Date resultDate = myCalendar.getTime();
            dueDate = sdf.format(resultDate);
            //Toast.makeText(context, dueDate, Toast.LENGTH_SHORT).show();
        } else if (dateType.endsWith("endDate")) {
            endDate.setText(sdf.format(myCalendar.getTime()));
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.done:
                validate();
                return;
            case R.id.startDate:
                dateType = "startDate";
                // custom.showDialog();
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.endDate:
                dateType = "endDate";
                // custom.showDialog();
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
        }


    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(startDate.getText().toString().trim()) || startDate.length() <= 0) {
                startDate.setError(getString(R.string.validate_Date));
                startDate.requestFocus();
            } else if (selectSubscriptionSpinner.getSelectedItem().equals("Select subscription")) {
                Toast.makeText(context, "Please select subscription", Toast.LENGTH_SHORT).show();
            } else {
                SubmitBookApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitBookApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("amenity_id", ID);
            requestBody.put("amenity_subscription_plan_id", amenitiesModel.data.getId());
            requestBody.put("subscription_from", startDate.getText().toString());
            requestBody.put("amount", rate.getText().toString());
            requestBody.put("duration", String.valueOf(NoOfDays));
            requestBody.put("amenity_time_slot_id", "1");
            if (isAnySelected) {
                requestBody.put("subscription_to", endDate.getText().toString());
            } else {
                requestBody.put("subscription_to", dueDate);
            }
            Call<Amenities_model> call = apiInterface.bookAmenity(requestBody);
            call.enqueue(new Callback<Amenities_model>() {
                @Override
                public void onResponse(Call<Amenities_model> call, Response<Amenities_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Amenities_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

