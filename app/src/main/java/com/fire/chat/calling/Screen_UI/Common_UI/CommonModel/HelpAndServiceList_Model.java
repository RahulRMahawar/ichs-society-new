package com.fire.chat.calling.Screen_UI.Common_UI.CommonModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HelpAndServiceList_Model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;
    @SerializedName("data")
    public List<DataValue> data;

    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public List<DataValue> getData() {
        return data;
    }

    public class DataValue {

        String id, title, status, created, description, image, type;
        @SerializedName("users")
        public List<User> users;

        public List<User> getUsers() {
            return users;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }


        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

    public class User {

        String id, phone, fullName, image, rating;
        @SerializedName("users")
        public List<User> users;

        public List<User> getUsers() {
            return users;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }
    }
}