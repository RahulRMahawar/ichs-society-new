package com.fire.chat.calling.Screen_UI.Admin.Fragment.CrudDashboardFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.AdminDashboardContent_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.DashboardContent_model;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by Rahul Mahawar.
 */

public class CreateApproval_fragment extends Fragment implements View.OnClickListener /*SwipeRefreshLayout.OnRefreshListener*/ {

    Context context;
    private Toolbar toolbar;
    RecyclerView contentRecycleView;
    private View view;
    FloatingActionButton floating_call, floating_msg, floating_GuardsList;
    ConnectionDetector cd;
    private RecyclerView.LayoutManager layoutManager;
    List<DashboardContent_model> List;
    private AdminDashboardContent_adapter Adapter;
    String LoginType;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.create_approval_fragment, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType=userDetail.get(SessionManager.LOGIN_TYPE);
        initializeViews();
        click();
        return view;
    }


    private void initializeViews() {
        findView();
        setData();


    }

    private void setData() {
        contentRecycleView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);

        contentRecycleView.setLayoutManager(layoutManager);

        List = new ArrayList<>();

        //Adding Data into ArrayList
        List.add(new DashboardContent_model("SOCIETY REGISTER", R.drawable.ic_amenities));
        List.add(new DashboardContent_model("ADD MEMBERS", R.drawable.ic_amenities));
        List.add(new DashboardContent_model("VEHICLE MANAGEMENT", R.drawable.ic_vehicle));
        List.add(new DashboardContent_model("BROADCAST MESSAGE", R.drawable.ic_broadcast));
        List.add(new DashboardContent_model("AMENITIES", R.drawable.ic_amenities));



        Adapter = new AdminDashboardContent_adapter(context, List, LoginType);

        contentRecycleView.setAdapter(Adapter);
    }


    private void findView() {
        contentRecycleView = view.findViewById(R.id.contentRecycleview);
    }


    private void click() {


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }

    }

}
