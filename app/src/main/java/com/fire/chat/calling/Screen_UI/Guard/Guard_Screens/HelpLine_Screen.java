package com.fire.chat.calling.Screen_UI.Guard.Guard_Screens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.Emergency_help_adapter;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.HelpLine_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.PerformCall;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fire.chat.calling.sessionData.CommonMethod.isValidTelephone;

public class HelpLine_Screen extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title;
    Context context;
    private RecyclerView recyclerView;
    private Emergency_help_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    HelpLine_Model helpLineModel;
    FloatingActionButton floating_AddHelpLine;
    AlertDialog alertDialog;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_line_activity);
        context = HelpLine_Screen.this;
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        setToolbar();
        initializeViews();
        if (LoginType.equalsIgnoreCase("2")) {
            floating_AddHelpLine.hide();
        }
        click();
    }

    private void initializeViews() {
        findView();
        setRecycleView();
        cd = new ConnectionDetector(context);
        if (cd.isConnectingToInternet()) {
            getEmergencyContactAPI();
        } else {
            //Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }

    private void setRecycleView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void getEmergencyContactAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            Call<HelpLine_Model> call = apiInterface.getAllEmergencyContacts(requestBody);
            call.enqueue(new Callback<HelpLine_Model>() {
                @Override
                public void onResponse(Call<HelpLine_Model> call, Response<HelpLine_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                helpLineModel = response.body();
                                Adapter = new Emergency_help_adapter(context, helpLineModel, LoginType, new Emergency_help_adapter.EmergencyHelpCall() {
                                    @Override
                                    public void OnEmergencyHelpCall() {
                                        new PerformCall(HelpLine_Screen.this).performCall(false, "ARDhZ2MEDChjyrUFC6zuzH67JML2");

                                    }
                                });
                                recyclerView.setAdapter(Adapter);
                                Adapter.notifyDataSetChanged();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<HelpLine_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void findView() {
        floating_AddHelpLine = findViewById(R.id.floating_AddHelpLine);
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.helpline));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void click() {
        floating_AddHelpLine.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.floating_AddHelpLine:


                showPopUp("", "", "add", "");

                break;


        }
    }

    public void showPopUp(String fullName, String mobileNo, final String type, final String ID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.add_emergencyhelpline_popup, null);
        final EditText name = (EditText) customView.findViewById(R.id.name);
        final EditText number = (EditText) customView.findViewById(R.id.addNumber);
        Button done = (Button) customView.findViewById(R.id.done);
        name.setText(fullName);
        number.setText(mobileNo);
        builder.setView(customView);
        alertDialog = builder.create();
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                    name.setError(getString(R.string.hint_entername));
                    name.requestFocus();
                } else if (!isValidTelephone(number.getText().toString())) {
                    number.setError(getResources().getString(R.string.validate_userMobile));
                    number.requestFocus();
                } else
                    AddOREditEmergencyNo(name.getText().toString(), number.getText().toString(), type, ID);
            }
        });
        alertDialog.show();

    }

    public void AddOREditEmergencyNo(String name, String number, String type, String ID) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();

            if (type.equalsIgnoreCase("edit")) {
                requestBody.put("id", ID);
            }
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("full_name", name);
            requestBody.put("phone", number);
            Call<HelpLine_Model> call = apiInterface.addEmergencyContacts(requestBody);
            call.enqueue(new Callback<HelpLine_Model>() {
                @Override
                public void onResponse(Call<HelpLine_Model> call, Response<HelpLine_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                HelpLine_Model helpLineModel = response.body();
                                Toast.makeText(HelpLine_Screen.this, helpLineModel.getReplyMsg(), Toast.LENGTH_SHORT).show();

                                alertDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<HelpLine_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteEmergencyContact(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<Verification_Model> call = apiInterface.deleteEmergency(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Verification_Model body = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, body.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
