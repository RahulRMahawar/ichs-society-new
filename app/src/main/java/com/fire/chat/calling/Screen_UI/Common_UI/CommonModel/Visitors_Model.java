package com.fire.chat.calling.Screen_UI.Common_UI.CommonModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Visitors_Model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;
    @SerializedName("data")
    public List<DataValue> data;
    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public List<DataValue> getData() {
        return data;
    }

    public class DataValue {

        String id,user_id,full_name,phone,purpose,vehicle_no,image,no_of_persons,date_from,date_to,time_from,time_to,flat_no,created;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

        public String getVehicle_no() {
            return vehicle_no;
        }

        public void setVehicle_no(String vehicle_no) {
            this.vehicle_no = vehicle_no;
        }

        public String getNo_of_persons() {
            return no_of_persons;
        }

        public void setNo_of_persons(String no_of_persons) {
            this.no_of_persons = no_of_persons;
        }

        public String getDate_from() {
            return date_from;
        }

        public void setDate_from(String date_from) {
            this.date_from = date_from;
        }

        public String getDate_to() {
            return date_to;
        }

        public void setDate_to(String date_to) {
            this.date_to = date_to;
        }

        public String getTime_from() {
            return time_from;
        }

        public void setTime_from(String time_from) {
            this.time_from = time_from;
        }

        public String getTime_to() {
            return time_to;
        }

        public void setTime_to(String time_to) {
            this.time_to = time_to;
        }

        public String getFlat_no() {
            return flat_no;
        }

        public void setFlat_no(String flat_no) {
            this.flat_no = flat_no;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}