package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.EmployeeDetails_Model;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class EmployeeReviewLists_adapter extends RecyclerView.Adapter<EmployeeReviewLists_adapter.ViewHolder> {

    private Context context;
    private EmployeeDetails_Model list;
    AlertDialog.Builder builder;

    public EmployeeReviewLists_adapter(Context context, EmployeeDetails_Model list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviewlists_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(list.getData().getReviews().get(position));

        holder.review.setText(list.getData().getReviews().get(position).getReview());
        holder.ratingBar.setRating(Float.parseFloat(list.getData().getReviews().get(position).getRating()));
        Picasso.get().load(Constants.GALLARYFOLDER + list.getData().getReviews().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return list.getData().getReviews().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView review;
        RatingBar ratingBar;
        ImageViewZoom imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            review = (TextView) itemView.findViewById(R.id.review);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                /*    String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new MarketPlace_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}