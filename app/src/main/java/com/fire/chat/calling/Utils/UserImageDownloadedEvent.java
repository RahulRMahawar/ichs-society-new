package com.fire.chat.calling.Utils;

public class UserImageDownloadedEvent {
    String path;

    public String getPath() {
        return path;
    }

    public UserImageDownloadedEvent(String path) {
        this.path = path;
    }
}
