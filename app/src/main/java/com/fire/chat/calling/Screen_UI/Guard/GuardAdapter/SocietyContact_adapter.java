package com.fire.chat.calling.Screen_UI.Guard.GuardAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_Model;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class SocietyContact_adapter extends RecyclerView.Adapter<SocietyContact_adapter.ViewHolder> {

    private Context context;
    SocietyContact_Model List;
    AlertDialog.Builder builder;
    String calledFrom;

    public interface SocietyContactCallChat {
        void OnSocietyContactCallChat(int actionType);
    }

    SocietyContactCallChat societyContactCallChat;

    public SocietyContact_adapter(Context context, SocietyContact_Model List, SocietyContactCallChat societyContactCallChat) {
        this.context = context;
        this.List = List;
        this.societyContactCallChat = societyContactCallChat;
    }

    public SocietyContact_adapter(Context context, SocietyContact_Model List, String calledFrom, SocietyContactCallChat societyContactCallChat) {
        this.context = context;
        this.List = List;
        this.calledFrom = calledFrom;
        this.societyContactCallChat = societyContactCallChat;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.society_contact_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(List.getDatas().get(position));
        if (!(calledFrom == null)) {
            if (calledFrom.equalsIgnoreCase("societyInfo")) {
                holder.name.setText(List.getDatas().get(position).getName());
                holder.flatNo.setText(List.getDatas().get(position).getContact());
                holder.mobileNo.setText(List.getDatas().get(position).getDesignation());
            }

        } else {
            holder.name.setText(List.getDatas().get(position).getFullName());
            holder.flatNo.setText(List.getDatas().get(position).getFlat_no());
            holder.mobileNo.setText(List.getDatas().get(position).getPhone());
        }

        Picasso.get().load(Constants.GALLARYFOLDER + Constants.GALLARYFOLDER + List.getDatas().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);

        holder.diallerSocietyContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                societyContactCallChat.OnSocietyContactCallChat(0);
            }
        });
        holder.chatSocietyContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                societyContactCallChat.OnSocietyContactCallChat(1);
            }
        });
        holder.videoCallSocietyContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                societyContactCallChat.OnSocietyContactCallChat(2);
            }
        });
        holder.audioCallSocietyContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                societyContactCallChat.OnSocietyContactCallChat(3);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.getDatas().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView flatNo;
        public TextView mobileNo;
        LinearLayout LayoutForFeatures;
        ImageViewZoom imageView;
        public ImageView diallerSocietyContact, videoCallSocietyContact, chatSocietyContact, audioCallSocietyContact;

        public ViewHolder(View itemView) {
            super(itemView);

            diallerSocietyContact = itemView.findViewById(R.id.dialler_society_contact);
            videoCallSocietyContact = itemView.findViewById(R.id.video_call_society_contact);
            chatSocietyContact = itemView.findViewById(R.id.chat_society_contact);
            audioCallSocietyContact = itemView.findViewById(R.id.audio_call_society_contact);

            name = (TextView) itemView.findViewById(R.id.name);
            flatNo = (TextView) itemView.findViewById(R.id.flatNo);
            mobileNo = (TextView) itemView.findViewById(R.id.mobileNo);
            LayoutForFeatures = (LinearLayout) itemView.findViewById(R.id.LayoutForFeatures);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (LayoutForFeatures.getVisibility() == View.GONE) {
                        LayoutForFeatures.setVisibility(View.VISIBLE);
                    } else {
                        LayoutForFeatures.setVisibility(View.GONE);
                    }

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}