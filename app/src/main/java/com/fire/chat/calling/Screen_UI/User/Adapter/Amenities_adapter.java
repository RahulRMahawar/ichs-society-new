package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Amenities.AddAmenitiesNew_fragment;
import com.fire.chat.calling.Screen_UI.User.Model.Amenities_model;
import com.fire.chat.calling.Screen_UI.User.fragments.Amenities.Amenities_Detail_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.Amenities.Amenities_fragment;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class Amenities_adapter extends RecyclerView.Adapter<Amenities_adapter.ViewHolder> {

    private Context context;
    private Amenities_model amenitiesModel;
    AlertDialog.Builder builder;
    String LoginType;
    Amenities_fragment amenities_fragment;

    public Amenities_adapter(Context context, Amenities_model amenitiesModel, String LoginType, Amenities_fragment amenities_fragment) {
        this.context = context;
        this.amenitiesModel = amenitiesModel;
        this.LoginType = LoginType;
        this.amenities_fragment = amenities_fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.amenities_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(amenitiesModel.getData().get(position));

        holder.name.setText(amenitiesModel.getData().get(position).getTitle());
        holder.subscription.setText("Not in API");
        Picasso.get().load(Constants.GALLARYFOLDER + amenitiesModel.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {

                    AddAmenitiesNew_fragment fragment = new AddAmenitiesNew_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("data", amenitiesModel.data.get(position));
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Amenities_fragment) amenities_fragment).DeleteAmenityAPI(String.valueOf(amenitiesModel.getData().get(position).getId()), "2", position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return amenitiesModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView subscription;
        LinearLayout layoutForAdminEdit, layoutForUserBook;
        ImageViewZoom imageView;
        ImageView edit, delete;

        public ViewHolder(final View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            subscription = (TextView) itemView.findViewById(R.id.subscription);
            layoutForAdminEdit = (LinearLayout) itemView.findViewById(R.id.layoutForAdminEdit);
            layoutForUserBook = (LinearLayout) itemView.findViewById(R.id.layoutForUserBook);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            if (LoginType.equalsIgnoreCase("2")) {
                layoutForAdminEdit.setVisibility(View.GONE);
            } else {
                layoutForUserBook.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (LoginType.equalsIgnoreCase("2")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            Amenities_Detail_fragment fragment = new Amenities_Detail_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("ID", String.valueOf(amenitiesModel.getData().get(getAdapterPosition()).getId()));
                            bundle.putString("TITLE", String.valueOf(amenitiesModel.getData().get(getAdapterPosition()).getTitle()));
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }
                    } else {

                    }


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void removeAt(int adapterPosition) {
        amenitiesModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, amenitiesModel.getData().size());
    }
}