package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FlatList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.VehicleManagement_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.VehicleManagement_adapter;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleManagement_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private VehicleManagement_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    LinearLayout searchLayout;
    FloatingActionButton fabAddVehicle;
    String LoginType;
    AlertDialog alertDialog;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String flatName, flatID;
    ArrayList<String> FlatNames = new ArrayList<String>();
    ArrayList<String> FlatID = new ArrayList<String>();
    SearchableSpinner spinnerFlat;
    FlatList_Model responseBody;
    ArrayAdapter<String> flatAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.vehicle_management_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        try {
            LoginType = getArguments().getString("LoginType");
        } catch (NullPointerException e) {
            LoginType = "";
        }
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.vehicleManagement));
        initializeViews();
        initializeRecycler();
        SetLayoutForUser();
        handleListeners();
        return view;
    }

    private void SetLayoutForUser() {
        if (LoginType.equalsIgnoreCase("2")) {
            SetUserView();
        } else {
            SetAdminView();
        }
    }

    private void SetAdminView() {
        if (cd.isConnectingToInternet()) {
            getUserVehicleAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void SetUserView() {


        /////for lower Recycler/////////////////////////////
        if (cd.isConnectingToInternet()) {
            getUserVehicleAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    private void initializeViews() {

        searchLayout = view.findViewById(R.id.searchLayout);
        fabAddVehicle = view.findViewById(R.id.fabAddVehicle);
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
    }

    private void initializeRecycler() {
        searchLayout.setVisibility(View.GONE);

/////////////MainRecycler////////////////////////////
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void handleListeners() {

        fabAddVehicle.setOnClickListener(this);
    }

    public void getUserVehicleAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            if (LoginType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            }
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("keyword", "");
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplicationContext()).getDatabase_name());
            Call<VehicleManagement_Model> call = apiInterface.getUserVehicleList(requestBody);
            call.enqueue(new Callback<VehicleManagement_Model>() {
                @Override
                public void onResponse(Call<VehicleManagement_Model> call, Response<VehicleManagement_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, String.valueOf(response.body().getData().size()), Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                VehicleManagement_Model vehicleManagementModel = response.body();


                                Adapter = new VehicleManagement_adapter(context, vehicleManagementModel, LoginType,
                                        VehicleManagement_fragment.this, "VehicleManagement");
                                recyclerView.setAdapter(Adapter);
                                Adapter.notifyDataSetChanged();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<VehicleManagement_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fabAddVehicle:
                showPopUp("", "Add", "", "", "", "");

                break;

        }


    }

    public void showPopUp(final String ID, final String type, final String receivedParkingNo,
                          final String receivedVehicleNo, final String receivedBrandOfCar, final String receivedFlatNo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.add_vehicle_popup, null);
        final EditText parkingNo = (EditText) customView.findViewById(R.id.parkingNo);
        final EditText flatNo = (EditText) customView.findViewById(R.id.flatNo);
        final EditText vehicleNo = (EditText) customView.findViewById(R.id.vehicleNo);
        final EditText brandOfCar = (EditText) customView.findViewById(R.id.brandOfCar);
        spinnerFlat = (SearchableSpinner) customView.findViewById(R.id.spinnerFlat);
        spinnerFlat.setTitle("Select Flat");
        //spinnerFlat.setPositiveButton("OK");
        Button done = (Button) customView.findViewById(R.id.done);
        parkingNo.setText(receivedParkingNo);
        vehicleNo.setText(receivedVehicleNo);
        brandOfCar.setText(receivedBrandOfCar);
        loadFlatSpinner(spinnerFlat, receivedFlatNo);


        builder.setView(customView);
        alertDialog = builder.create();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(vehicleNo.getText().toString().trim()) || vehicleNo.length() <= 0) {
                    vehicleNo.setError(getString(R.string.validate_VehicleNo));
                    vehicleNo.requestFocus();
                } else if (TextUtils.isEmpty(parkingNo.getText().toString().trim()) || parkingNo.length() <= 0) {
                    parkingNo.setError(getString(R.string.parkingNumber));
                    parkingNo.requestFocus();
                } else if (TextUtils.isEmpty(flatNo.getText().toString().trim()) || flatNo.length() <= 0) {
                    flatNo.setError(getString(R.string.validate_userFlatNo));
                    flatNo.requestFocus();
                } else {
                    AddOrEditVehicleAPI(ID, type, brandOfCar.getText().toString(), parkingNo.getText().toString(), vehicleNo.getText().toString(), flatNo.getText().toString());
                }
            }
        });
        spinnerFlat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                if (responseBody == null) {

                } else {
                    ((TextView) adapter.getChildAt(0)).setTextColor(Color.BLACK);
                   /* flatID = FlatID.get(position);
                    flatName = FlatNames.get(position);*/
                    flatID = FlatID.get(adapter.getSelectedItemPosition());
                    flatName = adapter.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        alertDialog.show();

    }

    private void loadFlatSpinner(final Spinner spinner, final String assignToName) {
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            /*Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", receivedUserNo);*/
            Call<FlatList_Model> call = apiInterface.getFlatList();
            call.enqueue(new Callback<FlatList_Model>() {
                @Override
                public void onResponse(Call<FlatList_Model> call, Response<FlatList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    FlatNames.add(response.body().getData().get(i).getFlat_no());
                                    FlatID.add(response.body().getData().get(i).getId());
                                }

                                flatAdapter = new ArrayAdapter<String>
                                        (context, android.R.layout.simple_spinner_dropdown_item, FlatNames);
                                spinnerFlat.setAdapter(flatAdapter);
                                if (assignToName != null) {
                                    int spinnerPosition = flatAdapter.getPosition(assignToName);
                                    spinner.setSelection(spinnerPosition);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                }

                @Override
                public void onFailure(Call<FlatList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void AddOrEditVehicleAPI(String ID, String type, String brandOfCar, String parkingNo, String vehicleNo, String flatNo) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();

            if (type.equalsIgnoreCase("edit")) {
                requestBody.put("id", ID);
            }
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("vehicle_no", vehicleNo);
            requestBody.put("brand_name", brandOfCar);
            requestBody.put("parking_no", parkingNo);
            requestBody.put("flat_no", flatNo);
            Call<VehicleManagement_Model> call = apiInterface.AddOrEditVehicle(requestBody);
            call.enqueue(new Callback<VehicleManagement_Model>() {
                @Override
                public void onResponse(Call<VehicleManagement_Model> call, Response<VehicleManagement_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                VehicleManagement_Model vehicleManagementModel = response.body();
                                Toast.makeText(context, vehicleManagementModel.getReplyMsg(), Toast.LENGTH_SHORT).show();

                                alertDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<VehicleManagement_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DeleteVehicleAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<VehicleManagement_Model> call = apiInterface.deleteVehicle(requestBody);
            call.enqueue(new Callback<VehicleManagement_Model>() {
                @Override
                public void onResponse(Call<VehicleManagement_Model> call, Response<VehicleManagement_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                VehicleManagement_Model vehicleManagementModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, vehicleManagementModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<VehicleManagement_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void AcceptOrRejectVehicleAPI(String ID, String verified, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("verified", verified);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<VehicleManagement_Model> call = apiInterface.acceptOrDeclineVehicleRequest(requestBody);
            call.enqueue(new Callback<VehicleManagement_Model>() {
                @Override
                public void onResponse(Call<VehicleManagement_Model> call, Response<VehicleManagement_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                VehicleManagement_Model vehicleManagementModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, vehicleManagementModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<VehicleManagement_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

