package com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.User.Adapter.Notice_and_events_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.NoticeAndEvent_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PastEvents_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private Notice_and_events_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    LinearLayout emptyDataLayout;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_page_fragment, container, false);
        context = getActivity();
        cd=new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        // ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.amenities));
        initilizeViews();
        initilizeRecycler();
        getEvents();
        handleListeners();
        return view;
    }


    private void initilizeViews() {
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }

    private void initilizeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void getEvents() {
        if (cd.isConnectingToInternet()) {

            getEventsAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getEventsAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("type", "2");
            Call<NoticeAndEvent_Model> call = apiInterface.getEvents(requestBody);
            call.enqueue(new Callback<NoticeAndEvent_Model>() {
                @Override
                public void onResponse(Call<NoticeAndEvent_Model> call, Response<NoticeAndEvent_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                NoticeAndEvent_Model noticeAndEventModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new Notice_and_events_adapter(context, noticeAndEventModel, LoginType,
                                            PastEvents_fragment.this, "pastEvent");
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<NoticeAndEvent_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleListeners() {

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
    public void DeletePastEventAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<NoticeAndEvent_Model> call = apiInterface.deleteEvents(requestBody);
            call.enqueue(new Callback<NoticeAndEvent_Model>() {
                @Override
                public void onResponse(Call<NoticeAndEvent_Model> call, Response<NoticeAndEvent_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                NoticeAndEvent_Model noticeAndEventModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, noticeAndEventModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<NoticeAndEvent_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

