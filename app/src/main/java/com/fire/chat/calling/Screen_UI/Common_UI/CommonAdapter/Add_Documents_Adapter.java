package com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Members.Add_Members_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_Profile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class Add_Documents_Adapter extends RecyclerView.Adapter<Add_Documents_Adapter.ViewHolder>{

    Context context;
    ArrayList<String> steps;
    Add_Members_fragment add_members_fragment;
    String calledFrom;
    Uri Path;
    Boolean setImage=false;

    public Add_Documents_Adapter(ArrayList<String> steps, Context context, Add_Members_fragment add_members_fragment, String calledFrom) {

        this.steps = steps;
        this.context = context;
        this.calledFrom = calledFrom;
        this.add_members_fragment = add_members_fragment;
    }
    public Add_Documents_Adapter(ArrayList<String> steps, Context context){
        this.steps = steps;
        this.context = context;
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageButton plus, minus;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            plus = (ImageButton) itemView.findViewById(R.id.plus);
            minus = (ImageButton) itemView.findViewById(R.id.minus);
            imageView= (ImageView) itemView.findViewById(R.id.imageView);

          /*  imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Add_Members_fragment)add_members_fragment).selectImage(getAdapterPosition());
                }
            });*/


            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    try {
                        steps.remove(position);
                        notifyItemRemoved(position);
                    }catch (ArrayIndexOutOfBoundsException e){e.printStackTrace();}
                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    try {
                        steps.add(position + 1, "");
                        notifyItemInserted(position + 1);
                    }catch (ArrayIndexOutOfBoundsException e){e.printStackTrace();}
                }
            });

        }
    }




    @Override
    public int getItemCount() {
        return steps.size();
    }


    @Override
    public Add_Documents_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_documents_adapter, viewGroup, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(Add_Documents_Adapter.ViewHolder holder, final int i) {

        int x = holder.getLayoutPosition();
        Picasso.get().load(Path).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
    }


    public ArrayList<String> getStepList(){
        return steps;
    }
    public void SetImageAt(int position, Uri path) {
        Path = path;
        setImage=true;
        notifyItemChanged(position);
    }
}