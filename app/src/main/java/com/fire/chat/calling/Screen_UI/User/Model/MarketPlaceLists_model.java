package com.fire.chat.calling.Screen_UI.User.Model;

public class MarketPlaceLists_model {

    private String Name;
    private String opens;
    private String shopNo;
    private String ratings;

    public MarketPlaceLists_model(String Name, String opens, String shopNo,String ratings) {
        this.Name = Name;
        this.opens = opens;
        this.shopNo = shopNo;
        this.ratings = ratings;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOpens() {
        return opens;
    }

    public void setOpens(String opens) {
        this.opens = opens;
    }

    public String getShopNo() {
        return shopNo;
    }

    public void setShopNo(String shopNo) {
        this.shopNo = shopNo;
    }
}