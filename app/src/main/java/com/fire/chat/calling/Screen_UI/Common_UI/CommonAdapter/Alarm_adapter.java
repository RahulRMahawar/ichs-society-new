package com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Alarm_model;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Guard_Alaram_fragment;

public class Alarm_adapter extends RecyclerView.Adapter<Alarm_adapter.ViewHolder> {

    private Context context;
    private Alarm_model List;
    String LoginType;
    MediaPlayer ring;
    Guard_Alaram_fragment guard_alaram_fragment;
    boolean isSpeakButtonLongPressed = false;

    public Alarm_adapter(Context context, Alarm_model List, String LoginType, Guard_Alaram_fragment guard_alaram_fragment) {
        this.context = context;
        this.List = List;
        this.LoginType = LoginType;
        this.guard_alaram_fragment = guard_alaram_fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.alarm_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemName.setText(List.getData().get(position).getFullName());
        ring = MediaPlayer.create(context, R.raw.alarm_tone);


        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Guard_Alaram_fragment) guard_alaram_fragment).showAddOrEditPopUp(List.getData().get(position).getId(),
                        List.getData().get(position).getFullName(),
                        List.getData().get(position).getAlarm_date(), List.getData().get(position).getAlarm_time(),
                        List.getData().get(position).getTo_user_id(), List.getData().get(position).getFrequency(), "edit");
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((Guard_Alaram_fragment) guard_alaram_fragment).DeleteAlarmAPI(List.getData().get(position).getId(), "2", position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Hold 3 seconds to start alarm", Toast.LENGTH_SHORT).show();

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isSpeakButtonLongPressed = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!ring.isPlaying()) {
                            holder.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                            holder.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);
                            ring.start();
                            //ring.setVolume(5,5);
                            ring.setLooping(true);
                        }
                    }
                }, 3000);
                Toast.makeText(context, "Keep holding to Sending alarm to Guard No:" + " " + List.getData().get(position).getTo_user_id(), Toast.LENGTH_LONG).show();
                return true;
            }
        });
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                // We're only interested in when the button is released.
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // We're only interested in anything if our speak button is currently pressed.
                    if (isSpeakButtonLongPressed) {
                        // Do something when the button is released.
                        ring.pause();
                        isSpeakButtonLongPressed = false;
                    }
                }
                return false;
            }
        });


    }


    @Override
    public int getItemCount() {
        return List.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName;
        ImageView imageView;
        LinearLayout LayoutForEdits;
        ImageView edit, delete;
        AudioManager audioManager;
        CardView cardView;
        private final static String default_notification_channel_id = "default";

        public ViewHolder(final View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.itemName);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            LayoutForEdits = (LinearLayout) itemView.findViewById(R.id.LayoutForEdits);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            cardView = (CardView) itemView.findViewById(R.id.card);
            audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            if (LoginType.equalsIgnoreCase("1")) {


            } else {
                LayoutForEdits.setVisibility(View.GONE);
            }

        /*    itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(context, "jasdgd", Toast.LENGTH_SHORT).show();
                   *//* Uri alarmSound = RingtoneManager. getDefaultUri (RingtoneManager. TYPE_NOTIFICATION );
                    MediaPlayer mp = MediaPlayer. create (context, alarmSound);
                    mp.start();
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context, default_notification_channel_id )
                                    .setSmallIcon(R.drawable. ic_launcher_foreground )
                                    .setContentTitle( "Test" )
                                    .setContentText( "Hello! This is my first push notification" ) ;
                    NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(Context. NOTIFICATION_SERVICE );
                    mNotificationManager.notify(( int ) System. currentTimeMillis () ,
                            mBuilder.build());*//*

                     MediaPlayer ring = MediaPlayer.create(context, R.raw.alarm_tone);
                    ring.start();









                   *//* if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("amenities"))
                    {
                        ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container,new Amenities_fragment()).commit();
                    }*//*
             *//* Bundle bundle = new Bundle();
                    bundle.putString("USERTYPE",USERTYPE);
                    Intent intent = new Intent(context, SingleGalleryPic_Screen.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);*//*

                }
            });*/

        }
    }

    public void removeAt(int adapterPosition) {
        List.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, List.getData().size());
    }

}