package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Admin_Visitors_fragment;
import com.fire.chat.calling.Screen_UI.Admin.Screens.SecurityGuardsList_Screen;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.CommonTabView_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.ContactUs.ContactUs_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.Help_And_Certificate.HelpAndServices_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.VehicleManagement_fragment;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.HelpLine_Screen;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Guard_Alaram_fragment;
import com.fire.chat.calling.Screen_UI.User.Model.DashboardContent_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryFolder_Screen;
import com.fire.chat.calling.Screen_UI.User.fragments.Amenities.Amenities_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents.ImportantNotice_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.NoticeAndEvents.NoticeAndEvent_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.SocietyInformation.SocietyInformation_fragment;

import java.util.List;

public class DashboardContent_adapter extends RecyclerView.Adapter<DashboardContent_adapter.ViewHolder> {

    private Context context;
    private List<DashboardContent_model> List;
    String LoginType;

    public DashboardContent_adapter(Context context, List List, String LoginType) {
        this.context = context;
        this.List = List;
        this.LoginType = LoginType;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_content_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemName.setText(List.get(position).getItemName());
        holder.imageView.setImageResource(List.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName;
        ImageView imageView;

        public ViewHolder(final View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.itemName);
            imageView = (ImageView) itemView.findViewById(R.id.image);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("MARKETPLACE")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            CommonTabView_fragment fragment = new CommonTabView_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("CalledFor", "MARKETPLACE");
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("visitors")) {

                        if (LoginType.equalsIgnoreCase("2")) {

                            String backStateName = context.getClass().getName();
                            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                            boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                            if (!fragmentPopped) {

                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.container, new CommonTabView_fragment());
                                fragmentTransaction.addToBackStack(backStateName);
                                fragmentTransaction.commit();
                            }
                        } else {
                            String backStateName = context.getClass().getName();
                            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                            boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                            if (!fragmentPopped) {

                                Admin_Visitors_fragment fragment = new Admin_Visitors_fragment();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.container, fragment);
                                fragmentTransaction.addToBackStack(backStateName);
                                fragmentTransaction.commit();
                            }
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("amenities")) {
                        if (LoginType.equalsIgnoreCase("1")) {
                            LoginType = "2";
                        }

                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            Amenities_fragment fragment = new Amenities_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("COMPLAINTS/SUGGESTIONS")) {
                        if (LoginType.equalsIgnoreCase("1")) {
                            LoginType = "2";
                        }
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            CommonTabView_fragment fragment = new CommonTabView_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("CalledFor", "COMPLAINTS/SUGGESTIONS");
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("BROADCAST MESSAGE")) {
                        if (LoginType.equalsIgnoreCase("1")) {
                            LoginType = "2";
                        }
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            CommonTabView_fragment fragment = new CommonTabView_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("CalledFor", "BROADCAST MESSAGE");
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("DASHBOARD")) {

                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            CommonTabView_fragment fragment = new CommonTabView_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("CalledFor", "ADMIN CRUD");
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("alarm")) {

                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            Guard_Alaram_fragment fragment = new Guard_Alaram_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("SECURITY GUARDS")) {
                        Intent intent = new Intent(context, SecurityGuardsList_Screen.class);
                        context.startActivity(intent);

/*
                        Bundle bundle = new Bundle();
                        bundle.putString("USERTYPE","ADMIN");
                        bundle.putString("HIDEDASHBOARD","True");
                        Intent intent = new Intent(context, DashboardActivity.class);
                        intent.putExtras(bundle);
                        context.startActivity(intent);*/
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("events")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            NoticeAndEvent_fragment fragment = new NoticeAndEvent_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("NOTICES")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, new ImportantNotice_fragment());
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("HELP & SERVICES")) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            HelpAndServices_fragment fragment = new HelpAndServices_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("VEHICLE MANAGEMENT")) {
                        if (LoginType.equalsIgnoreCase("1")) {
                            LoginType = "2";
                        }
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            VehicleManagement_fragment fragment = new VehicleManagement_fragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("LoginType", LoginType);
                            fragment.setArguments(bundle);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("emergency helpline")) {

                        Intent intent = new Intent(context, HelpLine_Screen.class);
                        context.startActivity(intent);
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("gallery")) {
                        if (LoginType.equalsIgnoreCase("1")) {
                            LoginType = "2";
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString("LoginType", LoginType);
                        Intent intent = new Intent(context, GalleryFolder_Screen.class);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    } else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("SOCIETY INFORMATION")) {

                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            SocietyInformation_fragment fragment = new SocietyInformation_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    }
                    else if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("Contact us")) {

                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            ContactUs_fragment fragment = new ContactUs_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }

                    }



                }
            });

        }
    }

}