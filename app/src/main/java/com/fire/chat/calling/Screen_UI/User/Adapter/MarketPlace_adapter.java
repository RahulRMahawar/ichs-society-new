package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.VehicleManagement_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace.MarketPlaceList_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace.MarketPlace_fragment;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class MarketPlace_adapter extends RecyclerView.Adapter<MarketPlace_adapter.ViewHolder> {

    private Context context;
    private MarketPlaceList_Model marketPlaceListModel;
    AlertDialog.Builder builder;
    String UserType;
    MarketPlace_fragment marketPlace_fragment;


    public MarketPlace_adapter(Context context, MarketPlaceList_Model marketPlaceListModel, String UserType, MarketPlace_fragment marketPlace_fragment) {
        this.context = context;
        this.marketPlaceListModel = marketPlaceListModel;
        this.UserType = UserType;
        this.marketPlace_fragment = marketPlace_fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.marketplace_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(marketPlaceListModel.getData().get(position));

        holder.name.setText(marketPlaceListModel.getData().get(position).getTitle());
        Picasso.get().load(Constants.GALLARYFOLDER + marketPlaceListModel.getData().get(position).getImage()).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MarketPlace_fragment)marketPlace_fragment).DeleteMarketPlaceAPI(marketPlaceListModel.getData().get(position).getId(),"2",position);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MarketPlace_fragment) marketPlace_fragment)
                        .showPopUp(String.valueOf(marketPlaceListModel.getData().get(position).getId()),
                                marketPlaceListModel.getData().get(position).getImage(), "Edit",
                                marketPlaceListModel.getData().get(position).getTitle());

            }
        });
    }


    @Override
    public int getItemCount() {
        return marketPlaceListModel.getData().size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        LinearLayout layoutForAdminEdit;
        ImageViewZoom imageView;
        ImageView delete, edit;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            layoutForAdminEdit = (LinearLayout) itemView.findViewById(R.id.layoutForAdminEdit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            if (UserType.equalsIgnoreCase("1")) {
                layoutForAdminEdit.setVisibility(View.VISIBLE);
            } else {
                layoutForAdminEdit.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {

                        MarketPlaceList_fragment fragment = new MarketPlaceList_fragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("ID", marketPlaceListModel.getData().get(getAdapterPosition()).getId());
                        bundle.putString("TITLE", marketPlaceListModel.getData().get(getAdapterPosition()).getTitle());
                        fragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragment);
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }
    public void removeAt(int adapterPosition) {
        marketPlaceListModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, marketPlaceListModel.getData().size());
    }
}