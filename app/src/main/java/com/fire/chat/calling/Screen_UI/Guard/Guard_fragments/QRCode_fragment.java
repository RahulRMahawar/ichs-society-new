package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Rough.QrScanner.QrCodeUser_Details;
import com.fire.chat.calling.Rough.QrScanner.ScannedBarcodeActivity;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Home;

public class QRCode_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private View view;
    private TextView name;
    Button btnScanBarcode, inputValue;
    AlertDialog alertDialog;
    EditText uniqueID;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.qrcode_fragment, container, false);
        context = getActivity();
        ((Guard_Home) getActivity()).setActionBarTitle(getString(R.string.hint_QR));
        initializeViews();
        click();
        return view;
    }


    private void initializeViews() {
        btnScanBarcode = view.findViewById(R.id.btnScanBarcode);
        inputValue = view.findViewById(R.id.inputValue);


    }


    private void click() {
        btnScanBarcode.setOnClickListener(this);
        inputValue.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnScanBarcode:
                startActivity(new Intent(context, ScannedBarcodeActivity.class));

                return;
            case R.id.inputValue:
                showPopUp();

                return;
        }
    }

    public void showPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.unique_id_popup, null);
        uniqueID = (EditText) customView.findViewById(R.id.uniqueID);
        Button done = (Button) customView.findViewById(R.id.done);
        builder.setView(customView);
        alertDialog = builder.create();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(uniqueID.getText().toString().trim()) || uniqueID.length() <= 0) {
                    uniqueID.setError("Unique ID required");
                    uniqueID.requestFocus();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("data", uniqueID.getText().toString());
                    Intent intent = new Intent(context, QrCodeUser_Details.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();

    }
}

