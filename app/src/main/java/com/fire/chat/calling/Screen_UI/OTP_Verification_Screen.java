package com.fire.chat.calling.Screen_UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_SocietyList_Activity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Utils.AppVerUtil;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.NetworkHelper;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.Utils.constants.FireManager;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTP_Verification_Screen extends AppCompatActivity implements View.OnClickListener {

    Button sendOTPButton;
    private Context context;
    private SessionManager sessionManager;
    ConnectionDetector cd;
    String receivedOTP, receivedUserNo;
    EditText otpField;
    FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_varification_screen_activity);
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
            }
        };
        context = OTP_Verification_Screen.this;
        cd = new ConnectionDetector(getApplicationContext());
        sessionManager = new SessionManager(this);
        getIntents();
        Toast.makeText(context, receivedOTP, Toast.LENGTH_SHORT).show();
        //getToken();
        _initializeView();
        handleListeners();

    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    private void getIntents() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            receivedOTP = bundle.getString("OTP");
            receivedUserNo = bundle.getString("UserNo");
        }
    }

    private void _initializeView() {
        sendOTPButton = findViewById(R.id.sendOTP);
        otpField = findViewById(R.id.otpField);
    }

    private void handleListeners() {
        sendOTPButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sendOTP) {
            CallVerifyOTP();
        }
    }

    private void CallVerifyOTP() {
        if (cd.isConnectingToInternet()) {
            if (otpField.getText().toString().isEmpty()) {
                Toast.makeText(context, "Please provide valid OTP", Toast.LENGTH_SHORT).show();
            } else {
                String OTP = otpField.getText().toString();
                if (receivedOTP.equalsIgnoreCase(OTP)) {
                    CallSuccessLoginApi();
                } else {
                    Toast.makeText(context, "Incorrect OTP", Toast.LENGTH_SHORT).show();
                }

            }
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void CallSuccessLoginApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("phone", receivedUserNo);
            requestBody.put("role_id", ((GlobalVariables) context.getApplicationContext()).getRole_id());
            Call<Verification_Model> call = apiInterface.VerifyUser(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(@NonNull Call<Verification_Model> call, @NonNull Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (Objects.requireNonNull(response.body()).getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                Verification_Model model = response.body();

                                /* Toast.makeText(OTP_Verification_Screen.this, model.getData().getRole_id(), Toast.LENGTH_SHORT).show();*/

                                firebaseAuth.createUserWithEmailAndPassword(receivedUserNo + "@gmail.com", "123456").addOnCompleteListener(OTP_Verification_Screen.this, new OnCompleteListener() {
                                    @Override
                                    public void onComplete(@NonNull Task task) {

                                        if (!task.isSuccessful()) {
                                            firebaseAuth.signInWithEmailAndPassword(receivedUserNo + "@gmail.com", "123456").addOnCompleteListener(OTP_Verification_Screen.this, new OnCompleteListener<AuthResult>() {
                                                @Override
                                                public void onComplete(@NonNull Task<AuthResult> task) {
                                                    if (!task.isSuccessful()) {
                                                        Toast.makeText(OTP_Verification_Screen.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        if (NetworkHelper.isConnected(OTP_Verification_Screen.this)) {
                                                            FireManager.updateMyUserName("+91" + model.getData().getPhone(), new FireManager.OnComplete() {
                                                                @Override
                                                                public void onComplete(boolean isSuccessful) {
                                                                    dialog.cancel();
                                                                    if (!isSuccessful) {
                                                                        Toast.makeText(OTP_Verification_Screen.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                                                                    } else {
                                                                        FireManager.updateMyPhone("+91" + model.getData().getPhone());
                                                                        FireManager.updateMyPhoto("https://firebasestorage.googleapis.com/v0/b/firechatcallingapp.appspot.com/o/image_profile%2F159866b4-2d5e-4c7a-986b-d4d59cc6df94.jpg?alt=media&token=dfc4a484-136d-4963-afba-9f737c087b38");
                                                                        FireManager.updateMyStatus("Hi, I am using I CHS!");
                                                                        String appVersion = new AppVerUtil().getAppVersion(OTP_Verification_Screen.this);
                                                                        if (appVersion != "")
                                                                            FireManager.updateMyAppVersion(appVersion);
                                                                        sessionManager.createLoginSession(
                                                                                model.getData().getRole_id(),
                                                                                model.getData().getId(),
                                                                                model.getSid()
                                                                        );
                                                                        startActivity(new Intent(context, User_SocietyList_Activity.class));
                                                                    }
                                                                }
                                                            });

                                                        }
                                                    }
                                                }
                                            });
                                        } else {

                                            if (NetworkHelper.isConnected(OTP_Verification_Screen.this)) {
                                                FireManager.updateMyUserName("+91" + model.getData().getPhone(), new FireManager.OnComplete() {
                                                    @Override
                                                    public void onComplete(boolean isSuccessful) {
                                                        dialog.cancel();
                                                        if (!isSuccessful) {
                                                            Toast.makeText(OTP_Verification_Screen.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            FireManager.updateMyPhone("+91" + model.getData().getPhone());
                                                            FireManager.updateMyPhoto("https://firebasestorage.googleapis.com/v0/b/firechatcallingapp.appspot.com/o/image_profile%2F159866b4-2d5e-4c7a-986b-d4d59cc6df94.jpg?alt=media&token=dfc4a484-136d-4963-afba-9f737c087b38");
                                                            FireManager.updateMyStatus("Hi, I am using I CHS!");
                                                            String appVersion = new AppVerUtil().getAppVersion(OTP_Verification_Screen.this);
                                                            if (appVersion != "")
                                                                FireManager.updateMyAppVersion(appVersion);
                                                            sessionManager.createLoginSession(
                                                                    model.getData().getRole_id(),
                                                                    model.getData().getId(),
                                                                    model.getSid()
                                                            );
                                                            startActivity(new Intent(context, User_SocietyList_Activity.class));
                                                        }
                                                    }
                                                });

                                            }
                                        }
                                    }
                                });


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }


                }

                @Override
                public void onFailure(@NonNull Call<Verification_Model> call, @NonNull Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
