package com.fire.chat.calling.Screen_UI.Admin.Screens;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fire.chat.calling.sessionData.CommonMethod.isValidTelephone;

public class AddSecurityGuard extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title;
    EditText name, mobileNo, gateNo, remark;
    Button done;
    ConnectionDetector cd;
    private Context context;
    String receivedName = "", receivedMobileNo = "", receivedGateNo = "", receivedRemarks = "", receivedID = "", type = "add", receivedImage = "";
    ImageViewZoom userImg;
    ImageView documentImage1, documentImage2, documentImage3;
    Intent myFiles;
    String ReceivedFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_security_guard_activity);
        context = AddSecurityGuard.this;
        cd = new ConnectionDetector(context);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            type = "edit";
            receivedName = bundle.getString("name");
            receivedMobileNo = bundle.getString("mobileNo");
            receivedGateNo = bundle.getString("gateNo");
            receivedRemarks = bundle.getString("remarks");
            receivedID = bundle.getString("id");
            receivedImage = bundle.getString("image");
            ReceivedFileName = receivedImage;
        }
        findViews();
        setViews();
        setToolbar();
        handleListeners();
    }

    private void setViews() {
        name.setText(receivedName);
        mobileNo.setText(receivedMobileNo);
        gateNo.setText(receivedGateNo);
        remark.setText(receivedRemarks);
        Picasso.get().load(Constants.GALLARYFOLDER + receivedImage).fit()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(userImg);
    }


    private void findViews() {
        name = findViewById(R.id.name);
        mobileNo = findViewById(R.id.mobileNo);
        gateNo = findViewById(R.id.gateNo);
        remark = findViewById(R.id.remark);
        done = findViewById(R.id.done);
        userImg = findViewById(R.id.userImg);
        documentImage1 = findViewById(R.id.documentImage1);
        documentImage2 = findViewById(R.id.documentImage2);
        documentImage3 = findViewById(R.id.documentImage3);
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText("Add Security Guard");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void handleListeners() {

        done.setOnClickListener(this);
        userImg.setOnClickListener(this);
        documentImage1.setOnClickListener(this);
        documentImage2.setOnClickListener(this);
        documentImage3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.done:
                validate();

                break;

            case R.id.userImg:
                requestAppPermissions();
                selectFile(1);

                break;
            case R.id.documentImage1:
                requestAppPermissions();
                selectFile(2);

                break;
            case R.id.documentImage2:
                requestAppPermissions();
                selectFile(3);

                break;
            case R.id.documentImage3:
                requestAppPermissions();
                selectFile(4);

                break;
        }
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                name.setError(getString(R.string.validate_userName));
                name.requestFocus();
            } else if (!isValidTelephone(mobileNo.getText().toString())) {
                mobileNo.setError(getResources().getString(R.string.validate_userMobile));
                mobileNo.requestFocus();
            } else if (TextUtils.isEmpty(gateNo.getText().toString().trim()) || gateNo.length() <= 0) {
                gateNo.setError(getString(R.string.validate_gate));
                gateNo.requestFocus();
            } else if (TextUtils.isEmpty(remark.getText().toString().trim()) || remark.length() <= 0) {
                remark.setError(getString(R.string.validate_remark));
                remark.requestFocus();
            } else {
                SubmitAddOrEditGuardApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitAddOrEditGuardApi() {

        Call<Verification_Model> call = null;
        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("phone", mobileNo.getText().toString());
            requestBody.put("fullName", name.getText().toString());
            requestBody.put("description", remark.getText().toString());
            requestBody.put("image", ReceivedFileName);
            requestBody.put("gate_no", gateNo.getText().toString());
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("device_token", "");

            if (type.equalsIgnoreCase("edit")) {
                requestBody.put("user_id", receivedID);
                call = apiInterface.EditGuard(requestBody);
            } else {
                requestBody.put("role_id", "5");
                call = apiInterface.AddGuard(requestBody);
            }

            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, "Register Request Sent Successfully", Toast.LENGTH_SHORT).show();
                            finish();
                            //startActivity(new Intent(context, Request_Approval_Info.class));
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(AddSecurityGuard.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 112); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void selectFile(int position) {

        myFiles = new Intent(Intent.ACTION_GET_CONTENT);
        myFiles.setType("image/*");
        startActivityForResult(myFiles, position);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(userImg);
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    uploadFiles(file);
                }
            case 2:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(documentImage1);
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    //uploadFiles(file);
                }
            case 3:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(documentImage2);
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    //uploadFiles(file);
                }
            case 4:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    final String link = AppUtils.getPath(context, path);
                    File file = new File(link);
                    // File file = new File(getRealPathFromURI(data.getData()));
                    String fileExt = MimeTypeMap.getFileExtensionFromUrl(link);
                    Picasso.get().load(path).fit()
                            .placeholder(R.drawable.ic_splash_icon)
                            .error(R.drawable.ic_splash_icon)
                            .into(documentImage3);
                    Toast.makeText(context, fileExt, Toast.LENGTH_SHORT).show();
                    //uploadFiles(file);
                }

        }

    }

    private void uploadFiles(File path) {
        final Dialog dialog = ProgressDialog.show(context, "File Uploading", "Uploading media...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), path);

            MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("attachment", path.getName(), requestFile);
            Call<FileUpload_model> call = apiInterface.UploadFile(((GlobalVariables) getApplicationContext()).getDatabase_name(), multipartBody);
            call.enqueue(new Callback<FileUpload_model>() {
                @Override
                public void onResponse(Call<FileUpload_model> call, Response<FileUpload_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                FileUpload_model fileUploadModel = response.body();
                                ReceivedFileName = response.body().getName();
                                Log.e("IMAGEEE", ReceivedFileName);
                                Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<FileUpload_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
