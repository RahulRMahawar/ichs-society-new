package com.fire.chat.calling.Interfaces;

/**
 * Created by Rahul Mahawar.
 */

public interface DashBoardItemClickListener {

    void onItemClicked(int position);
}


