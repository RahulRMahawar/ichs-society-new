package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Home;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class Dialler_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    LinearLayout one,two,three,four,five,six,seven,eight,nine,zero,ok,delete;
    EditText editText;
    String total;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.dialler_fragment, container, false);
        context = getActivity();
       ((Guard_Home) getActivity()).setActionBarTitle(getString(R.string.home));
        initilizeViews();
        click();
        return view;
    }


    private void initilizeViews() {


        total="";
        one=view.findViewById(R.id.one);
        two=view.findViewById(R.id.two);
        three=view.findViewById(R.id.three);
        four=view.findViewById(R.id.four);
        five=view.findViewById(R.id.five);
        six=view.findViewById(R.id.six);
        seven=view.findViewById(R.id.seven);
        eight=view.findViewById(R.id.eight);
        nine=view.findViewById(R.id.nine);
        zero=view.findViewById(R.id.zero);
        ok=view.findViewById(R.id.ok);
        delete=view.findViewById(R.id.delete);
        editText=view.findViewById(R.id.editText);

    }


    private void click() {

        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);
        delete.setOnClickListener(this);
        ok.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.one:
                if (editText.getText().toString().isEmpty())
                {
                   total="1";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.two:
                if (editText.getText().toString().isEmpty())
                {
                    total="2";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;

            case R.id.three:
                if (editText.getText().toString().isEmpty())
                {
                    total="3";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.four:
                if (editText.getText().toString().isEmpty())
                {
                    total="4";
                }
                else
                {

                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.five:
                if (editText.getText().toString().isEmpty())
                {
                    total="five";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.six:
                if (editText.getText().toString().isEmpty())
                {
                    total="6";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.seven:
                if (editText.getText().toString().isEmpty())
                {
                    total="7";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.eight:
                if (editText.getText().toString().isEmpty())
                {
                    total="8";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.nine:
                if (editText.getText().toString().isEmpty())
                {
                    total="9";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.zero:
                if (editText.getText().toString().isEmpty())
                {
                    total="0";
                }
                else
                {
                    total=editText.getText().toString()+total;
                }

                editText.setText(total);
                break;
            case R.id.delete:
                if (editText.getText().toString().isEmpty())
                {

                }
                else
                {
                    total=editText.getText().toString();
                    String str = total.substring(0, total.length() - 1);
                    editText.setText(str);
                }

                editText.setText(total);
                break;
            case R.id.ok:

                Bundle  bundle=new Bundle();
                bundle.putString("Type","Home");
                Guard_Home_Fragment fragment2 = new Guard_Home_Fragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragment2.setArguments(bundle);
                fragmentTransaction.replace(R.id.container, fragment2);
                fragmentTransaction.commit();
                break;
        }


    }
}

