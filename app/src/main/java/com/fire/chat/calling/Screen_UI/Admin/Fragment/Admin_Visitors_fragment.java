package com.fire.chat.calling.Screen_UI.Admin.Fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.Admin_Visitors_adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FlatList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Visitors_Model;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Add_Visitor;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.PerformCall;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Admin_Visitors_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private Admin_Visitors_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    LinearLayout searchLayout;
    FloatingActionButton fabAddVisitor;
    EditText searchVisitor, fromDate, toDate;
    Boolean isSearching = false;
    ProgressDialog dialog;
    LinearLayout emptyDataLayout;
    final Calendar myCalendar = Calendar.getInstance();
    String dateType = "";
    String flatName, flatID;
    ArrayList<String> FlatNames = new ArrayList<String>();
    ArrayList<String> FlatID = new ArrayList<String>();
    FlatList_Model responseBody;
    SearchableSpinner spinnerFlat;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.admin_visitors_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.visitors));
        initializeViews();
        initializeRecycler();
        if (cd.isConnectingToInternet()) {
            loadFlatSpinner();
            Searching();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        handleListeners();
        LinearLayout myLayout = (LinearLayout) view.findViewById(R.id.my_layout);
        myLayout.requestFocus();
        return view;
    }

    private void initializeViews() {

        searchLayout = view.findViewById(R.id.searchLayout);
        searchVisitor = view.findViewById(R.id.searchVisitor);
        fromDate = view.findViewById(R.id.fromDate);
        toDate = view.findViewById(R.id.toDate);
        fabAddVisitor = view.findViewById(R.id.fabAddVisitor);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
        spinnerFlat = view.findViewById(R.id.spinnerFlat);
        spinnerFlat.setTitle("Select Flat");
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void handleListeners() {

        fabAddVisitor.setOnClickListener(this);
        toDate.setOnClickListener(this);
        fromDate.setOnClickListener(this);
    }

    private void Searching() {
        searchVisitor.addTextChangedListener(MyTextWatcher);
        searchVisitor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
        fromDate.addTextChangedListener(MyTextWatcher);
        fromDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
        toDate.addTextChangedListener(MyTextWatcher);
        toDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
        spinnerFlat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (responseBody == null) {

                } else {
                    flatName = adapterView.getSelectedItem().toString();
                    flatID = FlatID.get(adapterView.getSelectedItemPosition());
                    String from = null;
                    String to = null;
                    String flat = null;
                    String search = null;
                    if (searchVisitor.getText().toString().isEmpty()) {
                        search = "";
                    } else {
                        search = searchVisitor.getText().toString();
                    }
                    if (flatName.equalsIgnoreCase("Flat")) {
                        flat = "";
                    } else {
                        flat = flatName;
                    }
                    if (fromDate.getText().toString().isEmpty()) {
                        from = "";
                    } else {
                        from = fromDate.getText().toString();
                    }
                    if (toDate.getText().toString().isEmpty()) {
                        to = "";
                    } else {
                        to = toDate.getText().toString();
                    }
                    isSearching = true;
                    getVisitorListAPI(search, from, to, flat);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerFlat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
    }

    private TextWatcher MyTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

            if (searchVisitor.getText().hashCode() == s.hashCode()) {

            } else if (fromDate.getText().hashCode() == s.hashCode()) {

            } else if (toDate.getText().hashCode() == s.hashCode()) {

            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {


            if (searchVisitor.getText().hashCode() == s.hashCode()) {

            } else if (fromDate.getText().hashCode() == s.hashCode()) {

            } else if (toDate.getText().hashCode() == s.hashCode()) {

            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            if (searchVisitor.getText().hashCode() == s.hashCode()) {
                String from = null;
                String to = null;
                String flat = null;
                if (flatName.equalsIgnoreCase("Flat")) {
                    flat = "";
                } else {
                    flat = flatName;
                }
                if (fromDate.getText().toString().isEmpty()) {
                    from = "";
                } else {
                    from = fromDate.getText().toString();
                }
                if (toDate.getText().toString().isEmpty()) {
                    to = "";
                } else {
                    to = toDate.getText().toString();
                }
                isSearching = true;
                getVisitorListAPI(s.toString(), from, to, flat);
            } else if (fromDate.getText().hashCode() == s.hashCode()) {
                String from = null;
                String to = null;
                String search = null;
                String flat = null;
                if (flatName.equalsIgnoreCase("Flat")) {
                    flat = "";
                } else {
                    flat = flatName;
                }
                if (fromDate.getText().toString().isEmpty()) {
                    from = "";
                } else {
                    from = fromDate.getText().toString();
                }
                if (toDate.getText().toString().isEmpty()) {
                    to = "";
                } else {
                    to = toDate.getText().toString();
                }
                if (searchVisitor.getText().toString().isEmpty()) {
                    search = "";
                } else {
                    search = searchVisitor.getText().toString();
                }
                isSearching = true;
                getVisitorListAPI(search, s.toString(), to, flat);
            } else if (toDate.getText().hashCode() == s.hashCode()) {
                String from = null;
                String to = null;
                String search = null;
                String flat = null;
                if (flatName.equalsIgnoreCase("Flat")) {
                    flat = "";
                } else {
                    flat = flatName;
                }
                if (fromDate.getText().toString().isEmpty()) {
                    from = "";
                } else {
                    from = fromDate.getText().toString();
                }
                if (toDate.getText().toString().isEmpty()) {
                    to = "";
                } else {
                    to = toDate.getText().toString();
                }
                if (searchVisitor.getText().toString().isEmpty()) {
                    search = "";
                } else {
                    search = searchVisitor.getText().toString();
                }
                isSearching = true;
                getVisitorListAPI(search, from, s.toString(), flat);
            }

        }

    };

    public void getVisitorListAPI(String searchKey, String from, String to, String flat) {
        if (!isSearching) {
            dialog = ProgressDialog.show(context, "", "Please wait...", false);
        }


        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("type", "1");
            requestBody.put("sid", "");
            requestBody.put("keyword", searchKey);
            requestBody.put("from_date", from);
            requestBody.put("to_date", to);
            requestBody.put("flat_no", flat);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<Visitors_Model> call = apiInterface.getVisitors(requestBody);
            call.enqueue(new Callback<Visitors_Model>() {
                @Override
                public void onResponse(Call<Visitors_Model> call, Response<Visitors_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Visitors_Model adminVisitorsModel = response.body();

                                if (!response.body().getData().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new Admin_Visitors_adapter(context, adminVisitorsModel, new Admin_Visitors_adapter.AdminVisitorCall() {
                                        @Override
                                        public void OnAdminVisitorCall() {
                                            new PerformCall(getActivity()).performCall(false, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                                        }
                                    });
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    if (!isSearching) {
                        dialog.cancel();
                    }


                }

                @Override
                public void onFailure(Call<Visitors_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    if (!isSearching) {
                        dialog.cancel();
                    }
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadFlatSpinner() {
        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            /*Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", receivedUserNo);*/
            Call<FlatList_Model> call = apiInterface.getFlatList();
            call.enqueue(new Callback<FlatList_Model>() {
                @Override
                public void onResponse(Call<FlatList_Model> call, Response<FlatList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                FlatNames.add("Flat");
                                FlatID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    FlatNames.add(response.body().getData().get(i).getFlat_no());
                                    FlatID.add(response.body().getData().get(i).getId());
                                }

                                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>
                                        (context, android.R.layout.simple_spinner_dropdown_item, FlatNames);
                                spinnerFlat.setAdapter(categoryAdapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();
                }

                @Override
                public void onFailure(Call<FlatList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fabAddVisitor:
                Intent intent = new Intent(context, Add_Visitor.class);
                context.startActivity(intent);
                break;
            case R.id.fromDate:
                dateType = "from";
                SelectDate();

                break;
            case R.id.toDate:
                dateType = "to";
                SelectDate();

                break;
        }


    }

    private void SelectDate() {
        DatePickerDialog dialog = new DatePickerDialog(context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.show();
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(dateType);
        }

    };

    private void updateLabel(String dateType) {
        String myFormat = "yy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if (dateType.equalsIgnoreCase("from")) {
            fromDate.setText(sdf.format(myCalendar.getTime()));
        } else if (dateType.equalsIgnoreCase("to")) {
            toDate.setText(sdf.format(myCalendar.getTime()));
        }

    }

}

