package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.ContactUs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUs_fragment extends Fragment implements View.OnClickListener {

    ConnectionDetector cd;
    Context context;
    private View view;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    EditText title, description;
    Button done;

    public ContactUs_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.contact_us_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        initializeViews();
        handleListeners();
        return view;
    }

    private void initializeViews() {
        title = view.findViewById(R.id.title);
        description = view.findViewById(R.id.description);
        done = view.findViewById(R.id.done);
    }


    private void handleListeners() {
        done.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.done:
                validate();
                break;
        }

    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(description.getText().toString().trim()) || description.length() <= 0) {
                description.setError(getString(R.string.validate_Discription));
                description.requestFocus();
            } else if (TextUtils.isEmpty(title.getText().toString().trim()) || title.length() <= 0) {
                title.setError(getString(R.string.title));
                title.requestFocus();
            } else {
                SubmitComplaintApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitComplaintApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("title", title.getText().toString());
            requestBody.put("description", description.getText().toString());
            requestBody.put("device_token", "");

            Call<Verification_Model> call = apiInterface.ContactUs(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            title.getText().clear();
                            description.getText().clear();
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

