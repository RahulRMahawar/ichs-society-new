package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devlomi.hidely.hidelyviews.HidelyImageView;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.CallLog_adapter;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_GetSet;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Adapter.CallsAdapter;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Interface.FragmentCallback;
import com.fire.chat.calling.Utils.Database.CallLogData;
import com.fire.chat.calling.Utils.Database.FireAppLocalDatabase;
import com.fire.chat.calling.Utils.PerformCall;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CallLog_fragment extends BaseFragment implements View.OnClickListener, CallsAdapter.OnClickListener, ActionMode.Callback {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private CallLog_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    List<SocietyContact_GetSet> contactModelList;
    ConnectionDetector cd;

    private CallsAdapter adapter;
    FragmentCallback listener;
    ActionMode actionMode;
    private FireAppLocalDatabase appLocalDatabase;

    private List<CallLogData> fireCallList;
    private List<CallLogData> selectedFireCallListActionMode = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_page_fragment, container, false);
        context = getActivity();
        // ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.amenities));
        initilizeViews();
        click();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (FragmentCallback) context;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }
    }


    private void initilizeViews() {

        cd = new ConnectionDetector(context);
       /* if (cd.isConnectingToInternet()) {
        } else {
            //Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }*/
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);

        recyclerView.setLayoutManager(layoutManager);

        appLocalDatabase = new FireAppLocalDatabase(getActivity());

        fireCallList = appLocalDatabase.getCallLogList();
        adapter = new CallsAdapter(fireCallList, selectedFireCallListActionMode, getActivity(), CallLog_fragment.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);


        /*contactModelList = new ArrayList<>();

        //Adding Data into ArrayList
        contactModelList.add(new SocietyContact_GetSet("Mahesh Singh", "Flat 428","+91-1234567890"));
        contactModelList.add(new SocietyContact_GetSet("Mahesh Singh", "Flat 428","+91-1234567890"));
        contactModelList.add(new SocietyContact_GetSet("Mahesh Singh", "Flat 428","+91-1234567890"));

        Adapter = new CallLog_adapter(context, contactModelList);*/


    }


    private void click() {


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }

    private void itemRemovedFromActionList(HidelyImageView selectedCircle, View itemView, CallLogData callLogData) {

        selectedFireCallListActionMode.remove(callLogData);
        if (selectedFireCallListActionMode.isEmpty()) {
            actionMode.finish();
        } else {
            selectedCircle.hide();
            itemView.setBackgroundColor(-1);
            actionMode.setTitle(selectedFireCallListActionMode.size() + "");
        }


    }

    private void itemAddedToActionList(HidelyImageView selectedCircle, View itemView, CallLogData callLogData) {
        selectedCircle.show();
        itemView.setBackgroundColor(getResources().getColor(R.color.light_blue));
        selectedFireCallListActionMode.add(callLogData);
        actionMode.setTitle(selectedFireCallListActionMode.size() + "");
    }

    public void exitActionMode() {

        if (actionMode != null)
            actionMode.finish();

    }

    @Override
    public void onItemClick(HidelyImageView hidelyImageView, View itemView, CallLogData callLogData) {
        if (actionMode != null) {
            if (selectedFireCallListActionMode.contains(callLogData))
                itemRemovedFromActionList(hidelyImageView, itemView, callLogData);
            else
                itemAddedToActionList(hidelyImageView, itemView, callLogData);
        } else if (callLogData.getUserName() != null && callLogData.getUserID() != null) {
            boolean isVideo;
            isVideo = callLogData.getCallType().contains("video");
            new PerformCall(getActivity()).performCall(isVideo, callLogData.getUserID());
        }
    }

    @Override
    public void onIconButtonClick(View itemView, CallLogData callLogData) {
        if (actionMode != null) return;

        if (callLogData.getUserName() != null && callLogData.getUserID() != null) {
            boolean isVideo;
            isVideo = callLogData.getCallType().contains("video");

            new PerformCall(getActivity()).performCall(isVideo, callLogData.getUserID());
        }
    }

    @Override
    public void onLongClick(HidelyImageView hidelyImageView, View itemView, CallLogData callLogData) {
        if (actionMode == null) {
            fragmentCallback.startTheActionMode(CallLog_fragment.this);
            itemAddedToActionList(hidelyImageView, itemView, callLogData);
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        this.actionMode = actionMode;
        actionMode.getMenuInflater().inflate(R.menu.menu_action_calls, menu);
        actionMode.setTitle("1");
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (actionMode != null && menuItem != null) {
            if (menuItem.getItemId() == R.id.menu_item_delete)
                deleteClicked();
        }
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

    }

    private void deleteClicked() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.confirmation);
        dialog.setMessage(R.string.delete_calls_confirmation);
        dialog.setNegativeButton(R.string.no, null);
        dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (CallLogData callLogData : selectedFireCallListActionMode) {
                    appLocalDatabase.deleteCallLogData(callLogData);
                }
                exitActionMode();
            }
        });
        dialog.show();

    }
}

