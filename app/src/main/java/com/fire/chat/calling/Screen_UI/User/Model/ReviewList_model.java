package com.fire.chat.calling.Screen_UI.User.Model;

public class ReviewList_model {

    private String Review;
    private int Rating;

    public ReviewList_model(String Review, int Rating) {
        this.Review = Review;
        this.Rating = Rating;
    }

    public String getReview() {
        return Review;
    }

    public void setReview(String review) {
        Review = review;
    }

    public int getRating() {
        return Rating;
    }

    public void setRating(int rating) {
        Rating = rating;
    }
}