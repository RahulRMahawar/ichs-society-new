package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.CrudDashboardFragments.CreateApproval_fragment;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.CrudDashboardFragments.View_Assign_fragment;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Requests.RequestApproved_fragment;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Requests.RequestPending_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.ComplaintAndSuggestion.Approved_Complaint_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.ComplaintAndSuggestion.Pending_Complaint_fragment;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.TabAdapter;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Add_Visitor;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Add_Complain;
import com.fire.chat.calling.Screen_UI.User.fragments.BroadcastMessage.Add_BroadcastMessage;
import com.fire.chat.calling.Screen_UI.User.fragments.BroadcastMessage.Approved_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.BroadcastMessage.Pending_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace.MarketPlace_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.Visitor.MyVisitor_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.Visitor.VisitorHistory_fragment;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class CommonTabView_fragment extends Fragment implements View.OnClickListener {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ConnectionDetector cd;
    Context context;
    String CalledFrom, LoginType;
    FloatingActionButton fabAddNew;
    LinearLayout LayoutForSearch;

    public CommonTabView_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_tabview_fragment, container, false);
        context = getActivity();

        try {
            CalledFrom = getArguments().getString("CalledFor");
        } catch (NullPointerException e) {
            CalledFrom = "";
        }
        try {
            LoginType = getArguments().getString("LoginType");
        } catch (NullPointerException e) {
            LoginType = "";
        }
        ((DashboardActivity) getActivity()).setActionBarTitle(CalledFrom);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        fabAddNew = (FloatingActionButton) view.findViewById(R.id.fabAddNew);
        LayoutForSearch = (LinearLayout) view.findViewById(R.id.LayoutForSearch);
        adapter = new TabAdapter(getChildFragmentManager());
        if (CalledFrom.equalsIgnoreCase("MARKETPLACE")) {

            fabAddNew.hide();
            LayoutForSearch.setVisibility(View.GONE);
            tabLayout.setVisibility(View.GONE);
            /*Properties_List_fragment propertiesListFragment  = new Properties_List_fragment();
            Bundle bundle = new Bundle();
            bundle.putString("USERTYPE", USERTYPE);
            bundle.putString("CALLEDFROM", CalledFrom);
            propertiesListFragment.setArguments(bundle);
            adapter.addFragment(propertiesListFragment, "PROPERTIES");*/

            MarketPlace_fragment marketPlaceFragment = new MarketPlace_fragment();
            adapter.addFragment(marketPlaceFragment, "OTHER SERVICES");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else if (CalledFrom.equalsIgnoreCase("COMPLAINTS/SUGGESTIONS")) {

            LayoutForSearch.setVisibility(View.GONE);
            Pending_Complaint_fragment pendingFragment = new Pending_Complaint_fragment();
            Bundle bundle = new Bundle();
            bundle.putString("LoginType", LoginType);
            pendingFragment.setArguments(bundle);
            adapter.addFragment(pendingFragment, "PENDING");


            Approved_Complaint_fragment approvedFragment = new Approved_Complaint_fragment();
            Bundle bundleB = new Bundle();
            bundleB.putString("LoginType", LoginType);
            approvedFragment.setArguments(bundleB);
            adapter.addFragment(approvedFragment, "RESOLVED");

            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else if (CalledFrom.equalsIgnoreCase("BROADCAST MESSAGE")) {
            LayoutForSearch.setVisibility(View.GONE);
            Pending_fragment pendingFragment = new Pending_fragment();
            Bundle bundle = new Bundle();
            bundle.putString("LoginType", LoginType);
            pendingFragment.setArguments(bundle);
            adapter.addFragment(pendingFragment, "PENDING");


            Approved_fragment approvedFragment = new Approved_fragment();
            Bundle bundleB = new Bundle();
            bundleB.putString("LoginType", LoginType);
            approvedFragment.setArguments(bundleB);
            adapter.addFragment(approvedFragment, "APPROVED");

            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else if (CalledFrom.equalsIgnoreCase("Requests")) {

            fabAddNew.hide();
            LayoutForSearch.setVisibility(View.GONE);
            ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.requests));
            adapter.addFragment(new RequestPending_fragment(), "PENDING");
            adapter.addFragment(new RequestApproved_fragment(), "APPROVED");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else if (CalledFrom.equalsIgnoreCase("ADMIN CRUD")) {

            fabAddNew.hide();
            LayoutForSearch.setVisibility(View.GONE);
            ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.Admin));

            CreateApproval_fragment createApprovalFragment = new CreateApproval_fragment();
            adapter.addFragment(createApprovalFragment, "CREATE APPROVALS");

            View_Assign_fragment viewAssignFragment = new View_Assign_fragment();
            adapter.addFragment(viewAssignFragment, "VIEW ASSIGN");

            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else {
            LayoutForSearch.setVisibility(View.GONE);
            ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.visitors));
            adapter.addFragment(new MyVisitor_fragment(), "MY VISITORS");
            adapter.addFragment(new VisitorHistory_fragment(), "VISITORS HISTORY");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        }

        initializeViews();
        click();
        return view;
    }


    private void initializeViews() {


    }


    private void click() {

        fabAddNew.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fabAddNew:
                if (CalledFrom.equalsIgnoreCase("BROADCAST MESSAGE")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("LoginType", LoginType);
                    Intent intent = new Intent(context, Add_BroadcastMessage.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                } else if (CalledFrom.equalsIgnoreCase("COMPLAINTS/SUGGESTIONS")) {
                    Intent intent = new Intent(context, Add_Complain.class);
                    startActivity(intent);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("Type", "User");
                    Intent intent = new Intent(context, Add_Visitor.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

                break;

        }


    }


}

