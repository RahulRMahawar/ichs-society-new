package com.fire.chat.calling.Screen_UI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.sessionData.SessionManager;

import androidx.appcompat.app.AppCompatActivity;

public class Select_Activity extends AppCompatActivity {

    Button guard, user, admin;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_activity);
        session = new SessionManager(getApplicationContext());
        guard = findViewById(R.id.guard);
        user = findViewById(R.id.user);
        admin = findViewById(R.id.admin);

        guard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((GlobalVariables) getApplicationContext().getApplicationContext()).setRole_id("5");
                Intent intent = new Intent(Select_Activity.this, Login_Screen.class);
                startActivity(intent);

            }
        });
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((GlobalVariables) getApplicationContext().getApplicationContext()).setRole_id("2");
                Intent intent = new Intent(Select_Activity.this, Login_Screen.class);
                startActivity(intent);
            }
        });
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((GlobalVariables) getApplicationContext().getApplicationContext()).setRole_id("1");
                Intent intent = new Intent(Select_Activity.this, Login_Screen.class);
                startActivity(intent);

               /* Bundle bundle = new Bundle();
                bundle.putString("USERTYPE","1");
                bundle.putString("HIDEDASHBOARD","False");
                Intent intent = new Intent(Select_Activity.this, DashboardActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);*/
            }
        });
    }
}
