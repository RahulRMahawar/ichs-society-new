package com.fire.chat.calling.retrofit_files;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Apiclient extends Application {

    public static final String BASE_URL ="http://3.7.63.119:8113/";

    private static Retrofit retrofit = null;


    public static Retrofit retrofit(){
        if(retrofit==null)
        {

            //create Okhttp client
         /*   OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES) // write timeout
                    .readTimeout(5, TimeUnit.MINUTES) // read timeout
                    .build();*/
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                   /* .client(client)*/
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();






        }
        return retrofit;
    }
}
