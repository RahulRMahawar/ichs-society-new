package com.fire.chat.calling.Screen_UI.Admin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Requests.RequestApproved_fragment;
import com.fire.chat.calling.Screen_UI.Admin.Fragment.Requests.RequestPending_fragment;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Users_Request_Model;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class VehicleManagementRequests_adapter extends RecyclerView.Adapter<VehicleManagementRequests_adapter.ViewHolder> {

    private Context context;
    private Users_Request_Model List;
    String type;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    RequestPending_fragment requestPending_fragment;
    RequestApproved_fragment requestApproved_fragment;
    public VehicleManagementRequests_adapter(Context context, Users_Request_Model List, RequestPending_fragment requestPending_fragment, String type) {
        this.context = context;
        this.List = List;
        this.type = type;
        this.requestPending_fragment=requestPending_fragment;
    }

    public VehicleManagementRequests_adapter(Context context, Users_Request_Model List, RequestApproved_fragment requestApproved_fragment, String type) {
        this.context = context;
        this.List = List;
        this.type = type;
        this.requestApproved_fragment=requestApproved_fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_management_requests_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setTag(List.getData().get(position));

        holder.name.setText(List.getData().get(position).getFullName());
        holder.slotNo.setText(List.getData().get(position).getPhone());
        holder.flatNo.setText(List.getData().get(position).getFlat_no());
        Picasso.get().load(Constants.GALLARYFOLDER + List.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RequestPending_fragment) requestPending_fragment).AcceptOrRejectUserAPI(String.valueOf(List.getData().get(position).getId()), "1", position);
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RequestPending_fragment) requestPending_fragment).AcceptOrRejectUserAPI(String.valueOf(List.getData().get(position).getId()), "2", position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return List.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, flatNo, slotNo, status, accept, decline;
        LinearLayout layoutForRequest;
        ImageViewZoom imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageViewZoom) itemView.findViewById(R.id.userImg);
            name = (TextView) itemView.findViewById(R.id.name);
            slotNo = (TextView) itemView.findViewById(R.id.slotNo);
            flatNo = (TextView) itemView.findViewById(R.id.flatNo);
            layoutForRequest = (LinearLayout) itemView.findViewById(R.id.layoutForRequest);
            accept = (TextView) itemView.findViewById(R.id.accept);
            decline = (TextView) itemView.findViewById(R.id.decline);
            if (type.equalsIgnoreCase("Approved")) {
                layoutForRequest.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


                    // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }
    public void removeAt(int adapterPosition) {
        List.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, List.getData().size());
    }
}