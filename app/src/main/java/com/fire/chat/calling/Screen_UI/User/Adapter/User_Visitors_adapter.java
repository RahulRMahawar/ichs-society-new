package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Visitors_Model;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User_Visitors_adapter extends RecyclerView.Adapter<User_Visitors_adapter.ViewHolder> {

    private Context context;
    Visitors_Model visitorsModel;
    AlertDialog.Builder builder;
    String CalledFrom;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    public User_Visitors_adapter(Context context, Visitors_Model visitorsModel, String CalledFrom) {
        this.context = context;
        this.visitorsModel = visitorsModel;
        this.CalledFrom = CalledFrom;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_visitors_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemView.setTag(visitorsModel.getData().get(position));

        holder.name.setText(visitorsModel.getData().get(position).getFull_name());
        holder.mobileNo.setText(visitorsModel.getData().get(position).getPhone());
        holder.flateNo.setText(visitorsModel.getData().get(position).getFlat_no());
        holder.startDate.setText(visitorsModel.getData().get(position).getDate_from());
        holder.startTime.setText(visitorsModel.getData().get(position).getTime_from());
        holder.endDate.setText(visitorsModel.getData().get(position).getDate_to());
        holder.endTime.setText(visitorsModel.getData().get(position).getTime_to());
        Picasso.get().load(Constants.GALLARYFOLDER+visitorsModel.getData().get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                acceptOrDeclineVisitorAPI(holder,"1",visitorsModel.getData().get(position).getId());
            }
        });
        holder.decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                acceptOrDeclineVisitorAPI(holder,"2", visitorsModel.getData().get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return visitorsModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView flateNo,startDate,endDate,startTime,endTime,mobileNo,accept,decline;
        LinearLayout LayoutForRespond;
        ImageViewZoom imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            flateNo = (TextView) itemView.findViewById(R.id.flatNo);
            mobileNo = (TextView) itemView.findViewById(R.id.mobileNo);
            startDate = (TextView) itemView.findViewById(R.id.startDate);
            endDate = (TextView) itemView.findViewById(R.id.endDate);
            startTime = (TextView) itemView.findViewById(R.id.startTime);
            endTime = (TextView) itemView.findViewById(R.id.endTime);
            accept = (TextView) itemView.findViewById(R.id.accept);
            decline = (TextView) itemView.findViewById(R.id.decline);
            LayoutForRespond= (LinearLayout) itemView.findViewById(R.id.LayoutForRespond);
            imageView=(ImageViewZoom)itemView.findViewById(R.id.userImg);
            if (CalledFrom.equalsIgnoreCase("VisitorHistory"))
            {
                LayoutForRespond.setVisibility(View.GONE);
            }



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  String backStateName = context.getClass().getName();
                    FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                    boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                    if (!fragmentPopped) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, new Amenities_Detail_fragment());
                        fragmentTransaction.addToBackStack(backStateName);
                        fragmentTransaction.commit();
                    }*/


           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    public void acceptOrDeclineVisitorAPI(final ViewHolder holder, String type, String id) {

          final Dialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", id);
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("verified", type);
            requestBody.put("database_name",((GlobalVariables)context.getApplicationContext()).getDatabase_name());

            Call<Visitors_Model> call = apiInterface.acceptOrDeclineVisitors(requestBody);
            call.enqueue(new Callback<Visitors_Model>() {
                @Override
                public void onResponse(Call<Visitors_Model> call, Response<Visitors_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Visitors_Model visitorsModel = response.body();
                                removeAt(holder.getAdapterPosition());
                                Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                        dialog.cancel();


                }

                @Override
                public void onFailure(Call<Visitors_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                        dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void removeAt(int adapterPosition) {
        visitorsModel.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, visitorsModel.getData().size());
    }
}