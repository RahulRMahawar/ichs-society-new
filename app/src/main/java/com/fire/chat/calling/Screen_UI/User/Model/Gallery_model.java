package com.fire.chat.calling.Screen_UI.User.Model;

import com.google.gson.annotations.SerializedName;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.BroadCastMessage_Model;

import java.util.List;

public class Gallery_model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;
    @SerializedName("data")
    public List<DataValue> data;
    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public List<DataValue> getData() {
        return data;
    }

    public class DataValue {

        String id,title,user_id,image,status,only_me_visible,created;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }


        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getOnly_me_visible() {
            return only_me_visible;
        }

        public void setOnly_me_visible(String only_me_visible) {
            this.only_me_visible = only_me_visible;
        }

        public String getStatus() {
            return status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}