package com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceDetails_Model;
import com.fire.chat.calling.Utils.Constants;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MarketPlaceSliderAdapter extends SliderViewAdapter<MarketPlaceSliderAdapter.SliderAdapterVH> {

    private Context context;
    private List<MarketPlaceDetails_Model.Images> marketPlaceListModel;
    String LoginType;


    public MarketPlaceSliderAdapter(Context context, List<MarketPlaceDetails_Model.Images> marketPlaceListModel, String loginType) {
        this.context = context;
        this.marketPlaceListModel = marketPlaceListModel;
        this.LoginType = LoginType;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.marketplace_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        Picasso.get().load(Constants.GALLARYFOLDER + marketPlaceListModel.get(position).getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(viewHolder.imageView);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   String backStateName = context.getClass().getName();
                FragmentManager fragmentManager =  ((FragmentActivity)context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {
                    Notice_Detail_fragment fragment=new Notice_Detail_fragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("ID",noticeAndEventModel.getData().get(position).getId());
                    bundle.putString("TITLE","Event Detail");
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }*/


                // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return marketPlaceListModel.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageView;

        public SliderAdapterVH(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);

            this.itemView = itemView;

        }
    }

}