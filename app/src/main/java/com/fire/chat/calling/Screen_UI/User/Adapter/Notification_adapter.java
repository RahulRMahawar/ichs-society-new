package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.Screen_UI.User.Model.Notification_model;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class Notification_adapter extends RecyclerView.Adapter<Notification_adapter.ViewHolder> {

    private Context context;
    private List<Notification_model> notificationList;
    AlertDialog.Builder builder;

    public Notification_adapter(Context context, List notificationList) {
        this.context = context;
        this.notificationList = notificationList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(notificationList.get(position));

        holder.pName.setText(notificationList.get(position).getPersonName());
        holder.pJobProfile.setText(notificationList.get(position).getJobProfile());
        Picasso.get().load(Constants.GALLARYFOLDER+notificationList.get(position).getJobProfile())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView pName;
        public TextView pJobProfile;
        ImageViewZoom imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            pName = (TextView) itemView.findViewById(R.id.name);
            pJobProfile = (TextView) itemView.findViewById(R.id.subscription);
            imageView=(ImageViewZoom)itemView.findViewById(R.id.userImg);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



           // Toast.makeText(view.getContext(), cpu.getPersonName() + " is " + cpu.getJobProfile(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}