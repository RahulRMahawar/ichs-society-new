package com.fire.chat.calling.Utils.realms;

public class CallLogData {

    int tableId;
    String userID;
    String userName;
    String userNumber;
    String userImage;
    String callType;
    String callMissedFull;
    String callIncomingOutgoing;
    String callTime;
    int callCount;

    public int getCallCount() {
        return callCount;
    }

    public void setCallCount(int callCount) {
        this.callCount = callCount;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getCallMissedFull() {
        return callMissedFull;
    }

    public void setCallMissedFull(String callMissedFull) {
        this.callMissedFull = callMissedFull;
    }

    public String getCallIncomingOutgoing() {
        return callIncomingOutgoing;
    }

    public void setCallIncomingOutgoing(String callIncomingOutgoing) {
        this.callIncomingOutgoing = callIncomingOutgoing;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }
}
