package com.fire.chat.calling.retrofit_files;

import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Alarm_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.BroadCastMessage_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.ComplaintAndSuggest_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.EditFamilyMember_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.EmployeeDetails_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FamilyMember_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FileUpload_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.GuardDetails_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.HelpAndServiceList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceDetails_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Users_Request_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Profile_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Visitors_Model;
import com.fire.chat.calling.Screen_UI.Admin.Model.SecurityGuardList_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.FlatList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.SocietyList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.VehicleManagement_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.HelpLine_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.Notification_model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.QrCode_model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.SocietyContact_Model;
import com.fire.chat.calling.Screen_UI.User.Model.AmenitiesDetails_Model;
import com.fire.chat.calling.Screen_UI.User.Model.Amenities_model;
import com.fire.chat.calling.Screen_UI.User.Model.Complaint_model;
import com.fire.chat.calling.Screen_UI.User.Model.Gallery_model;
import com.fire.chat.calling.Screen_UI.User.Model.NoticeAndEvent_Model;
import com.fire.chat.calling.Screen_UI.User.Model.SocietyInformation_Model;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @Multipart
    @POST("upload_file")
    Call<FileUpload_model> UploadFile(@Query("database_name") String database_name, @Part MultipartBody.Part file);

    @POST("otp_generate")
    Call<Verification_Model> getUser(@Body Map<String, String> body);

    @POST("otp_verify")
    Call<Verification_Model> VerifyUser(@Body Map<String, String> body);

    @POST("sign_up")
    Call<Verification_Model> UserSignUp(@Body Map<String, String> body);

    @POST("society_contact_list")
    Call<SocietyContact_Model> getSocietyContactList(@Body Map<String, String> body);

    @POST("user_society_list")
    Call<SocietyList_Model> getUserSocietyList(@Body Map<String, String> body);

    @POST("user_flats")
    Call<FlatList_Model> getUserFlatList(@Body Map<String, String> body);

    @POST("client_list")
    Call<SocietyList_Model> getAllSocietyList();

    @POST("emergency_contact_list")
    Call<HelpLine_Model> getAllEmergencyContacts(@Body Map<String, String> body);

    @POST("add_emergency_contact")
    Call<HelpLine_Model> addEmergencyContacts(@Body Map<String, String> body);

    @Headers("Content-Type:application/json")
    @POST("society_user_list")
    Call<SecurityGuardList_model> getSecurityGuardList(@Body Map<String, String> body);

    @Headers("Content-Type:application/json")
    @POST("society_user_list")
    Call<SecurityGuardList_model> getAllUserList(@Body Map<String, String> body);

    @POST("society_user_list")
    Call<SecurityGuardList_model> getAllEmployeesList(@Body Map<String, String> body);

    @POST("sign_up")
    Call<Verification_Model> AddGuard(@Body Map<String, String> body);

    @POST("edit_profile")
    Call<Verification_Model> EditGuard(@Body Map<String, String> body);

    @POST("notice_board_list")
    Call<NoticeAndEvent_Model> getEvents(@Body Map<String, String> body);

    @POST("add_news")
    Call<Verification_Model> AddEvents(@Body Map<String, String> body);

    @POST("event_details")
    Call<NoticeAndEvent_Model> getEventDetail(@Body Map<String, String> body);

    @POST("amenities_list")
    Call<Amenities_model> getAmenitiesList(@Body Map<String, String> body);

    @POST("amenity_details")
    Call<AmenitiesDetails_Model> getAmenityDetails(@Body Map<String, String> body);

    @POST("book_amenity")
    Call<Amenities_model> bookAmenity(@Body Map<String, String> body);

    @POST("vehicles_list")
    Call<VehicleManagement_Model> getUserVehicleList(@Body Map<String, String> body);

    @POST("add_vehicle")
    Call<VehicleManagement_Model> AddOrEditVehicle(@Body Map<String, String> body);

    @POST("notification_list")
    Call<Notification_model> getNotificationList(@Body Map<String, String> body);

    @POST("add_visitor_request")
    Call<Notification_model> addVisitor(@Body Map<String, String> body);

    @POST("visitor_list")
    Call<Visitors_Model> getVisitors(@Body Map<String, String> body);

    @POST("complaint_list")
    Call<ComplaintAndSuggest_Model> getComplaintList(@Body Map<String, String> body);

    @POST("assign_employee")
    Call<ComplaintAndSuggest_Model> AssignComplaint(@Body Map<String, String> body);

    @POST("user_details")
    Call<Profile_Model> getProfile(@Body Map<String, String> body);

    @POST("my_family_members")
    Call<FamilyMember_Model> getFamilyMembers(@Body Map<String, String> body);

    @POST("add_family_member")
    Call<EditFamilyMember_Model> addFamilyMembers(@Body Map<String, String> body);

    @POST("edit_family_member")
    Call<EditFamilyMember_Model> editFamilyMembers(@Body Map<String, String> body);

    @POST("notifications_list")
    Call<BroadCastMessage_Model> getBroadCastMessage(@Body Map<String, String> body);

    @POST("send_notification")
    Call<BroadCastMessage_Model> sendBroadCastMessage(@Body Map<String, String> body);

    @POST("edit_profile")
    Call<Verification_Model> editProfile(@Body Map<String, String> body);

    @POST("market_place_category_list")
    Call<MarketPlaceList_Model> getMarketPlace(@Body Map<String, String> body);

    @POST("market_place_shop_list")
    Call<MarketPlaceList_Model> getMarketPlaceList(@Body Map<String, String> body);

    @POST("shop_details")
    Call<MarketPlaceDetails_Model> getMarketPlaceDetails(@Body Map<String, String> body);

    @POST("create_shop_review")
    Call<MarketPlaceDetails_Model> addOrEditReview(@Body Map<String, String> body);

    @POST("folder_list")
    Call<Gallery_model> getGalleryFolders(@Body Map<String, String> body);

    @POST("folder_details")
    Call<Gallery_model> getGalleryFoldersDetails(@Body Map<String, String> body);

    @POST("society_information")
    Call<SocietyInformation_Model> getSocietyInfo(@Body Map<String, String> body);

    @POST("society_committee_members")
    Call<SocietyContact_Model> getCommitteeList(@Body Map<String, String> body);

    @POST("services_list_with_users")
    Call<HelpAndServiceList_Model> getServiceList(@Body Map<String, String> body);

    @POST("alarm_list")
    Call<Alarm_model> getAlarmList(@Body Map<String, String> body);

    @POST("add_alarm")
    Call<Alarm_model> addOrEditAlarm(@Body Map<String, String> body);

    @POST("alarm_delete")
    Call<Alarm_model> deleteAlarm(@Body Map<String, String> body);

    @POST("accept_visitor")
    Call<Visitors_Model> acceptOrDeclineVisitors(@Body Map<String, String> body);

    @POST("update_complaint_stage")
    Call<ComplaintAndSuggest_Model> updateComplaint(@Body Map<String, String> body);

    @POST("add_market_place")
    Call<MarketPlaceList_Model> addOrEditMarketPlace(@Body Map<String, String> body);

    @POST("update_market_place_status")
    Call<MarketPlaceList_Model> deleteMarketPlace(@Body Map<String, String> body);

    @POST("update_status_users")
    Call<MarketPlaceList_Model> deleteUser(@Body Map<String, String> body);

    @POST("flat_list")
    Call<FlatList_Model> getFlatList();

    @POST("event_delete")
    Call<NoticeAndEvent_Model> deleteEvents(@Body Map<String, String> body);

    @POST("user_vehicles_list")
    Call<VehicleManagement_Model> getVehicleList(@Body Map<String, String> body);

    @POST("vehicle_approve")
    Call<VehicleManagement_Model> acceptOrDeclineVehicleRequest(@Body Map<String, String> body);

    @POST("vehicle_delete")
    Call<VehicleManagement_Model> deleteVehicle(@Body Map<String, String> body);

    @POST("complaint_topics_list")
    Call<Complaint_model> getTopicList(@Body Map<String, String> body);

    @POST("add_complaint")
    Call<Complaint_model> addComplaint(@Body Map<String, String> body);

    @POST("pending_user_list")
    Call<Users_Request_Model> getPendingUsersRequests(@Body Map<String, String> body);

    @POST("approve_pending_user")
    Call<Users_Request_Model> acceptOrDeclineUserRequest(@Body Map<String, String> body);

    @POST("approved_user_list")
    Call<Users_Request_Model> getApprovedUsersRequests(@Body Map<String, String> body);

    @POST("create_folder")
    Call<Gallery_model> addOrEditGalleryFolder(@Body Map<String, String> body);

    @POST("image_folders")
    Call<Gallery_model> deleteFolder(@Body Map<String, String> body);

    @POST("folder_image_delete")
    Call<Gallery_model> deleteImageInFolder(@Body Map<String, String> body);

    @POST("upload_folder_image")
    Call<Gallery_model> addImageToFolder(@Body Map<String, String> body);

    @POST("sign_up")
    Call<Verification_Model> AddMembers(@Body Map<String, String> body);
    @POST("upload_user_documents")
    Call<Verification_Model> UploadDocument(@Body Map<String, String> body);
    @POST("create_support_request")
    Call<Verification_Model> ContactUs(@Body Map<String, String> body);
    @POST("employee_details")
    Call<EmployeeDetails_Model> EmployeeDetails(@Body Map<String, String> body);
    @POST("add_review")
    Call<Verification_Model> addOrEditReviewOfEmployee(@Body Map<String, String> body);
    @POST("notifications_delete")
    Call<Verification_Model> broadcastApproveOrDelete(@Body Map<String, String> body);
    @POST("create_support_request")
    Call<Verification_Model> addSociety(@Body Map<String, String> body);
    @POST("view_user_details")
    Call<GuardDetails_Model> guardDetails(@Body Map<String, String> body);
    @POST("add_amenity_subscription_plan")
    Call<Verification_Model> addAmenity(@Body Map<String, String> body);
    @POST("verify_code")
    Call<QrCode_model> sendQrCode(@Body Map<String, String> body);
    @POST("mark_visitor_in")
    Call<Verification_Model> markVisitor(@Body Map<String, String> body);
    @POST("mark_intrested")
    Call<Verification_Model> hire(@Body Map<String, String> body);
    @POST("amenity_delete")
    Call<Verification_Model> deleteAmenity(@Body Map<String, String> body);
    @POST("remove_emergency_contact")
    Call<Verification_Model> deleteEmergency(@Body Map<String, String> body);

   /* ///headerExample
    @POST("society_user_list")
    Call<SecurityGuardList_model> getSecurityGuardList(@Header("Content-Type") String header, @Body Map<String, String> body);*/


  /*  @POST("uploadImage")
    Call<ImageResponse>UploadImage(@Body Map<String, String> body);



    @GET("get_parent_cat")
    Call<ArrayList<MenuResponse>> getMenu();

    @POST("product-registration")
    @FormUrlEncoded
    Call<ArrayList<Product_Post>> savePostProductReg(@Field("customer_name") String customer_name, @Field("address") String address, @Field("city") String city
            , @Field("pin_code") String pin_code, @Field("contact_number") String contact_number, @Field("email_id") String email_id
            , @Field("product_group") String product_group, @Field("product_category") String product_category
            , @Field("product_model") String product_model, @Field("serial_no") String serial_no, @Field("store_id") String store_id
            , @Field("purchase_date") String purchase_date, @Field("invoice_image") String invoice_image);*/

}