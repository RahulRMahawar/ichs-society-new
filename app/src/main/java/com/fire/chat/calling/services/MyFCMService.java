package com.fire.chat.calling.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.calling.RingerActivity;
import com.fire.chat.calling.Utils.DownloadManager;
import com.fire.chat.calling.Utils.IntentUtils;
import com.fire.chat.calling.Utils.JsonUtil;
import com.fire.chat.calling.Utils.ListUtil;
import com.fire.chat.calling.Utils.NotificationHelper;
import com.fire.chat.calling.Utils.ServiceHelper;
import com.fire.chat.calling.Utils.SharedPreferencesManager;
import com.fire.chat.calling.Utils.constants.DBConstants;
import com.fire.chat.calling.Utils.constants.DownloadUploadStat;
import com.fire.chat.calling.Utils.constants.FireCallType;
import com.fire.chat.calling.Utils.constants.FireManager;
import com.fire.chat.calling.Utils.constants.MessageType;
import com.fire.chat.calling.Utils.constants.PendingGroupTypes;
import com.fire.chat.calling.Utils.realms.FireCall;
import com.fire.chat.calling.Utils.realms.GroupEvent;
import com.fire.chat.calling.Utils.realms.Message;
import com.fire.chat.calling.Utils.realms.PendingGroupJob;
import com.fire.chat.calling.Utils.realms.PhoneNumber;
import com.fire.chat.calling.Utils.realms.QuotedMessage;
import com.fire.chat.calling.Utils.realms.RealmContact;
import com.fire.chat.calling.Utils.realms.RealmHelper;
import com.fire.chat.calling.Utils.realms.RealmLocation;
import com.fire.chat.calling.Utils.realms.User;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.SinchHelpers;
import com.sinch.android.rtc.calling.CallNotificationResult;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Map;

import androidx.core.app.NotificationCompat;
import io.realm.RealmList;

public class MyFCMService extends FirebaseMessagingService {

    private static final int TWENTY_SECOND = 20 * 1000;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        if (FireManager.getUid() == null)
            return;//if the user clears the app data or sign out we don't wan't to do nothing
        SharedPreferencesManager.setTokenSaved(false);

        ServiceHelper.saveToken(this, s);
    }


    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (FireManager.getUid() == null)
            return;//if the user clears the app data or sign out we don't wan't to do nothing

        Map data = remoteMessage.getData();

        //if this payload is Sinch Call
        if (SinchHelpers.isSinchPushPayload(remoteMessage.getData())) {
            new ServiceConnection() {

                private Map payload;

                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    if (payload != null) {
                        CallingService.SinchServiceInterface sinchService = (CallingService.SinchServiceInterface) service;
                        if (sinchService != null) {
                            NotificationResult result = sinchService.relayRemotePushNotificationPayload(payload);
                            //if the Messages is a call
                            if (result.isValid() && result.isCall()) {

                                CallNotificationResult callResult = result.getCallResult();
                                String callId = callResult.getCallId();

                                //if this call was missed (user did not answer)
                                if (callResult.isCallCanceled()) {
                                    RealmHelper.getInstance().setCallAsMissed(callId);
                                    User user = RealmHelper.getInstance().getUser(callResult.getRemoteUserId());
                                    FireCall fireCall = RealmHelper.getInstance().getFireCall(callId);
                                    if (user != null && fireCall != null) {
                                        String phoneNumber = fireCall.getPhoneNumber();
                                        new NotificationHelper(MyFCMService.this).createMissedCallNotification(user, phoneNumber);
                                    }
                                } else {
                                    Map<String, String> headers = callResult.getHeaders();
                                    if (!headers.isEmpty()) {
                                        String phoneNumber = headers.get("phoneNumber");
                                        String timestampStr = headers.get("timestamp");
                                        if (phoneNumber != null && timestampStr != null) {
                                            long timestamp = Long.parseLong(timestampStr);
                                            User user = RealmHelper.getInstance().getUser(callResult.getRemoteUserId());
                                            FireCall fireCall = new FireCall(callId, user, FireCallType.INCOMING, timestamp, phoneNumber, callResult.isVideoOffered());
                                            RealmHelper.getInstance().saveObjectToRealm(fireCall);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    payload = null;
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                }

                public void relayMessageData(Map<String, String> data) {
                    payload = data;
                    Intent intent = new Intent(getApplicationContext(), CallingService.class);
                    getApplicationContext().bindService(intent, this, BIND_AUTO_CREATE);
                }
            }.relayMessageData(data);


        } else if (remoteMessage.getData().containsKey("event")) {
            //this will called when something is changed in group.
            // like member removed,added,admin changed, group info changed...
            if (remoteMessage.getData().get("event").equals("group_event")) {
                String groupId = remoteMessage.getData().get("groupId");
                String eventId = remoteMessage.getData().get("eventId");
                String contextStart = remoteMessage.getData().get("contextStart");
                int eventType = Integer.parseInt(remoteMessage.getData().get("eventType"));
                String contextEnd = remoteMessage.getData().get("contextEnd");
                //if this event was by the admin himself  OR if the event already exists do nothing
                if (contextStart.equals(SharedPreferencesManager.getPhoneNumber())
                        || RealmHelper.getInstance().getMessage(eventId) != null) {
                    return;
                }


                GroupEvent groupEvent = new GroupEvent(contextStart, eventType, contextEnd, eventId);
                PendingGroupJob pendingGroupJob = new PendingGroupJob(groupId, PendingGroupTypes.CHANGE_EVENT, groupEvent);
                RealmHelper.getInstance().saveObjectToRealm(pendingGroupJob);
                ServiceHelper.updateGroupInfo(this, groupId, groupEvent);

            } else if (remoteMessage.getData().get("event").equals("new_group")) {

                String groupId = remoteMessage.getData().get("groupId");
                User user = RealmHelper.getInstance().getUser(groupId);

                //if the group is not exists,fetch and download it
                if (user == null) {
                    PendingGroupJob pendingGroupJob = new PendingGroupJob(groupId, PendingGroupTypes.CREATION_EVENT, null);
                    RealmHelper.getInstance().saveObjectToRealm(pendingGroupJob);
                    ServiceHelper.fetchAndCreateGroup(this, groupId);
                } else {

                    RealmList<User> users = user.getGroup().getUsers();
                    User userById = ListUtil.getUserById(FireManager.getUid(), users);

                    //if the group is not active or the group does not contain current user
                    // then fetch and download it and set it as Active
                    if (!user.getGroup().isActive() || !users.contains(userById)) {
                        PendingGroupJob pendingGroupJob = new PendingGroupJob(groupId, PendingGroupTypes.CREATION_EVENT, null);
                        RealmHelper.getInstance().saveObjectToRealm(pendingGroupJob);
                        ServiceHelper.fetchAndCreateGroup(this, groupId);
                    }
                }


            } else if (remoteMessage.getData().get("event").equals("message_deleted")) {
                String messageId = remoteMessage.getData().get("messageId");
                Message message = RealmHelper.getInstance().getMessage(messageId);
                RealmHelper.getInstance().setMessageDeleted(messageId);

                if (message != null) {
                    if (message.getDownloadUploadStat() == DownloadUploadStat.LOADING) {
                        if (MessageType.isSentType(message.getType())) {
                            DownloadManager.cancelUpload(message.getMessageId());
                        } else
                            DownloadManager.cancelDownload(message.getMessageId());
                    }
                    new NotificationHelper(this).messageDeleted(message);
                }


            } else if (remoteMessage.getData().get("event").equals("call")) {


            }
        } else if (remoteMessage.getData().get(DBConstants.TYPE).equals("app_rtc_call")) {

            Log.e("Notification", remoteMessage.getData().toString());

            if (remoteMessage.getData().get("connection").equals("0")) { //0 Disconnect 1 Connect
                EventBus.getDefault().post("Disconnect");
            } else {
                if (remoteMessage.getData().get("subtype").equals("video_call")) {

                    long currentTimestamp = System.currentTimeMillis();
                    long notificationTime = Long.parseLong(remoteMessage.getData().get("timestamp"));
                    long twentyAgo = currentTimestamp - TWENTY_SECOND;

                    if (notificationTime < twentyAgo) {
                        createNotificationChannelAndBuilder(remoteMessage.getData().get("callerName"));
                    } else {
                        if (!isCallActive(this)) {
                            Intent callScreen = new Intent(this, RingerActivity.class);
                            callScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            callScreen.putExtra(IntentUtils.PHONE_CALL_TYPE, FireCallType.OUTGOING);
                            callScreen.putExtra(IntentUtils.RAND_ID, remoteMessage.getData().get("rand_id"));
                            callScreen.putExtra(IntentUtils.UID, remoteMessage.getData().get("caller_id"));
                            callScreen.putExtra("CallerName", remoteMessage.getData().get("callerName"));
                            callScreen.putExtra("CallerImage", remoteMessage.getData().get("callerImage"));
                            callScreen.putExtra("CallType", 1); //0 Audio 1 Video
                            startActivity(callScreen);
                        } else {
                            createNotificationChannelAndBuilder(remoteMessage.getData().get("callerName"));
                        }
                    }

                } else if (remoteMessage.getData().get("subtype").equals("audio_call")) {

                    long currentTimestamp = System.currentTimeMillis();
                    long notificationTime = Long.parseLong(remoteMessage.getData().get("timestamp"));
                    long twentyAgo = currentTimestamp - TWENTY_SECOND;

                    if (notificationTime < twentyAgo) {
                        createNotificationChannelAndBuilder(remoteMessage.getData().get("callerName"));
                    } else {
                        if (isCallActive(this)) {
                            Intent callScreen = new Intent(this, RingerActivity.class);
                            callScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            callScreen.putExtra(IntentUtils.PHONE_CALL_TYPE, FireCallType.OUTGOING);
                            callScreen.putExtra("callState", 1);
                            callScreen.putExtra(IntentUtils.RAND_ID, remoteMessage.getData().get("rand_id"));
                            callScreen.putExtra(IntentUtils.UID, remoteMessage.getData().get("caller_id"));
                            callScreen.putExtra("CallerName", remoteMessage.getData().get("callerName"));
                            callScreen.putExtra("CallerImage", remoteMessage.getData().get("callerImage"));
                            callScreen.putExtra("CallType", 0); //0 Audio 1 Video
                            startActivity(callScreen);
                        } else {
                            Intent callScreen = new Intent(this, RingerActivity.class);
                            callScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            callScreen.putExtra(IntentUtils.PHONE_CALL_TYPE, FireCallType.OUTGOING);
                            callScreen.putExtra("callState", 0);
                            callScreen.putExtra(IntentUtils.RAND_ID, remoteMessage.getData().get("rand_id"));
                            callScreen.putExtra(IntentUtils.UID, remoteMessage.getData().get("caller_id"));
                            callScreen.putExtra("CallerName", remoteMessage.getData().get("callerName"));
                            callScreen.putExtra("CallerImage", remoteMessage.getData().get("callerImage"));
                            callScreen.putExtra("CallType", 0); //0 Audio 1 Video
                            startActivity(callScreen);
                        }
                    }

                }
            }

        } else {

            final String messageId = remoteMessage.getData().get(DBConstants.MESSAGE_ID);

            //if message is deleted do not save it
            if (RealmHelper.getInstance().getDeletedMessage(messageId) != null)
                return;


            boolean isGroup = remoteMessage.getData().containsKey("isGroup");
            //getting data from fcm message and convert it to a message
            final String phone = remoteMessage.getData().get(DBConstants.PHONE);
            final String content = remoteMessage.getData().get(DBConstants.CONTENT);
            final String timestamp = remoteMessage.getData().get(DBConstants.TIMESTAMP);
            final int type = Integer.parseInt(remoteMessage.getData().get(DBConstants.TYPE));
            //get sender uid
            final String fromId = remoteMessage.getData().get(DBConstants.FROM_ID);
            String toId = remoteMessage.getData().get(DBConstants.TOID);
            final String metadata = remoteMessage.getData().get(DBConstants.METADATA);
            //convert sent type to received
            int convertedType = MessageType.convertSentToReceived(type);

            //if it's a group message and the message sender is the same
            if (fromId.equals(FireManager.getUid()))
                return;

            //create the message
            final Message message = new Message();
            message.setContent(content);
            message.setTimestamp(timestamp);
            message.setFromId(fromId);
            message.setType(convertedType);
            message.setMessageId(messageId);
            message.setMetadata(metadata);
            message.setToId(toId);
            message.setChatId(isGroup ? toId : fromId);
            message.setGroup(isGroup);
            if (isGroup)
                message.setFromPhone(phone);
            //set default state
            message.setDownloadUploadStat(DownloadUploadStat.FAILED);


            //check if it's text message
            if (MessageType.isSentText(type)) {
                //set the state to default
                message.setDownloadUploadStat(DownloadUploadStat.DEFAULT);


                //check if it's a contact
            } else if (remoteMessage.getData().containsKey(DBConstants.CONTACT)) {
                message.setDownloadUploadStat(DownloadUploadStat.DEFAULT);
                //get the json contact as String
                String jsonString = remoteMessage.getData().get(DBConstants.CONTACT);
                //convert contact numbers from JSON to ArrayList
                ArrayList<PhoneNumber> phoneNumbersList = JsonUtil.getPhoneNumbersList(jsonString);
                // convert it to RealmContact and set the contact name using content
                RealmContact realmContact = new RealmContact(content, phoneNumbersList);

                message.setContact(realmContact);


                //check if it's a location message
            } else if (remoteMessage.getData().containsKey(DBConstants.LOCATION)) {
                message.setDownloadUploadStat(DownloadUploadStat.DEFAULT);
                //get the json location as String
                String jsonString = remoteMessage.getData().get(DBConstants.LOCATION);
                //convert location from JSON to RealmLocation
                RealmLocation location = JsonUtil.getRealmLocationFromJson(jsonString);
                message.setLocation(location);
            }

            //check if it's image or Video
            else if (remoteMessage.getData().containsKey(DBConstants.THUMB)) {
                final String thumb = remoteMessage.getData().get(DBConstants.THUMB);

                //Check if it's Video and set Video Duration
                if (remoteMessage.getData().containsKey(DBConstants.MEDIADURATION)) {
                    final String mediaDuration = remoteMessage.getData().get(DBConstants.MEDIADURATION);
                    message.setMediaDuration(mediaDuration);
                }

                message.setThumb(thumb);


                //check if it's Voice Message or Audio File
            } else if (remoteMessage.getData().containsKey(DBConstants.MEDIADURATION)
                    && type == MessageType.SENT_VOICE_MESSAGE || type == MessageType.SENT_AUDIO) {

                //set audio duration
                final String mediaDuration = remoteMessage.getData().get(DBConstants.MEDIADURATION);


                message.setMediaDuration(mediaDuration);

                //check if it's a File
            } else if (remoteMessage.getData().containsKey(DBConstants.FILESIZE)) {
                String fileSize = remoteMessage.getData().get(DBConstants.FILESIZE);


                message.setFileSize(fileSize);

            }

            //if the message was quoted save it and get the quoted message
            if (remoteMessage.getData().containsKey("quotedMessageId")) {
                String quotedMessageId = remoteMessage.getData().get("quotedMessageId");
                //sometimes the message is not saved because of threads,
                //so we need to make sure that we refresh the database before checking if the message is exists
                RealmHelper.getInstance().refresh();
                Message quotedMessage = RealmHelper.getInstance().getMessage(quotedMessageId, fromId);
                if (quotedMessage != null)
                    message.setQuotedMessage(QuotedMessage.messageToQuotedMessage(quotedMessage));
            }


            //Save it to database and fire notification
            new NotificationHelper(MyFCMService.this).handleNewMessage(phone, message);

        }
    }

    public boolean isCallActive(Context context) {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (manager.getMode() == AudioManager.MODE_IN_CALL) {
            return true;
        } else {
            return false;
        }
    }

    private void createNotificationChannelAndBuilder(String callerName) {

        int NOTIFICATION_ID = 1001;
        String CHANNEL_ID = "fire_app_01";
        CharSequence name = "iCHS Channel";
        String Description = "iCHS Channel";

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 600});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        Intent playIntent = new Intent(getApplicationContext(), DashboardActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                playIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Missed Call")
                .setColor(getResources().getColor(R.color.red))
                .setContentText("From" + " " + callerName)
                .setSmallIcon(android.R.drawable.stat_notify_missed_call)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        notificationManager.notify(NOTIFICATION_ID, notification);

    }


}