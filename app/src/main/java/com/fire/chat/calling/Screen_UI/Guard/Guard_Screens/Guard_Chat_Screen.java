package com.fire.chat.calling.Screen_UI.Guard.Guard_Screens;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.Guard_chat_adapter;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.Guard_Chat_model;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Guard_Chat_Screen extends AppCompatActivity {
    private Toolbar toolbar;
    TextView title;
    Context context;
    private RecyclerView recyclerView;
    private Guard_chat_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    List<Guard_Chat_model> notificationList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guard_chat_screen_activity);
        setToolbar();
        setdata();
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.chat));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void setdata() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);

        recyclerView.setLayoutManager(layoutManager);

        notificationList = new ArrayList<>();

        //Adding Data into ArrayList
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        notificationList.add(new Guard_Chat_model("John Deo" ,"14 March 2018"));
        Adapter = new Guard_chat_adapter(context, notificationList);

        recyclerView.setAdapter(Adapter);

    }
}
