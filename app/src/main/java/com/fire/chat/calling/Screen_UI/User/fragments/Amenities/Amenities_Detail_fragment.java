package com.fire.chat.calling.Screen_UI.User.fragments.Amenities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.User.Model.AmenitiesDetails_Model;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Amenities_Detail_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView title, subscription, openingTime, avalableQuantity, description;
    ConnectionDetector cd;
    ImageView imageView;
    String ID, ReceivedTITLE;
    Button BookNow;
    AmenitiesDetails_Model amenitiesModel;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.amenities_detail_fragment, container, false);
        context = getActivity();
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        initializeViews();

        try {
            ID = getArguments().getString("ID");
        } catch (NullPointerException e) {
            ID = "";
        }
        try {
            ReceivedTITLE = getArguments().getString("TITLE");
        } catch (NullPointerException e) {
            ReceivedTITLE = getString(R.string.amenities);
        }
        /* imageView.setImageResource(Integer.parseInt(IMAGE));*/
        ((DashboardActivity) getActivity()).setActionBarTitle(ReceivedTITLE);
        cd = new ConnectionDetector(context);
        if (cd.isConnectingToInternet()) {
            getAmenitiesDetailAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        handleListeners();
        return view;
    }


    private void initializeViews() {
        imageView = view.findViewById(R.id.imageView);
        BookNow = view.findViewById(R.id.done);
        title = view.findViewById(R.id.title);
        subscription = view.findViewById(R.id.subscription);
        openingTime = view.findViewById(R.id.openingTime);
        avalableQuantity = view.findViewById(R.id.availableQuantity);
        description = view.findViewById(R.id.description);
    }

    public void getAmenitiesDetailAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("amenity_id", ID);
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            Call<AmenitiesDetails_Model> call = apiInterface.getAmenityDetails(requestBody);
            call.enqueue(new Callback<AmenitiesDetails_Model>() {
                @Override
                public void onResponse(Call<AmenitiesDetails_Model> call, Response<AmenitiesDetails_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                amenitiesModel = response.body();
                                setDataToViews();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<AmenitiesDetails_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataToViews() {
        title.setText(amenitiesModel.data.getTitle());
        description.setText(amenitiesModel.data.getDescription());
        if (amenitiesModel.data.getType().equalsIgnoreCase("0")) {
            subscription.setText("Free");
        } else {
            subscription.setText("Paid");
        }
        openingTime.setText(amenitiesModel.data.getTime_from() + " To " + amenitiesModel.data.getTime_to());
        avalableQuantity.setText("Available quantity: " + amenitiesModel.data.getQty());
        description.setText(amenitiesModel.data.getDescription());
        Picasso.get().load(Constants.GALLARYFOLDER + amenitiesModel.data.getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.yoga)
                .into(imageView);
    }

    private void handleListeners() {
        BookNow.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.done:

                String backStateName = context.getClass().getName();
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                if (!fragmentPopped) {

                    Amenities_BookingFor_fragment fragment = new Amenities_BookingFor_fragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("ID", String.valueOf(amenitiesModel.getData().getId()));
                    fragment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.addToBackStack(backStateName);
                    fragmentTransaction.commit();
                }
                return;

        }


    }
}

