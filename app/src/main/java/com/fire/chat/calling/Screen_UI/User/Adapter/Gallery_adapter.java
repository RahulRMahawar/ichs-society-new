package com.fire.chat.calling.Screen_UI.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Guard_Alaram_fragment;
import com.fire.chat.calling.Screen_UI.User.Model.Gallery_model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryFolder_Screen;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Gallery.GalleryPicList_Screen;
import com.fire.chat.calling.Utils.Constants;
import com.squareup.picasso.Picasso;

import androidx.recyclerview.widget.RecyclerView;

public class Gallery_adapter extends RecyclerView.Adapter<Gallery_adapter.ViewHolder> {

    private Context context;
    private Gallery_model List;
    String LoginType;


    public Gallery_adapter(Context context, Gallery_model List, String LoginType) {

        this.context = context;
        this.List = List;
        this.LoginType = LoginType;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemName.setText(List.getData().get(position).getTitle());
        Picasso.get().load(Constants.GALLARYFOLDER+List.getData().get(position).getImage()).fit().centerCrop()
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(holder.imageView);

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((GalleryFolder_Screen) context).showPopUp(List.getData().get(position).getId(),
                        List.getData().get(position).getImage(),
                        List.getData().get(position).getTitle(), List.getData().get(position).getOnly_me_visible(), "edit");
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((GalleryFolder_Screen) context).DeleteFolderAPI(List.getData().get(position).getId(), "2", position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return List.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemName;
        ImageView imageView;
        LinearLayout LayoutForEdits;
        ImageView edit, delete;
        public ViewHolder(final View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.itemName);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            LayoutForEdits = (LinearLayout) itemView.findViewById(R.id.LayoutForEdits);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            if (LoginType.equalsIgnoreCase("1")) {

            }
            else
            {
                LayoutForEdits.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* if (List.get(getLayoutPosition()).getItemName().equalsIgnoreCase("amenities"))
                    {
                        ((FragmentActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container,new Amenities_fragment()).commit();
                    }*/
                    Bundle bundle = new Bundle();
                    bundle.putString("LoginType",LoginType);
                    bundle.putString("ID",List.getData().get(getAdapterPosition()).getId());
                    bundle.putString("TITLE",List.getData().get(getAdapterPosition()).getTitle());
                    Intent intent = new Intent(context, GalleryPicList_Screen.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
            });

        }
    }
    public void removeAt(int adapterPosition) {
        List.getData().remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, List.getData().size());
    }


}