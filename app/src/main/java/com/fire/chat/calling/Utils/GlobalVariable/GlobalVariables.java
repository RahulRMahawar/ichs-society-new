package com.fire.chat.calling.Utils.GlobalVariable;

import android.app.Application;

public class GlobalVariables extends Application {

    private  String role_id;
    private  String database_name;

    public  String getRole_id()
    {
        return  role_id;
    }

    public  void setRole_id(String role_id){
        this.role_id=role_id;
    }

    public String getDatabase_name() {
        return database_name;
    }

    public void setDatabase_name(String database_name) {
        this.database_name = database_name;
    }
}
