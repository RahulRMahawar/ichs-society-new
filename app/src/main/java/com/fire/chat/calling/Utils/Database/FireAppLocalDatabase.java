package com.fire.chat.calling.Utils.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class FireAppLocalDatabase extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FireAppLocal.db";

    // table name
    private static final String TABLE_CALL_LOG = "table_call_log";

    //table column name
    private static final String CALLER_USER_ID = "caller_user_id";
    private static final String CALLER_USER_NAME = "caller_user_name";
    private static final String CALLER_USER_NUMBER = "caller_user_number";
    private static final String CALLER_USER_IMAGE = "caller_user_image";
    private static final String CALLER_USER_CALL_TYPE = "caller_user_call_type";
    private static final String CALLER_USER_CALL_FULL_MISS = "caller_user_call_full_miss";
    private static final String CALLER_USER_CALL_TIME = "caller_user_call_time";
    private static final String CALLER_USER_CALL_INCOMING_OUTGOING = "caller_user_call_incoming_outgoing";
    private static final String CALL_LOG_TABLE_ID = "id";
    private static final String CALL_LOG_COUNT = "count";

    //table create query array
    private String[] SQL_CREATE_ENTRIES = {CREATE_TABLE_CALL_LOG};
    private String[] SQL_TABLE_NAME = {TABLE_CALL_LOG};

    //table create call log query
    private static final String CREATE_TABLE_CALL_LOG = "CREATE TABLE " + TABLE_CALL_LOG + "("
            + CALL_LOG_TABLE_ID + " INTEGER PRIMARY KEY,"
            + CALL_LOG_COUNT + " INTEGER,"
            + CALLER_USER_ID + " TEXT,"
            + CALLER_USER_NAME + " TEXT,"
            + CALLER_USER_NUMBER + " TEXT,"
            + CALLER_USER_IMAGE + " TEXT,"
            + CALLER_USER_CALL_FULL_MISS + " TEXT,"
            + CALLER_USER_CALL_TIME + " TEXT,"
            + CALLER_USER_CALL_INCOMING_OUTGOING + " TEXT,"
            + CALLER_USER_CALL_TYPE + " TEXT" + ")";

    public FireAppLocalDatabase(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //insert call log data into table "TABLE_CALL_LOG"
    public void insertCallLogData(String callerUserId, String callerUserName,
                                  String callerUserNumber, String callerUserImage,
                                  String callerUserCallType, String callerUserCallFullMiss,
                                  String callerCallTime, String callIncomingOutgoing) {
        SQLiteDatabase fireAppDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALLER_USER_ID, callerUserId);
        if(callIncomingOutgoing.equalsIgnoreCase("OUTGOING")){
            contentValues.put(CALL_LOG_COUNT, 0);
        }else {
            contentValues.put(CALL_LOG_COUNT, 1);
        }
        contentValues.put(CALLER_USER_NAME, callerUserName);
        contentValues.put(CALLER_USER_NUMBER, callerUserNumber);
        contentValues.put(CALLER_USER_IMAGE, callerUserImage);
        contentValues.put(CALLER_USER_CALL_TYPE, callerUserCallType);
        contentValues.put(CALLER_USER_CALL_FULL_MISS, callerUserCallFullMiss);
        contentValues.put(CALLER_USER_CALL_TIME, callerCallTime);
        contentValues.put(CALLER_USER_CALL_INCOMING_OUTGOING, callIncomingOutgoing);
        fireAppDatabase.insert(TABLE_CALL_LOG, null, contentValues);
    }

    //get or retrieve call log data into table "TABLE_CALL_LOG"
    public List<CallLogData> getCallLogList() {
        SQLiteDatabase fireAppDatabase = this.getReadableDatabase();
        String sortOrder =
                CALL_LOG_TABLE_ID + " DESC";
        Cursor cursorCallLog = fireAppDatabase.query(TABLE_CALL_LOG, null, null, null, null, null, sortOrder);

        List<CallLogData> callLogDataList = new ArrayList<>();
        if (cursorCallLog.moveToFirst()) {
            do {
                CallLogData callLogData = new CallLogData();

                callLogData.setCallCount(cursorCallLog.getInt(cursorCallLog.getColumnIndex(CALL_LOG_COUNT)));
                callLogData.setTableId(cursorCallLog.getInt(cursorCallLog.getColumnIndex(CALL_LOG_TABLE_ID)));
                callLogData.setUserID(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_ID)));
                callLogData.setUserName(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_NAME)));
                callLogData.setUserNumber(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_NUMBER)));
                callLogData.setUserImage(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_IMAGE)));
                callLogData.setCallType(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_TYPE)));
                callLogData.setCallMissedFull(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_FULL_MISS)));
                callLogData.setCallIncomingOutgoing(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_INCOMING_OUTGOING)));
                callLogData.setCallTime(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_TIME)));

                callLogDataList.add(callLogData);
            } while (cursorCallLog.moveToNext());
        }

        return callLogDataList;

    }

    //get or retrieve call log data into table "TABLE_CALL_LOG"
    public List<CallLogData>getCallLogListByName(String query) {
        SQLiteDatabase fireAppDatabase = this.getReadableDatabase();

        String selection = CALLER_USER_NAME + " = ? " + " OR " + CALLER_USER_NUMBER + " = ? ";
        String[] selectionArgs = {query, query};

        String sortOrder =
                CALL_LOG_TABLE_ID + " DESC";
        Cursor cursorCallLog = fireAppDatabase.query(TABLE_CALL_LOG, null, selection, selectionArgs, null, null, sortOrder);

        List<CallLogData> callLogDataList = new ArrayList<>();
        if (cursorCallLog.moveToFirst()) {
            do {
                CallLogData callLogData = new CallLogData();

                callLogData.setCallCount(cursorCallLog.getInt(cursorCallLog.getColumnIndex(CALL_LOG_COUNT)));
                callLogData.setTableId(cursorCallLog.getInt(cursorCallLog.getColumnIndex(CALL_LOG_TABLE_ID)));
                callLogData.setUserID(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_ID)));
                callLogData.setUserName(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_NAME)));
                callLogData.setUserNumber(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_NUMBER)));
                callLogData.setUserImage(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_IMAGE)));
                callLogData.setCallType(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_TYPE)));
                callLogData.setCallMissedFull(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_FULL_MISS)));
                callLogData.setCallIncomingOutgoing(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_INCOMING_OUTGOING)));
                callLogData.setCallTime(cursorCallLog.getString(cursorCallLog.getColumnIndex(CALLER_USER_CALL_TIME)));

                callLogDataList.add(callLogData);
            } while (cursorCallLog.moveToNext());
        }
        return callLogDataList;

    }

    // update the single call log data
    public int updateCallLogList(CallLogData callLogData,int receiveMissed) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CALLER_USER_CALL_TYPE, callLogData.getCallType());
        if(receiveMissed==0){
            values.put(CALLER_USER_CALL_FULL_MISS, "MISSED");
        }else {
            values.put(CALLER_USER_CALL_FULL_MISS, "RECEIVE");
        }
        values.put(CALLER_USER_NUMBER, callLogData.getUserNumber());
        values.put(CALLER_USER_IMAGE, callLogData.getUserImage());
        values.put(CALLER_USER_CALL_TIME, callLogData.getCallTime());
        values.put(CALLER_USER_CALL_INCOMING_OUTGOING, callLogData.getCallIncomingOutgoing());

        // updating row
        return db.update(TABLE_CALL_LOG, values, CALL_LOG_TABLE_ID + " = ?",
                new String[]{String.valueOf(callLogData.getTableId())});
    }

    // update the single call log data
    public int updateCallLogCountList(CallLogData callLogData) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CALL_LOG_COUNT, 0);

        // updating row
        return db.update(TABLE_CALL_LOG, values, CALL_LOG_TABLE_ID + " = ?",
                new String[]{String.valueOf(callLogData.getTableId())});
    }

    // Deleting single call log data
    public void deleteCallLogData(CallLogData callLogData) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CALL_LOG, CALL_LOG_TABLE_ID + " = ?",
                new String[]{String.valueOf(callLogData.getTableId())});
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String sql_create_entry : SQL_CREATE_ENTRIES) {
            db.execSQL(sql_create_entry);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion > newVersion) {
            // Drop older table if existed
            for (String tableName : SQL_TABLE_NAME) {
                db.execSQL("DROP TABLE IF EXISTS " + tableName);
            }

            // Create tables again
            onCreate(db);
        }
    }
}
