package com.fire.chat.calling.Screen_UI.Admin.Screens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BroadcastMessageDetail extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    TextView title, description;
    private ImageButton forwardButton, backwardButton, pauseButton, playButton;
    private MediaPlayer mPlayer;
    private TextView songName, startTime, songTime;
    private SeekBar songProgress;
    private static int oTime = 0, sTime = 0, eTime = 0, fTime = 5000, bTime = 5000;
    private Handler handler = new Handler();
    private LinearLayout ImageLayout, AudioLayout, VideoLayout;
    private VideoView videoView;
    Button decline, approve;
    Context context;
    ImageView imageView;
    String receivedID, receivedDescription, receivedFile, calledFrom;
    String filename;
    String fileType;
    AlertDialog alertDialog;
    Verification_Model model;
    ConnectionDetector cd;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    LinearLayout actionLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.broadcast_message_detail_activity);
        context = this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(this);
        userDetail = sessionManager.getLoginSavedDetails();
        setToolbar();
        initializeViews();
        getIntents();
        handleListeners();
        setView();
    }


    private void getIntents() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            receivedID = bundle.getString("id");
            receivedDescription = bundle.getString("description");
            receivedFile = bundle.getString("file");
            calledFrom = bundle.getString("calledFrom");
            if (calledFrom.equalsIgnoreCase("approved")) {
                actionLayout.setVisibility(View.GONE);
            }

        }
    }

    private void setView() {
        filename = receivedFile;
        String ext = null;
        int i = filename.lastIndexOf('.');
        if (i > 0 && i < filename.length() - 1) {
            ext = filename.substring(i + 1).toLowerCase();
        }
        if (ext == null) {

        } else if (ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png") || ext.equals("gif")) {
            fileType = "image";
            AudioLayout.setVisibility(View.GONE);
            VideoLayout.setVisibility(View.GONE);
            ImageLayout.setVisibility(View.VISIBLE);
            createImageView();
        } else if (ext.equals("3gp") || ext.equals("mkv") || ext.equals("mp4") || ext.equals("flv")) {
            fileType = "video";
            AudioLayout.setVisibility(View.GONE);
            VideoLayout.setVisibility(View.VISIBLE);
            ImageLayout.setVisibility(View.GONE);
            createVideoView();
        } else if (ext.equals("mp3") || ext.equals("WAV ") || ext.equals("AIFF ")) {
            fileType = "audio";
            AudioLayout.setVisibility(View.VISIBLE);
            VideoLayout.setVisibility(View.GONE);
            ImageLayout.setVisibility(View.GONE);
            createAudioView();
        }

    }

    private void createImageView() {

    }

    private void createAudioView() {
        songName.setText("rahul's music");
        mPlayer = MediaPlayer.create(this, R.raw.simple_msg_tone);
        songProgress.setClickable(false);
        pauseButton.setEnabled(false);
    }

    private void createVideoView() {
        setVideoView();
    }


    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(getString(R.string.addBroadcastMessage));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeViews() {
        backwardButton = (ImageButton) findViewById(R.id.btnBackward);
        forwardButton = (ImageButton) findViewById(R.id.btnForward);
        playButton = (ImageButton) findViewById(R.id.btnPlay);
        pauseButton = (ImageButton) findViewById(R.id.btnPause);
        songName = (TextView) findViewById(R.id.songName);
        startTime = (TextView) findViewById(R.id.startTime);
        songTime = (TextView) findViewById(R.id.songTime);
        songProgress = (SeekBar) findViewById(R.id.sBar);
        ImageLayout = (LinearLayout) findViewById(R.id.ImageLayout);
        AudioLayout = (LinearLayout) findViewById(R.id.AudioLayout);
        VideoLayout = (LinearLayout) findViewById(R.id.VideoLayout);
        description = (TextView) findViewById(R.id.description);
        videoView = (VideoView) findViewById(R.id.videoView);
        decline = (Button) findViewById(R.id.decline);
        imageView = (ImageView) findViewById(R.id.imageView);
        approve = (Button) findViewById(R.id.approve);
        actionLayout = (LinearLayout) findViewById(R.id.actionLayout);
    }

    private void handleListeners() {
        playButton.setOnClickListener(this);
        pauseButton.setOnClickListener(this);
        forwardButton.setOnClickListener(this);
        backwardButton.setOnClickListener(this);
        description.setOnClickListener(this);
        decline.setOnClickListener(this);
        approve.setOnClickListener(this);
    }

    private void setVideoView() {
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);

        //specify the location of media file
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video_example);

        //Setting MediaController and URI, then starting the videoView
        if (mediaController == null) {
            // create an object of media controller class
            mediaController = new MediaController(BroadcastMessageDetail.this);
            mediaController.setAnchorView(videoView);
        }
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.description:
                if (ImageLayout.getVisibility() == View.VISIBLE) {
                    ImageLayout.setVisibility(View.GONE);
                    AudioLayout.setVisibility(View.VISIBLE);
                    songName.setText("rahul's music");
                    mPlayer = MediaPlayer.create(this, R.raw.simple_msg_tone);
                    songProgress.setClickable(false);
                    pauseButton.setEnabled(false);
                } else if (AudioLayout.getVisibility() == View.VISIBLE) {
                    AudioLayout.setVisibility(View.GONE);
                    VideoLayout.setVisibility(View.VISIBLE);
                    videoView.start();
                    createVideoView();
                } else if (VideoLayout.getVisibility() == View.VISIBLE) {
                    videoView.pause();
                    VideoLayout.setVisibility(View.GONE);
                    ImageLayout.setVisibility(View.VISIBLE);
                  /*  String extension = MimeTypeMap.getFileExtensionFromUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/220px-Image_created_with_a_mobile_phone.png");
                    String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    type=MimeTypeMap.getSingleton().getExtensionFromMimeType(type);
                    Toast.makeText(context, type, Toast.LENGTH_SHORT).show();*/
                }
                break;
            case R.id.btnPlay:
                playAudio();
                break;
            case R.id.btnPause:
                pauseAudio();
                break;
            case R.id.btnForward:
                forwardAudio();
                break;
            case R.id.btnBackward:
                backwardAudio();
                break;
            case R.id.decline:
                showDeletePopUp();
                break;
            case R.id.approve:
                if (cd.isConnectingToInternet()) {
                    approveOrRejectAPI("1");
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private void backwardAudio() {
        if ((sTime - bTime) > 0) {
            sTime = sTime - bTime;
            mPlayer.seekTo(sTime);
        } else {
            Toast.makeText(getApplicationContext(), "Cannot jump backward 5 seconds", Toast.LENGTH_SHORT).show();
        }
        if (!playButton.isEnabled()) {
            playButton.setEnabled(true);
        }
    }

    private void forwardAudio() {
        if ((sTime + fTime) <= eTime) {
            sTime = sTime + fTime;
            mPlayer.seekTo(sTime);
        } else {
            Toast.makeText(getApplicationContext(), "Cannot jump forward 5 seconds", Toast.LENGTH_SHORT).show();
        }
        if (!playButton.isEnabled()) {
            playButton.setEnabled(true);
        }
    }


    private void playAudio() {
        Toast.makeText(BroadcastMessageDetail.this, "Playing Audio", Toast.LENGTH_SHORT).show();
        mPlayer.start();
        eTime = mPlayer.getDuration();
        sTime = mPlayer.getCurrentPosition();
        if (oTime == 0) {
            songProgress.setMax(eTime);
            oTime = 1;
        }
        songTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(eTime),
                TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(eTime))));
        startTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
        songProgress.setProgress(sTime);
        handler.postDelayed(UpdateSongTime, 100);
        pauseButton.setEnabled(true);
        playButton.setEnabled(false);
    }

    private void pauseAudio() {
        mPlayer.pause();
        pauseButton.setEnabled(false);
        playButton.setEnabled(true);
        Toast.makeText(getApplicationContext(), "Pausing Audio", Toast.LENGTH_SHORT).show();
    }

    private Runnable UpdateSongTime = new Runnable() {
        @Override
        public void run() {
            sTime = mPlayer.getCurrentPosition();
            startTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                    TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
            songProgress.setProgress(sTime);
            handler.postDelayed(this, 100);
        }
    };

    private void showDeletePopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.delete_broadcast_popup, null);

        final EditText reason = (EditText) customView.findViewById(R.id.reason);
        Button done = (Button) customView.findViewById(R.id.done);
        builder.setView(customView);
        alertDialog = builder.create();
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    if (!reason.getText().toString().isEmpty()) {
                        approveOrRejectAPI("2");
                    } else {
                        reason.setError("Please provide reason");
                        reason.requestFocus();
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.show();
    }

    private void approveOrRejectAPI(String status) {


        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", receivedID);
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<Verification_Model> call = apiInterface.broadcastApproveOrDelete(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                model = response.body();
                                if (alertDialog != null && alertDialog.isShowing()) {
                                    alertDialog.dismiss();
                                }
                                Toast.makeText(BroadcastMessageDetail.this, model.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, model.getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
