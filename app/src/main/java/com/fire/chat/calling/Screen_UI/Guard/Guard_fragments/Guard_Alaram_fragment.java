package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Model.SecurityGuardList_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter.Alarm_adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Alarm_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Home;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Guard_Alaram_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    String LoginType;
    RecyclerView recycleView;
    private RecyclerView.LayoutManager layoutManager;
    private Alarm_adapter Adapter;
    FloatingActionButton floating_AddAlarm;
    ConnectionDetector cd;
    AlertDialog alertDialog;
    String guardName, guardID;
    ArrayList<String> GuardNames = new ArrayList<String>();
    ArrayList<String> GuardID = new ArrayList<String>();
    SecurityGuardList_model responseBody;
    final Calendar myCalendar = Calendar.getInstance();
    EditText selectDate, selectTime;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    ArrayAdapter<String> categoryAdapter;
    LinearLayout emptyDataLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.guard_alarm_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType=userDetail.get(SessionManager.LOGIN_TYPE);

        initializeViews();
        initializeRecycler();
        SetViews();
        getAlarm();
        handleListeners();

        return view;
    }

    private void SetViews() {
        if (LoginType.equalsIgnoreCase("2")) {
            ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.alarm));
            floating_AddAlarm.hide();
        } else if (LoginType.equalsIgnoreCase("1")) {
            ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.alarm));
        } else {
            ((Guard_Home) getActivity()).setActionBarTitle(getString(R.string.alarm));
            floating_AddAlarm.hide();
        }
    }

    private void initializeViews() {
        recycleView = view.findViewById(R.id.recycleView);
        floating_AddAlarm = view.findViewById(R.id.floating_AddAlarm);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }

    private void initializeRecycler() {
        recycleView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(context, 2);
        recycleView.setLayoutManager(layoutManager);

    

    }


    private void handleListeners() {
        floating_AddAlarm.setOnClickListener(this);
    }

    private void getAlarm() {
        if (cd.isConnectingToInternet()) {

            getAlarmAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getAlarmAPI() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("keyword", "");
            Call<Alarm_model> call = apiInterface.getAlarmList(requestBody);
            call.enqueue(new Callback<Alarm_model>() {
                @Override
                public void onResponse(Call<Alarm_model> call, Response<Alarm_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Alarm_model alarmModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    recycleView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new Alarm_adapter(context, alarmModel, LoginType, Guard_Alaram_fragment.this);
                                    recycleView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recycleView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Alarm_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.floating_AddAlarm:

                showAddOrEditPopUp("", "", "", "", "", "", "add");

                break;


        }


    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDate();
        }

    };

    private void updateDate() {
        String myFormat = "yy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        selectDate.setText(sdf.format(myCalendar.getTime()));
    }

    public void showAddOrEditPopUp(final String id, String assignToName, String receivedDate, String receivedTime, String assignToID,
                                     String receivedFrequency, final String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View customView = layoutInflater.inflate(R.layout.add_alarm_popup, null);
        Button done = (Button) customView.findViewById(R.id.done);
        selectDate = (EditText) customView.findViewById(R.id.selectDate);
        selectTime = (EditText) customView.findViewById(R.id.selectTime);
        final Spinner spinnerFrequency = (Spinner) customView.findViewById(R.id.spinnerFrequency);
        Spinner spinnerAssign = (Spinner) customView.findViewById(R.id.spinnerAssign);
        loadGuardSpinner(spinnerAssign, assignToName);
        loadFrequencySpinner(spinnerFrequency, receivedFrequency);
        selectDate.setText(receivedDate);
        selectTime.setText(receivedTime);

        builder.setView(customView);
        alertDialog = builder.create();
        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar currentTime = Calendar.getInstance();
                int hour = currentTime.get(Calendar.HOUR_OF_DAY);
                int minute = currentTime.get(Calendar.MINUTE);
                final int sec = currentTime.get(Calendar.SECOND);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        selectTime.setText(selectedHour + ":" + selectedMinute + ":" + sec);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet()) {

                    if (selectDate.getText().toString().equalsIgnoreCase("Date")) {
                        selectDate.setError(getString(R.string.validate_Date));
                        selectDate.requestFocus();
                    } else if (selectTime.getText().toString().equalsIgnoreCase("Time")) {
                        selectTime.setError(getString(R.string.validate_Date));
                        selectTime.requestFocus();
                    } else if (guardName.equalsIgnoreCase("Assign to")) {
                        Toast.makeText(context, "Please select user to assign.", Toast.LENGTH_SHORT).show();
                    } else if (spinnerFrequency.getSelectedItem().toString().equalsIgnoreCase("Select frequency")) {
                        Toast.makeText(context, "Please select frequency", Toast.LENGTH_SHORT).show();
                    } else {
                        AddOREditAlarm(id, selectDate.getText().toString(), selectTime.getText().toString(), guardID,
                                spinnerFrequency.getSelectedItem().toString(), type);
                    }

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        spinnerAssign.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (responseBody == null) {

                } else {
                    guardName = adapterView.getSelectedItem().toString();
                    guardID = GuardID.get(adapterView.getSelectedItemPosition());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        alertDialog.show();

    }


    private void AddOREditAlarm(String alarmID, String date, String time, String assignTo, String frequency, String type) {

        Call<Alarm_model> call = null;
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();

            if (type.equalsIgnoreCase("edit")) {
                requestBody.put("id", alarmID);
            }
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("alarm_date", date);
            requestBody.put("alarm_time", time);
            requestBody.put("to_user_id", assignTo);
            requestBody.put("frequency", frequency);
            requestBody.put("device_token", "");
            call = apiInterface.addOrEditAlarm(requestBody);

            call.enqueue(new Callback<Alarm_model>() {
                @Override
                public void onResponse(Call<Alarm_model> call, Response<Alarm_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    progressDialog.cancel();


                }

                @Override
                public void onFailure(Call<Alarm_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    progressDialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void DeleteAlarmAPI(String ID, String status, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("id", ID);
            requestBody.put("status", status);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<Alarm_model> call = apiInterface.deleteAlarm(requestBody);
            call.enqueue(new Callback<Alarm_model>() {
                @Override
                public void onResponse(Call<Alarm_model> call, Response<Alarm_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Alarm_model alarmModel = response.body();
                                Adapter.removeAt(position);
                                Toast.makeText(context, alarmModel.getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Alarm_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void loadFrequencySpinner(Spinner spinnerFrequency, String receivedFrequency) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.frequency, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFrequency.setAdapter(adapter);
        if (receivedFrequency != null) {
            int spinnerPosition = adapter.getPosition(receivedFrequency);
            spinnerFrequency.setSelection(spinnerPosition);
        }
    }

    private void loadGuardSpinner(final Spinner spinnerAssign, final String assignToName) {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("role_id", "5");
            Call<SecurityGuardList_model> call = apiInterface.getSecurityGuardList(requestBody);
            call.enqueue(new Callback<SecurityGuardList_model>() {
                @Override
                public void onResponse(Call<SecurityGuardList_model> call, Response<SecurityGuardList_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                GuardNames.add("Assign to");
                                GuardID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    GuardNames.add(response.body().getData().get(i).getFullName());
                                    GuardID.add(response.body().getData().get(i).getId());
                                }
                                categoryAdapter = new ArrayAdapter<String>
                                        (context, android.R.layout.simple_spinner_dropdown_item, GuardNames);
                                spinnerAssign.setAdapter(categoryAdapter);
                                if (assignToName != null) {
                                    int spinnerPosition = categoryAdapter.getPosition(assignToName);
                                    spinnerAssign.setSelection(spinnerPosition);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SecurityGuardList_model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

