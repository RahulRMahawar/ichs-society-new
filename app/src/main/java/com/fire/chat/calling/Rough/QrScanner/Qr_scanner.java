package com.fire.chat.calling.Rough.QrScanner;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.fire.chat.calling.R;

public class Qr_scanner extends AppCompatActivity {
    Button btnScanBarcode, inputValue;
    AlertDialog alertDialog;
    EditText uniqueID;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scanner);
        context = Qr_scanner.this;
        btnScanBarcode = findViewById(R.id.btnScanBarcode);
        inputValue = findViewById(R.id.inputValue);

        btnScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Qr_scanner.this, ScannedBarcodeActivity.class));
            }
        });
        inputValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopUp();
            }
        });
    }

    public void showPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(this);

        View customView = layoutInflater.inflate(R.layout.unique_id_popup, null);
        uniqueID = customView.findViewById(R.id.uniqueID);
        Button done = customView.findViewById(R.id.done);
        builder.setView(customView);
        alertDialog = builder.create();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(uniqueID.getText().toString().trim()) || uniqueID.length() <= 0) {
                    uniqueID.setError("Unique ID required");
                    uniqueID.requestFocus();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("data", uniqueID.getText().toString());
                    Intent intent = new Intent(context, QrCodeUser_Details.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();

    }
}