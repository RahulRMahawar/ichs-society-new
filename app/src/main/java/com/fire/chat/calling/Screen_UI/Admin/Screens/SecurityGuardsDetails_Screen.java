package com.fire.chat.calling.Screen_UI.Admin.Screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.GuardDetails_Model;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecurityGuardsDetails_Screen extends AppCompatActivity implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    TextView title;
    private TextView name, userEmail, phone, alternatePhone, flatNo, qr_code, gateNo, dateOfJoining;
    private ImageView image;
    ConnectionDetector cd;
    // LinearLayout LayoutForSharing, share;
    String ReceivedTITLE, ReceivedUserID;
    ProgressDialog dialog;
    GuardDetails_Model employeeDetailsModel;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guard_details_fragment);
        context = SecurityGuardsDetails_Screen.this;
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        //LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        getIntents();
        initializeViews();
        setToolbar();
        initializeRecycler();
        handleListeners();
        getEmployeeDetails();
    }

    private void getIntents() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            ReceivedUserID = bundle.getString("UserId");
            ReceivedTITLE = bundle.getString("title");
        }
    }

    private void setToolbar() {

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        setSupportActionBar(toolbar);

        title.setText(ReceivedTITLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initializeViews() {
       /* LayoutForSharing = view.findViewById(R.id.LayoutForSharing);
        share = view.findViewById(R.id.share);*/
        name = findViewById(R.id.name);
        image = findViewById(R.id.image);
        userEmail = findViewById(R.id.userEmail);
        phone = findViewById(R.id.phone);
        alternatePhone = findViewById(R.id.alternatePhone);
        flatNo = findViewById(R.id.flatNo);
        qr_code = findViewById(R.id.qr_code);
        gateNo = findViewById(R.id.gateNo);
        dateOfJoining = findViewById(R.id.dateOfJoining);

    }

    private void initializeRecycler() {

    }

    private void handleListeners() {

        // share.setOnClickListener(this);
    }

    private void getEmployeeDetails() {
        if (cd.isConnectingToInternet()) {
            getEmployeeDetailsAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getEmployeeDetailsAPI() {
        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            //requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("uid", ReceivedUserID);
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            Call<GuardDetails_Model> call = apiInterface.guardDetails(requestBody);
            call.enqueue(new Callback<GuardDetails_Model>() {
                @Override
                public void onResponse(Call<GuardDetails_Model> call, Response<GuardDetails_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                employeeDetailsModel = response.body();
                                SetDataToViews(employeeDetailsModel);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<GuardDetails_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetDataToViews(GuardDetails_Model List) {
        name.setText(List.data.getFullName());
        Picasso.get().load(Constants.GALLARYFOLDER + List.getData().getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(image);
        userEmail.setText("Email: " + List.data.getEmail());
        phone.setText("Phone No.: " + List.data.getPhone());
        alternatePhone.setText("Alternate No.: " + List.data.getAlternate_no());
        flatNo.setText("Flat No.: " + List.data.getFlat_no());
        //qr_code.setText(List.data.getQr_code());
        gateNo.setText("Gate No.: " + List.data.getGate_no());
        dateOfJoining.setText("Date of joining: " + List.data.getDate_of_joining());

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.share:
              /*  if (LayoutForSharing.getVisibility() == View.GONE) {
                    LayoutForSharing.setVisibility(View.VISIBLE);
                } else {
                    LayoutForSharing.setVisibility(View.GONE);
                }
                break;*/
                return;

        }


    }

}
