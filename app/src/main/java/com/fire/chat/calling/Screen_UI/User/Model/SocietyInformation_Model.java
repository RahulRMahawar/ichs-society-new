package com.fire.chat.calling.Screen_UI.User.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SocietyInformation_Model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;
    @SerializedName("data")
    public DataValue data;
    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public DataValue getData() {
        return data;
    }

    public class DataValue {

        String id,Title,created,description,image;


        @SerializedName("society_committee")
        public List<Committees> society_committee;
        public List<Committees> getSociety_committee() {
            return society_committee;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }



        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }


        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }



        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }


    }
    public static class Committees {

        String id,title,image,status,created;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }
    }
}