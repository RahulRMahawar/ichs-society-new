package com.fire.chat.calling.services;

import android.app.IntentService;
import android.content.Intent;


import com.fire.chat.calling.Utils.IntentUtils;
import com.fire.chat.calling.Utils.SharedPreferencesManager;
import com.fire.chat.calling.Utils.SyncContactsFinishedEvent;
import com.fire.chat.calling.Utils.constants.ContactUtils;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.Nullable;

public class SyncContactsService extends IntentService {


    //Required Constructor
    public SyncContactsService() {
        super("SyncContactsService");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent != null && intent.getAction() != null && intent.getAction().equals(IntentUtils.INTENT_ACTION_SYNC_CONTACTS)) {

            ContactUtils.syncContacts(this, new ContactUtils.OnContactSyncFinished() {
                @Override
                public void onFinish() {
                    //update ui when sync is finished
                    EventBus.getDefault().post(new SyncContactsFinishedEvent());
                    //to prevent initial sync contacts when the app is launched for first time
                    SharedPreferencesManager.setContactSynced(true);
                    stopSelf();
                }
            });

        }
    }


}
