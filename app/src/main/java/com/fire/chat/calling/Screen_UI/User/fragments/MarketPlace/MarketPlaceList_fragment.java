package com.fire.chat.calling.Screen_UI.User.fragments.MarketPlace;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.MarketPlaceList_Model;
import com.fire.chat.calling.Screen_UI.User.Adapter.MarketPlaceLists_adapter;
import com.fire.chat.calling.Screen_UI.User.Model.MarketPlaceLists_model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketPlaceList_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name;
    private RecyclerView recyclerView;
    private MarketPlaceLists_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    List<MarketPlaceLists_model> List;
    ConnectionDetector cd;
    String UserType, ReceivedID, ReceivedTITLE;
    ProgressDialog dialog;
    LinearLayout emptyDataLayout;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_page_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        UserType=userDetail.get(SessionManager.LOGIN_TYPE);
        try {
            ReceivedID = getArguments().getString("ID");
        } catch (NullPointerException e) {
            ReceivedID = "";
        }
        try {
            ReceivedTITLE = getArguments().getString("TITLE");
        } catch (NullPointerException e) {
            ReceivedTITLE = getString(R.string.marketPlaceTitle);
        }
        ((DashboardActivity) getActivity()).setActionBarTitle(ReceivedTITLE);

        initializeViews();
        initializeRecycler();
        handleListeners();
        getMarketPlaceList();
        return view;
    }


    private void initializeViews() {
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);
    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void handleListeners() {


    }

    private void getMarketPlaceList() {
        if (cd.isConnectingToInternet()) {
            getMarketPlaceListAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void getMarketPlaceListAPI() {
        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            if (UserType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }
            requestBody.put("market_place_category_id", ReceivedID);
            requestBody.put("database_name", ((GlobalVariables)getActivity().getApplication()).getDatabase_name());
            Call<MarketPlaceList_Model> call = apiInterface.getMarketPlaceList(requestBody);
            call.enqueue(new Callback<MarketPlaceList_Model>() {
                @Override
                public void onResponse(Call<MarketPlaceList_Model> call, Response<MarketPlaceList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                MarketPlaceList_Model marketPlaceListModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    Adapter = new MarketPlaceLists_adapter(context, marketPlaceListModel);
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();

                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<MarketPlaceList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
}

