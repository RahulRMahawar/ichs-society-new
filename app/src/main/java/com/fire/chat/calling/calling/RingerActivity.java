package com.fire.chat.calling.calling;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Utils.Database.FireAppLocalDatabase;
import com.fire.chat.calling.Utils.IntentUtils;
import com.fire.chat.calling.Utils.constants.FireCallType;
import com.fire.chat.calling.Utils.constants.FireConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;

public class RingerActivity extends AppCompatActivity {

    public static final int LOCAL_VIEW_WIDTH = 100;
    public static final int LOCAL_VIEW_HEIGHT = 150;
    private ImageView imgUser;
    private TextView tvUsername;
    private TextView tvStatus;
    private TextView tvCallType;
    private FloatingActionButton btnAnswer;
    private FloatingActionButton btnReject;
    private FloatingActionButton btnHangup;
    private ImageButton btnSpeaker;
    private ImageButton btnMic;
    private ImageButton btnVideo;
    private ConstraintLayout constraint;
    private ImageButton btnFlipCamera;
    private ImageView bottomHolder;

    private String roomId;
    private String callerName;
    private String callerImage;
    private int callState; //0 Normal 1 Busy
    private int callType; //0 Audio 1 Video

    Handler handler;
    Runnable myRunnable;
    private int finishTime = 35; //secs

    private Ringtone r;
    private Vibrator v;

    private FireAppLocalDatabase appLocalDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ringer);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        // Get Intent parameters.
        roomId = getIntent().getStringExtra(IntentUtils.RAND_ID);
        callerName = getIntent().getStringExtra("CallerName");
        callerImage = getIntent().getStringExtra("CallerImage");
        callType = getIntent().getIntExtra("CallType", 0);
        callState = getIntent().getIntExtra("callState", 0);
        Log.d("RINGER", "Room ID: " + roomId);
        if (roomId == null || roomId.length() == 0) {
            Log.e("RINGER", "Incorrect room ID in intent!");
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
            //Silent
            if (callState == 1) {
                //busy
                playBusyTone();
            }
        } else if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
            //Vibrate
            if (callState == 0) {
                doVibration();
            } else {
                //busy
                playBusyTone();
            }
        } else {
            //Ring
            if (callState == 0) {
                playRingtone();
            } else {
                //busy
                playBusyTone();
            }
        }

        handler = new Handler();
        myRunnable = new Runnable() {
            public void run() {
                createNotificationChannelAndBuilder();
                RingerActivity.this.finish();
            }
        };
        handler.postDelayed(myRunnable, finishTime * 1000);

        initViews();

    }

    private void createNotificationChannelAndBuilder() {

        int NOTIFICATION_ID = 1001;
        String CHANNEL_ID = "fire_app_01";
        CharSequence name = "iCHS Channel";
        String Description = "iCHS Channel";

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 600});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        Intent playIntent = new Intent(getApplicationContext(), DashboardActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                playIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Missed Call")
                .setColor(getResources().getColor(R.color.red))
                .setContentText("From" + " " + callerName)
                .setSmallIcon(android.R.drawable.stat_notify_missed_call)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        notificationManager.notify(NOTIFICATION_ID, notification);

    }

    private void initViews() {

        imgUser = findViewById(R.id.img_user);
        tvUsername = findViewById(R.id.tv_username);
        tvStatus = findViewById(R.id.tv_status);
        btnAnswer = findViewById(R.id.btn_answer);
        btnReject = findViewById(R.id.btn_reject);
        constraint = findViewById(R.id.constraint);
        tvCallType = findViewById(R.id.tv_call_type);

        tvUsername.setText(callerName);

        if (callType == 0) {
            tvCallType.setText("Audio Call");
        } else {
            tvCallType.setText("Video Call");
        }

        Glide.with(this).load(callerImage).into(imgUser);

        btnAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callType == 0) {
                    Intent callScreen = new Intent(RingerActivity.this, AudioCallActivity.class);
                    callScreen.putExtra(IntentUtils.PHONE_CALL_TYPE, FireCallType.INCOMING);
                    callScreen.putExtra(IntentUtils.RAND_ID, roomId);
                    callScreen.putExtra(IntentUtils.UID, getIntent().getStringExtra(IntentUtils.UID));
                    callScreen.putExtra(IntentUtils.CALLING_PERSON_IMAGE, callerImage);
                    startActivity(callScreen);
                    finish();
                } else {
                    Intent callScreen = new Intent(RingerActivity.this, VideoCallActivity.class);
                    callScreen.putExtra(IntentUtils.PHONE_CALL_TYPE, FireCallType.INCOMING);
                    callScreen.putExtra(IntentUtils.RAND_ID, roomId);
                    callScreen.putExtra(IntentUtils.UID, getIntent().getStringExtra(IntentUtils.UID));
                    startActivity(callScreen);
                    finish();
                }

            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RingerActivity.this, "Rejected", Toast.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAndRemoveTask();
                } else {
                    finish();
                }
            }
        });

    }

    private void playRingtone() {
        Uri call = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        r = RingtoneManager.getRingtone(getApplicationContext(), call);
        r.play();
        appLocalDatabase = new FireAppLocalDatabase(this);
        if (callType == 0) {
            FireConstants.usersRef.child(getIntent().getStringExtra(IntentUtils.UID))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();

                            appLocalDatabase.insertCallLogData(getIntent().getStringExtra(IntentUtils.UID),
                                    callerName, callerPhone, thumbImg,
                                    "audio", "MISSED",
                                    "" + System.currentTimeMillis(), "INCOMING");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        } else {
            FireConstants.usersRef.child(getIntent().getStringExtra(IntentUtils.UID))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();

                            appLocalDatabase.insertCallLogData(getIntent().getStringExtra(IntentUtils.UID),
                                    callerName, callerPhone, thumbImg,
                                    "video", "MISSED",
                                    "" + System.currentTimeMillis(), "INCOMING");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

        }
    }

    private void playBusyTone() {
        Uri busy = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.call_dialing);
        r = RingtoneManager.getRingtone(getApplicationContext(), busy);
        r.play();

        appLocalDatabase = new FireAppLocalDatabase(this);
        if (callType == 0) {
            FireConstants.usersRef.child(getIntent().getStringExtra(IntentUtils.UID))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();

                           /* appLocalDatabase.insertCallLogData(getIntent().getStringExtra(IntentUtils.UID),
                                    callerName, callerPhone, thumbImg,
                                    "audio", "MISSED",
                                    "" + System.currentTimeMillis(), "INCOMING");*/
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        } else {
            FireConstants.usersRef.child(getIntent().getStringExtra(IntentUtils.UID))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();

                            /*appLocalDatabase.insertCallLogData(getIntent().getStringExtra(IntentUtils.UID),
                                    callerName, callerPhone, thumbImg,
                                    "video", "MISSED",
                                    "" + System.currentTimeMillis(), "INCOMING");*/
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

        }
    }

    private void doVibration() {
        long[] pattern = {50, 100, 500, 1000, 2000, 3000};
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createWaveform(pattern, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(pattern, 0);
        }

        appLocalDatabase = new FireAppLocalDatabase(this);
        if (callType == 0) {
            FireConstants.usersRef.child(getIntent().getStringExtra(IntentUtils.UID))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();

                            appLocalDatabase.insertCallLogData(getIntent().getStringExtra(IntentUtils.UID),
                                    callerName, callerPhone, thumbImg, "audio",
                                    "MISSED", "" + System.currentTimeMillis(),
                                    "INCOMING");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        } else {
            FireConstants.usersRef.child(getIntent().getStringExtra(IntentUtils.UID))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();

                            appLocalDatabase.insertCallLogData(getIntent().getStringExtra(IntentUtils.UID),
                                    callerName, callerPhone, thumbImg, "video",
                                    "MISSED", "" + System.currentTimeMillis(),
                                    "INCOMING");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String message) {
        Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
        if (message.equals("Disconnect")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAndRemoveTask();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (handler != null) {
            if (myRunnable != null) {
                handler.removeCallbacks(myRunnable);
            }
        }
        if (r != null) {
            r.stop();
        }
        if (v != null) {
            v.cancel();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
        super.onDestroy();
    }
}
