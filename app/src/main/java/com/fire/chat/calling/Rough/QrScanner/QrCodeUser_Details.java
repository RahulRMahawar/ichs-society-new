package com.fire.chat.calling.Rough.QrScanner;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.Guard.GuardModel.QrCode_model;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QrCodeUser_Details extends AppCompatActivity {
    String data;
    Context context;
    Button approve, reject;
    TextView name, mobileNo, email, flatNo;
    ImageViewZoom imageView;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code_user_details_activity);
        context = QrCodeUser_Details.this;
        initializeView();
        getIntentBundles();
        handlers();
    }


    private void initializeView() {
        approve = findViewById(R.id.approve);
        reject = findViewById(R.id.reject);
        name = findViewById(R.id.name);
        mobileNo = findViewById(R.id.mobileNo);
        email = findViewById(R.id.email);
        flatNo = findViewById(R.id.flatNo);
        imageView = findViewById(R.id.imageView);
    }

    private void getIntentBundles() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            data = bundle.getString("data");
            sendUniqueId(data);
        }
    }

    private void handlers() {
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveApi();
            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void sendUniqueId(String intentData) {

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);
        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("code", intentData);
            requestBody.put("type", "1");
            Call<QrCode_model> call = apiInterface.sendQrCode(requestBody);
            call.enqueue(new Callback<QrCode_model>() {
                @Override
                public void onResponse(Call<QrCode_model> call, Response<QrCode_model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            setData(response.body());
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<QrCode_model> call, Throwable t) {
                    Log.e("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void approveApi() {
        Calendar calender = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        //time = simpleDateFormat.format(calender.getTime());
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);
        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("database_name", ((GlobalVariables) context.getApplicationContext()).getDatabase_name());
            requestBody.put("user_id", id);
            requestBody.put("in_time", simpleDateFormat.format(calender.getTime()));
            Call<Verification_Model> call = apiInterface.markVisitor(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.e("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setData(QrCode_model body) {
        name.setText(body.data.getFullName());
        mobileNo.setText(body.data.getPhone());
        email.setText(body.data.getEmail());
        flatNo.setText(body.data.getFlat_no());
        id = body.data.getId();

        Picasso.get().load(Constants.GALLARYFOLDER + body.data.getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(imageView);
    }
}