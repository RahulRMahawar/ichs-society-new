package com.fire.chat.calling.Screen_UI.Guard.Guard_fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Guard.GuardAdapter.TabAdapter;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Add_Visitor;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Chat_Screen;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Home;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Visitor.TodayVisitor_fragment;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Visitor.VisitorHistory_fragment;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

public class Guard_Home_Fragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    LinearLayout one;
    EditText editText;
    String type;
    String numbers;
    FloatingActionButton fabMsg, fabDialler, fabQRcode, fabAddVisitor;
    BottomSheetBehavior behavior;

    public Guard_Home_Fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.guard__home_fragment, container, false);
        type = getArguments().getString("Type");

        initializeView(view);
        SetBottomSheetForDialler(view);
        // Setting ViewPager for each Tabs
        adapter = new TabAdapter(getChildFragmentManager());
        SetViewsForTabView();
        handleListeners();
        return view;

    }

    private void handleListeners() {
        fabAddVisitor.setOnClickListener(this);
        fabMsg.setOnClickListener(this);
        fabDialler.setOnClickListener(this);
        fabQRcode.setOnClickListener(this);

    }

    private void SetViewsForTabView() {

        if (type.equalsIgnoreCase("Home")) {
            ((Guard_Home) getActivity()).setActionBarTitle(getString(R.string.home));
            fabAddVisitor.hide();
            adapter.addFragment(new SocietyContact_fragment(), "SOCIETY CONTACTS");
            adapter.addFragment(new CallLog_fragment(), "CALL LOG");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else if (type.equalsIgnoreCase("Visitor")) {
            ((Guard_Home) getActivity()).setActionBarTitle(getString(R.string.visitors));
            fabAddVisitor.show();
            fabQRcode.hide();
            fabMsg.hide();
            fabDialler.hide();
            adapter.addFragment(new TodayVisitor_fragment(), "TODAY VISITOR");
            adapter.addFragment(new VisitorHistory_fragment(), "PAST VISITOR");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        } else if (type.equalsIgnoreCase("UserPhonebook")) {
            ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.phonebook));
            fabAddVisitor.hide();
            fabMsg.hide();
            fabQRcode.hide();
            adapter.addFragment(new CallLog_fragment(), "CALL LOG");
            adapter.addFragment(new SocietyContact_fragment(), "SOCIETY CONTACTS");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void SetBottomSheetForDialler(View view) {
        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fl_bottomSheet);
        one = frameLayout.findViewById(R.id.one);
        editText = frameLayout.findViewById(R.id.editText);
        LinearLayout CloseDialler = frameLayout.findViewById(R.id.CloseDialler);
        behavior = BottomSheetBehavior.from(frameLayout);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //... Handle the state changes
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                //.. handle sliding if you want to
            }
        });
        CloseDialler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        editText.setShowSoftInputOnFocus(false);
        editText.setOnTouchListener(this);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().isEmpty()) {
                    editText.setText("1");
                    numbers = editText.getText().toString();
                } else {
                    numbers = numbers + "1";
                    editText.setText(numbers);
                }

            }
        });
    }

    private void initializeView(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        fabMsg = (FloatingActionButton) view.findViewById(R.id.fabMsg);
        fabDialler = (FloatingActionButton) view.findViewById(R.id.fabDialler);
        fabAddVisitor = (FloatingActionButton) view.findViewById(R.id.fabAddVisitor);
        fabQRcode = (FloatingActionButton) view.findViewById(R.id.fabQRcode);


        adapter = new TabAdapter(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddVisitor:
                startActivity(new Intent(getContext(), Add_Visitor.class));
                break;
            case R.id.fabMsg:
                startActivity(new Intent(getContext(), Guard_Chat_Screen.class));
                break;
            case R.id.fabDialler:
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) getContext()).getWindowManager()
                        .getDefaultDisplay()
                        .getMetrics(displayMetrics);
                DisplayMetrics dm = new DisplayMetrics();

                int h = dm.heightPixels;
                int w = dm.widthPixels;
                h = h / 60; // 10 is for example for 10% of display height
                w = ((w * 100) / 100); // 20%of display width
                //frameLayout.setLayoutParams(new LinearLayout.LayoutParams(100,100);

                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;

            case R.id.fabQRcode:
                QRCode_fragment fragment2 = new QRCode_fragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment2);
                fragmentTransaction.commit();
                break;
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
        final int DRAWABLE_BOTTOM = 3;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                editText.setText("");
                return true;
            }
        }
        return false;
    }

}