package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.ComplaintAndSuggestion;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.ComplaintAndSuggest_Model;
import com.fire.chat.calling.Screen_UI.User.Adapter.Complaint_Suggestion_adapter;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Pending_Complaint_fragment extends Fragment implements View.OnClickListener {

    ConnectionDetector cd;
    Context context;
    private View view;
    private RecyclerView recyclerView;
    private Complaint_Suggestion_adapter Adapter;
    private RecyclerView.LayoutManager layoutManager;
    String LoginType;
    LinearLayout LayoutForSearch;
    EditText searchVisitor;
    ProgressDialog dialog;
    Boolean isSearching = false;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    LinearLayout emptyDataLayout;
    public Pending_Complaint_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_page_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        try {
            LoginType = getArguments().getString("LoginType");
        } catch (NullPointerException e) {
            LoginType = "";
        }
        initializeViews();
        initializeRecycler();
        LayoutForSearch.setVisibility(View.VISIBLE);
        handleListeners();
        if (cd.isConnectingToInternet()) {
            getPendingComplaintListAPI("");
            Searching();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        LinearLayout myLayout = (LinearLayout) view.findViewById(R.id.my_layout);
        myLayout.requestFocus();
        return view;
    }


    private void initializeViews() {

        LayoutForSearch = (LinearLayout) view.findViewById(R.id.LayoutForSearch);
        searchVisitor = (EditText) view.findViewById(R.id.searchContact);
        recyclerView = (RecyclerView) view.findViewById(R.id.notificationrecycler);
        emptyDataLayout = (LinearLayout) view.findViewById(R.id.emptyDataLayout);

    }

    private void initializeRecycler() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void handleListeners() {

    }

    private void Searching() {
        searchVisitor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isSearching = true;
                getPendingComplaintListAPI(editable.toString());
            }
        });

        searchVisitor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    isSearching = true;

                } else {
                    isSearching = false;
                }
            }
        });
    }

    public void getPendingComplaintListAPI(String searchKey) {
        if (!isSearching) {
            dialog = ProgressDialog.show(context, "", "Please wait...", false);
        }
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            if (LoginType.equalsIgnoreCase("1")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", userDetail.get(SessionManager.SID));
            }
            requestBody.put("stage", "0");
            requestBody.put("keyword", searchKey);
            requestBody.put("database_name", ((GlobalVariables)getActivity().getApplication()).getDatabase_name());
            Call<ComplaintAndSuggest_Model> call = apiInterface.getComplaintList(requestBody);
            call.enqueue(new Callback<ComplaintAndSuggest_Model>() {
                @Override
                public void onResponse(Call<ComplaintAndSuggest_Model> call, Response<ComplaintAndSuggest_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                ComplaintAndSuggest_Model complaintAndSuggestModel = response.body();
                                if (!response.body().getData().isEmpty()) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyDataLayout.setVisibility(View.GONE);
                                    Adapter = new Complaint_Suggestion_adapter(context, complaintAndSuggestModel, LoginType, "Pending");
                                    recyclerView.setAdapter(Adapter);
                                    Adapter.notifyDataSetChanged();
                                } else {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyDataLayout.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    if (!isSearching) {
                        dialog.cancel();
                    }


                }

                @Override
                public void onFailure(Call<ComplaintAndSuggest_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    if (!isSearching) {
                        dialog.cancel();
                    }
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }


    }
}

