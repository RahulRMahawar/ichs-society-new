package com.fire.chat.calling.sessionData;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_SocietyList_Activity;
import com.fire.chat.calling.Screen_UI.Login_Screen;
import com.fire.chat.calling.Screen_UI.Select_Activity;

import java.util.HashMap;


/**
 * Created by Rahul Mahawar.
 */

public class SessionManager {

    private static final String PREF_NAME = "PREF_NAME";
    private static final String IS_LOGIN = "IS_LOGIN";
    private static final String KEY_FILE_PATH = "KEY_FILE_PATH";
    private static final String DATABASE_PATH = "DATABASE_PATH";

    public static final String USERID = "USERID";
    public static final String SID = "SID";
    public static final String KEY_USERTYPE = "KEY_USERTYPE";
    public static final String KEY_USERFNAME = "KEY_USERFNAME";
    public static final String KEY_USERLNAME = "KEY_USERLNAME";
    public static final String KEY_USEREMAIL = "KEY_USEREMAIL";
    public static final String KEY_USERNO = "KEY_USERNO";
    public static final String KEY_ROLES = "KEY_ROLES";
    public static final String LOGIN_TYPE = "LOGIN_TYPE";

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */






   /* public void createLoginSession(String userId, String userfname, String userlname,
                                   String useremail, String userno, String roles) {

        editor.putBoolean(IS_LOGIN, true);
        editor.putInt(LOGIN_TYPE, 1);
        editor.putString(USERID, userId);

        editor.putString(KEY_USERFNAME, userfname);
        editor.putString(KEY_USERLNAME, userlname);
        editor.putString(KEY_USEREMAIL, useremail);
        editor.putString(KEY_USERNO, userno);
        editor.putString(KEY_ROLES, roles);
        editor.commit();


    }*/
    public void createLoginSession(String loginType, String userId, String sId) {

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(LOGIN_TYPE, loginType);
        editor.putString(USERID, userId);
        editor.putString(SID, sId);
        editor.commit();


    }


    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            ///commented

            Intent i = new Intent(_context, Select_Activity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

        } else {
            ///commented
            Intent i = new Intent(_context, User_SocietyList_Activity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }


    }


  /*  public String getLoginSavedDetails() {
        String user = null;
        user= pref.getString(USERID, "");
        return user;
    }*/

    public HashMap<String, String> getLoginSavedDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(USERID, pref.getString(USERID, ""));
        user.put(SID, pref.getString(SID, ""));
        user.put(LOGIN_TYPE, pref.getString(LOGIN_TYPE, ""));

        return user;
    }

   /* public HashMap<String, String> getLoginSavedDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(USERID, pref.getString(USERID, ""));
        user.put(KEY_USERFNAME, pref.getString(KEY_USERFNAME, ""));
        user.put(KEY_USERLNAME, pref.getString(KEY_USERLNAME, ""));
        user.put(KEY_USEREMAIL, pref.getString(KEY_USEREMAIL, ""));
        user.put(KEY_USERNO, pref.getString(KEY_USERNO, ""));
        user.put(KEY_ROLES, pref.getString(KEY_ROLES, ""));

        return user;
    }*/


    public void logoutUser() {
        editor.clear();
        editor.commit();
        ////Commeted
        Intent i = new Intent(_context, Select_Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    public void saveFilePath(String path) {
        editor.putString(KEY_FILE_PATH, path);
        editor.commit();
    }

    public void saveDataBasePath(String database) {
        editor.putString(DATABASE_PATH, database);
        editor.commit();
    }

    public String getDatabasePath() {
        return pref.getString(DATABASE_PATH, "");
    }

    public String getFilePath() {
        return pref.getString(KEY_FILE_PATH, null);
    }


}