package com.fire.chat.calling.Screen_UI.User.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Amenities_model {

    @SerializedName("replyCode")
    private String replyCode;
    @SerializedName("replyMsg")
    private String replyMsg;
    @SerializedName("data")
    public List<DataValue> data;

    public String getReplyCode() {
        return replyCode;
    }

    public void setReplyCode(String replyCode) {
        this.replyCode = replyCode;
    }

    public String getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(String replyMsg) {
        this.replyMsg = replyMsg;
    }

    public List<DataValue> getData() {
        return data;
    }

    public class DataValue implements Parcelable {

        String id, title, image, description, time_from, time_to;

        protected DataValue(Parcel in) {
            id = in.readString();
            title = in.readString();
            image = in.readString();
            description = in.readString();
            time_from = in.readString();
            time_to = in.readString();
        }

        public final Creator<DataValue> CREATOR = new Creator<DataValue>() {
            @Override
            public DataValue createFromParcel(Parcel in) {
                return new DataValue(in);
            }

            @Override
            public DataValue[] newArray(int size) {
                return new DataValue[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTime_from() {
            return time_from;
        }

        public void setTime_from(String time_from) {
            this.time_from = time_from;
        }

        public String getTime_to() {
            return time_to;
        }

        public void setTime_to(String time_to) {
            this.time_to = time_to;
        }

        @Override
        public int describeContents() {
            return hashCode();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(title);
            dest.writeString(image);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeString(time_from);
            dest.writeString(time_to);

        }
    }
}