package com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.fire.chat.calling.Interfaces.DashBoardItemClickListener;
import com.fire.chat.calling.Interfaces.RetryClickListener;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_Profile;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.CommonTabView_fragment;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Notification_screen;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.PermissionsUtil;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;

/*import com.fire.chat.calling.Screen_UI.User.fragments.Notification_fragment;
import com.fire.chat.calling.Screen_UI.User.fragments.Profile_screen_fragment;*/


public class DashboardActivity extends AppCompatActivity implements DashBoardItemClickListener, RetryClickListener {

    private static final int TIME_INTERVAL = 2000;
    boolean flag = false;
    private long mBackPressed;
    private Toolbar toolbar;
    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    TextView title;
    private SessionManager sessionManager;
    ImageView profile, notification;
    RelativeLayout requestLayout;
    String USERTYPE, HIDEDASHBOARD;
    private HashMap<String, String> userDetail;
    private static final int PERMISSION_REQUEST_CODE = 159;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dashboard_activity);

        context = DashboardActivity.this;
        sessionManager = new SessionManager(this);
        userDetail = sessionManager.getLoginSavedDetails();
        requestLayout = findViewById(R.id.RequestLayout);


        if (userDetail.get(SessionManager.LOGIN_TYPE).equals("2")) {
            SetViewsForUSER();
            HIDEDASHBOARD = "true";
        } else {
            SetViewsForADMIN();
            HIDEDASHBOARD = "false";
        }

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        profile = toolbar.findViewById(R.id.profile);
        notification = toolbar.findViewById(R.id.notificationimage);


        callHomeFragment();

        requestPermissions();

    }

    private void SetViewsForADMIN() {

    }

    private void SetViewsForUSER() {
        requestLayout.setVisibility(View.GONE);
        PopUpForVisitorRequest();

    }

    private void PopUpForVisitorRequest() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
        builder.setTitle("Visitor Alert");
        builder.setMessage("You Have A Visitor Waiting.");
        builder.setPositiveButton("CHECK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, new CommonTabView_fragment());
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }
                    }
                });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setCancelable(false);
        builder.show();

    }

    private void callHomeFragment() {
        title.setText(getString(R.string.home));
        Bundle bundle = new Bundle();
        DashBoardFragment dashBoardFragment = new DashBoardFragment();
        dashBoardFragment.setArguments(bundle);
        AppUtils.setFragment(dashBoardFragment, true, DashboardActivity.this, R.id.container);

    }


    @Override
    public void onItemClicked(int position) {
    }


    @Override
    public void onRetryClick() {

    }


    public void home(View view) {

        callHomeFragment();

    }

    public void requests(View view) {

        String backStateName = context.getClass().getName();
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) {
            CommonTabView_fragment fragment = new CommonTabView_fragment();
            Bundle bundle = new Bundle();
            bundle.putString("CalledFor", "Requests");
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();
        }
    }

    public void profile(View view) {

        startActivity(new Intent(this, User_Profile.class));
    }

    public void notification(View view) {

        startActivity(new Intent(this, Notification_screen.class));

    }

    public void LogOUT(View view) {
        FirebaseAuth.getInstance().signOut();
        sessionManager.logoutUser();
        finish();

    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PermissionsUtil.permissions, PERMISSION_REQUEST_CODE);
    }

    public void setActionBarTitle(String name) {
        title.setText(name);
    }

    /*  @Override
      protected void onResumeFragments() {
          super.onResumeFragments();

          callHomeFragment();
      }
      @Override
      protected void onResume() {
          super.onResume();


      }*/
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
   /* @Override
    public void onRestart() {
        super.onRestart();
        // do some stuff here
        startActivity(new Intent(context, DashboardActivity.class));
    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //check if user granted permissions
        //then check if he is already logged in start nextActivity
        // if he is not logged in then launch login activity
        //if he does not grant the permissions then show alert
        // dialog to make him grant the permissions9
        if (!PermissionsUtil.permissionsGranted(grantResults)) {
            requestPermissions();
        }
    }

}
