package com.fire.chat.calling.Rough.ForMultipleImageInRecycler;

import android.net.Uri;

public class ListModel {
    public String id;
    public Uri image;

    public ListModel(String id, Uri image) {
        this.id = id;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public Uri getImage() {
        return image;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(Uri image) {
        this.image = image;
    }
}
