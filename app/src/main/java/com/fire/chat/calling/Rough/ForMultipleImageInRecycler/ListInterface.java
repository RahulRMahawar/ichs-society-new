package com.fire.chat.calling.Rough.ForMultipleImageInRecycler;

public interface ListInterface {

    void onClickDelete(String id, int position);

    void onClickAdd(String id, int position);

    void onClickImage(String id, int position);

}
