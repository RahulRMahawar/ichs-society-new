package com.fire.chat.calling.Screen_UI.Admin.Screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.User.UI_Screens.Request_Approval_Info;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_society extends AppCompatActivity implements View.OnClickListener {
    ConnectionDetector cd;
    private Context context;
    Button done;
    EditText name, address, city, state, pocName, MobileNo, designation;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.society_register_activity);
        context = Add_society.this;
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        cd = new ConnectionDetector(context);
        _initializeView();
        handleListeners();
    }


    private void _initializeView() {
        name = findViewById(R.id.name);
        address = findViewById(R.id.address);
        city = findViewById(R.id.city);
        state = findViewById(R.id.state);
        pocName = findViewById(R.id.pocName);
        MobileNo = findViewById(R.id.MobileNo);
        designation = findViewById(R.id.designation);
        done = findViewById(R.id.done);
    }

    private void handleListeners() {
        done.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.done:
                validate();
                break;
        }
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(name.getText().toString().trim()) || name.length() <= 0) {
                name.setError(getString(R.string.validate_userName));
                name.requestFocus();
            } else if (TextUtils.isEmpty(address.getText().toString().trim()) || address.length() <= 0) {
                address.setError(getString(R.string.validate_address));
                address.requestFocus();
            } else if (TextUtils.isEmpty(city.getText().toString().trim()) || city.length() <= 0) {
                city.setError(getString(R.string.validate_city));
                city.requestFocus();
            } else if (TextUtils.isEmpty(state.getText().toString().trim()) || state.length() <= 0) {
                state.setError(getString(R.string.validate_state));
                state.requestFocus();
            } else if (TextUtils.isEmpty(pocName.getText().toString().trim()) || pocName.length() <= 0) {
                pocName.setError(getString(R.string.validate_poc_name));
                pocName.requestFocus();
            } else if (!AppUtils.isValidPhone(MobileNo.getText().toString())) {
                MobileNo.setError(getString(R.string.validate_poc_no));
                MobileNo.requestFocus();
            } else if (TextUtils.isEmpty(designation.getText().toString().trim()) || designation.length() <= 0) {
                designation.setError(getString(R.string.validate_designation));
                designation.requestFocus();
            } else {
                SubmitRegisterApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitRegisterApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            //requestBody.put("role_id", ((GlobalVariables) context.getApplicationContext()).getRole_id());
            requestBody.put("lead_name", name.getText().toString());
            requestBody.put("address", address.getText().toString());
            requestBody.put("city", city.getText().toString());
            requestBody.put("state", state.getText().toString());
            requestBody.put("poc_name", pocName.getText().toString());
            requestBody.put("poc_phone", MobileNo.getText().toString());
            requestBody.put("poc_designation", designation.getText().toString());
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("database_name", ((GlobalVariables) getApplicationContext()).getDatabase_name());
            Call<Verification_Model> call = apiInterface.addSociety(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(Add_society.this, "Register Request Sent Successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(context, Request_Approval_Info.class));
                            finish();
                        } else {
                            Toast.makeText(Add_society.this, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
