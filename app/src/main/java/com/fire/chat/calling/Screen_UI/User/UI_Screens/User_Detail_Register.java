package com.fire.chat.calling.Screen_UI.User.UI_Screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.SocietyList_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fire.chat.calling.sessionData.CommonMethod.isEmailValid;

public class User_Detail_Register extends AppCompatActivity implements View.OnClickListener {
    ConnectionDetector cd;
    private Context context;
    Button done;
    EditText userName, userEmail, userMobileNo, userFlatNo, userVehicleNo;
    Spinner societySpinner;
    String societyName, societyID;
    ArrayList<String> SocietyNames = new ArrayList<String>();
    ArrayList<String> SocietyID = new ArrayList<String>();
    SocietyList_Model responseBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_detail_register_activity);
        context = User_Detail_Register.this;
        cd = new ConnectionDetector(context);
        _initializeView();
        handleListeners();
        loadSocietySpinner();
    }


    private void _initializeView() {
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userEmail);
        userMobileNo = findViewById(R.id.userMobileNo);
        userFlatNo = findViewById(R.id.userFlatNo);
        userVehicleNo = findViewById(R.id.userVehicleNo);
        societySpinner = findViewById(R.id.societySpinner);
        done = findViewById(R.id.done);
    }

    private void handleListeners() {
        done.setOnClickListener(this);
        societySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (responseBody == null) {

                } else {
                    societyName = adapterView.getSelectedItem().toString();
                    societyID = SocietyID.get(adapterView.getSelectedItemPosition());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadSocietySpinner() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            /*Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", receivedUserNo);*/
            Call<SocietyList_Model> call = apiInterface.getAllSocietyList();
            call.enqueue(new Callback<SocietyList_Model>() {
                @Override
                public void onResponse(Call<SocietyList_Model> call, Response<SocietyList_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));

                                responseBody = response.body();
                                SocietyNames.add("Select your society");
                                SocietyID.add("0");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    SocietyNames.add(response.body().getData().get(i).getName());
                                    SocietyID.add(response.body().getData().get(i).getId());
                                }

                                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>
                                        (context, android.R.layout.simple_spinner_dropdown_item, SocietyNames);
                                societySpinner.setAdapter(categoryAdapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<SocietyList_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.done:
                validate();
                break;
        }
    }

    private void validate() {
        if (cd.isConnectingToInternet()) {

            if (TextUtils.isEmpty(userName.getText().toString().trim()) || userName.length() <= 0) {
                userName.setError(getString(R.string.validate_userName));
                userName.requestFocus();
            } else if (!isEmailValid(userEmail.getText().toString())) {
                userEmail.setError(getResources().getString(R.string.validate_userEmail));
                userEmail.requestFocus();
            } else if (TextUtils.isEmpty(userMobileNo.getText().toString().trim()) || userMobileNo.length() <= 0) {
                userMobileNo.setError(getString(R.string.validate_userMobile));
                userMobileNo.requestFocus();
            } else if (TextUtils.isEmpty(userFlatNo.getText().toString().trim()) || userFlatNo.length() <= 0) {
                userFlatNo.setError(getString(R.string.validate_userFlatNo));
                userFlatNo.requestFocus();
            } else if (TextUtils.isEmpty(userVehicleNo.getText().toString().trim()) || userVehicleNo.length() <= 0) {
                userVehicleNo.setError(getString(R.string.validate_VehicleNo));
                userVehicleNo.requestFocus();
            } else if (societyName.equalsIgnoreCase("Select your society")) {
                Toast.makeText(context, "Please select your society", Toast.LENGTH_SHORT).show();
            } else {
                SubmitRegisterApi();
            }

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    public void SubmitRegisterApi() {

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", false);

        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            final Map<String, String> requestBody = new HashMap<>();
            requestBody.put("role_id", ((GlobalVariables) context.getApplicationContext()).getRole_id());
            requestBody.put("email", userEmail.getText().toString());
            requestBody.put("phone", userMobileNo.getText().toString());
            requestBody.put("fullName", userName.getText().toString());
            requestBody.put("client_id", societyID);
            requestBody.put("flat_no", userFlatNo.getText().toString());
            requestBody.put("device_token", "");


            Call<Verification_Model> call = apiInterface.UserSignUp(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            Toast.makeText(User_Detail_Register.this, "Register Request Sent Successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(context, Request_Approval_Info.class));
                        } else {
                            Toast.makeText(User_Detail_Register.this, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
