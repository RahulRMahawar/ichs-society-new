package com.fire.chat.calling.Screen_UI.User.Model;

public class NoticeAndEvent_get_set {

    private String name;
    private String des;
    private String date;

    public NoticeAndEvent_get_set(String name, String des, String date) {
        this.name = name;
        this.des = des;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}