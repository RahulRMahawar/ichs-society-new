package com.fire.chat.calling.Screen_UI.Common_UI.CommonAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_FlatList_Activity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.SocietyList_Model;
import com.fire.chat.calling.Screen_UI.Guard.Guard_Screens.Guard_Home;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.sessionData.SessionManager;

import java.util.HashMap;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

public class Society_List_adapter extends RecyclerView.Adapter<Society_List_adapter.ViewHolder> {

    private Context context;
    AlertDialog.Builder builder;
    String type;
    SocietyList_Model model;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;

    public Society_List_adapter(Context context, SocietyList_Model model) {
        this.context = context;
        this.model = model;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_society_list_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        sessionManager = new SessionManager(context);
        userDetail = sessionManager.getLoginSavedDetails();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(model.getData().get(position));
        holder.name.setText(model.getData().get(position).getName());

    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((GlobalVariables) context.getApplicationContext())
                            .setDatabase_name(model.getData().get(getAdapterPosition()).getDatabase_name());

                    if ( userDetail.get(SessionManager.LOGIN_TYPE).equalsIgnoreCase("5")) {
                        context.startActivity(new Intent(context, Guard_Home.class));
                        ((Activity) context).finishAffinity();
                    }
                    else {
                        context.startActivity(new Intent(context, User_FlatList_Activity.class));
                    }

                }
            });

        }
    }

}