/*
 *  Copyright 2015 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package com.fire.chat.calling.calling;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Utils.Database.CallLogData;
import com.fire.chat.calling.Utils.Database.FireAppLocalDatabase;
import com.fire.chat.calling.Utils.IntentUtils;
import com.fire.chat.calling.Utils.constants.FireCallType;
import com.fire.chat.calling.Utils.constants.FireConstants;
import com.fire.chat.calling.Utils.constants.FireManager;
import com.fire.chat.calling.databinding.ActivityAudioCallBinding;
import com.fire.chat.calling.web_rtc.AppRTCAudioManager;
import com.fire.chat.calling.web_rtc.AppRTCClient;
import com.fire.chat.calling.web_rtc.PeerConnectionClient;
import com.fire.chat.calling.web_rtc.WebSocketRTCClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoRenderer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import static com.fire.chat.calling.web_rtc.util.Constants.CAPTURE_PERMISSION_REQUEST_CODE;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_HEIGHT_CONNECTED;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_HEIGHT_CONNECTING;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_WIDTH_CONNECTED;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_WIDTH_CONNECTING;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_X_CONNECTED;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_X_CONNECTING;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_Y_CONNECTED;
import static com.fire.chat.calling.web_rtc.util.Constants.LOCAL_Y_CONNECTING;
import static com.fire.chat.calling.web_rtc.util.Constants.REMOTE_HEIGHT;
import static com.fire.chat.calling.web_rtc.util.Constants.REMOTE_WIDTH;
import static com.fire.chat.calling.web_rtc.util.Constants.REMOTE_X;
import static com.fire.chat.calling.web_rtc.util.Constants.REMOTE_Y;
import static com.fire.chat.calling.web_rtc.util.Constants.STAT_CALLBACK_PERIOD;
import static org.webrtc.RendererCommon.ScalingType.SCALE_ASPECT_FILL;
import static org.webrtc.RendererCommon.ScalingType.SCALE_ASPECT_FIT;

/**
 * Activity for peer connection call setup, call waiting
 * and call view.
 */

public class AudioCallActivity extends AppCompatActivity
        implements AppRTCClient.SignalingEvents, PeerConnectionClient.PeerConnectionEvents, OnCallEvents, SensorEventListener {

    private static final String LOG_TAG = "AudioCallActivity";

    private PeerConnectionClient peerConnectionClient;
    private AppRTCClient appRtcClient;
    private AppRTCClient.SignalingParameters signalingParameters;
    private AppRTCAudioManager audioManager;
    private EglBase rootEglBase;
    private final List<VideoRenderer.Callbacks> remoteRenderers = new ArrayList<>();
    private Toast logToast;
    private boolean activityRunning;

    private AppRTCClient.RoomConnectionParameters roomConnectionParameters;
    private PeerConnectionClient.PeerConnectionParameters peerConnectionParameters;

    private boolean iceConnected;
    private boolean isError;
    private long callStartedTimeMs;
    private boolean micEnabled = true;
    private boolean videoEnabled = false;
    private boolean speakerEnabled = false;

    private ActivityAudioCallBinding binding;

    private SensorManager mSensorManager;
    private Sensor mProximity;
    private static final int SENSOR_SENSITIVITY = 4;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private int field = 0x00000020;

    private String uid, roomId, userToken;
    private String ownImg, ownName, callingPersonImg;
    private int callType;

    Handler handler;
    Runnable myRunnable;
    private int finishTime = 35; //secs

    private AudioManager m_amAudioManager;
    private MediaPlayer mediaPlayer;
    private FireAppLocalDatabase appLocalDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD | LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_audio_call);

        remoteRenderers.add(binding.remoteVideoView);

        appLocalDatabase = new FireAppLocalDatabase(this);

        // Create Video Renderers.
        rootEglBase = EglBase.create();
        binding.localVideoView.init(rootEglBase.getEglBaseContext(), null);
        binding.remoteVideoView.init(rootEglBase.getEglBaseContext(), null);

        binding.localVideoView.setZOrderMediaOverlay(true);
        binding.localVideoView.setEnableHardwareScaler(true);
        binding.remoteVideoView.setEnableHardwareScaler(true);
        updateVideoView();

        // Get Intent Parameters
        uid = getIntent().getStringExtra(IntentUtils.UID);
        roomId = getIntent().getStringExtra(IntentUtils.RAND_ID);
        callType = getIntent().getIntExtra(IntentUtils.PHONE_CALL_TYPE, 0);
        callingPersonImg = getIntent().getStringExtra(IntentUtils.CALLING_PERSON_IMAGE);


        Log.d(LOG_TAG, "Room ID: " + roomId);
        if (roomId == null || roomId.length() == 0) {
            logAndToast("Something Went Wrong");
            Log.e(LOG_TAG, "Incorrect room ID in intent!");
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                String type, speed;
                if (isConnected) {
                    switch (connectionType) {
                        case -1:
                            type = "Any";
                            break;
                        case ConnectivityManager.TYPE_WIFI:
                            type = "Wifi";
                            break;
                        case ConnectivityManager.TYPE_MOBILE:
                            type = "Mobile";
                            break;
                        default:
                            type = "Unknown";
                            break;
                    }

                    if (isFast) {
                        speed = "Fast";
                        //Toast.makeText(AudioCallActivity.this, "Fast", Toast.LENGTH_SHORT).show();
                    } else if (type.equals("Unknown")) {
                        speed = "N/A";
                    } else {
                        speed = "Slow";
                        Toast.makeText(AudioCallActivity.this, "Slow Connection \n Please Consider a Text Chat", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    type = "None";
                    speed = "N/A";
                    Toast.makeText(AudioCallActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        if (callType == FireCallType.INCOMING) {
            FireConstants.usersRef.child(uid)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();
                            /*appLocalDatabase.insertCallLogData(uid, callerName, callerPhone, thumbImg,
                                    "audio",
                                    "MISSED",
                                    "" + System.currentTimeMillis(),
                                    "INCOMING");*/
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

            Glide.with(AudioCallActivity.this).load(callingPersonImg).placeholder(R.drawable.user_img).into(binding.userImg);
        }

        if (callType == FireCallType.OUTGOING) {

            FireConstants.usersRef.child(uid)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                            String callerName = (String) dataSnapshot.child("name").getValue();
                            String callerPhone = (String) dataSnapshot.child("phone").getValue();
                            appLocalDatabase.insertCallLogData(uid, callerName, callerPhone, thumbImg,
                                    "audio",
                                    "MISSED",
                                    "" + System.currentTimeMillis(),
                                    "OUTGOING");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

            FireConstants.usersRef.child(FireManager.getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d("DataSnapOwn", dataSnapshot.toString());
                            ownImg = (String) dataSnapshot.child("photo").getValue();
                            ownName = (String) dataSnapshot.child("name").getValue();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

            m_amAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            m_amAudioManager.setMode(AudioManager.MODE_IN_CALL);
            m_amAudioManager.setSpeakerphoneOn(false);
            Uri myUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.call_dialing);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
            mediaPlayer.reset();
            mediaPlayer.setLooping(true);
            try {
                mediaPlayer.setDataSource(this, myUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

            handler = new Handler();
            myRunnable = new Runnable() {
                public void run() {
                    Toast.makeText(AudioCallActivity.this, "Call Not Connected", Toast.LENGTH_SHORT).show();
                    AudioCallActivity.this.finish();
                }
            };
            handler.postDelayed(myRunnable, finishTime * 1000);

        }

        // If capturing format is not specified for screencapture, use screen resolution.
        peerConnectionParameters = PeerConnectionClient.PeerConnectionParameters.createDefault();

        // Create connection client. Use DirectRTCClient if room name is an IP otherwise use the
        // standard WebSocketRTCClient.
        appRtcClient = new WebSocketRTCClient(this);

        // Create connection parameters.
        roomConnectionParameters = new AppRTCClient.RoomConnectionParameters("https://appr.tc", roomId, false);

        setupListeners();

        peerConnectionClient = PeerConnectionClient.getInstance();
        peerConnectionClient.createPeerConnectionFactory(this, peerConnectionParameters, this);

        // Get an instance of the sensor service, and use that to get an instance of
        // a particular sensor.
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        try {
            field = PowerManager.class.getClass().getField("PROXIMITY_SCREEN_OFF_WAKE_LOCK").getInt(null);
        } catch (Throwable ignored) {
            Log.e("POWER_MANAGER_ERR", ignored.getMessage());
        }

        startCall();
    }

    private void setupListeners() {
        binding.buttonCallDisconnect.setOnClickListener(view -> onCallHangUp());

        binding.buttonCallToggleMic.setOnClickListener(view -> {
            boolean enabled = onToggleMic();
            binding.buttonCallToggleMic.setAlpha(enabled ? 1.0f : 0.4f);
        });

        binding.btnSwitchSpeaker.setOnClickListener(view -> {
            boolean enabled = onToggleSpeaker();
            binding.btnSwitchSpeaker.setAlpha(enabled ? 1.0f : 0.4f);
        });
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != CAPTURE_PERMISSION_REQUEST_CODE) {
            return;
        }
        startCall();
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this);
    }

    private boolean captureToTexture() {
        return true;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        Logging.d(LOG_TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(LOG_TAG, "Creating front facing camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Logging.d(LOG_TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(LOG_TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    // Activity interfaces
    @Override
    public void onPause() {
        super.onPause();
        activityRunning = false;
        // Don't stop the video when using screencapture to allow user to show other apps to the remote
        // end.
        if (peerConnectionClient != null) {
            peerConnectionClient.stopVideoSource();
        }

        mSensorManager.unregisterListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        activityRunning = true;
        // Video is not paused for screencapture. See onPause.
        if (peerConnectionClient != null) {
            peerConnectionClient.startVideoSource();
        }

        mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        Tovuti.from(this).stop();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String message) {
        Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
        if (message.equals("Disconnect")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAndRemoveTask();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        disconnect();

        if (handler != null) {
            if (myRunnable != null) {
                handler.removeCallbacks(myRunnable);
            }
        }

        if (logToast != null) {
            logToast.cancel();
        }
        activityRunning = false;
        rootEglBase.release();

        if (callType == FireCallType.OUTGOING) {
            mediaPlayer.release();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        } else {
            finish();
        }

        super.onDestroy();
    }

    // CallFragment.OnCallEvents interface implementation.
    @Override
    public void onCallHangUp() {
        disconnect();
    }

    @Override
    public void onCameraSwitch() {
        if (peerConnectionClient != null) {
            peerConnectionClient.switchCamera();
        }
    }

    @Override
    public void onCaptureFormatChange(int width, int height, int framerate) {
        if (peerConnectionClient != null) {
            peerConnectionClient.changeCaptureFormat(width, height, framerate);
        }
    }

    @Override
    public boolean onToggleMic() {
        if (peerConnectionClient != null) {
            micEnabled = !micEnabled;
            peerConnectionClient.setAudioEnabled(micEnabled);
        }
        return micEnabled;
    }

    @Override
    public boolean onToggleVideo() {
        if (peerConnectionClient != null) {
            videoEnabled = !videoEnabled;
            peerConnectionClient.setVideoEnabled(videoEnabled);
        }
        return videoEnabled;
    }

    @Override
    public boolean onToggleSpeaker() {
        if (peerConnectionClient != null) {
            speakerEnabled = !speakerEnabled;
            if (speakerEnabled) {
                audioManager.setDefaultAudioDevice(AppRTCAudioManager.AudioDevice.SPEAKER_PHONE);
            } else {
                audioManager.setDefaultAudioDevice(AppRTCAudioManager.AudioDevice.EARPIECE);
            }
        }
        return speakerEnabled;
    }

    private void updateVideoView() {
        binding.remoteVideoLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
        binding.remoteVideoView.setScalingType(SCALE_ASPECT_FILL);
        binding.remoteVideoView.setMirror(false);

        if (iceConnected) {
            binding.localVideoLayout.setPosition(
                    LOCAL_X_CONNECTED, LOCAL_Y_CONNECTED, LOCAL_WIDTH_CONNECTED, LOCAL_HEIGHT_CONNECTED);
            binding.localVideoView.setScalingType(SCALE_ASPECT_FIT);
        } else {
            binding.localVideoLayout.setPosition(
                    LOCAL_X_CONNECTING, LOCAL_Y_CONNECTING, LOCAL_WIDTH_CONNECTING, LOCAL_HEIGHT_CONNECTING);
            binding.localVideoView.setScalingType(SCALE_ASPECT_FILL);
        }
        binding.localVideoView.setMirror(true);

        binding.localVideoView.requestLayout();
        binding.remoteVideoView.requestLayout();
    }

    private void startCall() {
        callStartedTimeMs = System.currentTimeMillis();

        // Start room connection.
        //logAndToast(getString(R.string.connecting_to, roomConnectionParameters.roomUrl));
        logAndToast("Connecting");
        appRtcClient.connectToRoom(roomConnectionParameters);

        // Create and audio manager that will take care of audio routing,
        // audio modes, audio device enumeration etc.
        audioManager = AppRTCAudioManager.create(this);
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        Log.d(LOG_TAG, "Starting the audio manager...");
        audioManager.start(this::onAudioManagerDevicesChanged);
        audioManager.setDefaultAudioDevice(AppRTCAudioManager.AudioDevice.EARPIECE);
    }

    // Should be called from UI thread
    private void callConnected() {
        if (callType == FireCallType.OUTGOING) {
            mediaPlayer.stop();
        }
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        Log.i(LOG_TAG, "Call connected: delay=" + delta + "ms");
        if (peerConnectionClient == null || isError) {
            Log.w(LOG_TAG, "Call is connected in closed or error state");
            return;
        }
        // Update video view.
        updateVideoView();
        // Enable statistics callback.
        peerConnectionClient.enableStatsEvents(true, STAT_CALLBACK_PERIOD);

        FireConstants.usersRef.child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("DataSnapOwn", dataSnapshot.toString());
                        String thumbImg = (String) dataSnapshot.child("thumbImg").getValue();
                        String callerName = (String) dataSnapshot.child("name").getValue();
                        String callerPhone = (String) dataSnapshot.child("phone").getValue();

                        CallLogData callLogData = appLocalDatabase.getCallLogList().get(0);

                        callLogData.setUserName(callerName);
                        callLogData.setUserNumber(callerPhone);
                        callLogData.setUserImage(thumbImg);
                        callLogData.setCallTime("" + System.currentTimeMillis());

                        appLocalDatabase.updateCallLogList(callLogData, 1);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    // This method is called when the audio manager reports audio device change,
    // e.g. from wired headset to speakerphone.
    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
        Log.d(LOG_TAG, "onAudioManagerDevicesChanged: " + availableDevices + ", "
                + "selected: " + device);
        // TODO(henrika): add callback handler.
    }

    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect() {

        disconnectCall(userToken, roomId);

        activityRunning = false;
        if (appRtcClient != null) {
            appRtcClient.disconnectFromRoom();
            appRtcClient = null;
        }
        if (peerConnectionClient != null) {
            peerConnectionClient.close();
            peerConnectionClient = null;
        }
        binding.localVideoView.release();
        binding.remoteVideoView.release();
        if (audioManager != null) {
            audioManager.stop();
            audioManager = null;
        }
        if (iceConnected && !isError) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    private void disconnectWithErrorMessage(final String errorMessage) {
        if (!activityRunning) {
            Log.e(LOG_TAG, "Critical error: " + errorMessage);
            disconnect();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(getText(R.string.channel_error_title))
                    .setMessage(errorMessage)
                    .setCancelable(false)
                    .setNeutralButton(R.string.ok,
                            (dialog, id) -> {
                                dialog.cancel();
                                disconnect();
                            })
                    .create()
                    .show();
        }
    }

    // Log |msg| and Toast about it.
    private void logAndToast(String msg) {
        Log.d(LOG_TAG, msg);
        if (logToast != null) {
            logToast.cancel();
        }
        logToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        logToast.show();
    }

    private void reportError(final String description) {
        runOnUiThread(() -> {
            if (!isError) {
                isError = true;
                disconnectWithErrorMessage(description);
            }
        });
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer;
        if (useCamera2()) {
            Logging.d(LOG_TAG, "Creating capturer using camera2 API.");
            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {
            Logging.d(LOG_TAG, "Creating capturer using camera1 API.");
            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
        }
        if (videoCapturer == null) {
            reportError("Failed to open camera");
            return null;
        }
        return videoCapturer;
    }

    // -----Implementation of AppRTCClient.AppRTCSignalingEvents ---------------
    // All callbacks are invoked from websocket signaling looper thread and
    // are routed to UI thread.
    private void onConnectedToRoomInternal(final AppRTCClient.SignalingParameters params) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;

        signalingParameters = params;
        //logAndToast("Creating peer connection, delay=" + delta + "ms");
        VideoCapturer videoCapturer = null;
        /*if (peerConnectionParameters.videoCallEnabled) {
            videoCapturer = createVideoCapturer();
        }*/
        peerConnectionClient.createPeerConnection(rootEglBase.getEglBaseContext(), binding.localVideoView,
                remoteRenderers, videoCapturer, signalingParameters);

        if (signalingParameters.initiator) {
            logAndToast("Creating OFFER...");
            // Create offer. Offer SDP will be sent to answering client in
            // PeerConnectionEvents.onLocalDescription event.
            peerConnectionClient.createOffer();
        } else {
            if (params.offerSdp != null) {
                peerConnectionClient.setRemoteDescription(params.offerSdp);
                logAndToast("Creating ANSWER...");
                // Create answer. Answer SDP will be sent to offering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createAnswer();
            }
            if (params.iceCandidates != null) {
                // Add remote ICE candidates from room.
                for (IceCandidate iceCandidate : params.iceCandidates) {
                    peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                }
            }
        }
    }

    @Override
    public void onConnectedToRoom(final AppRTCClient.SignalingParameters params) {

        runOnUiThread(() -> onConnectedToRoomInternal(params));
    }

    @Override
    public void onRemoteDescription(final SessionDescription sdp) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(() -> {
            if (peerConnectionClient == null) {
                Log.e(LOG_TAG, "Received remote SDP for non-initialized peer connection.");
                return;
            }
            //logAndToast("Received remote " + sdp.type + ", delay=" + delta + "ms");
            peerConnectionClient.setRemoteDescription(sdp);
            if (!signalingParameters.initiator) {
                logAndToast("Creating ANSWER...");
                // Create answer. Answer SDP will be sent to offering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createAnswer();
            }
        });
    }

    @Override
    public void onRemoteIceCandidate(final IceCandidate candidate) {
        runOnUiThread(() -> {
            if (peerConnectionClient == null) {
                Log.e(LOG_TAG, "Received ICE candidate for a non-initialized peer connection.");
                return;
            }
            peerConnectionClient.addRemoteIceCandidate(candidate);
        });
    }

    @Override
    public void onRemoteIceCandidatesRemoved(final IceCandidate[] candidates) {
        runOnUiThread(() -> {
            if (peerConnectionClient == null) {
                Log.e(LOG_TAG, "Received ICE candidate removals for a non-initialized peer connection.");
                return;
            }
            peerConnectionClient.removeRemoteIceCandidates(candidates);
        });
    }

    @Override
    public void onChannelClose() {
        runOnUiThread(() -> {
            logAndToast("Disconnected");
            disconnect();
        });
    }

    @Override
    public void onChannelError(final String description) {
        reportError(description);
    }

    // -----Implementation of PeerConnectionClient.PeerConnectionEvents.---------
    // Send local peer connection SDP and ICE candidates to remote party.
    // All callbacks are invoked from peer connection client looper thread and
    // are routed to UI thread.
    @Override
    public void onLocalDescription(final SessionDescription sdp) {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(() -> {
            if (appRtcClient != null) {
                if (callType == FireCallType.OUTGOING) {
                    logAndToast("Calling");
                    mediaPlayer.start();
                    powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                    wakeLock = powerManager.newWakeLock(field, getLocalClassName());
                    getUserData(uid);
                }
                if (signalingParameters.initiator) {
                    appRtcClient.sendOfferSdp(sdp);
                } else {
                    appRtcClient.sendAnswerSdp(sdp);
                }
            }
            if (peerConnectionParameters.videoMaxBitrate > 0) {
                Log.d(LOG_TAG, "Set video maximum bitrate: " + peerConnectionParameters.videoMaxBitrate);
                peerConnectionClient.setVideoMaxBitrate(peerConnectionParameters.videoMaxBitrate);
            }
        });
    }

    @Override
    public void onIceCandidate(final IceCandidate candidate) {
        runOnUiThread(() -> {
            if (appRtcClient != null) {
                appRtcClient.sendLocalIceCandidate(candidate);
            }
        });
    }

    @Override
    public void onIceCandidatesRemoved(final IceCandidate[] candidates) {
        runOnUiThread(() -> {
            if (appRtcClient != null) {
                appRtcClient.sendLocalIceCandidateRemovals(candidates);
            }
        });
    }

    @Override
    public void onIceConnected() {
        final long delta = System.currentTimeMillis() - callStartedTimeMs;
        runOnUiThread(() -> {
            if (handler != null) {
                if (myRunnable != null) {
                    handler.removeCallbacks(myRunnable);
                }
            }
            logAndToast("Call Connected");
            iceConnected = true;
            callConnected();
        });
    }

    @Override
    public void onIceDisconnected() {
        runOnUiThread(() -> {
            logAndToast("Call Disconnected");
            iceConnected = false;
            disconnect();
        });
    }

    @Override
    public void onPeerConnectionClosed() {
    }

    @Override
    public void onPeerConnectionStatsReady(final StatsReport[] reports) {
        runOnUiThread(() -> {
        });
    }

    @Override
    public void onPeerConnectionError(final String description) {
        reportError(description);
    }

    private void getUserData(String uid) {
        FireConstants.usersRef.child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("DataSnapUser", dataSnapshot.toString());
                        userToken = (String) dataSnapshot.child("fcmToken").getValue();
                        String callerName = (String) dataSnapshot.child("name").getValue();
                        String callerImage = (String) dataSnapshot.child("photo").getValue();
                        Glide.with(AudioCallActivity.this).load(callerImage).placeholder(R.drawable.user_img).into(binding.userImg);
                        sendCall(userToken, "Calling", ownName, ownImg, roomId);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void sendCall(String token, String title, String callerName, String callerImage, String rand_id) {

        String Legacy_SERVER_KEY = "AIzaSyD1QCiB6NFh0on6ea3F60CfCb35JC9YwQM";
        String msg = callerName;
        String FCM_PUSH_URL = "https://fcm.googleapis.com/fcm/send";

        JSONObject obj = null;
        JSONObject objNotification = null;
        JSONObject dataobjData = null;

        try {

            //MainObj
            obj = new JSONObject();

            //NotificationObj
            objNotification = new JSONObject();
            objNotification.put("body", msg);
            objNotification.put("title", title);
            objNotification.put("sound", "default");
            objNotification.put("tag", token);
            objNotification.put("priority", "high");

            //DataObj
            dataobjData = new JSONObject();
            dataobjData.put("text", msg);
            dataobjData.put("title", title);
            dataobjData.put("type", "app_rtc_call");
            dataobjData.put("subtype", "audio_call");
            dataobjData.put("connection", "1"); //0 Disconnect 1 Connect
            dataobjData.put("rand_id", rand_id);
            dataobjData.put("caller_id", FireManager.getUid());
            dataobjData.put("callerName", callerName);
            dataobjData.put("callerImage", callerImage);
            dataobjData.put("timestamp", System.currentTimeMillis());

            obj.put("to", token);
            obj.put("priority", "high");
            //obj.put("notification", objNotification);
            obj.put("data", dataobjData);

            Log.e("!_@rj@_@@_PASS:>", obj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, FCM_PUSH_URL, obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("!_@@_SUCESS connect", response + "");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("!_@@_Errors--", error + "");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "key=" + Legacy_SERVER_KEY);
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 1000 * 60;// 60 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);
    }

    private void disconnectCall(String token, String rand_id) {

        String Legacy_SERVER_KEY = "AIzaSyD1QCiB6NFh0on6ea3F60CfCb35JC9YwQM";
        String FCM_PUSH_URL = "https://fcm.googleapis.com/fcm/send";

        JSONObject obj = null;
        JSONObject objNotification = null;
        JSONObject dataobjData = null;

        try {

            //MainObj
            obj = new JSONObject();

            //NotificationObj
            objNotification = new JSONObject();
            objNotification.put("sound", "default");
            objNotification.put("tag", token);
            objNotification.put("priority", "high");

            //DataObj
            dataobjData = new JSONObject();
            dataobjData.put("type", "app_rtc_call");
            dataobjData.put("subtype", "audio_call");
            dataobjData.put("connection", "0"); //0 Disconnect 1 Connect
            dataobjData.put("rand_id", rand_id);
            dataobjData.put("timestamp", System.currentTimeMillis());

            obj.put("to", token);
            obj.put("priority", "high");
            obj.put("data", dataobjData);

            Log.e("!_@rj@_@@_PASS:>", obj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, FCM_PUSH_URL, obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("!_@@_SUCESS disconnect", response + "");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("!_@@_Errors--", error + "");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "key=" + Legacy_SERVER_KEY);
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 1000 * 60;// 60 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            if (event.values[0] >= -SENSOR_SENSITIVITY && event.values[0] <= SENSOR_SENSITIVITY) {
                //near
                if (wakeLock != null) {
                    if (!wakeLock.isHeld()) {
                        wakeLock.acquire();
                    }
                }
            } else {
                //far
                if (wakeLock != null) {
                    if (wakeLock.isHeld()) {
                        wakeLock.release();
                    }
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
