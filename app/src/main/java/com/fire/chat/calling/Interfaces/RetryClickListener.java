package com.fire.chat.calling.Interfaces;

/**
 * Created by Rahul Mahawar.
 */

public interface RetryClickListener {
    void onRetryClick();

}
