package com.fire.chat.calling.Screen_UI.Admin.Fragment.Amenities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.Add_OpeningTime_Adapter;
import com.fire.chat.calling.Screen_UI.Admin.Adapter.Add_TimeSlot_Adapter;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.User.Model.Notification_model;
import com.fire.chat.calling.Utils.MultipleSelect.Days_model;
import com.fire.chat.calling.Utils.MultipleSelect.Multiple_Select_Adapter;
import com.fire.chat.calling.Utils.MyCustomPagerAdapter;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class AddAmenities_fragment extends Fragment implements View.OnClickListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView name, SelectDate, startTime, endTime;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    List<Notification_model> notificationList;
    ConnectionDetector cd;
    private ArrayList<Days_model> employees = new ArrayList<>();
    private Multiple_Select_Adapter adapter;
    int mHour, mMinute;
    ViewPager viewPager;
    MyCustomPagerAdapter myCustomPagerAdapter;
    int images[] = {R.drawable.yoga, R.drawable.yoga, R.drawable.yoga};
    LinearLayout LayoutForEdits;


    RecyclerView timeSlotRecycler, OpeningTimeRecycler;
    ArrayList<String> list;
    ArrayList<String> list2;
    Add_TimeSlot_Adapter listAdapter;
    LinearLayoutManager llm;
    ImageView AddTimeSlot, AddOpeningTime;
    Add_OpeningTime_Adapter openingTimeAdapter;
    CircleIndicator circleIndicator;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.add_amenities_fragment, container, false);
        context = getActivity();


        ((DashboardActivity) getActivity()).setActionBarTitle(getString(R.string.amenities));
        initilizeViews();

        myCustomPagerAdapter = new MyCustomPagerAdapter(context, images);
        viewPager.setAdapter(myCustomPagerAdapter);
        circleIndicator = view.findViewById(R.id.circle);
        circleIndicator.setViewPager(viewPager);
        click();
        return view;
    }

    private void setdata() {
        recyclerView.setLayoutManager(new GridLayoutManager(context, 4));
        adapter = new Multiple_Select_Adapter(context, employees);
        recyclerView.setAdapter(adapter);

        createList();

    }


    private void initilizeViews() {

        findViews();
        cd = new ConnectionDetector(context);
        if (cd.isConnectingToInternet()) {
        } else {
            //Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }

    private void findViews() {
       /* SelectDate=view.findViewById(R.id.SelectDate);
        endTime=view.findViewById(R.id.endTime);
        startTime=view.findViewById(R.id.startTime);*/
        timeSlotRecycler = view.findViewById(R.id.timeSlotRecycler);
        AddTimeSlot = view.findViewById(R.id.AddTimeSlot);
        OpeningTimeRecycler = view.findViewById(R.id.OpeningTimeRecycler);
        AddOpeningTime = view.findViewById(R.id.AddOpeningTime);
        viewPager = view.findViewById(R.id.viewPager);
        LayoutForEdits = view.findViewById(R.id.LayoutForEdits);
    }


    private void click() {

       /* SelectDate.setOnClickListener(this);
        startTime.setOnClickListener(this);
        endTime.setOnClickListener(this);*/
        AddTimeSlot.setOnClickListener(this);
        AddOpeningTime.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

           /* case R.id.SelectDate:


                showPopUp(view);

                break;*/

            case R.id.AddOpeningTime:

                if (list == null || list.size() == 0) {
                    list = new ArrayList<>();
                    list.add("");
                }
                if (OpeningTimeRecycler.getVisibility() == View.GONE) {
                    OpeningTimeRecycler.setVisibility(View.VISIBLE);
                } else {
                    OpeningTimeRecycler.setVisibility(View.GONE);
                }
                openingTimeAdapter = new Add_OpeningTime_Adapter(list, context);
                llm = new LinearLayoutManager(context);

                //Setting the adapter
                OpeningTimeRecycler.setAdapter(openingTimeAdapter);
                OpeningTimeRecycler.setLayoutManager(llm);

                break;
            case R.id.AddTimeSlot:

                if (list2 == null || list2.size() == 0) {
                    list2 = new ArrayList<>();
                    list2.add("");
                }
                if (timeSlotRecycler.getVisibility() == View.GONE) {
                    timeSlotRecycler.setVisibility(View.VISIBLE);
                } else {
                    timeSlotRecycler.setVisibility(View.GONE);
                }
                listAdapter = new Add_TimeSlot_Adapter(list2, context);
                llm = new LinearLayoutManager(context);

                //Setting the adapter
                timeSlotRecycler.setAdapter(listAdapter);
                timeSlotRecycler.setLayoutManager(llm);

                break;

          /*  case R.id.startTime:

                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                startTime.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

                break;
            case R.id.endTime:


                showPopUp(view);

                break;*/

        }


    }

    public void showPopUp(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View customView = layoutInflater.inflate(R.layout.select_days_popup, null);
        recyclerView = customView.findViewById(R.id.recyclerView);
        setdata();
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String selected = null;
                if (adapter.getSelected().size() > 0) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < adapter.getSelected().size(); i++) {
                        stringBuilder.append(adapter.getSelected().get(i).getName());
                        stringBuilder.append(",");
                        selected = stringBuilder.substring(0, stringBuilder.length() - 1);
                    }
                    dialog.dismiss();
                    SelectDate.setText(selected);

                } else {
                    showToast("No Selection");
                    SelectDate.setText("Select Opening Days");
                }
            }
        });
        builder.setNegativeButton("CANCLE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

       /* TextView complaint = (TextView) customView.findViewById(R.id.complaint);
        TextView reply = (TextView) customView.findViewById(R.id.reply);*/
        builder.setView(customView);
        builder.create();
        builder.show();

    }

    private void createList() {
        employees = new ArrayList<>();
        employees.add(new Days_model("SUN"));
        employees.add(new Days_model("MON"));
        employees.add(new Days_model("TUE"));
        employees.add(new Days_model("WED"));
        employees.add(new Days_model("THU"));
        employees.add(new Days_model("FRI"));
        employees.add(new Days_model("SAT"));

        adapter.setEmployees(employees);
    }

    private void showToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}

