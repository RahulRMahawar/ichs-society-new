package com.fire.chat.calling.Screen_UI.Guard.GuardModel;

public class Visitors_getset {

    private String Name;
    private String FlatNo;
    private String Purpose;
    private String MemberWhitelisted;
    private String MobileNo;
    private String StartDate;
    private String StartTime;
    private String EndDate;
    private String EndTime;

    public Visitors_getset(String Name, String FlatNo, String Purpose, String MemberWhitelisted, String MobileNo, String StartDate
    , String StartTime, String EndDate, String EndTime) {
        this.Name = Name;
        this.FlatNo = FlatNo;
        this.Purpose = Purpose;
        this.MemberWhitelisted = MemberWhitelisted;
        this.MobileNo = MobileNo;
        this.StartDate = StartDate;
        this.StartTime = StartTime;
        this.EndDate = EndDate;
        this.EndTime = EndTime;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFlatNo() {
        return FlatNo;
    }

    public void setFlatNo(String flatNo) {
        FlatNo = flatNo;
    }

    public String getPurpose() {
        return Purpose;
    }

    public void setPurpose(String purpose) {
        Purpose = purpose;
    }

    public String getMemberWhitelisted() {
        return MemberWhitelisted;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public void setMemberWhitelisted(String memberWhitelisted) {
        MemberWhitelisted = memberWhitelisted;
    }
}