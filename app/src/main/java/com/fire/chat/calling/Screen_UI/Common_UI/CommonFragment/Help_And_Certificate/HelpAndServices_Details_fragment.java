package com.fire.chat.calling.Screen_UI.Common_UI.CommonFragment.Help_And_Certificate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonDashboard.DashboardActivity;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.EmployeeDetails_Model;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonModel.Verification_Model;
import com.fire.chat.calling.Screen_UI.User.Adapter.EmployeeReviewLists_adapter;
import com.fire.chat.calling.Screen_UI.User.Adapter.Offering_adapter;
import com.fire.chat.calling.Utils.Constants;
import com.fire.chat.calling.Utils.GlobalVariable.GlobalVariables;
import com.fire.chat.calling.Utils.PerformCall;
import com.fire.chat.calling.Utils.connictivity.ConnectionDetector;
import com.fire.chat.calling.retrofit_files.ApiInterface;
import com.fire.chat.calling.retrofit_files.Apiclient;
import com.fire.chat.calling.sessionData.SessionManager;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpAndServices_Details_fragment extends Fragment implements View.OnClickListener, RatingDialogListener {

    Context context;
    private Toolbar toolbar;
    private View view;
    private TextView writeReview, name, openingDays, openingTime, address;
    private RecyclerView reviewRecycler;
    private EmployeeReviewLists_adapter Adapter;
    private Offering_adapter offeringAdapter;
    private ImageViewZoom image;
    private RecyclerView.LayoutManager layoutManager;
    ConnectionDetector cd;
    RatingBar ratingBar;
    LinearLayout LayoutForSharing, share, callHelpSupportLayout, layoutForFeatures;
    private ImageView videoCallHelpSupportImage;
    private ImageView audioCallHelpSupportImage;
    private ImageView chatHelpSupportImage;
    String ReceivedTITLE, ReceivedUserID;
    ProgressDialog dialog;
    EmployeeDetails_Model employeeDetailsModel;
    private HashMap<String, String> userDetail;
    private SessionManager sessionManager;
    String LoginType, UserRating, UserImage;
    Button hire;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.help_support_details_fragment, container, false);
        context = getActivity();
        cd = new ConnectionDetector(context);
        sessionManager = new SessionManager(context);
        //LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        userDetail = sessionManager.getLoginSavedDetails();
        LoginType = userDetail.get(SessionManager.LOGIN_TYPE);
        try {
            ReceivedUserID = getArguments().getString("UserId");
        } catch (NullPointerException e) {
            ReceivedUserID = "";
        }
        try {
            ReceivedTITLE = getArguments().getString("title");
        } catch (NullPointerException e) {
            ReceivedTITLE = "";
        }
        ((DashboardActivity) getActivity()).setActionBarTitle(ReceivedTITLE);
        initializeViews();
        initializeRecycler();
        handleListeners();
        getEmployeeDetails();
        return view;
    }


    private void initializeViews() {

        LayoutForSharing = view.findViewById(R.id.LayoutForSharing);
        layoutForFeatures = view.findViewById(R.id.layout_for_features);

        share = view.findViewById(R.id.share);
        writeReview = view.findViewById(R.id.writeReview);
        ratingBar = view.findViewById(R.id.ratingBar);
        reviewRecycler = (RecyclerView) view.findViewById(R.id.reviewRecycler);
        name = view.findViewById(R.id.name);
        address = view.findViewById(R.id.description);
        image = view.findViewById(R.id.image);
        hire = view.findViewById(R.id.hire);

        callHelpSupportLayout = view.findViewById(R.id.call_help_support);
        videoCallHelpSupportImage = view.findViewById(R.id.video_call_help_support);
        chatHelpSupportImage = view.findViewById(R.id.chat_help_support);
        audioCallHelpSupportImage = view.findViewById(R.id.audio_call_help_support);
    }

    private void initializeRecycler() {
        reviewRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        reviewRecycler.setLayoutManager(layoutManager);
    }

    private void handleListeners() {

        writeReview.setOnClickListener(this);
        share.setOnClickListener(this);
        hire.setOnClickListener(this);

        callHelpSupportLayout.setOnClickListener(this);
        videoCallHelpSupportImage.setOnClickListener(this);
        audioCallHelpSupportImage.setOnClickListener(this);
        chatHelpSupportImage.setOnClickListener(this);
    }

    private void getEmployeeDetails() {
        if (cd.isConnectingToInternet()) {
            getEmployeeDetailsAPI();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void getEmployeeDetailsAPI() {
        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("employee_id", ReceivedUserID);
            requestBody.put("database_name", ((GlobalVariables) getActivity().getApplication()).getDatabase_name());
            Call<EmployeeDetails_Model> call = apiInterface.EmployeeDetails(requestBody);
            call.enqueue(new Callback<EmployeeDetails_Model>() {
                @Override
                public void onResponse(Call<EmployeeDetails_Model> call, Response<EmployeeDetails_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                employeeDetailsModel = response.body();
                                SetDataToViews(employeeDetailsModel);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<EmployeeDetails_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetDataToViews(EmployeeDetails_Model List) {
        name.setText(List.data.getFullName());
        if (List.data.getDescription().isEmpty()) {
            address.setText("No Description Available");
        } else {
            address.setText(List.data.getDescription());
        }

        if (List.data.getHired() != null) {
            if (List.data.getHired().equalsIgnoreCase("0")) {
                hire.setBackground(getResources().getDrawable(R.drawable.roundbuttondesign));
                hire.setText("Hire");
            } else {
                hire.setBackground(getResources().getDrawable(R.drawable.activeroundbuttondesign));
                hire.setText("Hired");
            }
        }
        if (List.data.getRating() != null) {
            ratingBar.setRating(Float.parseFloat(List.data.getRating()));
        }

        Picasso.get().load(Constants.GALLARYFOLDER + List.getData().getImage())
                .placeholder(R.drawable.ic_splash_icon)
                .error(R.drawable.ic_splash_icon)
                .into(image);
        if (employeeDetailsModel.getData().getMy_reviews().isEmpty()) {
            writeReview.setText("Write Reviews");
        } else {
            writeReview.setText("Edit Reviews");
        }
        SetRatingRecycler(List);
    }


    private void SetRatingRecycler(EmployeeDetails_Model List) {
        Adapter = new EmployeeReviewLists_adapter(context, List);
        reviewRecycler.setAdapter(Adapter);
        Adapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.writeReview:

                if (writeReview.getText().toString().equalsIgnoreCase("Write Reviews")) {
                    initializeReviewPop("add", "1", "Please select some stars and give your feedback");
                } else if (writeReview.getText().toString().equalsIgnoreCase("Edit Reviews")) {
                    initializeReviewPop("edit", employeeDetailsModel.getData().getMy_reviews().get(0).getRating()
                            , employeeDetailsModel.getData().getMy_reviews().get(0).getReview());
                }
                break;
            case R.id.share:
                if (LayoutForSharing.getVisibility() == View.GONE) {
                    LayoutForSharing.setVisibility(View.VISIBLE);
                } else {
                    LayoutForSharing.setVisibility(View.GONE);
                }

                return;
            case R.id.hire:
                if (hire.getText().toString().equalsIgnoreCase("hire")) {
                    callHireApi("1");
                } else {
                    callHireApi("0");
                }

                break;
            case R.id.call_help_support:
                if (layoutForFeatures.getVisibility() == View.GONE) {
                    layoutForFeatures.setVisibility(View.VISIBLE);
                    LayoutForSharing.setVisibility(View.GONE);
                } else {
                    layoutForFeatures.setVisibility(View.GONE);
                }
                break;
            case R.id.video_call_help_support:
                new PerformCall(getActivity()).performCall(true, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                break;
            case R.id.chat_help_support:
                break;
            case R.id.audio_call_help_support:
                new PerformCall(getActivity()).performCall(false, "ARDhZ2MEDChjyrUFC6zuzH67JML2");
                break;
        }


    }

    private void callHireApi(final String status) {
        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("employee_id", employeeDetailsModel.getData().getId());
            requestBody.put("status", status);
            requestBody.put("database_name", "ichs");
            Call<Verification_Model> call = apiInterface.hire(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Verification_Model marketPlaceDetailsModel = response.body();
                                Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                                if (status.equalsIgnoreCase("1")) {
                                    hire.setBackground(getResources().getDrawable(R.drawable.activeroundbuttondesign));
                                    hire.setText("Hired");
                                } else {
                                    hire.setBackground(getResources().getDrawable(R.drawable.roundbuttondesign));
                                    hire.setText("Hire");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initializeReviewPop(String type, String rating, String review) {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(Integer.parseInt(rating))
                .setTitle("Rate This Shop")
                .setDescription("Please select some stars and give your feedback")
                .setCommentInputEnabled(true)
                .setDefaultComment(review)
                .setStarColor(R.color.starColor)
                .setNoteDescriptionTextColor(R.color.black)
                .setTitleTextColor(R.color.titleTextColor)
                .setDescriptionTextColor(R.color.descriptionTextColor)
                .setWindowAnimation(R.style.MyDialogSlideHorizontalAnimation)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.darkgrey)
                .setCommentTextColor(R.color.commentTextColor)
                .setCommentBackgroundColor(R.color.commentBackgroundColor)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(getActivity())
                .setTargetFragment(this, 0)
                .show();
    }

    @Override
    public void onNegativeButtonClicked() {
    }

    @Override
    public void onNeutralButtonClicked() {

        Toast.makeText(context, "Please do provide feedback later.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositiveButtonClicked(int rating, String review) {
        SendReviewAPI(rating, review);
    }

    public void SendReviewAPI(int rating, String review) {

        dialog = ProgressDialog.show(context, "", "Please wait...", false);
        ApiInterface apiInterface = Apiclient.retrofit().create(ApiInterface.class);

        try {
            Map<String, String> requestBody = new HashMap<>();
          /*  if (USERTYPE.equalsIgnoreCase("admin")) {
                requestBody.put("sid", "");
            } else {
                requestBody.put("sid", "pv");
            }*/
            requestBody.put("sid", userDetail.get(SessionManager.SID));
            requestBody.put("user_id", employeeDetailsModel.getData().getId());
            requestBody.put("rating", String.valueOf(rating));
            requestBody.put("review", review);
            requestBody.put("database_name", "ichs");
            Call<Verification_Model> call = apiInterface.addOrEditReviewOfEmployee(requestBody);
            call.enqueue(new Callback<Verification_Model>() {
                @Override
                public void onResponse(Call<Verification_Model> call, Response<Verification_Model> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getReplyCode().equalsIgnoreCase("success")) {
                            try {
                                JSONObject object = new JSONObject(new Gson().toJson(response.body()));
                                Verification_Model marketPlaceDetailsModel = response.body();

                                Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, response.body().getReplyMsg(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Log.d("NotSuccess", new Gson().toJson(response.errorBody()));
                    }

                    dialog.cancel();


                }

                @Override
                public void onFailure(Call<Verification_Model> call, Throwable t) {
                    Log.d("FAILURE", t.toString());
                    dialog.cancel();
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

