package com.fire.chat.calling.Screen_UI.Guard.Guard_Screens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.fire.chat.calling.R;
import com.fire.chat.calling.Screen_UI.Common_UI.CommonActicity.User_Profile;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.GuardVehicleManagement_fragment;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Guard_Alaram_fragment;
import com.fire.chat.calling.Screen_UI.Guard.Guard_fragments.Guard_Home_Fragment;
import com.fire.chat.calling.Utils.AppUtils;
import com.fire.chat.calling.sessionData.SessionManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class Guard_Home extends AppCompatActivity {
    private Toolbar toolbar;
    TextView title;
    ImageView profile, notification;
    private long mBackPressed;
    private static final int TIME_INTERVAL = 1500;
    boolean flag = false;
    Context context;
    private boolean exit = false;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guard_home_activity);


        context = Guard_Home.this;
        sessionManager = new SessionManager(this);
        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.ActivityTitle);
        profile = toolbar.findViewById(R.id.profile);
        notification = toolbar.findViewById(R.id.notificationimage);
        callHomeFragment();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        Bundle bundle = new Bundle();
                        bundle.putString("Type", "Home");
                        Guard_Home_Fragment dashBoardFragment = new Guard_Home_Fragment();
                        dashBoardFragment.setArguments(bundle);
                        AppUtils.setFragment(dashBoardFragment, true, Guard_Home.this, R.id.container);
                        break;
                    case R.id.visitor:
                        String backStateName = context.getClass().getName();
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                        if (!fragmentPopped) {

                            Guard_Home_Fragment fragment = new Guard_Home_Fragment();
                            Bundle bundle2 = new Bundle();
                            bundle2.putString("Type", "Visitor");
                            fragment.setArguments(bundle2);
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName);
                            fragmentTransaction.commit();
                        }
                        break;
                    case R.id.alaram:
                        String backStateName2 = context.getClass().getName();
                        FragmentManager fragmentManager2 = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped2 = fragmentManager2.popBackStackImmediate(backStateName2, 0);
                        if (!fragmentPopped2) {

                            Guard_Alaram_fragment fragment = new Guard_Alaram_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager2.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName2);
                            fragmentTransaction.commit();
                        }
                        break;
                    case R.id.vehicle:
                        String backStateName3 = context.getClass().getName();
                        FragmentManager fragmentManager3 = ((FragmentActivity) context).getSupportFragmentManager();
                        boolean fragmentPopped3 = fragmentManager3.popBackStackImmediate(backStateName3, 0);
                        if (!fragmentPopped3) {

                            GuardVehicleManagement_fragment fragment = new GuardVehicleManagement_fragment();
                            FragmentTransaction fragmentTransaction = fragmentManager3.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.addToBackStack(backStateName3);
                            fragmentTransaction.commit();
                        }
                        break;
                }
                return true;
            }
        });
    }

    private void callHomeFragment() {

        title.setText(getString(R.string.home));
        Bundle bundle = new Bundle();
        bundle.putString("Type", "Home");
        Guard_Home_Fragment dashBoardFragment = new Guard_Home_Fragment();
        dashBoardFragment.setArguments(bundle);
        AppUtils.setFragment(dashBoardFragment, true, Guard_Home.this, R.id.container);
    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (exit)
                Guard_Home.this.finish();
            else {
                Toast.makeText(this, "Press Back again to Exit.",
                        Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }


        } else {
            callHomeFragment();
            setHomeItem(Guard_Home.this);
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
            int seletedItemId = bottomNavigationView.getSelectedItemId();


        }


    }

    public static void setHomeItem(Activity activity) {
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                activity.findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.home);
    }


    public void profile(View view) {

        startActivity(new Intent(this, User_Profile.class));
    }

    public void helpline(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("USERTYPE", "USER");
        Intent intent = new Intent(Guard_Home.this, HelpLine_Screen.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void notification(View view) {

        startActivity(new Intent(this, Notification_screen.class));

    }

    public void LogOUT(View view) {
        FirebaseAuth.getInstance().signOut();
        sessionManager.logoutUser();
        finish();

    }

    public void setActionBarTitle(String name) {
        title.setText(name);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
   /* @Override
    public void onRestart() {
        super.onRestart();
        // do some stuff here
        startActivity(new Intent(context, DashboardActivity.class));
    }*/
}
